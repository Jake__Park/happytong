/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kr.wisestone.happytong;

import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.plugin.GCMController;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

	Handler mHandler;

	public GCMIntentService() {
		super(GCMController.SENDER_ID);
		mHandler = new Handler();
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Logg.i("Device registered: regId = " + registrationId);
		GCMController.sendBroadRegistrationId(context, registrationId);
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Logg.i("Device unregistered");
		if (GCMRegistrar.isRegisteredOnServer(context)) {
			Logg.i("Device unregistered");
		} else {
			Logg.i("Ignoring unregister callback");
		}
	}

	@Override
	protected void onMessage(final Context context, Intent intent) {
		SharedPrefManager spm = SharedPrefManager.getInstance(context);
		if (TextUtils.isEmpty(spm.getPrefLoginEmail()) || TextUtils.isEmpty(spm.getPrefLoginPwd())) {
			// 아이디나 비번 없을 경우, 행동하지 않음
			Logg.d("GCMIntentService - onMessage - have not email, password");
			return;
		}
		String msg = intent.getStringExtra("msg");
		Logg.d("GCMIntentService - onMessage - " + msg);
		onMessageControl(context, msg);
	}

	/**
	 * GCM 메시지 받아서 처리 - JSON파싱, - 앱실행 - 노티알림(토스트알림) - 선택시 이동 - 앱꺼짐 - 노티알림 - 선택시 이동
	 * 
	 * @param context
	 * @param stringExtra
	 */
	private void onMessageControl(final Context context, final String stringExtra) {
		try {
			JSONObject jObj = new JSONObject(stringExtra);
			String message = jObj.getString("message");
			String badgeCount = jObj.getString("badgecount");
			String couponBoxSeq = jObj.getString("couponBoxSeq");
			String pushType = jObj.getString("pushType");
			Logg.d(message + " / badgecount:" + badgeCount + " / couponBoxSeq:" + couponBoxSeq + " / pushType:" + pushType);

			generateNotification(context, message, "", couponBoxSeq, pushType);
		} catch (JSONException e) {
			e.printStackTrace();
			Logg.e(e);
		}
	}

	@Override
	protected void onDeletedMessages(Context context, int total) {
		Logg.i("Received deleted messages notification");
	}

	@Override
	public void onError(Context context, String errorId) {
		Logg.i("Received error: " + errorId);
		GCMController.sendBroadRegistrationId(context, errorId, true);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Logg.i("Received recoverable error: " + errorId);
		GCMController.sendBroadRegistrationId(context, errorId, true);
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private void generateNotification(final Context context, final String message, String badgeCount, String couponBoxSeq, String pushType) {

		// 앱실행중이면 토스트도 띄우기?
		if (GCMController.getActiveActivity(context).isRunning) {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
				}
			});
		}

		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		String title = context.getString(R.string.app_name);

		Intent notificationIntent = new Intent(GCMController.ACTION_GCM_RECEIVE);
		notificationIntent.putExtra(IntentDefine.EXTRA_GCM_BADGE_COUNT, badgeCount);
		notificationIntent.putExtra(IntentDefine.EXTRA_GCM_COUPON_BOX_SEQ, couponBoxSeq);
		notificationIntent.putExtra(IntentDefine.EXTRA_GCM_PUSH_TYPE, pushType);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int) when, notificationIntent, 0);

		notification.setLatestEventInfo(context, title, message, pendingIntent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_VIBRATE;

		notificationManager.notify((int) when, notification);
	}

}
