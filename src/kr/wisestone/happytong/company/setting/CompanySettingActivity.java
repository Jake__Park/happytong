package kr.wisestone.happytong.company.setting;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.UserInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.UserProtocol;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.DummyMainActivity;
import kr.wisestone.happytong.ui.setting.LocationListViewActivity;
import kr.wisestone.happytong.ui.widget.CompanyMenuInfoView;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CompanySettingActivity extends BaseActivity{
    Context      mContext;
    
    ImageView    mIvCompanyImg;
    LinearLayout mPf_list;
    LinearLayout mPf_list2;
    LinearLayout mPf_list3;
    LinearLayout mPf_list4;
    
    TextView     mTvSelectArea;
    
    UserProtocol m_UserProtocol;
    UserInfoData m_CompanyInfoData;
    
    final int    PROFILE_PHOTO = 0;
    
    /**
     * 업체 프로필 요청 
     */
    final IWiseCallback iOkCompanyProfile = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkCompanyProfile");
            if (param.param1 != null) {
                m_CompanyInfoData = (UserInfoData) param.param1;

                Logg.d( "main 1 = " + m_CompanyInfoData.getUserImageUrl() );

                showProfileInfo();
                closeProgressDialog();
            }
            return false;
        }
    };
    
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }
    
    protected void onResume() {
        super.onResume();
        
        getProfileInfo();
    }
    

    protected int setContentViewId() {
        return R.layout.activity_company_setting;
    }
    
    private void init() {
        mContext = getApplicationContext();
    
        mIvCompanyImg = (ImageView)findViewById(R.id.pf_img);
        
        mPf_list = (LinearLayout)findViewById(R.id.pf_list);
        mPf_list.setOnClickListener(this);
        mPf_list2 = (LinearLayout)findViewById(R.id.pf_list2);
        mPf_list2.setOnClickListener(this);
        mPf_list3 = (LinearLayout)findViewById(R.id.pf_list3);
        mPf_list3.setOnClickListener(this);
        mPf_list4 = (LinearLayout)findViewById(R.id.pf_list4);
        mPf_list4.setOnClickListener(this);
        
        mTvSelectArea = (TextView)findViewById(R.id.select_area);
        
        new CompanyMenuInfoView( this, (View)findViewById( R.id.company_menu_tab ), CommonData.ENTRY_PONINT_COMPAMY_SETTING );
        new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_COMPANY).updateView(R.string.setting_title, TitleInfoView.TITLE_MODE_NORMAL);
    }
    
    private void getProfileInfo(){
        showProgressDialog();
        
        m_UserProtocol = new UserProtocol( iOkCompanyProfile, iFail );
        m_UserProtocol.getCompaniesProfile(0, SharedPrefManager.getLoginEmail(this));
    }
    
    private void showProfileInfo(){
        ImageLoader ImageLoader = new ImageLoader(mContext);
        
        if(m_CompanyInfoData.getCompanyCropImageURL()!=null && m_CompanyInfoData.getCompanyCropImageURL() != "" && !(m_CompanyInfoData.getCompanyCropImageURL().contains("none"))){
            ImageLoader.DisplayImage(m_CompanyInfoData.getCompanyCropImageURL(), mIvCompanyImg);
        } else {
            mIvCompanyImg.setImageBitmap(null);
        }
        
        if(m_CompanyInfoData.getLocationBigName()!=null){
        	SharedPrefManager.getInstance(mContext).setPrefLocationCompanySmallCode(m_CompanyInfoData.getLocationSmallCode());
            String location = m_CompanyInfoData.getLocationBigName() + " - " + m_CompanyInfoData.getLocationSmallName();
            mTvSelectArea.setText(location);
        } else {
            mTvSelectArea.setText("");
        }
        if(m_CompanyInfoData.getCompanyBusinessNumber() != null){
        	SharedPrefManager.getInstance(mContext).setPrefCompanyBusinessNumber(m_CompanyInfoData.getCompanyBusinessNumber());
        }
    }

    @Override
    public void onProtoColFailResponse(int eventId, CallbackParam param) {
        super.onProtoColFailResponse(eventId, param);
        closeProgressDialog();
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.pf_list:
            //[SDJ 130923] 업체 프로필 
            Intent intent = new Intent(mContext, CompanyProfileEditActivity.class);
            intent.putExtra(IntentDefine.EXTRA_COMPANY_INFO, m_CompanyInfoData);
            startActivityForResult(intent, PROFILE_PHOTO);
            break;
        case R.id.pf_list2: //업체 지역설정
            Intent areaIntent = new Intent(this, LocationListViewActivity.class);
            areaIntent.putExtra(IntentDefine.EXTRA_LOCATION_LIST_TYPE, CommonData.LOCATION_LIST_TYPE_COMPANY);
            areaIntent.putExtra(IntentDefine.EXTRA_LOCATION_COMPANY_DATA, m_CompanyInfoData.getLocationSmallCode());
            startActivity(areaIntent);
            break;
        case R.id.pf_list3:
            //[SDJ 130923] 업체 도움말 
            startActivity(new Intent(mContext, CompanyGuideActivity.class));
            break;
        case R.id.pf_list4:
            //[SDJ 130923] 일반모드전환
            Intent mainIntent = new Intent(CompanySettingActivity.this, DummyMainActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            mainIntent.putExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_SETTING);
            startActivity(mainIntent);
            finish();
            break;
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logg.d("**CompanySettingActivity requestCode = "+requestCode);
        Logg.d("**CompanySettingActivity resultCode = "+ resultCode);
        if(requestCode == PROFILE_PHOTO ){
            getProfileInfo();
        }
        
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
     
        super.onDestroy();
    }   
    
    
    @Override
    public void onBackPressed() {
        backKeyToExit();
    }
}
