package kr.wisestone.happytong.company.setting;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import android.os.Bundle;
import android.view.View;

public class CompanyGuideActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        
        init();
    }
    
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
    }
    
    @Override
    protected int setContentViewId() {
        // TODO Auto-generated method stub
        return R.layout.activity_company_guide;
    }
    
    private void init() {
        new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_COMPANY).updateView(R.string.setting_guide_title,
                                                                         TitleInfoView.TITLE_MODE_NORMAL);
    }    
}
