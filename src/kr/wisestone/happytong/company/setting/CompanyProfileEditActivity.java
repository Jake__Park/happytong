package kr.wisestone.happytong.company.setting;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.dataset.UserInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.UserProtocol;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class CompanyProfileEditActivity extends BaseActivity  {
    static final int  MENU_TAKE_CAMERA  = 0;
    static final int  MENU_TAKE_ALBUM   = 1;
    static final int  MENU_DELETE_IMAGE = 2;
    static final int  MENU_CANCLE       = 3;
    
    Context           mContext;
    
    ImageView         mIvCompanyImg;
    ImageView         mIvEdit;
    
    EditText          mEtCompanyName;
    EditText          mEtPhone;
    EditText          mEtAddress;
    
    ImageView         mIvEditOk;
    
    UserProtocol      m_CompanyProtocol;
    UserInfoData      m_CompanyInfoData;
    
    Bitmap            mPhoto           = null;
    SharedPrefManager spMgr;
    String            mNameStr         = null;
    Uri               mImageCaptureUri;
    
    static final int  TAKE_CAMERA      = 1;
    static final int  PICK_ALBUM       = 2;
    static final int  CROP_FROM_CAMERA = 3;
    
    boolean           isImage          = false;
    boolean           isMenu           = false;
    boolean           isProfileDel     = false; //프로필 이미지 삭제 선택시 서버에 업로드 체크
    boolean           isExistImage     = false; //프로필 이미지가 존재여부, 이미지 있을시 카메라,앨범->취소 시 이미지삭제버튼 활성화 위해
	
	private void SetIsMenu(boolean bmenu){isMenu = bmenu;}//프로필 메뉴 이미지 여부에 따라/ 이미지 있음: true, 없으면 false
	private boolean GetIsMenu(){return isMenu;}
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		m_CompanyInfoData = (UserInfoData)getIntent().getSerializableExtra(IntentDefine.EXTRA_COMPANY_INFO);
		
		init();
		getProfileInfo();
	}
	
	private void getProfileInfo(){
        if(m_CompanyInfoData.getUserImageUrl().endsWith("none")) {
            isExistImage = false;
            SetIsMenu(false);
        } else {
            isExistImage = true;
            SetIsMenu(true);
        }
        drawProfileData();
	}

	private void drawProfileData(){
		ImageLoader imageLoader = new ImageLoader(mContext);
		
		if(m_CompanyInfoData.getCompanyCropImageURL()!=null && m_CompanyInfoData.getCompanyCropImageURL() != "" && !(m_CompanyInfoData.getCompanyCropImageURL().equalsIgnoreCase("null"))){
		    imageLoader.DisplayImage(m_CompanyInfoData.getCompanyCropImageURL(), mIvCompanyImg);
		}

		if(m_CompanyInfoData.getCompanyName()!=null && m_CompanyInfoData.getCompanyName().length() !=0) {
			mEtCompanyName.setText(m_CompanyInfoData.getCompanyName());
			mEtCompanyName.setSelection(mEtCompanyName.getText().length());
		}
		
		if(m_CompanyInfoData.getCompanyNumber()!=null && m_CompanyInfoData.getCompanyNumber().length() !=0) {
	        String formatNum = PhoneNumberUtils.formatNumber(m_CompanyInfoData.getCompanyNumber());
	        
		    mEtPhone.setText(formatNum);
		    mEtPhone.setSelection(formatNum.length());
		}        
		
        if(m_CompanyInfoData.getAddress()!=null && m_CompanyInfoData.getAddress().length() !=0) {
            mEtAddress.setText(m_CompanyInfoData.getAddress());
            mEtAddress.setSelection(mEtAddress.getText().length());
        }
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.company_img_edit://편집
			Logg.d("onClick - profile edit");
			openContextMenu(findViewById(R.id.company_img_edit));
	        break;
		case R.id.company_edit_ok://완료
			Logg.d("**onClick - edit_ok");
			checkVaritation();
			break;
		}	
	}
	
	@Override
	protected int setContentViewId() {
		return R.layout.activity_company_profile;
	}
	
	private void init() {
		mContext = getApplicationContext();
		spMgr = SharedPrefManager.getInstance(mContext);
		
		mIvEdit = (ImageView) findViewById(R.id.company_img_edit);
		mIvEdit.setOnClickListener(this);
        registerForContextMenu(findViewById(R.id.company_img_edit));		

        mEtCompanyName = (EditText) findViewById(R.id.company_name_edit);
        mEtPhone = (EditText) findViewById(R.id.company_phone_edit);
        mEtAddress = (EditText) findViewById(R.id.company_address_edit);
        
		mIvEditOk = (ImageView) findViewById(R.id.company_edit_ok);
		mIvEditOk.setOnClickListener(this);

		mIvCompanyImg = (ImageView)findViewById(R.id.company_img);
		
		new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_COMPANY).updateView(R.string.company_profile, TitleInfoView.TITLE_MODE_NORMAL);
		registerForContextMenu(findViewById(R.id.company_img_edit));
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		
		if(!GetIsMenu()) {
			menu.add(0, MENU_TAKE_CAMERA, 0, R.string.take_camera);
			menu.add(0, MENU_TAKE_ALBUM, 0, R.string.select_album);
			menu.add(0, MENU_CANCLE, 0, R.string.setting_cancel);	
		} else {
			menu.add(0, MENU_TAKE_CAMERA, 0, R.string.take_camera);
			menu.add(0, MENU_TAKE_ALBUM, 0, R.string.select_album);
			menu.add(0, MENU_DELETE_IMAGE, 0, R.string.del_profile_img);
			menu.add(0, MENU_CANCLE, 0, R.string.setting_cancel);
		}
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_TAKE_CAMERA://카메라촬영 
			takeCamera();
			break;
		case MENU_TAKE_ALBUM://앨범에서 선택 
			pickAlbum();
			break;
		case MENU_DELETE_IMAGE://프로필 이미지 삭제 
			mIvCompanyImg.setImageBitmap(null);
			SetIsMenu(false);
			isProfileDel = true;
			break;
		}
		return super.onContextItemSelected(item);
	}
	
	private void takeCamera(){
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
		mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));
		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
		// 특정기기에서 사진을 저장못하는 문제가 있어 다음을 주석처리 합니다.  
		//intent.putExtra("return-data", true);
		startActivityForResult(intent, TAKE_CAMERA);
	}

	private void pickAlbum(){
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");
		intent.putExtra("crop", "true");
		//intent.putExtra("scale", true); 
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 170);
		intent.putExtra("outputY", 170);
		intent.putExtra("return-data", true);
	
        startActivityForResult(intent,PICK_ALBUM);
	}
	
	private void checkVaritation(){
	    String companyName = mEtCompanyName.getText().toString();
	    String companyNumber = mEtPhone.getText().toString();
	    String companyAddress = mEtAddress.getText().toString();
	    
		//[SDJ 131014]유효성 체크 변경 
	    if (TextUtils.isEmpty(companyName)){
            DialogUtil.getConfirmDialog(this, null, getString(R.string.company_reg_hint_name), null).show();
	    } else if (TextUtils.isEmpty(companyNumber)){
            DialogUtil.getConfirmDialog(this, null, getString(R.string.company_reg_hint_tel), null).show();
        } else if (TextUtils.isEmpty(companyAddress)){
            DialogUtil.getConfirmDialog(this, null, getString(R.string.company_reg_hint_address), null).show();
        } else {
            showProfileEditOK();
        }
	}
	
	private void showProfileEditOK(){
		Dialog dialog = DialogUtil.getConfirmCancelDialog(this, R.string.pf_edit_title, R.string.pr_edit_popup_msg, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					showProgressDialog();
					
					if(isProfileDel)
						UploadImageFileDel();
					else 
						uploadImageFile(mPhoto);
				}
			}
		});
		dialog.show();
	}
	
	final IWiseCallback profileEditOk = new IWiseCallback() {
		
		@Override
		public boolean WiseCallback(CallbackParam param) {
			Logg.d("profileEditOk ");
			if(param.param1 != null) {
				Toast.makeText(mContext, R.string.pf_edit_toast, Toast.LENGTH_SHORT).show();
			}
			
			isProfileDel = false;
			closeProgressDialog();
			finish();
			return false;
		}
	};
			
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == RESULT_CANCELED) {
			//카메라,앨범 -> 취소 왔을시 기존에 이미지 있는지 체크
			if(isExistImage)
				SetIsMenu(true);
			else
				SetIsMenu(false);
				
			return;
		} else if(resultCode == RESULT_OK) {
			SetIsMenu(true);
		}

        switch(requestCode) {
            case TAKE_CAMERA:
                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(mImageCaptureUri, "image/*");
                
                intent.putExtra("scale", true);
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("outputX", 155);
                intent.putExtra("outputY", 155);
                intent.putExtra("return-data", true);
                
                startActivityForResult(intent, CROP_FROM_CAMERA);
                break;
            case PICK_ALBUM:
                mPhoto = (Bitmap)data.getParcelableExtra("data");
                mIvCompanyImg.setImageBitmap(mPhoto);
                isExistImage = true;
                break;
            
            case CROP_FROM_CAMERA:
                final Bundle extras = data.getExtras();
                if(extras != null) {
                    mPhoto = extras.getParcelable("data");
                    mIvCompanyImg.setImageBitmap(mPhoto);
                    isExistImage = true;
                }
                // 임시 파일 삭제
                File f = new File(mImageCaptureUri.getPath());
                if(f.exists()) {
                    f.delete();
                }
                break;
        }
	}
	
	//서버에 업로드 시키기
	private void uploadImageFile(Bitmap profileBitmap){
		if(profileBitmap == null) {
            new UserProtocol( profileEditOk, iFail ).companiesInfoUpdate(0,
                                                                         mEtCompanyName.getText().toString(),
                                                                         m_CompanyInfoData.getCompanyBusinessNumber(),
                                                                         mEtPhone.getText().toString(),
                                                                         mEtAddress.getText().toString(),
                                                                         spMgr.getPrefLoginEmail());
		} else {
		    new UserProtocol( profilePhoteEditOk, iFail ).companiesImagePathUpdate(CallBackParamWrapper.CODE_COMPANY_PHOTO_EDIT, SharedPrefManager.getLoginEmail(mContext),
		                                                                           m_CompanyInfoData.getCompanyBusinessNumber(), profileBitmap);
		}
	}
	
	private void UploadImageFileDel(){
		new UserProtocol( profilePhoteEditOk, iFail ).companiesImagePathDelete(CallBackParamWrapper.CODE_COMPANY_PHOTO_DEL, SharedPrefManager.getLoginEmail(mContext),
		                                                                       m_CompanyInfoData.getCompanyBusinessNumber());
	}
	
	/*
	 * 프로필사진 추가 / 삭제
	 */
	final IWiseCallback profilePhoteEditOk = new IWiseCallback() {
		
		@Override
		public boolean WiseCallback(CallbackParam param) {
			Logg.d("**profilePhoteEditOk- SUCCESS***");
			
            new UserProtocol( profileEditOk, iFail ).companiesInfoUpdate(0,
                                                                         mEtCompanyName.getText().toString(),
                                                                         m_CompanyInfoData.getCompanyBusinessNumber(),
                                                                         mEtPhone.getText().toString(),
                                                                         mEtAddress.getText().toString(),
                                                                         spMgr.getPrefLoginEmail());
			return false;
		}
	};
	
	public String getDateTime(){
		SimpleDateFormat formatter = new SimpleDateFormat ( "yyyy.MM.dd a h:mm", Locale.KOREA );
		Date currentTime = new Date ( );
		String m_dateTime = formatter.format ( currentTime );
		Logg.d("manualSync = "+ m_dateTime);
		return m_dateTime;
	}
	
}