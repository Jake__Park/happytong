package kr.wisestone.happytong.company.history;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.company.history.CompanySendCouponHistoryDetailActivity.CouponDetailViewFactory.CouponDetailViewHolder;
import kr.wisestone.happytong.data.dataset.CompanySendedCouponData;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.cbox.CouponType;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CompanySendCouponHistoryDetailActivity extends BaseActivity {

	Context								mContext;

	ViewPager							mViewPager;
	CompanyCouponDetailPagerAdapter		mAdapter;

	ArrayList<CompanySendedCouponData>	mList;
	int									mListSelectPosition;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = this;

		Logg.d("onCreated");

		if (!isRightIntentValues(getIntent())) {
			finish();
			return;
		}
		init();
	}

	@SuppressWarnings("unchecked")
	private boolean isRightIntentValues(Intent intent) {
		mList = (ArrayList<CompanySendedCouponData>) intent.getSerializableExtra(IntentDefine.EXTRA_CBOX_LIST);
		mListSelectPosition = intent.getIntExtra(IntentDefine.EXTRA_CBOX_LIST_SELECT_POSITION, 0);
		if (mList == null) {
			return false;
		}
		Logg.d("isRightIntentValues - true - receivedListSize : " + mList.size());
		return true;
	}

	private void init() {
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mAdapter = new CompanyCouponDetailPagerAdapter(mList);

		mViewPager.setAdapter(mAdapter);
		mViewPager.setCurrentItem(mListSelectPosition);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		}
	}

	@Override
	protected int setContentViewId() {
		return R.layout.activity_coupon_detail;
	}

	class CompanyCouponDetailPagerAdapter extends PagerAdapter {

		List<CompanySendedCouponData>	mList;

		LayoutInflater					mInflater;
		ImageLoader						mImgLoader;
		CouponDetailViewFactory			mCouponDetailViewFactory;

		public CompanyCouponDetailPagerAdapter(List<CompanySendedCouponData> mList) {
			super();
			this.mList = mList;

			this.mInflater = LayoutInflater.from(mContext);
			this.mImgLoader = new ImageLoader(mContext);

			mCouponDetailViewFactory = new CouponDetailViewFactory();

		}

		@Override
		public int getCount() {
			return mList.size();
		}

		@Override
		public boolean isViewFromObject(View v, Object o) {
			return v == o;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			View v = mCouponDetailViewFactory.create(mInflater);
			CouponDetailViewHolder holder = (CouponDetailViewFactory.CouponDetailViewHolder) v.getTag();

			CompanySendedCouponData data = mList.get(position);

			holderTitleChange(holder, data.couponType, data.couponText);

			// if (!TextUtils.isEmpty(data.mCouponImageURL) && !data.mCouponImageURL.endsWith("none")) {
			// mImgLoader.DisplayImage(data.mCouponImageURL, holder.photo);
			// }
			holder.name.setText(data.mLocationSmallName);

			StringBuilder countAndSendDate = new StringBuilder();
			countAndSendDate.append(getString(R.string.with_people, data.mReceiverCount));
			countAndSendDate.append("\n");

			countAndSendDate.append(Utils.getDateFormatStringFromServerDateFormat(data.mSendDate, mContext.getString(R.string.yyyy_mm_dd_eeee_a_hh_mm), Locale.KOREA));
			holder.date.setText(countAndSendDate.toString());

			// holder.company_valid_date.setText(data.m_validPeriod);
			holder.valid.setText(Utils.getDateFormatStringFromServerDateFormat(data.validDateEnd, mContext.getString(R.string.until_yyyy_mm_dd), Locale.KOREA));

			holder.positions.setText((position + 1) + " / " + mList.size());

			this.mImgLoader.DisplayImage(data.mCouponImageURL, holder.coupon);

			((ViewPager) container).addView(v);
			return v;
		}

		private void holderTitleChange(CouponDetailViewHolder holder, String couponType, String couponText) {
			switch (CouponType.getCouponType(couponType)) {
			case NA:
			case Center:
				holder.title0.setText(couponText);
				holder.title1.setText("");
				holder.title2.setText("");
				holder.title3.setText("");
				break;
			case Up:
				holder.title0.setText("");
				holder.title1.setText(couponText);
				holder.title2.setText("");
				holder.title3.setText("");
				break;
			case Left:
				holder.title0.setText("");
				holder.title1.setText("");
				holder.title2.setText(couponText);
				holder.title3.setText("");
				break;
			case Right:
				holder.title0.setText("");
				holder.title1.setText("");
				holder.title2.setText("");
				holder.title3.setText(couponText);
				break;
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
			// TODO recycle
		}

	}

	class CouponDetailViewFactory {

		View create(LayoutInflater inflater) {
			View v = inflater.inflate(R.layout.company_sended_coupon_detail_viewpager_item, null);
			setViewHolder(v);
			return v;
		}

		CouponDetailViewHolder setViewHolder(View v) {
			CouponDetailViewHolder holder = new CouponDetailViewHolder();
			holder.photo = (ImageView) v.findViewById(R.id.photo);
			holder.name = (TextView) v.findViewById(R.id.name);
			holder.date = (TextView) v.findViewById(R.id.datetime);
			// holder.company_valid_date = (TextView) v.findViewById(R.id.company_valid_date);
			// holder.couponLayout = v.findViewById(R.id.coupon_layout);
			holder.title0 = (TextView) v.findViewById(R.id.title0);
			holder.title1 = (TextView) v.findViewById(R.id.title1);
			holder.title2 = (TextView) v.findViewById(R.id.title2);
			holder.title3 = (TextView) v.findViewById(R.id.title3);

			holder.coupon = (ImageView) v.findViewById(R.id.coupon_image);
			holder.positions = (TextView) v.findViewById(R.id.positions);
			holder.valid = (TextView) v.findViewById(R.id.coupon_valid);
			holder.state = (ImageView) v.findViewById(R.id.request_state); // [20130620 hyoungjoon.lee] OWL No109 사용 쿠폰 상세 화면에 만료, 사용 아이콘 추가
			v.setTag(holder);
			return holder;
		}

		class CouponDetailViewHolder {
			TextView	name;
			TextView	date;
			ImageView	photo;
			ImageView	coupon;
			TextView	positions;
			TextView	valid;
			ImageView	state;		// [20130620 hyoungjoon.lee] OWL No109 사용 쿠폰 상세 화면에 만료, 사용 아이콘 추가
			TextView	title0;
			TextView	title1;
			TextView	title2;
			TextView	title3;
		}

	}

}
