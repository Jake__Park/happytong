package kr.wisestone.happytong.company.history;

import java.util.ArrayList;
import java.util.List;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.CompanySendedCouponData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.CompanyProtocol;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.CompanyMenuInfoView;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CompanySendCouponHistoryActivity extends BaseActivity implements OnItemClickListener {

	Context								mContext;

	TitleInfoView						mTitleInfoView;

	TextView							mTextViewLimitCount;
	TextView							mTextViewUnLimitCount;

	ArrayList<CompanySendedCouponData>	mList;
	ListView							mListView;
	CouponHistoryListAdapter			mAdapter;

	ListMode							mListMode	= ListMode.NORMAL;

	private enum ListMode {
		NORMAL, EDIT
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
	}

	private void init() {
		mContext = getApplicationContext();

		mTitleInfoView = new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_COMPANY);

		new CompanyMenuInfoView(this, (View) findViewById(R.id.company_menu_tab), CommonData.ENTRY_PONINT_COMPAMY_SEND_LIST);

		mList = new ArrayList<CompanySendedCouponData>();
		mAdapter = new CouponHistoryListAdapter(this, mList);
		mListView = (ListView) findViewById(R.id.listview);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);

		mTextViewLimitCount = (TextView) findViewById(R.id.tv_coupon_ticket_cnt);
		mTextViewUnLimitCount = (TextView) findViewById(R.id.tv_disposable_ticket_cnt);

		setTitleMode(ListMode.NORMAL);

		getServerData();
		setTransmissiveCouponCount();
	}

	private void setTransmissiveCouponCount() {
		SharedPrefManager spm = SharedPrefManager.getInstance(mContext);

		String limitCount = spm.getCompanyOneTimeCoupon();
		if (TextUtils.isEmpty(limitCount)) {
			limitCount = "0";
		}

		mTextViewLimitCount.setText(getString(R.string.send_coupon_ticket, limitCount));

		String unLimitCount = spm.getCompanyNoLimitTimeCoupon();
		if (TextUtils.isEmpty(unLimitCount)) {
			unLimitCount = "0";
		}
		mTextViewUnLimitCount.setText(getString(R.string.send_disposable_ticket, unLimitCount));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, R.string.update).setIcon(android.R.drawable.ic_popup_sync);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Logg.d(item.getItemId());
		getServerData();
		return super.onOptionsItemSelected(item);
	}

	private void getServerData() {
		showProgressDialog();
		new CompanyProtocol(iOK, iFail).getCompaniesSendedCouponList(CallBackParamWrapper.CODE_COMPANY_SEND_LIST, SharedPrefManager.getLoginEmail(mContext));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onProtocolResponse(int eventId, CallbackParam param) {
		super.onProtocolResponse(eventId, param);

		mList.clear();
		if (param.param1 != null) {
			mList.addAll((ArrayList<CompanySendedCouponData>) param.param1);
		}
		mListView.setEmptyView(findViewById(R.id.empty));
		mAdapter.notifyDataSetChanged();

		closeProgressDialog();
	}

	@Override
	public void onBackPressed() {
		// if (mHelpLayout.getVisibility() == View.VISIBLE) {
		// mHelpClickListener.onClick(mHelpLayout);
		// return;
		// }

		if (mListMode == ListMode.EDIT) {
			setTitleMode(ListMode.NORMAL);
		} else {
			backKeyToExit();
		}
	}

	@Override
	public void onProtoColFailResponse(int eventId, CallbackParam param) {
		super.onProtoColFailResponse(eventId, param);
		closeProgressDialog();
	}

	public void setTitleMode(ListMode mode) {
		mListMode = mode;
		if (mode == ListMode.NORMAL) {
			mAdapter.setCheckMode(false);
			mTitleInfoView.updateView(R.string.sending_history, TitleInfoView.TITLE_MODE_COUPON_BOX, titleEditClickListener);
		} else {
			mAdapter.setCheckMode(true);
			mTitleInfoView.updateView(R.string.sending_history, TitleInfoView.TITLE_MODE_COUPON_BOX_DEL, titleClickListener);
			// mCompanyCouponListAdapter.setCheckMode(true);
		}
	}

	final View.OnClickListener	titleEditClickListener	= new View.OnClickListener() {
															@Override
															public void onClick(View v) {
																setTitleMode(ListMode.EDIT);
															}
														};

	final View.OnClickListener	titleClickListener		= new View.OnClickListener() {
															@Override
															public void onClick(View v) {
																switch (v.getId()) {
																case R.id.btn_title_left:
																	// 취소
																	setTitleMode(ListMode.NORMAL);
																	break;
																case R.id.btn_title_right:
																	// 완료
																	confirmDeleteCompanySenedCoupon();
																	break;
																}
															}
														};

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (mAdapter.isCheckMode()) {
			mAdapter.toggleItem(position);
		} else {
			Intent intent = new Intent(this, CompanySendCouponHistoryDetailActivity.class);
			intent.putExtra(IntentDefine.EXTRA_CBOX_LIST, mList);
			intent.putExtra(IntentDefine.EXTRA_CBOX_LIST_SELECT_POSITION, position);
			startActivity(intent);
			// startActivityForResult(intent, IntentDefine.REQCODE_COUPON_DETAIL);
		}
	}

	protected void confirmDeleteCompanySenedCoupon() {

		final List<CompanySendedCouponData> list = mAdapter.getCheckedItemList();
		if (list.size() > 0) {
			// 삭제 요청
			DialogUtil.getConfirmCancelDialog(this, "", getString(R.string.delete_selected_coupon), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						requestNetworkForDeleteCoupon(list);
						break;
					}
					setTitleMode(ListMode.NORMAL);
				}
			}).show();
		} else {
			setTitleMode(ListMode.NORMAL);
		}
	}

	private void requestNetworkForDeleteCoupon(List<CompanySendedCouponData> list) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			if (i != 0) {
				sb.append("#");
			}
			sb.append(list.get(i).itemGroup);
		}

		new CompanyProtocol(iOkCompanyCouponDelete, iFailCouponDelete).delCompaniesSendedCoupon(CallBackParamWrapper.CODE_DEL_COMPANY_COUPON,
				SharedPrefManager.getLoginEmail(mContext), sb.toString());
		showProgressDialog();
	}

	/**
	 * 받은 쿠폰 콜백 - 완료 후 - 보낸 쿠폰 데이터 요청
	 */
	final IWiseCallback	iOkCompanyCouponDelete	= new IWiseCallback() {
													@Override
													public boolean WiseCallback(CallbackParam param) {
														Logg.d("iOkCouponDelete");
														Toast.makeText(mContext, R.string.coupon_deleted, Toast.LENGTH_SHORT).show();
														getServerData();
														setTitleMode(ListMode.NORMAL);
														return false;
													}
												};

	final IWiseCallback	iFailCouponDelete		= new IWiseCallback() {
													@Override
													public boolean WiseCallback(CallbackParam param) {
														Logg.d("iFailCouponDelete");
														closeProgressDialog();
														Toast.makeText(mContext, R.string.fail_to_delete_coupon, Toast.LENGTH_SHORT).show();
														return false;
													}
												};

	@Override
	public void onClick(View v) {
	}

	@Override
	protected int setContentViewId() {
		return R.layout.activity_company_send_coupon_history;
	}

}
