package kr.wisestone.happytong.company.history;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.dataset.CompanySendedCouponData;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.ui.cbox.CouponType;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class CouponHistoryListAdapter extends BaseAdapter {

	Context								mContext;
	LayoutInflater						mInflater;

	ArrayList<CompanySendedCouponData>	mList;

	Picasso								mPicasso;

	private boolean						isCheckMode	= false;

	public CouponHistoryListAdapter(Context mContext, ArrayList<CompanySendedCouponData> mList) {
		super();
		this.mContext = mContext;
		this.mList = mList;
		this.mInflater = LayoutInflater.from(mContext);
		mPicasso = Picasso.with(mContext);
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public CompanySendedCouponData getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.company_coupon_history_list_item, null);
			holder = new Holder();
			holder.chkbox = (CheckBox) convertView.findViewById(R.id.coupon_item_chkbox);
			holder.layout = convertView.findViewById(R.id.layout);
			holder.thumnail = (ImageView) convertView.findViewById(R.id.thumnail);
			holder.thumnailStatus = (ImageView) convertView.findViewById(R.id.thumnail_status);
			holder.title0 = (TextView) convertView.findViewById(R.id.title0);
			holder.title1 = (TextView) convertView.findViewById(R.id.title1);
			holder.title2 = (TextView) convertView.findViewById(R.id.title2);
			holder.title3 = (TextView) convertView.findViewById(R.id.title3);
			holder.name = (TextView) convertView.findViewById(R.id.text_name);
			holder.count = (TextView) convertView.findViewById(R.id.text_count);
			holder.date = (TextView) convertView.findViewById(R.id.text_date);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		CompanySendedCouponData data = mList.get(position);

		if (isCheckMode) {
			holder.chkbox.setVisibility(View.VISIBLE);
			holder.chkbox.setChecked(data.isChecked);
		} else {
			holder.chkbox.setVisibility(View.GONE);
		}

		if (!TextUtils.isEmpty(data.couponCropImageURL)) {
			mPicasso.load(data.couponCropImageURL).placeholder(R.drawable.sub3_default).into(holder.thumnail);
		} else {
			holder.thumnail.setImageResource(0);
		}

		// 쿠폰 텍스트
		holderTitleChange(holder, data.couponType, data.couponText);

		// holder.name.setText(data.mRecevier);
		holder.name.setText(data.mLocationSmallName);
		// holder.count.setText(data.mReceiverCount);
		holder.count.setText(mContext.getString(R.string.bracket_with_people, data.mReceiverCount));
		// holder.date.setText(data.mSendDate);
		holder.date.setText(Utils.getDateFormatStringFromServerDateFormat(data.mSendDate, "yyyy.MM.dd", Locale.ENGLISH));

		return convertView;
	}

	/**
	 * 쿠폰 타입에 맞는 텍스트뷰에만 메시지 표시하도록 함
	 * 
	 * @param holder
	 * @param typeString
	 * @param title
	 */
	private void holderTitleChange(Holder holder, String typeString, String title) {
		switch (CouponType.getCouponType(typeString)) {
		case NA:
		case Center:
			holder.title0.setText(title);
			holder.title1.setText("");
			holder.title2.setText("");
			holder.title3.setText("");
			break;
		case Up:
			holder.title0.setText("");
			holder.title1.setText(title);
			holder.title2.setText("");
			holder.title3.setText("");
			break;
		case Left:
			holder.title0.setText("");
			holder.title1.setText("");
			holder.title2.setText(title);
			holder.title3.setText("");
			break;
		case Right:
			holder.title0.setText("");
			holder.title1.setText("");
			holder.title2.setText("");
			holder.title3.setText(title);
			break;
		}
	}

	public void setCheckMode(boolean mode) {
		isCheckMode = mode;
		if (!isCheckMode) {
			clearCheckedItem();
		}
		notifyDataSetChanged();
	}

	private void clearCheckedItem() {
		for (CompanySendedCouponData data : mList) {
			data.isChecked = false;
		}
	}

	public boolean isCheckMode() {
		return isCheckMode;
	}

	public List<CompanySendedCouponData> getCheckedItemList() {
		List<CompanySendedCouponData> list = new ArrayList<CompanySendedCouponData>();
		for (CompanySendedCouponData data : mList) {
			if (data.isChecked) {
				list.add(data);
			}
		}
		return list;
	}

	public void toggleItem(int position) {
		CompanySendedCouponData data = mList.get(position);
		data.isChecked = !data.isChecked;
		notifyDataSetChanged();
	}

	final OnClickListener	NothingOnClick	= new OnClickListener() {
												@Override
												public void onClick(View v) {
													return;
												}
											};

	class Holder {
		CheckBox	chkbox;
		View		layout;
		ImageView	thumnail;
		ImageView	thumnailStatus;
		TextView	name;
		TextView	count;
		TextView	date;
		TextView	title0;
		TextView	title1;
		TextView	title2;
		TextView	title3;
	}

}
