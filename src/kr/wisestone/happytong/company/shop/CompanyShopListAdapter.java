package kr.wisestone.happytong.company.shop;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.dataset.ProductInfoData;
import kr.wisestone.happytong.data.util.ImageLoader;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CompanyShopListAdapter
{
    OnClickListener mClListener;

    private Context mCtx;
    private int mType;
    ImageLoader mImageLoader;
    private ViewHolder mHolder = null;

    public CompanyShopListAdapter( Context context){
        mCtx = context;
        
        mImageLoader = new ImageLoader(mCtx);

    }

    public LinearLayout getLayout(){
        if(mHolder!=null){
            return mHolder.getTwoLayout();
        }
        return null;
    }

    class ViewHolder {
        int      nType;
        private LinearLayout twoLayout = null;

        ImageView mainImage;

        TextView couponName;
        TextView couponPrice;
        
        private View base;

        public ViewHolder(View base) {
            this.base = base;
        }

        void setViewHolder(int nType){

            mainImage       = (ImageView)base.findViewById(R.id.product_img);
            
            couponName       = (TextView)base.findViewById(R.id.list_name);
            couponPrice        = (TextView)base.findViewById(R.id.list_price);
            
        }

        public ImageView getMainImage() {
            if(mainImage == null){
                mainImage = (ImageView)base.findViewById(R.id.item_img);
            }
            return mainImage;
        }
        
        public TextView getCouponName() {
            if(couponName == null){
                couponName = (TextView)base.findViewById(R.id.item_coupon_name);
            }
            return couponName;
        }
        
        public TextView getCouponPrice() {
            if(couponPrice == null){
                couponPrice = (TextView)base.findViewById(R.id.item_price);
            }
            return couponPrice;
        }

        public LinearLayout getTwoLayout() {
            if(twoLayout==null){
                twoLayout = (LinearLayout)base.findViewById(R.id.list_lay);
            }
            return twoLayout;
        }
    }

    int getResourceId(){
        int res = 0;

        res = R.layout.company_shop_item;

        return res;
    }

    public void setClickListener(OnClickListener listener){
        mClListener = listener;
    }

    public View getView(ProductInfoData data, int index) {

        View view = null;

        LayoutInflater inflater = ((Activity) mCtx).getLayoutInflater();
        view = inflater.inflate(getResourceId(), null);
        mHolder = new ViewHolder(view);
        view.setTag(index);
        mHolder.setViewHolder(mType);


        if(mHolder!=null){
            if(data.getImageURL()!=null && data.getImageURL() != "" && !(data.getImageURL().equalsIgnoreCase("null"))){
                mImageLoader.DisplayImage(data.getImageURL(), mHolder.getMainImage());
            }
            
            //text
            mHolder.getCouponName().setText(data.getProductName());
            
            if(data.getPrice().equals( "0" )){
                mHolder.getCouponPrice().setText(R.string.free);
            } else {
            	mHolder.getCouponPrice().setText(data.getPrice());
            }
        }            

        return view;
    }
}
