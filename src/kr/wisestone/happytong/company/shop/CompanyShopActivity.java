package kr.wisestone.happytong.company.shop;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.ProductInfoData;
import kr.wisestone.happytong.data.dataset.PurchaseInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.AuthProtocol;
import kr.wisestone.happytong.data.protocol.ProductProtocol;
import kr.wisestone.happytong.data.protocol.PurchaseProtocol;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.cshop.IAPController;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.CompanyMenuInfoView;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CompanyShopActivity extends BaseActivity{
    LinearLayout mLeftListLay;
    LinearLayout mRightListLay;
    LinearLayout mPurchasedListLay;
        
    ArrayList<ProductInfoData> mArrCouponData;
    ArrayList<ProductInfoData> mArrDisposableData;

    ArrayList<PurchaseInfoData> mArrPurchaseListData;
    
    IAPController mIAPcontroller;
    
    TextView mTvCouponTicket;
    TextView mTvDisposableTicket;
    
    SharedPrefManager mSp;
    
    boolean mStartedByCSend = false;
	
    OnClickListener mClkCouponListener = new OnClickListener() {
        
        @Override
        public void onClick( View v ) {
            int index = (Integer)v.getTag();
            
            showPurchasePopup(mArrCouponData.get(index));
        }
    };
    
    OnClickListener mClkDisposableListener = new OnClickListener() {
        
        @Override
        public void onClick( View v ) {
            int index = (Integer)v.getTag();

            showPurchasePopup(mArrDisposableData.get(index));
        }
    };

    /**
     * 사업자 쿠폰 리스트 요청 
     */
    final IWiseCallback iOkCouponList = new IWiseCallback() {
        @SuppressWarnings("unchecked")
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkCouponList");
            if (param.param1 != null) {
                if(mArrCouponData != null) mArrCouponData.clear();

                mArrCouponData = (ArrayList<ProductInfoData>)param.param1; 
            }
            if (param.param2 != null) {
                if(mArrDisposableData != null) mArrDisposableData.clear();
                
                mArrDisposableData = (ArrayList<ProductInfoData>)param.param2; 
            }
            drawListData();
            closeProgressDialog();
            return false;
        }
    };
    
    /**
     * IAP Public key 요청
     */
    final IWiseCallback iOkPublickey = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkPublickey");
            if (param.param1 != null) {
                if(initIAPState((String)param.param1)){
                    requestCompanyCouponData();
                }
            }
            return false;
        }
    };
    
    final IAPController.PurchaseListener purchaseListener = new IAPController.PurchaseListener(){

        @Override
        public void onStartPurchase() {
            showProgressDialog();
        }

        @Override
        public void onEndPurchase( int retValue ) {
            closeProgressDialog();
            
            switch(retValue){
                case IAPController.PURCHASE_SUCCESS:
                    requestCompaniesItemCount();
                    break;
                case IAPController.PURCHASE_ERROR:
                    break;
            }
        }
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSp = SharedPrefManager.getInstance(this);
        mStartedByCSend = getIntent().getBooleanExtra(IntentDefine.EXTRA_CSEND_STARTED, false);

        findAllView();
        commonAreaSet();
        
        requestPublicKey();
    }
    
    @Override
    protected void onDestroy() {
        if(mIAPcontroller != null){
            mIAPcontroller.destoryData();
        }
        
        super.onDestroy();
    }
    
    @Override
    public void onBackPressed() {
        if (mStartedByCSend) {
            setResult(Activity.RESULT_OK);
            super.onBackPressed();
            return;
        }
        backKeyToExit();
    }
    
    private void requestPublicKey(){
        showProgressDialog();
        
        AuthProtocol mProtocol = new AuthProtocol(iOkPublickey, iFail);
        mProtocol.authGetPublicKey( 0 );
    }
    
    private void requestCompanyCouponData(){
        showProgressDialog();
        
        ProductProtocol ptAll = new ProductProtocol(iOkCouponList, iFail);
        ptAll.getCompaniesGoods(CallBackParamWrapper.CODE_ITEMS_GET_NORMAL, "2", SharedPrefManager.getLoginEmail(this));
    }    
    
    private boolean initIAPState(String key){
        String oriKey = null;
        
        oriKey = AESEncryption.decrypt( key );
        
        if(oriKey != null){
            mIAPcontroller = new IAPController( this, oriKey, IAPController.IAP_COMPANY_REQUEST, purchaseListener);
        } else {
            DialogUtil.getConfirmDialog( this, R.string.info, R.string.txt_fail_get_coupon_info, new DialogInterface.OnClickListener() {
                
                @Override
                public void onClick( DialogInterface dialog, int which ) {
                    finish();
                }
            } ).show();
            return false;
        }
        return true;
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logg.d( "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        
        if( requestCode == IAPController.RC_REQUEST){
            mIAPcontroller.handleActivityResult(requestCode, resultCode, data);
        } 
    }
    
    private void findAllView() {
        mTvCouponTicket = (TextView)findViewById( R.id.tv_coupon_ticket_cnt );
        mTvDisposableTicket = (TextView)findViewById( R.id.tv_disposable_ticket_cnt );
        
        mTvCouponTicket.setText(getString(R.string.send_coupon_ticket, mSp.getCompanyOneTimeCoupon()));
        mTvDisposableTicket.setText(getString(R.string.send_disposable_ticket, mSp.getCompanyNoLimitTimeCoupon()));
        
        mLeftListLay = (LinearLayout)findViewById( R.id.coupon_list_left );
        mRightListLay = (LinearLayout)findViewById( R.id.coupon_list_right );
    }
        
    void commonAreaSet(){
        TitleInfoView tiv = new TitleInfoView( this, (View)findViewById( R.id.title_lay ) , TitleInfoView.TITLE_TYPE_COMPANY);
        tiv.updateView( R.string.title_charge, TitleInfoView.TITLE_MODE_NORMAL);
 
        new CompanyMenuInfoView(this, (View)findViewById(R.id.company_menu_tab), CommonData.ENTRY_PONINT_COMPAMY_CHARGE);
        
        //[SDJ 131008] 보내기에서 쿠폰함가기로 진입 시 하단 Tab 숨김 처리
        if(mStartedByCSend){
            ((View) findViewById(R.id.company_menu_tab)).setVisibility(View.GONE);
            
            LinearLayout layout1 = (LinearLayout) findViewById(R.id.main_lay);
            RelativeLayout.LayoutParams plControl = (RelativeLayout.LayoutParams) layout1.getLayoutParams();
            plControl.bottomMargin = 0;
            layout1.setLayoutParams(plControl);
        }
    }
    
    private void drawListData(){
        Logg.d( "drawListData size ="  + mArrCouponData.size() );
        
        if(mLeftListLay.getChildCount() > 0) mLeftListLay.removeAllViews();
        if(mRightListLay.getChildCount() > 0) mRightListLay.removeAllViews();
        
        for (int i = 0; i < mArrCouponData.size(); i++) 
            mLeftListLay.addView(CouponListView(mArrCouponData.get( i ), i));
        
        for (int i = 0; i < mArrDisposableData.size(); i++) 
            mRightListLay.addView(CouponDisposableListView(mArrDisposableData.get( i ), i));
    }
    
    private View CouponListView(ProductInfoData data, final int index){
        CompanyShopListAdapter adpater = new CompanyShopListAdapter(this);          
        
        View view = adpater.getView(data, index);
        view.setOnClickListener(mClkCouponListener);
        
        return view;
    }
    
    private View CouponDisposableListView(ProductInfoData data, final int index){
        CompanyShopListAdapter adpater = new CompanyShopListAdapter(this);          
        
        View view = adpater.getView(data, index);
        view.setOnClickListener(mClkDisposableListener);
        
        return view;
    }
   
    private void showPurchasePopup(final ProductInfoData data){
        
        String strTitle = getResources().getString(R.string.info);
        String strMessage = String.format( getResources().getString( R.string.ask_buy_company_charge ), data.getProductName(), data.getPrice() );
        DialogUtil.getConfirmCancelDialog( this, strTitle, strMessage, new DialogInterface.OnClickListener() {
            
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                if(which == DialogInterface.BUTTON_POSITIVE){
                    mIAPcontroller.buyProduct( data.getProductID());        
                }
            }
        } ).show();
    }
    
    private void requestCompaniesItemCount(){
        SharedPrefManager sp = SharedPrefManager.getInstance(this);
        
        PurchaseProtocol protocol = new PurchaseProtocol(new IWiseCallback() {
            
            @Override
            public boolean WiseCallback( CallbackParam param ) {
                PurchaseInfoData data = (PurchaseInfoData)param.param1;
                
                mSp.setPrefCompanyOneTimeCoupon(data.getOneTimeCoupon());
                mSp.setPrefCompanyNoLimitTimeCoupon(data.getNoLimitTimeCoupon());
                
                mTvCouponTicket.setText(getString(R.string.send_coupon_ticket, mSp.getCompanyOneTimeCoupon()));
                mTvDisposableTicket.setText(getString(R.string.send_disposable_ticket, mSp.getCompanyNoLimitTimeCoupon()));

                closeProgressDialog();
                Toast.makeText( CompanyShopActivity.this, R.string.txt_complete_buy, Toast.LENGTH_SHORT ).show();
                
                return false;
            }
        }, iFail);
        protocol.companiesItemCount(0, SharedPrefManager.getLoginEmail(this), sp.getCompanyBusinessNumber());
    }
    
    @Override
    public void onClick( View v )
    {

    }

    @Override
    protected int setContentViewId()
    {
        return R.layout.activity_company_shop;
    }    

}
