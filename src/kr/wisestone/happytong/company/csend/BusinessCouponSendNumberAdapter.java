package kr.wisestone.happytong.company.csend;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class BusinessCouponSendNumberAdapter extends BaseAdapter{

	private LayoutInflater mInflater; // Inflater
	private int mLayout; // ListView의 row layout id
	private ArrayList<?> mArrayBusinessNumData;

	public BusinessCouponSendNumberAdapter(Context context,
			ArrayList<?> arrayBusinessNumData, int layout) {
		this.mArrayBusinessNumData = arrayBusinessNumData;
		this.mLayout = layout;
		this.mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		Logg.d("getCount = " + mArrayBusinessNumData.size());
		return mArrayBusinessNumData.size();
	}

	@Override
	public Object getItem(int position) {
		return mArrayBusinessNumData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		BusinessViewHolderList mHolder;
		
		if (convertView == null) {
			convertView = mInflater.inflate(mLayout, parent, false);

			mHolder = new BusinessViewHolderList();
			mHolder = BusinessHolderData(convertView);

			convertView.setTag(mHolder);
		} else {
			mHolder = (BusinessViewHolderList) convertView.getTag();
		}

		View v = setBusinessNumView(position, convertView, parent, mHolder);
		
		return v;
	}
	
	BusinessViewHolderList BusinessHolderData(View convertView) {
		BusinessViewHolderList holder = new BusinessViewHolderList();
		holder.mContactListItem = (LinearLayout) convertView
				.findViewById(R.id.ll_business_contactListItem);
		holder.mCbBusinessNum = (CheckBox) convertView
				.findViewById(R.id.cb_business_number);
		holder.mBusinessNum = (TextView) convertView
				.findViewById(R.id.tv_business_number);

		return holder;
	}
	
	public class BusinessViewHolderList {
		public LinearLayout mContactListItem;
		public TextView mBusinessNum;
		public CheckBox mCbBusinessNum;
	}
	
	public abstract View setBusinessNumView(int position, View convertView,
			ViewGroup parent, BusinessViewHolderList holder);

}
