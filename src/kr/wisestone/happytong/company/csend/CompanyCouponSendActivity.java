package kr.wisestone.happytong.company.csend;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.company.shop.CompanyShopActivity;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.LocationInfoData;
import kr.wisestone.happytong.data.dataset.OrderCouponPurchased;
import kr.wisestone.happytong.data.dataset.PurchaseInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.CompanyProtocol;
import kr.wisestone.happytong.data.protocol.LocationProtocol;
import kr.wisestone.happytong.data.protocol.PurchaseProtocol;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.DummyMainActivity;
import kr.wisestone.happytong.ui.cbox.CouponType;
import kr.wisestone.happytong.ui.csend.CouponSendSideMenu;
import kr.wisestone.happytong.ui.csend.CouponSendSideMenu.CouponSendSideMenuAnimationListener;
import kr.wisestone.happytong.ui.csend.CouponSendSideMenu.CouponSendSideMenuListener;
import kr.wisestone.happytong.ui.cshop.CouponShopActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.setting.LocationListViewActivity;
import kr.wisestone.happytong.ui.widget.CompanyMenuInfoView;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * LocationListView Intent로 넘어갈 때 putExtra에 이전에 선택했던 LocationSmallCode 보내주시길바랍니다. IntentDefine.EXTRA_LOCATION_COMPANY_DATA
 * 
 * - SharedPref로 Locatioin 정보 가져오는 방법 - LocationInfoData mLocationInfoData = new LocationInfoData();
 * mLocationInfoData.setSplitLocationData(SharedPrefManager.getLocationCompanyData)); mLocationInfoData.getLocationBigName();
 * 
 */
public class CompanyCouponSendActivity extends BaseActivity implements CouponSendSideMenuListener, OnCheckedChangeListener {

	static final int		DATE_DIALOG_ID	= 0;

	Context					mContext;

	// 우측 메뉴
	CouponSendSideMenu		mSideMenu;

	// 상단 발송권 영역
	RadioGroup				mCouponRadioGroup;
	RadioButton				mCouponRadioButtonOne;
	RadioButton				mCouponRadioButtonInfinite;

	// 대상 지역
	TextView				mTextViewTargetLocation;
	// 대상 발송건
	View					mViewTargetCountLayout;
	View					mViewTargetUnlimitCountLayout;
	TextView				mTextViewTargetCount;
	// 유효 기간
	TextView				mTextViewValid;

	EditText				mEditTextCouponContents0;
	EditText				mEditTextCouponContents1;
	EditText				mEditTextCouponContents2;
	EditText				mEditTextCouponContents3;

	TextWatcher				mEditTextWatcher0;
	TextWatcher				mEditTextWatcher1;
	TextWatcher				mEditTextWatcher2;
	TextWatcher				mEditTextWatcher3;

	ImageView				mImageViewCoupon;

	Calendar				mCalendar;

	OrderCouponPurchased	mCouponData;

	ImageLoader				mImageLoader;

	SharedPrefManager		mSharedPrefManager;

	int						tCount			= 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = this;
		mSharedPrefManager = SharedPrefManager.getInstance(mContext);

		initTitleView();
		init();
		initCoupon();

		setTransmissiveCouponCount();

		mSideMenu = new CouponSendSideMenu(this, findViewById(R.id.coupon_send_sidemenu), findViewById(R.id.coupon_send_sidemenu_content), findViewById(R.id.cover), this, iFail);
		mSideMenu.setGoButtonImageResource(R.drawable.selector_sub7_02_shop_btn);

		new CompanyMenuInfoView(this, (View) findViewById(R.id.company_menu_tab), CommonData.ENTRY_PONINT_COMPAMY_SEND);

		setDefaultLocation();
		setDefaultTransmissiveNumber();
	}

	private void setDefaultTransmissiveNumber() {
		// 발송 건수 기본값 지정
		
		Intent intent = new Intent();
		intent.putExtra(IntentDefine.EXTRA_BUSINESS_CUPON_SEND_NUM_DATA, 100);
		setTransmissiveNumber(intent);
	}

	private void setDefaultLocation() {
		// 기본 지역, 코드와 지역명 세팅
		new LocationProtocol(new IWiseCallback() {
			@SuppressWarnings("unchecked")
			@Override
			public boolean WiseCallback(CallbackParam param) {
				Logg.d("setDefaultLocation - iOkLocationUserCountList");
				if (param.param1 != null) {
					ArrayList<LocationInfoData> mArrLocationData = (ArrayList<LocationInfoData>) param.param1;
					findLocationInfoDataBySmallCode(mArrLocationData);
				} else {
					closeProgressDialog();
				}
				return false;
			}
		}, iFail).locationUserCountListData(0, SharedPrefManager.getLoginEmail(this));
		showProgressDialog();
	}

	protected void findLocationInfoDataBySmallCode(List<LocationInfoData> list) {
		final String locSmallCode = mSharedPrefManager.getLocationCompanySmallCode();
		LocationInfoData locData = new LocationInfoData();
		for (LocationInfoData data : list) {
			if (locSmallCode.equals(data.getLocationSmallCode())) {
				locData = data;
				break;
			}
		}
		setLocationData(locData);
		closeProgressDialog();
	}

	// 발송권 카운트 갱신
	private void setTransmissiveCouponCount() {
		String ontimeCouponCountString = mSharedPrefManager.getCompanyOneTimeCoupon();
		String infiniteCouponCountString = mSharedPrefManager.getCompanyNoLimitTimeCoupon();

		if (TextUtils.isEmpty(ontimeCouponCountString)) {
			ontimeCouponCountString = "0";
		}

		if (TextUtils.isEmpty(infiniteCouponCountString)) {
			infiniteCouponCountString = "0";
		}

		mCouponRadioButtonOne.setText(getString(R.string.send_coupon_ticket, ontimeCouponCountString));
		mCouponRadioButtonOne.setTag(ontimeCouponCountString);

		mCouponRadioButtonInfinite.setText(getString(R.string.send_disposable_ticket, infiniteCouponCountString));
		mCouponRadioButtonInfinite.setTag(infiniteCouponCountString);
	}

	private void init() {
		mTextViewTargetLocation = (TextView) findViewById(R.id.target_location);
		mTextViewTargetLocation.setOnClickListener(this);
		mTextViewTargetCount = (TextView) findViewById(R.id.target_count);
		mTextViewTargetCount.setOnClickListener(this);

		mViewTargetCountLayout = findViewById(R.id.target_count_layout);
		mViewTargetUnlimitCountLayout = findViewById(R.id.target_count_unlimit);

		mTextViewValid = (TextView) findViewById(R.id.coupon_valid);
		mTextViewValid.setOnClickListener(this);

		findViewById(R.id.coupon_send).setOnClickListener(this);

		mImageViewCoupon = (ImageView) findViewById(R.id.coupon_image);
		mImageViewCoupon.setOnClickListener(this);

		mEditTextCouponContents0 = (EditText) findViewById(R.id.title0);
		mEditTextCouponContents1 = (EditText) findViewById(R.id.title1);
		mEditTextCouponContents2 = (EditText) findViewById(R.id.title2);
		mEditTextCouponContents3 = (EditText) findViewById(R.id.title3);

		mEditTextWatcher0 = new CouponTextWatcher(mEditTextCouponContents1, mEditTextCouponContents2, mEditTextCouponContents3);
		mEditTextWatcher1 = new CouponTextWatcher(mEditTextCouponContents0, mEditTextCouponContents2, mEditTextCouponContents3);
		mEditTextWatcher2 = new CouponTextWatcher(mEditTextCouponContents0, mEditTextCouponContents1, mEditTextCouponContents3);
		mEditTextWatcher3 = new CouponTextWatcher(mEditTextCouponContents0, mEditTextCouponContents1, mEditTextCouponContents2);

		mCouponRadioGroup = (RadioGroup) findViewById(R.id.coupon_radio);
		mCouponRadioGroup.setOnCheckedChangeListener(this);
		mCouponRadioButtonOne = (RadioButton) findViewById(R.id.coupon_radio_onetime);
		mCouponRadioButtonInfinite = (RadioButton) findViewById(R.id.coupon_radio_infinite);
		// 라디오그룹 기본 체크 설정?!
		mCouponRadioGroup.check(R.id.coupon_radio_onetime);

		mCalendar = Calendar.getInstance();

		mImageLoader = new ImageLoader(this);
	}

	private void initCoupon() {
		String savedOcp = mSharedPrefManager.getRecentSelectedCoupon();

		if (TextUtils.isEmpty(savedOcp)) {
			// 쿠폰 보내기 처음 - 디폴트 이미지, 유효기간 없음. 쿠폰 보내기 비활성화. 알림 필요
			mTextViewValid.setVisibility(View.GONE);
			activeEditTextByCouponType(CouponType.NA);
		} else {
			// 안드로이드를 아이폰처럼 선택한 쿠폰 이미지 유지하도록 수정. 20130806.jish
			onCouponItemSelected(OrderCouponPurchased.convertStringToData(savedOcp));
		}
	}

	/**
	 * 쿠폰 타입에 맞게 쿠폰내용입력란 처리
	 * 
	 * @param couponType
	 *            (쿠폰의 타입 0:가운데, 1:왼쪽, 2:오른쪽)
	 */
	private void activeEditTextByCouponType(CouponType couponType) {
		mEditTextCouponContents0.setVisibility(View.GONE);
		mEditTextCouponContents1.setVisibility(View.GONE);
		mEditTextCouponContents2.setVisibility(View.GONE);
		mEditTextCouponContents3.setVisibility(View.GONE);

		// 재귀호출 되므로 Wathcer제거후 보여지는 EditText에만 Watcher추가
		mEditTextCouponContents0.removeTextChangedListener(mEditTextWatcher0);
		mEditTextCouponContents1.removeTextChangedListener(mEditTextWatcher1);
		mEditTextCouponContents2.removeTextChangedListener(mEditTextWatcher2);
		mEditTextCouponContents3.removeTextChangedListener(mEditTextWatcher3);
		switch (couponType) {
		case Center:
			mEditTextCouponContents0.setVisibility(View.VISIBLE);
			mEditTextCouponContents0.addTextChangedListener(mEditTextWatcher0);
			Utils.showKeypad(this, mEditTextCouponContents0);
			break;
		case Up:
			mEditTextCouponContents1.setVisibility(View.VISIBLE);
			mEditTextCouponContents1.addTextChangedListener(mEditTextWatcher1);
			Utils.showKeypad(this, mEditTextCouponContents1);
			break;
		case Left:
			mEditTextCouponContents2.setVisibility(View.VISIBLE);
			mEditTextCouponContents2.addTextChangedListener(mEditTextWatcher2);
			Utils.showKeypad(this, mEditTextCouponContents2);
			break;
		case Right:
			mEditTextCouponContents3.setVisibility(View.VISIBLE);
			mEditTextCouponContents3.addTextChangedListener(mEditTextWatcher3);
			Utils.showKeypad(this, mEditTextCouponContents3);
			break;
		default:
			break;
		}
	}

	private boolean isEmptyEditTexts() {
		if (TextUtils.isEmpty(mEditTextCouponContents1.getText().toString())) {
			return true;
		}
		if (TextUtils.isEmpty(mEditTextCouponContents2.getText().toString())) {
			return true;
		}
		if (TextUtils.isEmpty(mEditTextCouponContents3.getText().toString())) {
			return true;
		}
		return false;
	}

	private void initTitleView() {
		TitleInfoView titleInfoView = new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_COMPANY);
		// 메뉴하고 상단 타이틀 제목 맞추기(친구리스트:친구, 쿠폰발송:쿠폰보내기) - 20130806.jish
		titleInfoView.updateView(R.string.coupon_send, TitleInfoView.TITLE_MODE_NORMAL);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.target_location:
			// 대상 위치 선택
			Logg.d("target_location");
			LocationInfoData data = (LocationInfoData) mTextViewTargetLocation.getTag();

			Intent locIntent = new Intent(this, LocationListViewActivity.class);
			locIntent.putExtra(IntentDefine.EXTRA_LOCATION_LIST_TYPE, CommonData.LOCATION_LIST_TYPE_COMPANY_SEND);
			if (data != null && !TextUtils.isEmpty(data.getLocationSmallCode())) {
				locIntent.putExtra(IntentDefine.EXTRA_LOCATION_COMPANY_DATA, data.getLocationSmallCode());
			}
			startActivityForResult(locIntent, IntentDefine.REQCODE_GET_LOCATION);
			break;
		case R.id.target_count:
			// 대상 건수 선택
			Logg.d("target_count");
			Intent numIntent = new Intent(this, BusinessCouponSendNumberActivity.class);
			numIntent.putExtra(IntentDefine.EXTRA_BUSINESS_CUPON_SEND_NUM_DATA, tCount);
			startActivityForResult(numIntent, IntentDefine.REQCODE_GET_NUMBER);

			break;
		case R.id.coupon_image:
			sideMenuOpenClose();
			break;
		case R.id.coupon_send_sidemenu_handle:
			Logg.d("R.id.coupon_send_sidemenu_handle - onClick");
			sideMenuOpenClose();
			break;
		case R.id.coupon_valid:
			showDialog(DATE_DIALOG_ID);
			break;
		case R.id.coupon_send:
			if (isValid()) {
				requestNetworkForCouponSend();
			}
			break;
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.coupon_radio_onetime:
			// mViewTargetCountLayout.setVisibility(View.VISIBLE);
			// mViewTargetUnlimitCountLayout.setVisibility(View.GONE);
			mTextViewTargetCount.setVisibility(View.VISIBLE);
			mViewTargetUnlimitCountLayout.setVisibility(View.GONE);
			break;
		case R.id.coupon_radio_infinite:
			// mViewTargetCountLayout.setVisibility(View.GONE);
			// mViewTargetUnlimitCountLayout.setVisibility(View.VISIBLE);
			mTextViewTargetCount.setVisibility(View.GONE);
			mViewTargetUnlimitCountLayout.setVisibility(View.VISIBLE);
			break;
		}
	}

	private void sideMenuOpenClose() {
		if (mSideMenu.isAnimating()) {
			return;
		}
		if (mSideMenu.isOpen()) {
			mSideMenu.closeSideMenu();
		} else {
			mSideMenu.openSideMenu();
		}
	}

	@Override
	public void onCouponItemSelected(OrderCouponPurchased data) {
		mCouponData = data;

		Logg.d("onCouponItemSelected : " + data.couponType + " / " + data.couponSEQ + " / " + data.couponImageURL + " / " + data.productName);
		// 쿠폰 이미지 변경
		mImageLoader.DisplayImage(data.couponImageURL, mImageViewCoupon);
		// 유효기간 영역 활성화
		mTextViewValid.setVisibility(View.VISIBLE);
		// 쿠폰 타입에 맞게 입력란 활성화
		activeEditTextByCouponType(CouponType.getCouponType(data.couponType));
	}

	@Override
	public void onCouponShopButtonClick() {
		mSideMenu.closeSideMenu(sideMenuCloseL);
	}

	final CouponSendSideMenuAnimationListener	sideMenuCloseL	= new CouponSendSideMenuAnimationListener() {
																	@Override
																	public void onCloseAnimationEnd() {
																		Intent shopIntent = new Intent(mContext, CouponShopActivity.class);
																		shopIntent.putExtra(IntentDefine.EXTRA_CSEND_STARTED, true);
																		startActivityForResult(shopIntent, IntentDefine.REQCODE_MOVE_SHOP);
																	}
																};

	@Override
	public void onCouponImageUpdated() {
		// 우측 사이드메뉴 쿠폰 네트워크 완료 후 콜백
		closeProgressDialog();
	}

	private void requestNetworkForCouponSend() {
		Logg.d("requestNetworkForCouponSend()");
		String productType = (mCouponRadioGroup.getCheckedRadioButtonId() == mCouponRadioButtonOne.getId() ? "1" : "2");
		LocationInfoData locData = (LocationInfoData) mTextViewTargetLocation.getTag();

		String sendCount;
		if (mCouponRadioGroup.getCheckedRadioButtonId() == mCouponRadioButtonOne.getId()) {
			// 일반발송권일 경우, 발송 건수에 저장되어있는 데이터 이용하여 sendCount 세팅
			sendCount = (String) mTextViewTargetCount.getTag();
		} else {
			// 무제한발송권일 경우, 지역 이용자수로 sendCount 세팅
			sendCount = locData.getLocationUserCount();
		}

		new CompanyProtocol(couponSendingOk, iFail).companiesCouponSend(CallBackParamWrapper.CODE_COMPANY_COUPON_SEND, SharedPrefManager.getLoginEmail(mContext),
				locData.getLocationSmallCode(), mEditTextCouponContents1.getText().toString(), mTextViewValid.getTag().toString(), mSharedPrefManager.getCompanyBusinessNumber(),
				productType, mCouponData.couponSEQ, sendCount);
		showProgressDialog();
	}

	final IWiseCallback	couponSendingOk	= new IWiseCallback() {
											@Override
											public boolean WiseCallback(CallbackParam param) {

												// 안드로이드를 아이폰처럼 선택한 쿠폰 이미지 유지하도록 수정. 20130806.jish
												mSharedPrefManager.setRecentSelectedCoupon(OrderCouponPurchased.convertDataToString(mCouponData));

												// 남은 발송권 재조회
												requestCouponRemain();
												return false;
											}
										};

	/**
	 * 발송권 재조회 및 갱신
	 */
	private void requestCouponRemain() {
		PurchaseProtocol protocol = new PurchaseProtocol(new IWiseCallback() {
			@Override
			public boolean WiseCallback(CallbackParam param) {
				PurchaseInfoData data = (PurchaseInfoData) param.param1;

				mSharedPrefManager.setPrefCompanyOneTimeCoupon(data.getOneTimeCoupon());
				mSharedPrefManager.setPrefCompanyNoLimitTimeCoupon(data.getNoLimitTimeCoupon());

				closeProgressDialog();
				setTransmissiveCouponCount();

				Toast.makeText(mContext, R.string.complete_send_coupon, Toast.LENGTH_SHORT).show();

				Intent mainIntent = new Intent(mContext, DummyMainActivity.class);
				mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				mainIntent.putExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_COMPAMY_SEND_LIST);
				mContext.startActivity(mainIntent);

				return false;
			}
		}, iFail);
		protocol.companiesItemCount(0, SharedPrefManager.getLoginEmail(this), mSharedPrefManager.getCompanyBusinessNumber());
	}

	private boolean isValid() {

		if (mCouponRadioGroup.getCheckedRadioButtonId() == mCouponRadioButtonOne.getId()) {
			// 발송 건수 선택되지 않음
			if (TextUtils.isEmpty((String) mTextViewTargetCount.getTag())) {
				DialogUtil.getConfirmDialog(this, "", getString(R.string.select_coupon_send_count), null).show();
				return false;
			}

			// 일회발송권 선택 - 갯수 확인 0일 경우 샵 이동 유도
			String count = (String) mCouponRadioButtonOne.getTag();
			if (TextUtils.isEmpty(count) || "0".equals(count)) {
				DialogUtil.getConfirmCancelDialog(this, "", getString(R.string.onetime_coupon_count_is_shortage), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == DialogInterface.BUTTON_POSITIVE) {
							startCouponShopActivityForResult();
						}
					}
				}).show();
				return false;
			}
			// 일회발송권 선택 - 갯수 확인
			// 일회발송권 갯수와 선택된 지역의 인원수와 비교하여, 일회발송권 갯수가 더 적을경우 발송하지 않도록 처리하였으나, 처리 필요 없어졌음. 2013.10.11.jish
			// String count = (String) mCouponRadioButtonOne.getTag();
			// String userCount = ((LocationInfoData) mTextViewTargetLocation.getTag()).getLocationUserCount();
			// if (!TextUtils.isEmpty(count) && !TextUtils.isEmpty(userCount)) {
			// int couponCount = Integer.parseInt(count);
			// int receiverCount = Integer.parseInt(userCount);
			// if (couponCount < receiverCount) {
			// DialogUtil.getConfirmCancelDialog(this, "", getString(R.string.onetime_coupon_count_is_shortage), new DialogInterface.OnClickListener() {
			// @Override
			// public void onClick(DialogInterface dialog, int which) {
			// if (which == DialogInterface.BUTTON_POSITIVE) {
			// startCouponShopActivityForResult();
			// }
			// }
			// }).show();
			// return false;
			// }
			// }
		} else {
			// 무제한발송권 선택 - 갯수 확인
			String count = (String) mCouponRadioButtonInfinite.getTag();
			if (TextUtils.isEmpty(count) || "0".equals(count)) {
				DialogUtil.getConfirmCancelDialog(this, "", getString(R.string.infinite_coupon_count_is_shortage), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == DialogInterface.BUTTON_POSITIVE) {
							startCouponShopActivityForResult();
						}
					}
				}).show();
				return false;
			}
		}
		
		if (TextUtils.isEmpty(mTextViewTargetLocation.getText().toString())) {
			// 지역 선택되지 않음
			DialogUtil.getConfirmDialog(this, "", getString(R.string.select_location_for_send), null).show();
			return false;
		}

		if ("0".equals(((LocationInfoData) mTextViewTargetLocation.getTag()).getLocationUserCount())) {
			// 지역 사용자 수 확인 - 0일 경우 에러 메시지 표시 (발송권 종류 상관 없이 체크)
			DialogUtil.getConfirmDialog(this, "", getString(R.string.nobody_in_selected_location), null).show();
			return false;
		}

		if (mCouponData == null) {
			// 쿠폰 데이터 설정 안됐음. 쿠폰 이미지 선택하라는 메시지 표시
			DialogUtil.getConfirmDialog(this, "", getString(R.string.coupon_image_not_setted), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mSideMenu.openSideMenu();
				}
			}).show();
			return false;
		}
		if (isEmptyEditTexts()) {
			// 내용 입력 안됨
			DialogUtil.getConfirmDialog(this, "", getString(R.string.coupon_text_not_setted), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			}).show();
			return false;
		}
		if (mTextViewValid.getTag() == null) {
			// 유효기간 설정안됨
			DialogUtil.getConfirmDialog(this, "", getString(R.string.input_valid_date), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			}).show();
			return false;
		}
		return true;
	}

	private void startCouponShopActivityForResult() {
		Intent shopIntent = new Intent(this, CompanyShopActivity.class);
		shopIntent.putExtra(IntentDefine.EXTRA_CSEND_STARTED, true);
		startActivityForResult(shopIntent, IntentDefine.REQCODE_MOVE_COMPANY_SHOP);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_CANCELED) {
			return;
		}

		// 건수 가져오기
		if (requestCode == IntentDefine.REQCODE_GET_NUMBER) {
			setTransmissiveNumber(data);
			return;
		}

		// 지역 가져오기
		if (requestCode == IntentDefine.REQCODE_GET_LOCATION) {
			if (data == null) {
				return;
			}
			LocationInfoData mLocationInfoData = (LocationInfoData) data.getSerializableExtra(IntentDefine.EXTRA_COMPANY_INFO);
			setLocationData(mLocationInfoData);
			return;
		}

		// 발송권 구매 - CompanyShopActivity 이동
		if (requestCode == IntentDefine.REQCODE_MOVE_COMPANY_SHOP) {
			setTransmissiveCouponCount();
			setRecentPage();
			return;
		}

		// 쿠폰샵 이동 - 우측 쿠폰 이미지 리스트에 있는 쿠폰샵 이동 - 이미지 구매
		if (requestCode == IntentDefine.REQCODE_MOVE_SHOP) {
			setTransmissiveCouponCount();
			showProgressDialog();
			mSideMenu.updateMyCouponImages();
			setRecentPage();
			return;
		}
	}

	/**
	 * 쿠폰샵, 업체샵으로 이동하고 발송페이지에서 종료시, 재진입시 쿠폰샵이나 업체샵으로 페이지 열림. 이를 해결하기 위한 최신 접근 페이지 갱신
	 */
	private void setRecentPage() {
		SharedPrefManager.getInstance(mContext).setBeforeEntryPoint(CommonData.ENTRY_PONINT_COMPAMY_SEND);
	}

	private void setLocationData(LocationInfoData data) {
		mTextViewTargetLocation.setText(data.getLocationBigName() + " - " + data.getLocationSmallName() + " (" + data.getLocationUserCount() + ")");
		mTextViewTargetLocation.setTag(data);
	}

	private void setTransmissiveNumber(Intent data) {
		if (data == null) {
			return;
		}
		tCount = data.getIntExtra(IntentDefine.EXTRA_BUSINESS_CUPON_SEND_NUM_DATA, 0);
		mTextViewTargetCount.setText(tCount + "건");
		mTextViewTargetCount.setTag(String.valueOf(tCount));
	}

	@Override
	public void onBackPressed() {
		if (mSideMenu.isOpen()) {
			mSideMenu.closeSideMenu();
		} else {
			backKeyToExit();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle args) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, onDateSetListener, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
		}
		return super.onCreateDialog(id, args);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog, Bundle args) {
		switch (id) {
		case DATE_DIALOG_ID:
			((DatePickerDialog) dialog).updateDate(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
			break;
		}
		super.onPrepareDialog(id, dialog, args);
	}

	final DatePickerDialog.OnDateSetListener	onDateSetListener	= new DatePickerDialog.OnDateSetListener() {
																		@Override
																		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

																			Calendar cToday = Calendar.getInstance();

																			Calendar cSelected = Calendar.getInstance();
																			cSelected.set(year, monthOfYear, dayOfMonth);

																			if (cToday.after(cSelected)) {
																				Toast.makeText(mContext, R.string.setting_date_after_today, Toast.LENGTH_LONG).show();
																				showDialog(DATE_DIALOG_ID);
																				return;
																			}

																			mCalendar.set(year, monthOfYear, dayOfMonth);
																			Logg.d("showSelectValidDateDialog - onDateSetListener : " + mCalendar);
																			Date date = new Date(mCalendar.getTimeInMillis());
																			String convertedServerDateString = Utils.getServerDateFormatString(date);
																			mTextViewValid.setText(getDateStringForValidDateText(date));
																			mTextViewValid.setTag(convertedServerDateString);
																		}
																	};

	private String getDateStringForValidDateText(Date date) {
		return new SimpleDateFormat(getString(R.string.until_yyyy_mm_dd), Locale.KOREA).format(date);
	};

	@Override
	protected int setContentViewId() {
		return R.layout.activity_company_coupon_send;
	}

}

// EditText 텍스트 변경시, 보이지 않는 다른 EditText에도 변화를 주도록 함
class CouponTextWatcher implements TextWatcher {

	EditText[]	mEditTexts;

	public CouponTextWatcher(EditText... editTexts) {
		super();
		this.mEditTexts = editTexts;
	}

	@Override
	public void afterTextChanged(Editable s) {
		for (EditText et : mEditTexts) {
			et.setText(s);
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

}