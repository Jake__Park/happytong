package kr.wisestone.happytong.company.csend;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.company.csend.BusinessCouponSendNumberAdapter.BusinessViewHolderList;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

public class BusinessCouponSendNumberActivity extends BaseActivity{

	public static final int BUSINESS_CHECK_FLAG_DEACTIVE = -1; // 비 선택 모드
	public static final int BUSINESS_CHECK_FLAG_CHECKED = 1; // 체크 활성화
	public static final int BUSINESS_CHECK_FLAG_UNCHECK = 2; // 체크 비활성화
	
	Context mContext;
	
	private TitleInfoView mTitleInfoView;
	private ListView mBusinessListView;
	private BusinessCouponSendNumberAdapter mBusinessCouponSendNumberAdapter;
	
	private ArrayList<Integer> mArrBusinessNum = new ArrayList<Integer>();
	
	private int mBusinessNum;
	private int mDisplayNum;
	private int mCheck = -1;
	private int resultNum;
	private int mCheckNum = 0;
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected int setContentViewId() {
		// TODO Auto-generated method stub
		return R.layout.activity_business_coupon_send_number_list;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = this;
		
		onInit();

	}
	
	private void onInit(){
		onFindViewById();
		getNumberData();
		commonAreaSet();
		listViewInit();
	}
	
	private void onFindViewById(){
		mBusinessListView = (ListView) findViewById(R.id.lv_business_location);
		mBusinessListView.setDivider(null);
	}
	
	public boolean isChecked() {
		return (this.mCheck == BUSINESS_CHECK_FLAG_CHECKED) ? true : false;
	}

	public void setCheckBoxFlag(int flag) {
		this.mCheck = flag;
	}
	
	OnClickListener mTitleClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (isChecked())
			{
				if(resultNum == mCheckNum){
					finish();
				}else{
					String title  = mContext.getResources().getString( R.string.info );				
		            String message = String.format( mContext.getResources().getString( R.string.company_send_coupon_select_popup ), mBusinessNum );
					
					DialogUtil.getConfirmCancelDialog( mContext, title, message, new DialogInterface.OnClickListener() {
		                
		                @Override
		                public void onClick( DialogInterface dialog, int which ) {
		                    if (which != DialogInterface.BUTTON_POSITIVE) {
		                        return;
		                    } else{
			                    	setDataResultOkFinish();
		                    }
		                }
		            } ).show();
				}				
			} else {
				Logg.d("mTitleClick invite nodata");
				Toast.makeText(BusinessCouponSendNumberActivity.this,
						R.string.business_coupon_send_number_toast, Toast.LENGTH_SHORT)
						.show();
				return;
			}
		}
	};

	OnClickListener mCheckClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (mBusinessListView == null) {
				return;
			}

			int position = mBusinessListView.getPositionForView(v);
			Logg.d("position = " + position);

			if (position < 0) {
				Logg.d("can not find item from view");
				return;
			}

			int num = mArrBusinessNum.get(position);

			setCheckState(num, position);
		}
	};

	private void setCheckState(int num, int position) {
		if (isChecked()) {
			mBusinessNum = num;
		}
		else {
			setCheckBoxFlag(BUSINESS_CHECK_FLAG_CHECKED);
			mBusinessNum = num;
		}

		mBusinessCouponSendNumberAdapter.notifyDataSetChanged();
	}

	private void commonAreaSet() {
		mTitleInfoView = new TitleInfoView(this,
				(View) findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_COMPANY);
		mTitleInfoView
				.updateView(R.string.business_coupon_send_number_title,	TitleInfoView.TITLE_BUSINESS_COUPON_SEND_NUMBER, mTitleClick);
	}
	
	private void getNumberData(){
		Intent intent = getIntent();
		int checkNum = intent.getIntExtra(IntentDefine.EXTRA_BUSINESS_CUPON_SEND_NUM_DATA, 0);
		if(checkNum != 0){
			mBusinessNum = intent.getIntExtra(IntentDefine.EXTRA_BUSINESS_CUPON_SEND_NUM_DATA, 0);
			mCheckNum = mBusinessNum;
			setCheckBoxFlag(BUSINESS_CHECK_FLAG_CHECKED);
		}		
	}
	
	private void listViewInit() {
		
		int sendNum = 100;
		for(int i=0; i<10; i++){		
			mArrBusinessNum.add(sendNum);
			sendNum += 100;
		}

		mBusinessCouponSendNumberAdapter = new BusinessCouponSendNumberAdapter(this,
				mArrBusinessNum, R.layout.business_send_number_list_item) {
			public View setBusinessNumView(int position, View convertView,	ViewGroup parent, BusinessViewHolderList holder) {
				setBusinessViewData(holder,	(Integer) getItem(position), position);

				return convertView;
			}
		};
		mBusinessListView.setAdapter(mBusinessCouponSendNumberAdapter);
		
	}

	private void setBusinessViewData(BusinessViewHolderList holder,
			int num, int position) {
		
		mDisplayNum = num;
		holder.mBusinessNum.setText(String.valueOf(mDisplayNum) + "건");

		if (mDisplayNum == mBusinessNum) {
			holder.mCbBusinessNum.setChecked(true);
			resultNum = mBusinessNum;
		}
		else {
			holder.mCbBusinessNum.setChecked(false);
		}

		holder.mContactListItem.setOnClickListener(mCheckClickListener);
		
	}
	
	private void setDataResultOkFinish() {
		Intent resultIntent = new Intent();
		resultIntent.putExtra(IntentDefine.EXTRA_BUSINESS_CUPON_SEND_NUM_DATA, resultNum);
		setResult(Activity.RESULT_OK, resultIntent);
		finish();
	}
	
}
