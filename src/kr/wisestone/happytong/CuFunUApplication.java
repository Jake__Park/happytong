package kr.wisestone.happytong;

import java.util.Locale;

import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.util.AESEncryption;
import android.app.Application;
import android.text.TextUtils;

import com.google.android.gcm.GCMRegistrar;

public class CuFunUApplication extends Application {

	static CuFunUApplication instance;

	public static String getToken() {
		if ((instance != null) && !TextUtils.isEmpty(GCMRegistrar.getRegistrationId(instance))) {
			return AESEncryption.encrypt(CommonData.TOKEN_PREFIX + GCMRegistrar.getRegistrationId(instance));
		}
		return "";
	}

	public static String getNationCode() {
		Locale loc = Locale.getDefault();
		if (loc.equals(Locale.KOREA) || loc.equals(Locale.KOREAN)) {
			return CommonData.LOCALE_KOREA;
		} else if (loc.equals(Locale.JAPAN) || loc.equals(Locale.JAPANESE)) {
			return CommonData.LOCALE_JAPAN;
		} else if (loc.equals(Locale.ENGLISH) || loc.equals(Locale.US) || loc.equals(Locale.UK)) {
			return CommonData.LOCALE_ENGLISH;
		} else if (loc.equals(Locale.CHINA) || loc.equals(Locale.CHINESE) || loc.equals(Locale.SIMPLIFIED_CHINESE) || loc.equals(Locale.TAIWAN)
				|| loc.equals(Locale.TRADITIONAL_CHINESE)) {
			return CommonData.LOCALE_CHINA;
		}
		return CommonData.LOCALE_KOREA;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
	}

}
