package kr.wisestone.happytong.plugin;

import java.io.ByteArrayOutputStream;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.Toast;

public class FacebookLinker implements FacebookLink.LoginListener, FacebookLink.ReqListener {

	public interface OnResultListener {
		public static final int LOGIN_OK = 0x0;
		public static final int LOGIN_FAIL = 0x1;
		public static final int WRITE_OK = 0x2;
		public static final int WRITE_FAIL = 0x3;
		public static final int LOGINED = 0x4;

		public void onFacebookResult(int retCode);
	}

	OnResultListener mListener;

	Activity mActivity;
	FacebookLink mFacebookLink;
	byte[] mCouponBytes;
	String message;

	int mAdType;
	private final static int AD_HAPPYTONG = 0x01;
	private final static int AD_COUPON = 0x02;

	public FacebookLinker(Activity activity, OnResultListener lstener) {
		this(activity, activity.getString(R.string.facebook_ad_message), null, lstener, AD_HAPPYTONG);
	}

	public FacebookLinker(Activity activity, String message, byte[] mCouponBytes, OnResultListener listener) {
		this(activity, message, mCouponBytes, listener, AD_COUPON);
	}

	private FacebookLinker(Activity activity, String message, byte[] mCouponBytes, OnResultListener listener, int type) {
		this.mActivity = activity;
		this.message = message;
		this.mListener = listener;
		this.mCouponBytes = mCouponBytes;
		this.mAdType = type;
		checkLogin();
	}

	private void checkLogin() {
		mFacebookLink = new FacebookLink(mActivity);
		if (mFacebookLink.isLinked()) {
			mListener.onFacebookResult(OnResultListener.LOGINED);
			startPost();
		} else {
			mFacebookLink.setLoginListener(this);
			mFacebookLink.login();
		}
	}

	@Override
	public void loginComplete(boolean isSuccess) {
		if (isSuccess) {
			Logg.d("loginComplete");
			startPost();
			mListener.onFacebookResult(OnResultListener.LOGIN_OK);
		} else {
			Toast.makeText(mActivity, R.string.facebook_login_fail_message, Toast.LENGTH_LONG).show();
			mListener.onFacebookResult(OnResultListener.LOGIN_FAIL);
		}
	}

	@Override
	public void requestComplete(boolean isSuccess) {
		if (isSuccess) {
			mListener.onFacebookResult(OnResultListener.WRITE_OK);
		} else {
			mListener.onFacebookResult(OnResultListener.WRITE_FAIL);
		}
	}

	private void startPost() {
		mFacebookLink.setReqListener(this);
		switch (mAdType) {
		case AD_HAPPYTONG:
			startPostHappytong();
			break;
		case AD_COUPON:
			startPostCoupon();
			break;
		}
	}

	private void startPostCoupon() {
		mFacebookLink.publishCoupon(this.message, mCouponBytes);
	}

	private void startPostHappytong() {
		// mFacebookLink.publishHappytongMessage(this.message, getIconBytes());
		mFacebookLink.publishLink(this.message, getIconBytes());
	}

	private byte[] getIconBytes() {
		Drawable d = mActivity.getResources().getDrawable(R.drawable.ic_launcher);
		Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
		return baos.toByteArray();
	}

}
