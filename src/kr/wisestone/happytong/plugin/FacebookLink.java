package kr.wisestone.happytong.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;

public class FacebookLink {

	private String TAG = "FacebookLink ";

	// 페이스북 앱ID: 464918813541271
	public static final String APP_ID = "187426488076711";

	private static final String[] PERMISSIONS = new String[] { "publish_stream", "photo_upload", "read_stream", "offline_access" };

	private Facebook mFacebook = null;
	private AsyncFacebookRunner mAsyncRunner;

	private String access_token = null;
	private long expire = 0;

	private Activity ctx = null;

	public FacebookLink(Activity ctx) {
		mFacebook = new Facebook(APP_ID);
		mAsyncRunner = new AsyncFacebookRunner(mFacebook);

		this.ctx = ctx;
	}

	/**
	 * 세션을 이용하는 isValid()는 로그인여부와 상관없이 세션이 유효한지만을 판단? 일단 pref값을 이용하여, 로그인여부(연동여부)를 판별
	 * 
	 * @return
	 */
	public boolean isLinked() {
		SharedPreferences spref = ctx.getSharedPreferences("FBOOK", Activity.MODE_PRIVATE);
		access_token = spref.getString("access_token", "");
		expire = spref.getLong("expire", 0);

		if (access_token != null && access_token.equals("") == false && expire != 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isValid() {
		if (access_token != null && access_token.equals("") == false && expire != 0) {
			mFacebook.setAccessToken(access_token);
			mFacebook.setAccessExpires(expire);
		} else {
			mFacebook.setAccessToken(null);
			mFacebook.setAccessExpires(0);
		}
		return mFacebook.isSessionValid();
	}

	public void login() {
		mFacebook.authorize((Activity) ctx, PERMISSIONS, new LoginDialogListener());
	}

	// 20121023.jish.로그아웃추가
	public void logout() {
		// Pref 초기화
		SharedPreferences spref = ctx.getSharedPreferences("FBOOK", Activity.MODE_PRIVATE);
		Editor editor = spref.edit();
		editor.putString("access_token", "");
		editor.putLong("expire", 0);
		editor.commit();

		AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(mFacebook);
		asyncRunner.logout(ctx, new LogoutRequestListener());
	}

	public void publishPhoto(String path) {
		Bundle params = new Bundle();

		byte[] imgData = null;
		File file = new File(path);

		try {
			imgData = getBytesFromFile(file);
		} catch (IOException e1) {
			Logg.e(TAG + "error", e1);
		}

		// Bitmap bi = BitmapFactory.decodeFile(path);
		// ByteArrayOutputStream baos = new ByteArrayOutputStream();
		// bi.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		// imgData = baos.toByteArray();

		params.putString(Facebook.TOKEN, access_token);
		// params.putString("name", "photo");
		params.putString("method", "photos.upload");
		params.putByteArray("picture", imgData);

		mAsyncRunner.request(null, params, "POST", new PublishListener(), null);
	}

	public void publishMessage(String message) {
		publishMessage(message, null);
	}

	public void publishMessage(String message, byte[] imgBytes) {
		Bundle params = new Bundle();
		params.putString(Facebook.TOKEN, access_token);
		params.putString("message", message);
		if (imgBytes != null) {
			// params.putByteArray("facebookPictureData", imgBytes);
			params.putByteArray("picture", imgBytes);
			mAsyncRunner.request("me/photos", params, "POST", new PublishListener(), null);
		} else {
			mAsyncRunner.request("me/feed", params, "POST", new PublishListener(), null);
		}
	}

	public void publishHappytongMessage(String message, byte[] iconBytes) {
		Bundle params = new Bundle();
		params.putString(Facebook.TOKEN, access_token);
		params.putString("message", message);
		params.putByteArray("picture", iconBytes);
		mAsyncRunner.request("me/photos", params, "POST", new PublishListener(), null);
	}

	public void publishCoupon(String message, byte[] imgBytes) {
		Bundle params = new Bundle();
		params.putString(Facebook.TOKEN, access_token);
		params.putString("message", message);
		params.putByteArray("picture", imgBytes);
		mAsyncRunner.request("me/photos", params, "POST", new PublishListener(), null);
	}

	/**
	 * 페이스북 링크 홍보 -- 페이스북 홍보 수정 영역
	 * @param description
	 * @param imgBytes
	 */
	public void publishLink(String description, byte[] imgBytes) {
		Bundle params = new Bundle();
		params.putString(Facebook.TOKEN, access_token);
		// message : 게시글내용부분에 포함
		// params.putString("message", "message");
		params.putString("name", "CuFunU");
		params.putString("link", "http://cufunu.kr");
		// description : 링크영역
		params.putString("description", description);
		params.putString("icon", "http://newserver.wisestone.kr:12000/CuFunU/cufunu_logo_ver2.png");
		params.putString("picture", "http://newserver.wisestone.kr:12000/CuFunU/cufunu_logo_ver2.png");
		mAsyncRunner.request("me/feed", params, "POST", new PublishListener(), null);
	}

	/**
	 * Listener for login dialog completion status
	 */
	private final class LoginDialogListener implements com.facebook.android.Facebook.DialogListener {

		/**
		 * Called when the dialog has completed successfully
		 */
		public void onComplete(Bundle values) {
			// Process onComplete
			Logg.d(TAG + "LoginDialogListener.onComplete()");
			// Dispatch on its own thread

			access_token = values.getString(Facebook.TOKEN);
			String tmp = values.getString(Facebook.EXPIRES);
			try {
				expire = Long.parseLong(tmp);
			} catch (Exception e) {
				Logg.e(TAG + "Error", e);
			}
			Logg.d(TAG + "===================================");
			Logg.d(TAG + "access_token : " + access_token);
			Logg.d(TAG + "expire: " + expire);
			Logg.d(TAG + "===================================");

			SharedPreferences spref = ctx.getSharedPreferences("FBOOK", Activity.MODE_PRIVATE);
			Editor editor = spref.edit();
			editor.putString("access_token", access_token);
			editor.putLong("expire", expire);
			editor.commit();

			// mHandler.post(new Runnable() {
			// public void run() {
			// mText.setText("Facebook login successful. Press Menu...");
			// }
			// });
			if (loginListener != null) {
				loginListener.loginComplete(true);
			}
		}

		/**
         *
         */
		public void onFacebookError(FacebookError error) {
			// Process error
			Logg.d(TAG + "LoginDialogListener.onFacebookError()");
			if (loginListener != null) {
				loginListener.loginComplete(false);
			}
		}

		/**
         *
         */
		public void onError(DialogError error) {
			// Process error message
			Logg.d(TAG + "LoginDialogListener.onError()");
			if (loginListener != null) {
				loginListener.loginComplete(false);
			}
		}

		/**
         *
         */
		public void onCancel() {
			// Process cancel message
			Logg.d(TAG + "LoginDialogListener.onCancel()");
			if (loginListener != null) {
				loginListener.loginComplete(false);
			}
		}
	}

	/**
	 * Listener for logout status message
	 */
	private class LogoutRequestListener implements RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			Logg.d(TAG + "LogoutRequestListener.onComplete()");
			if (logoutListener != null) {
				logoutListener.logoutComplete(true);
			}
		}

		@Override
		public void onIOException(IOException e, Object state) {
			Logg.d(TAG + "LogoutRequestListener.onIOException()");
			if (logoutListener != null) {
				logoutListener.logoutComplete(false);
			}
		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e, Object state) {
			Logg.d(TAG + "LogoutRequestListener.onFileNotFoundException()");
			if (logoutListener != null) {
				logoutListener.logoutComplete(false);
			}
		}

		@Override
		public void onMalformedURLException(MalformedURLException e, Object state) {
			Logg.d(TAG + "LogoutRequestListener.onMalformedURLException()");
			if (logoutListener != null) {
				logoutListener.logoutComplete(false);
			}
		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {
			Logg.d(TAG + "LogoutRequestListener.onFacebookError()");
			if (logoutListener != null) {
				logoutListener.logoutComplete(false);
			}
		}

	}

	// ////////////////////////////////////////////////////////////////////
	// Wall post dialog completion listener
	// ////////////////////////////////////////////////////////////////////

	/**
	 * WallPostDialogListener implements a dialog lister/callback
	 */
	public class WallPostDialogListener implements com.facebook.android.Facebook.DialogListener {

		/**
		 * Called when the dialog has completed successfully
		 */
		public void onComplete(Bundle values) {
			final String postId = values.getString("post_id");
			if (postId != null) {
				Logg.d(TAG + "Dialog Success! post_id=" + postId);
				mAsyncRunner.request(postId, new WallPostRequestListener());
			} else {
				Logg.d(TAG + "No wall post made");
			}
		}

		@Override
		public void onCancel() {
		}

		@Override
		public void onError(DialogError e) {
		}

		@Override
		public void onFacebookError(FacebookError e) {

		}
	}

	// ////////////////////////////////////////////////////////////////////
	// Wall Post request listener
	// ////////////////////////////////////////////////////////////////////

	/**
	 * WallPostRequestListener implements a request lister/callback for "wall post requests"
	 */
	public class WallPostRequestListener implements com.facebook.android.AsyncFacebookRunner.RequestListener {

		/**
		 * Called when the wall post request has completed
		 */

		@Override
		public void onComplete(String response, Object state) {
			Logg.d(TAG + "Got response: " + response);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.facebook.android.AsyncFacebookRunner.RequestListener#onIOException (java.io.IOException, java.lang.Object)
		 */
		@Override
		public void onIOException(IOException e, Object state) {
			// Ignore Facebook errors
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.facebook.android.AsyncFacebookRunner.RequestListener# onFileNotFoundException(java.io.FileNotFoundException, java.lang.Object)
		 */
		@Override
		public void onFileNotFoundException(FileNotFoundException e, Object state) {
			// Ignore Facebook errors
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.facebook.android.AsyncFacebookRunner.RequestListener# onMalformedURLException(java.net.MalformedURLException, java.lang.Object)
		 */
		@Override
		public void onMalformedURLException(MalformedURLException e, Object state) {
			// Ignore Facebook errors
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.facebook.android.AsyncFacebookRunner.RequestListener#onFacebookError (com.facebook.android.FacebookError, java.lang.Object)
		 */
		@Override
		public void onFacebookError(FacebookError e, Object state) {
			// Ignore Facebook errors
		}

	}

	public class PublishListener implements RequestListener {

		public void onComplete(final String response, final Object state) {
			Logg.d(TAG + "Response: " + response.toString());
			if (reqListener != null) {
				if (response.contains("error")) {
					// Response: {"error":{"message":"Error validating access token: The session has been invalidated because the user has changed the password.","type":"OAuthException","code":190,"error_subcode":460}} (at kr.wisestone.happytong.plugin.FacebookLink$PublishListener.onComplete
					reqListener.requestComplete(false);
				} else {
					reqListener.requestComplete(true);
				}
			}
		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {
			Logg.e(TAG + "Error " + state, e);// + response.toString());
			if (reqListener != null) {
				reqListener.requestComplete(false);
			}
		}

		@Override
		public void onIOException(IOException e, Object state) {
			Logg.e(TAG + "Error " + state, e);// + response.toString());
			if (reqListener != null) {
				reqListener.requestComplete(false);
			}
		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e, Object state) {
			Logg.e(TAG + "Error " + state, e);// + response.toString());
			if (reqListener != null) {
				reqListener.requestComplete(false);
			}
		}

		@Override
		public void onMalformedURLException(MalformedURLException e, Object state) {
			Logg.e(TAG + "Error " + state, e);// + response.toString());
			if (reqListener != null) {
				reqListener.requestComplete(false);
			}
		}
	}

	public static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		// Get the size of the file
		long length = file.length();

		return getBytesFromFile(is, length);
	}

	public static byte[] getBytesFromFile(InputStream is, long length) throws IOException {

		if (length > Integer.MAX_VALUE) {
			// File is too large
		}

		byte[] bytes = new byte[(int) length];

		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		if (offset < bytes.length) {
			throw new IOException("Could not completely read file ");
		}

		is.close();
		return bytes;
	}

	// ////////////////////////////////////////////////////////////// 리스너 - 로그인
	LoginListener loginListener;

	public interface LoginListener {
		/**
		 * 로그인 응답
		 * 
		 * @param isSuccess
		 *            (성공, 실패)
		 */
		void loginComplete(boolean isSuccess);
	}

	public void setLoginListener(LoginListener loginListener) {
		this.loginListener = loginListener;
	}

	// ////////////////////////////////////////////////////////////// 리스너 - 로그아웃
	LogoutListener logoutListener;

	public interface LogoutListener {
		/**
		 * 로그아웃 응답
		 * 
		 * @param isSuccess
		 *            (성공, 실패)
		 */
		void logoutComplete(boolean isSuccess);
	}

	public void setLogoutListener(LogoutListener logoutListener) {
		this.logoutListener = logoutListener;
	}

	// ////////////////////////////////////////////////////////////// 리스너 - 요청 (현재 게시물등록)
	ReqListener reqListener;

	public interface ReqListener {
		/**
		 * 요청 응답
		 * 
		 * @param isSuccess
		 *            (성공, 실패)
		 */
		void requestComplete(boolean isSuccess);
	}

	public void setReqListener(ReqListener reqListener) {
		this.reqListener = reqListener;
	}

}
