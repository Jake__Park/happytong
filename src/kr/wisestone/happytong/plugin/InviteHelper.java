package kr.wisestone.happytong.plugin;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * 친구 초대 - SMS, 카카오톡 연동 헬퍼
 */
public class InviteHelper {

	private class DefaultInfo {
		String url;
		String msg;
		String version;
		String appName;
		String notAvailableKakaoMessage;

		public DefaultInfo(String nick, String addMessage, String kakaoErrorMessage) {
		    //[SDJ 130703] URL 변경
			url = "cufunu.kr";
			msg = nick + addMessage + url;
			notAvailableKakaoMessage = kakaoErrorMessage;
			appName = mContext.getString(R.string.app_name);
			version = Utils.getAppVersionName(mContext);

			Logg.d(this);
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("url:").append(url).append(", ");
			sb.append("msg:").append(msg).append(", ");
			sb.append("version:").append(version).append(", ");
			sb.append("appName:").append(appName).append(", ");
			sb.append("notAvailableKakaoMessage").append(notAvailableKakaoMessage).append(", ");
			return sb.toString();
		}
	}

	private static InviteHelper instance;

	public static InviteHelper getInstance(Context context) {
		if (instance == null) {
			instance = new InviteHelper(context);
		}
		return instance;
	}

	private Context mContext;
	private KakaoLink mKakaoLink;

	private InviteHelper(Context context) {
		mContext = context;
		mKakaoLink = KakaoLink.getLink(context);
	}

	/**
	 * 문자 보내기
	 * 
	 * @param context
	 * @param phoneNumbers
	 */
	public void sendSmsMessage(String... phoneNumbers) {

		StringBuilder uriStringBuilder = new StringBuilder("smsto:");
		String separator = "; ";

		// TODO 문자보내기 요청시 번호지정 구분자 - 테스트 후 적용
		// if(android.os.Build.MANUFACTURER.contains("Samsung")) {
		// separator = ", ";
		// }

		for (String s : phoneNumbers) {
			uriStringBuilder.append(s).append(separator);
		}
		Uri uri = Uri.parse(uriStringBuilder.toString());
		Intent it = new Intent(Intent.ACTION_SENDTO, uri);
		it.putExtra(
				"sms_body",
				new DefaultInfo(SharedPrefManager.getInstance(mContext).getPrefProfileNickName(), mContext.getString(R.string.invite_message), mContext
						.getString(R.string.invite_kakaotalk_is_not_exist_message)).msg);
		mContext.startActivity(it);
	}

	/**
	 * 카카오톡 메시지 보내기
	 */
	public void sendKakaoTextMessage() {
		if (!isAvailableKakao()) {
			return;
		}
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(
				Intent.EXTRA_TEXT,
				new DefaultInfo(SharedPrefManager.getInstance(mContext).getPrefProfileNickName(), mContext.getString(R.string.invite_message), mContext
						.getString(R.string.invite_kakaotalk_is_not_exist_message)).msg);
		intent.setPackage("com.kakao.talk");
		mContext.startActivity(intent);
	}

	/**
	 * 카카오톡 일반 링크 포함된 메시지 보내기
	 */
	@Deprecated
	public void sendKakaoLinkMessage() {
		if (!isAvailableKakao()) {
			return;
		}
		DefaultInfo dInfo = new DefaultInfo(SharedPrefManager.getInstance(mContext).getPrefProfileNickName(), mContext.getString(R.string.invite_message),
				mContext.getString(R.string.invite_kakaotalk_is_not_exist_message));
		mKakaoLink.openKakaoLink(mContext, dInfo.url, dInfo.msg, mContext.getPackageName(), dInfo.version, dInfo.appName, "UTF-8");
	}

	/**
	 * 카카오톡 앱실행 가능 메시지 보내기
	 */
	@Deprecated
	public void sendKakaoAppInfoMessage() {
		if (!isAvailableKakao()) {
			return;
		}

		ArrayList<Map<String, String>> metaInfoArray = new ArrayList<Map<String, String>>();

		// If application is support Android platform.
		Map<String, String> metaInfoAndroid = new Hashtable<String, String>(1);
		metaInfoAndroid.put("os", "android");
		metaInfoAndroid.put("devicetype", "phone");
		metaInfoAndroid.put("installurl", "market://details?id=kr.wisestone.happytong");
		metaInfoAndroid.put("executeurl", "happytong://kakao");

		// If application is support ios platform.
		Map<String, String> metaInfoIOS = new Hashtable<String, String>(1);
		metaInfoIOS.put("os", "ios");
		metaInfoIOS.put("devicetype", "phone");
		metaInfoIOS.put("installurl", "your iOS app install url");
		metaInfoIOS.put("executeurl", "kakaoLinkTest://starActivity");

		// add to array
		metaInfoArray.add(metaInfoAndroid);
		metaInfoArray.add(metaInfoIOS);

		DefaultInfo dInfo = new DefaultInfo(SharedPrefManager.getInstance(mContext).getPrefProfileNickName(), mContext.getString(R.string.invite_message),
				mContext.getString(R.string.invite_kakaotalk_is_not_exist_message));
		mKakaoLink.openKakaoAppLink(mContext, dInfo.url, dInfo.msg, mContext.getPackageName(), dInfo.version, dInfo.appName, "UTF-8", metaInfoArray);
	}

	/**
	 * 카톡 가능 여부. false-다이얼로그
	 * 
	 * @return
	 */
	private boolean isAvailableKakao() {
		if (mKakaoLink.isAvailableIntent()) {
			return true;
		}
		DefaultInfo dInfo = new DefaultInfo(SharedPrefManager.getInstance(mContext).getPrefProfileNickName(), mContext.getString(R.string.invite_message),
				mContext.getString(R.string.invite_kakaotalk_is_not_exist_message));
		DialogUtil.getConfirmDialog(mContext, "", dInfo.notAvailableKakaoMessage, null).show();
		return false;
	}
}
