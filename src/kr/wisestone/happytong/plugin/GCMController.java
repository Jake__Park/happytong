package kr.wisestone.happytong.plugin;

import java.util.List;

import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.ui.DummyMainActivity;
import kr.wisestone.happytong.ui.IntroActivity;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import com.google.android.gcm.GCMRegistrar;

public class GCMController extends BroadcastReceiver {

	public static final String SENDER_ID = "170984818925";

	public static final String ACTION_GCM_RECEIVE = "kr.wisestone.happytong.gcmcontroller.receive";
	public static final String SEND_BROADCAST_REGID = "kr.wisestone.happytong.gcm.SEND_BROAD_REGID";
	public static final String EXTRA_REGID = "registration_id";
	public static final String EXTRA_ISERROR = "is_error";

	public static final String TAG = "HappyTong - GCMController ";

	public static final String PACKAGE_NAME = "kr.wisestone.happytong";
	public static final String ACTIVITY_INTRO = "kr.wisestone.happytong.ui.IntroActivity";

	@Override
	public void onReceive(Context context, Intent data) {
		Logg.d("GCMController - receive");

		SharedPrefManager spm = SharedPrefManager.getInstance(context);
		spm.setMoveCBoxType(data.getStringExtra(IntentDefine.EXTRA_GCM_PUSH_TYPE));
		spm.setMoveCBoxSeq(data.getStringExtra(IntentDefine.EXTRA_GCM_COUPON_BOX_SEQ));

		ActiveActivities aa = getActiveActivity(context);
		if (aa.isRunning) {
			// 실행중 - 클리어탑으로 쿠폰함이동
			Intent intent = new Intent(context, DummyMainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			intent.putExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_CBOX);
			context.startActivity(intent);
		} else {
			// 실행중아님 - 인트로 실행
			Intent intro = new Intent(Intent.ACTION_MAIN);
			intro.addCategory(Intent.CATEGORY_LAUNCHER);
			intro.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
			intro.setComponent(new ComponentName(context.getPackageName(), IntroActivity.class.getName()));
			context.startActivity(intro);
		}

	}

	public static void registerGCM(Context context) {

		GCMRegistrar.checkDevice(context);
		GCMRegistrar.checkManifest(context);
		final String regId = GCMRegistrar.getRegistrationId(context);

		Logg.d(TAG + "registerGCM() : " + regId);
		if (regId.equals("")) {
			Logg.d(TAG + "registerGCM() register");
			GCMRegistrar.register(context, SENDER_ID);
		} else {
			Logg.d(TAG + "registerGCM() exist");
			sendBroadRegistrationId(context, regId);
		}
	}

	public static void sendBroadRegistrationId(Context context, String id) {
		sendBroadRegistrationId(context, id, false);
	}

	public static void sendBroadRegistrationId(Context context, String id, boolean isError) {
		Logg.d(TAG + "sendBroadRegistrationId() id : " + id + " / isError : " + isError);
		Intent intent = new Intent(SEND_BROADCAST_REGID);
		intent.putExtra(EXTRA_REGID, id);
		intent.putExtra(EXTRA_ISERROR, isError);
		context.sendBroadcast(intent);
	}

	public static class ActiveActivities {
		public boolean isRunning = false;
		public boolean isForeground = false;
		public boolean isIntroActivity = false;
	}

	/**
	 * 애플리케이션 상태 반환. 잠금화면, 인트로화면, 앱실행 여부, 백그라운드여부
	 * 
	 * @param context
	 * @return
	 */
	public static ActiveActivities getActiveActivity(Context context) {
		ActiveActivities aa = new ActiveActivities();

		ActivityManager activitymanager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> runningtaskinfo = activitymanager.getRunningTasks(100);

		if (runningtaskinfo.get(0).topActivity.getClassName().contains(PACKAGE_NAME)) {
			aa.isForeground = true;
		} else {
			aa.isForeground = false;
		}

		for (RunningTaskInfo info : runningtaskinfo) {
			String base = info.baseActivity.toString();
			String top = info.topActivity.toString();
			Logg.d(TAG + base + " / " + top);

			if (base.contains(ACTIVITY_INTRO)) {
				aa.isRunning = true;
				aa.isIntroActivity = true;
			} else if (base.contains(PACKAGE_NAME)) {
				aa.isRunning = true;
			}

			if (top.contains(ACTIVITY_INTRO)) {
				aa.isRunning = true;
				aa.isIntroActivity = true;
			} else if (top.contains(PACKAGE_NAME)) {
				aa.isRunning = true;
			}
		}

		Logg.d(TAG + "getActiveActivity() // isRunning:" + aa.isRunning + " / isForeground:" + aa.isForeground + " / isIntroActivity:" + aa.isIntroActivity);

		return aa;
	}

}
