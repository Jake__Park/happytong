package kr.wisestone.happytong.ui.cbox;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.CouponProtocol;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.plugin.FacebookLinker;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.cbox.CouponDetailViewFactory.CouponDetailViewHolder;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CouponDetailActivity extends BaseActivity {

	ViewPager mViewPager;
	CouponDetailPagerAdapter mAdapter;

	ArrayList<CouponInfoData2> mList;
	CouponBoxType mCouponBoxType;
	int mListSelectPosition;

	public static final int MSG_FACEBOOK_COMPLETE = 0x500;
	public static final int MSG_FACEBOOK_FAIL = 0x501;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Logg.d("onCreated");

		if (!isRightIntentValues(getIntent())) {
			finish();
			return;
		}
		init();
		setNetworkCallback();
	}

	@SuppressWarnings("unchecked")
	private boolean isRightIntentValues(Intent intent) {
		mList = (ArrayList<CouponInfoData2>) intent.getSerializableExtra(IntentDefine.EXTRA_CBOX_LIST);
		mCouponBoxType = CouponBoxType.getTypeMatchedIntValue(intent.getIntExtra(IntentDefine.EXTRA_CBOX_TYPE, 0));
		mListSelectPosition = intent.getIntExtra(IntentDefine.EXTRA_CBOX_LIST_SELECT_POSITION, 0);
		if (mList == null) {
			return false;
		}
		Logg.d("isRightIntentValues - true - receivedListSize : " + mList.size());
		return true;
	}

	private void init() {
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mAdapter = new CouponDetailPagerAdapter(this, mList, mCouponBoxType, mHandler);

		mViewPager.setAdapter(mAdapter);
		mViewPager.setCurrentItem(mListSelectPosition);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		}
	}

	@Override
	protected int setContentViewId() {
		return R.layout.activity_coupon_detail;
	}

	private void setNetworkCallback() {
		// 사용요청, 사용허가, 사용거절 네트워크 연동 전 호출
		mAdapter.setNetworkReadyCallback(new IWiseCallback() {
			@Override
			public boolean WiseCallback(CallbackParam param) {
				showProgressDialog();
				return false;
			}
		});
		mAdapter.setUsewNetworkCallback(new IWiseCallback() {
			@Override
			public boolean WiseCallback(CallbackParam param) {
				closeProgressDialog();
				Intent data = new Intent();
				data.putExtra(IntentDefine.EXTRA_CBOX_TYPE, mCouponBoxType.ordinal());
				setResult(RESULT_OK, data);
				finish();
				return false;
			}
		}, iFail);
		mAdapter.setPermitNetworkCallback(new IWiseCallback() {
			@Override
			public boolean WiseCallback(CallbackParam param) {
				closeProgressDialog();
				Intent data = new Intent();
				data.putExtra(IntentDefine.EXTRA_CBOX_TYPE, CouponBoxType.USED.ordinal());
				setResult(RESULT_OK, data);
				finish();
				return false;
			}
		}, iFail);
		mAdapter.setRefuseNetworkCallback(new IWiseCallback() {
			@Override
			public boolean WiseCallback(CallbackParam param) {
				closeProgressDialog();
				Intent data = new Intent();
				data.putExtra(IntentDefine.EXTRA_CBOX_TYPE, mCouponBoxType.ordinal());
				setResult(RESULT_OK, data);
				finish();
				return false;
			}
		}, iFail);
	}

	Handler mHandler = new Handler(new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_FACEBOOK_COMPLETE:
				DialogUtil.getConfirmDialog(CouponDetailActivity.this, "", getString(R.string.facebook_boast_complete_message), null).show();
				break;
			case MSG_FACEBOOK_FAIL:
				Toast.makeText(CouponDetailActivity.this, R.string.facebook_post_fail, Toast.LENGTH_LONG).show();
				break;
			}
			return false;
		}
	});

}

class CouponDetailPagerAdapter extends PagerAdapter {

	BaseActivity mActivity;
	List<CouponInfoData2> mList;
	CouponBoxType mCouponBoxType;
	Handler handler;

	LayoutInflater mInflater;
	ImageLoader mImgLoader;

	IWiseCallback ready;
	IWiseCallback useOk, useFail;
	IWiseCallback refuseOk, refuseFail;
	IWiseCallback permitOk, permitFail;

	String mMyId;

	public CouponDetailPagerAdapter(BaseActivity activity, List<CouponInfoData2> mList, CouponBoxType type, Handler handler) {
		super();
		this.mActivity = activity;
		this.mList = mList;
		this.mCouponBoxType = type;
		this.handler = handler;

		this.mInflater = LayoutInflater.from(activity);
		this.mImgLoader = new ImageLoader(activity);

		this.mMyId = SharedPrefManager.getLoginEmail(activity);
	}

	public void setNetworkReadyCallback(IWiseCallback ready) {
		this.ready = ready;
	}

	public void setUsewNetworkCallback(IWiseCallback ok, IWiseCallback fail) {
		useOk = ok;
		useFail = fail;
	}

	public void setPermitNetworkCallback(IWiseCallback ok, IWiseCallback fail) {
		permitOk = ok;
		permitFail = fail;
	}

	public void setRefuseNetworkCallback(IWiseCallback ok, IWiseCallback fail) {
		refuseOk = ok;
		refuseFail = fail;
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public boolean isViewFromObject(View v, Object o) {
		return v == o;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View v = CouponDetailViewFactory.create(mInflater);
		CouponDetailViewHolder holder = (CouponDetailViewFactory.CouponDetailViewHolder) v.getTag();

		CouponInfoData2 data = mList.get(position);
		// 공통 작업
		holder.valid.setText(Utils.getDateFormatStringFromServerDateFormat(data.m_validPeriod, mActivity.getString(R.string.until_yyyy_mm_dd), Locale.KOREA));
		holder.datetime.setText(data.mDateLabel + "\n" + data.mTime);
		holderTitleChange(holder, data.m_couponType, data.m_couponText);
		holder.positions.setText((position + 1) + " / " + mList.size());

		this.mImgLoader.DisplayImage(data.m_couponImageURL, holder.coupon);

		// 쿠폰함 타입에 맞게 처리
		handleType(holder, data);

		((ViewPager) container).addView(v);
		return v;
	}

	class FacebookOnClickListener implements View.OnClickListener {
		View layout;
		CouponInfoData2 data;

		private FacebookOnClickListener(CouponInfoData2 data, View layout) {
			super();
			this.layout = layout;
			this.data = data;
		}

		private String getFacebookMessage() {
			StringBuilder sb = new StringBuilder();
			sb.append(Utils.getDateFormatStringFromServerDateFormat(data.getCreatedCouponDate(), mActivity.getString(R.string.yyyy_mm_dd), Locale.KOREA));
			sb.append("\n");
			sb.append("\"");
			sb.append(data.getSenderNickname());
			sb.append(mActivity.getString(R.string.from));
			return sb.toString();
		}

		@Override
		public void onClick(View v) {
			DialogUtil.getConfirmCancelDialog(mActivity, "", mActivity.getString(R.string.boast_coupon_facebook), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (which == DialogInterface.BUTTON_POSITIVE) {
						layout.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
						layout.buildDrawingCache();
						Bitmap bitmap = layout.getDrawingCache();
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
						byte[] couponBytes = baos.toByteArray();
						// saveImageTemporary(couponBytes);

						new FacebookLinker(mActivity, getFacebookMessage(), couponBytes, new FacebookLinker.OnResultListener() {
							@Override
							public void onFacebookResult(int retCode) {
								switch (retCode) {
								case FacebookLinker.OnResultListener.LOGIN_FAIL:
									break;
								case FacebookLinker.OnResultListener.LOGIN_OK:
									mActivity.showProgressDialog();
									break;
								case FacebookLinker.OnResultListener.WRITE_FAIL:
									mActivity.closeProgressDialog();
									handler.sendEmptyMessage(CouponDetailActivity.MSG_FACEBOOK_FAIL);
									break;
								case FacebookLinker.OnResultListener.WRITE_OK:
									mActivity.closeProgressDialog();
									handler.sendEmptyMessage(CouponDetailActivity.MSG_FACEBOOK_COMPLETE);
									break;
								case FacebookLinker.OnResultListener.LOGINED:
									mActivity.showProgressDialog();
									break;
								}
							}
						});
					}
				}
			}).show();
		}

		/**
		 * 화질 확인용 이미지 임시 저장
		 */
		protected void saveImageTemporary(byte[] couponBytes) {
			try {
				FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/temp.jpg"));
				fos.write(couponBytes);
				fos.close();
				Toast.makeText(mActivity, "File saved", Toast.LENGTH_LONG).show();
			} catch (IOException e) {
				Toast.makeText(mActivity, "File save fail", Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

	private void handleType(CouponDetailViewHolder holder, CouponInfoData2 data) {
		Logg.d("CouponDetail - resres " + data.m_flag + " / " + data.m_requestState + " / " + data.m_endCoupon);
		switch (mCouponBoxType) {
		case RECEIVED:
			if (!data.m_senderPhotoURL.endsWith("none")) {
				mImgLoader.DisplayImage(data.m_senderPhotoURL, holder.photo);
			}
			// 쿠폰을 사용한 경우라면 버튼 disalbe
			holder.name.setText(data.mSenderNameNick);
			holder.allow.setImageResource(R.drawable.selector_sub3_01_use_btn);
			if (data.m_requestState.equals("1")) {
				holder.allow.setEnabled(false);
			} else {
				holder.allow.setOnClickListener(OnClickListenerFactory.create(OnClickListenerFactory.USE, mActivity, data, ready, useOk, useFail));
			}
			holder.refuse.setVisibility(View.GONE);
			holder.facebook.setVisibility(View.VISIBLE);
			holder.facebook.setOnClickListener(new FacebookOnClickListener(data, holder.couponLayout));
			break;
		case SENT:
			if (!data.m_receiverPhotoURL.endsWith("none")) {
				mImgLoader.DisplayImage(data.m_receiverPhotoURL, holder.photo);
			}
			// 쿠폰을 사용전이라면 버튼영역 gone
			holder.name.setText(data.mReceiverNameNick);
			if (data.m_requestState.equals("1")) {
				holder.allow.setImageResource(R.drawable.selector_sub3_02_accept_btn);
				holder.allow.setOnClickListener(OnClickListenerFactory.create(OnClickListenerFactory.ALLOW, mActivity, data, ready, permitOk, permitFail));
				holder.refuse.setImageResource(R.drawable.selector_sub3_02_reject_btn);
				holder.refuse.setOnClickListener(OnClickListenerFactory.create(OnClickListenerFactory.REFUSE, mActivity, data, ready, refuseOk, refuseFail));
			} else {
				holder.allow.setVisibility(View.INVISIBLE);
				holder.refuse.setVisibility(View.INVISIBLE);
			}
			holder.facebook.setVisibility(View.GONE);
			break;
		case USED:
			if (this.mMyId.equals(data.m_senderID)) {
				holder.name.setText(data.mReceiverNameNick);
				if (!data.m_receiverPhotoURL.endsWith("none")) {
					mImgLoader.DisplayImage(data.m_receiverPhotoURL, holder.photo);
				}
			} else {
				holder.name.setText(data.mSenderNameNick);
				if (!data.m_senderPhotoURL.endsWith("none")) {
					mImgLoader.DisplayImage(data.m_senderPhotoURL, holder.photo);
				}
			}
			holder.allow.setVisibility(View.INVISIBLE);
			holder.refuse.setVisibility(View.INVISIBLE);
			holder.facebook.setVisibility(View.GONE);
			
			// [20130620 hyoungjoon.lee] OWL No109 : 사용 쿠폰 상세 화면에 만료, 사용 아이콘 추가
			holder.state.setVisibility(View.VISIBLE);
			int stateImgId = 0;
			
			String state = data.getRequestState();
			if(state.equals("0") == true){
			    stateImgId = R.drawable.sub3_01_period;
			}else{
			    stateImgId = R.drawable.sub3_01_complete;
			}
			holder.state.setBackgroundResource(stateImgId);
			// [20130620 hyoungjoon.lee] OWL No109 : 사용 쿠폰 상세 화면에 만료, 사용 아이콘 추가

			// 이름영역 길경우, state영역 오버. 방지. 20130712.jish
			RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.name_area.getLayoutParams();
			lp.addRule(RelativeLayout.LEFT_OF, R.id.request_state);
			holder.name_area.setLayoutParams(lp);
			
			break;
		}
	}

	/**
	 * 쿠폰 타입에 맞는 텍스트뷰에만 메시지 표시하도록 함
	 * 
	 * @param holder
	 * @param type
	 * @param title
	 */
	private void holderTitleChange(CouponDetailViewHolder holder, String type, String title) {

		switch (CouponType.getCouponType(type)) {
		case NA:
		case Center:
			holder.title0.setText(title);
			holder.title1.setText("");
			holder.title2.setText("");
			holder.title3.setText("");
			break;
		case Up:
			holder.title0.setText("");
			holder.title1.setText(title);
			holder.title2.setText("");
			holder.title3.setText("");
			break;
		case Left:
			holder.title0.setText("");
			holder.title1.setText("");
			holder.title2.setText(title);
			holder.title3.setText("");
			break;
		case Right:
			holder.title0.setText("");
			holder.title1.setText("");
			holder.title2.setText("");
			holder.title3.setText(title);
			break;
		}
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((View) object);
		// TODO recycle
	}

}

class CouponDetailViewFactory {

	public static View create(LayoutInflater inflater) {
		View v = inflater.inflate(R.layout.coupon_detail_viewpager_item, null);
		setViewHolder(v);
		return v;
	}

	private static CouponDetailViewHolder setViewHolder(View v) {
		CouponDetailViewHolder holder = new CouponDetailViewHolder();
		holder.photo = (ImageView) v.findViewById(R.id.photo);
		holder.name = (TextView) v.findViewById(R.id.name);
		holder.datetime = (TextView) v.findViewById(R.id.datetime);
		holder.facebook = v.findViewById(R.id.facebook);
		holder.couponLayout = v.findViewById(R.id.coupon_layout);
		holder.coupon = (ImageView) v.findViewById(R.id.coupon_image);
		holder.title0 = (TextView) v.findViewById(R.id.title0);
		holder.title1 = (TextView) v.findViewById(R.id.title1);
		holder.title2 = (TextView) v.findViewById(R.id.title2);
		holder.title3 = (TextView) v.findViewById(R.id.title3);
		holder.positions = (TextView) v.findViewById(R.id.positions);
		holder.valid = (TextView) v.findViewById(R.id.coupon_valid);
		holder.allow = (ImageView) v.findViewById(R.id.allow);
		holder.refuse = (ImageView) v.findViewById(R.id.refuse);
		holder.state = (ImageView) v.findViewById(R.id.request_state); //[20130620 hyoungjoon.lee] OWL No109 사용 쿠폰 상세 화면에 만료, 사용 아이콘 추가
		holder.name_area = v.findViewById(R.id.name_area);
		v.setTag(holder);
		return holder;
	}

	static class CouponDetailViewHolder {
		ImageView photo;
		TextView name;
		TextView datetime;
		View facebook;
		View couponLayout;
		ImageView coupon;
		TextView title0;
		TextView title1;
		TextView title2;
		TextView title3;
		TextView positions;
		TextView valid;
		ImageView allow;
		ImageView refuse;
		ImageView state; // [20130620 hyoungjoon.lee] OWL No109 사용 쿠폰 상세 화면에 만료, 사용 아이콘 추가
		View name_area;
	}

}

class OnClickListenerFactory {
	public static final int USE = 0;
	public static final int ALLOW = 1;
	public static final int REFUSE = 2;

	public static View.OnClickListener create(int type, Context context, CouponInfoData2 data, IWiseCallback ready, IWiseCallback ok, IWiseCallback fail) {
		switch (type) {
		case USE:
			return new UseClickListener(context, data, ready, ok, fail);
		case ALLOW:
			return new AllowClickListener(context, data, ready, ok, fail);
		case REFUSE:
			return new RefuseClickListener(context, data, ready, ok, fail);
		}
		return null;
	}

}

class UseClickListener implements View.OnClickListener {
	Context mContext;
	CouponInfoData2 data;
	IWiseCallback ready, ok, fail;

	public UseClickListener(Context context, CouponInfoData2 data, IWiseCallback ready, IWiseCallback ok, IWiseCallback fail) {
		super();
		mContext = context;
		this.data = data;
		this.ready = ready;
		this.ok = ok;
		this.fail = fail;
	}

	@Override
	public void onClick(View v) {
		Logg.d("UseClickListener " + data.m_couponText + " / " + data.m_receiverNickname + " / " + data.m_senderNickname);

		CouponProtocol cp = new CouponProtocol(ok, fail);
		cp.couponUsingRequest(CallBackParamWrapper.CODE_COUPON_USING_REQUEST, data.m_boxSEQ, SharedPrefManager.getLoginEmail(mContext));
		ready.WiseCallback(null);
	}
}

class AllowClickListener implements View.OnClickListener {
	Context mContext;
	CouponInfoData2 data;
	IWiseCallback ready, ok, fail;

	public AllowClickListener(Context context, CouponInfoData2 data, IWiseCallback ready, IWiseCallback ok, IWiseCallback fail) {
		super();
		mContext = context;
		this.data = data;
		this.ready = ready;
		this.ok = ok;
		this.fail = fail;
	}

	@Override
	public void onClick(View v) {
		Logg.d("AllowClickListener " + data.m_couponText + " / " + data.m_receiverNickname + " / " + data.m_senderNickname);

		DialogUtil.getConfirmCancelDialog(mContext, "", mContext.getString(R.string.agree_using_coupon), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which != DialogInterface.BUTTON_POSITIVE) {
					return;
				}
				CouponProtocol cp = new CouponProtocol(ok, fail);
				cp.couponPermit(CallBackParamWrapper.CODE_COUPON_PERMIT, data.m_boxSEQ, SharedPrefManager.getLoginEmail(mContext), "1");
				ready.WiseCallback(null);
			}
		}).show();
	}
}

class RefuseClickListener implements View.OnClickListener {
	Context mContext;
	CouponInfoData2 data;
	IWiseCallback ready, ok, fail;

	public RefuseClickListener(Context context, CouponInfoData2 data, IWiseCallback ready, IWiseCallback ok, IWiseCallback fail) {
		super();
		mContext = context;
		this.data = data;
		this.ready = ready;
		this.ok = ok;
		this.fail = fail;
	}

	@Override
	public void onClick(View v) {
		Logg.d("RefuseClickListener " + data.m_couponText + " / " + data.m_receiverNickname + " / " + data.m_senderNickname);

		DialogUtil.getConfirmCancelDialog(mContext, "", mContext.getString(R.string.refuse_using_coupon), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which != DialogInterface.BUTTON_POSITIVE) {
					return;
				}
				CouponProtocol cp = new CouponProtocol(ok, fail);
				cp.couponPermit(CallBackParamWrapper.CODE_COUPON_PERMIT, data.m_boxSEQ, SharedPrefManager.getLoginEmail(mContext), "0");
				ready.WiseCallback(null);
			}
		}).show();
	}
}