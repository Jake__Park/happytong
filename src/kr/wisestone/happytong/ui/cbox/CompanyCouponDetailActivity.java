package kr.wisestone.happytong.ui.cbox;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.CouponProtocol;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.cbox.CompanyCouponDetailActivity.CouponDetailViewFactory.CouponDetailViewHolder;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CompanyCouponDetailActivity extends BaseActivity {

	Context							mContext;

	ViewPager						mViewPager;
	CompanyCouponDetailPagerAdapter	mAdapter;

	ArrayList<CouponInfoData2>		mList;
	int								mListSelectPosition;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = this;

		Logg.d("onCreated");

		if (!isRightIntentValues(getIntent())) {
			finish();
			return;
		}
		init();
	}

	@SuppressWarnings("unchecked")
	private boolean isRightIntentValues(Intent intent) {
		mList = (ArrayList<CouponInfoData2>) intent.getSerializableExtra(IntentDefine.EXTRA_CBOX_LIST);
		mListSelectPosition = intent.getIntExtra(IntentDefine.EXTRA_CBOX_LIST_SELECT_POSITION, 0);
		if (mList == null) {
			return false;
		}
		Logg.d("isRightIntentValues - true - receivedListSize : " + mList.size());
		return true;
	}

	private void init() {
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mAdapter = new CompanyCouponDetailPagerAdapter(mList);

		mViewPager.setAdapter(mAdapter);
		mViewPager.setCurrentItem(mListSelectPosition);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		}
	}

	@Override
	protected int setContentViewId() {
		return R.layout.activity_coupon_detail;
	}

	private void onUseClicked(final CouponInfoData2 data) {
		DialogUtil.getConfirmCancelDialog(mContext, "", mContext.getString(R.string.agree_using_coupon), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which != DialogInterface.BUTTON_POSITIVE) {
					return;
				}
				CouponProtocol cp = new CouponProtocol(networkUseOk, iFail);
				cp.companiesCouponUse(CallBackParamWrapper.CODE_COUPON_PERMIT, data.m_boxSEQ, SharedPrefManager.getLoginEmail(mContext));
				showProgressDialog();
			}
		}).show();
	}

	IWiseCallback	networkUseOk	= new IWiseCallback() {
										@Override
										public boolean WiseCallback(CallbackParam param) {
											closeProgressDialog();
											Intent data = new Intent();
											data.putExtra(IntentDefine.EXTRA_CBOX_TYPE, CouponBoxType.COMPANY.ordinal());
											setResult(RESULT_OK, data);
											finish();
											return false;
										}
									};

	class CompanyCouponDetailPagerAdapter extends PagerAdapter {

		List<CouponInfoData2>	mList;

		LayoutInflater			mInflater;
		ImageLoader				mImgLoader;
		CouponDetailViewFactory	mCouponDetailViewFactory;

		public CompanyCouponDetailPagerAdapter(List<CouponInfoData2> mList) {
			super();
			this.mList = mList;

			this.mInflater = LayoutInflater.from(mContext);
			this.mImgLoader = new ImageLoader(mContext);

			mCouponDetailViewFactory = new CouponDetailViewFactory();

		}

		@Override
		public int getCount() {
			return mList.size();
		}

		@Override
		public boolean isViewFromObject(View v, Object o) {
			return v == o;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			View v = mCouponDetailViewFactory.create(mInflater);
			CouponDetailViewHolder holder = (CouponDetailViewFactory.CouponDetailViewHolder) v.getTag();

			final CouponInfoData2 data = mList.get(position);

			if (!data.mCompanyPhotoURL.endsWith("none")) {
				mImgLoader.DisplayImage(data.mCompanyPhotoURL, holder.photo);
			} else {
				holder.photo.setImageResource(R.drawable.sub3_01_photo);
			}
			holderTitleChange(holder, data.m_couponType, data.m_couponText);

			holder.name.setText(data.mCompanyName);
			holder.datetime.setText(data.mCompanyAddress + "\n" + data.mCompanyPhoneNumber);
			holder.valid.setText(Utils.getDateFormatStringFromServerDateFormat(data.m_validPeriod, mContext.getString(R.string.until_yyyy_mm_dd), Locale.KOREA));
			holder.positions.setText((position + 1) + " / " + mList.size());

			this.mImgLoader.DisplayImage(data.m_couponImageURL, holder.coupon);

			if ("1".equals(data.m_flag)) {
				// 쿠폰 보내진 상태 - 사용하지 않았음
				if ("0".equals(data.m_endCoupon)) {
					// 사용가능 상태
					holder.use.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							onUseClicked(data);
						}
					});
					holder.state.setVisibility(View.GONE);
					holder.state.setImageResource(0);
				} else {
					// 쿠폰만료 - 사용불가
					holder.use.setEnabled(false);
					holder.state.setVisibility(View.VISIBLE);
					holder.state.setImageResource(R.drawable.sub3_01_period);
					// holder.use.setVisibility(View.GONE);
				}
			} else {
				// 쿠폰 사용 상태
				holder.use.setEnabled(false);
				holder.state.setVisibility(View.VISIBLE);
				holder.state.setImageResource(R.drawable.sub3_01_complete);
				// holder.use.setVisibility(View.GONE);
			}

			((ViewPager) container).addView(v);
			return v;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
			// TODO recycle
		}

		/**
		 * 쿠폰 타입에 맞는 텍스트뷰에만 메시지 표시하도록 함
		 * 
		 * @param holder
		 * @param type
		 * @param title
		 */
		private void holderTitleChange(CouponDetailViewHolder holder, String type, String title) {

			switch (CouponType.getCouponType(type)) {
			case NA:
			case Center:
				holder.title0.setText(title);
				holder.title1.setText("");
				holder.title2.setText("");
				holder.title3.setText("");
				break;
			case Up:
				holder.title0.setText("");
				holder.title1.setText(title);
				holder.title2.setText("");
				holder.title3.setText("");
				break;
			case Left:
				holder.title0.setText("");
				holder.title1.setText("");
				holder.title2.setText(title);
				holder.title3.setText("");
				break;
			case Right:
				holder.title0.setText("");
				holder.title1.setText("");
				holder.title2.setText("");
				holder.title3.setText(title);
				break;
			}
		}

	}

	class CouponDetailViewFactory {

		View create(LayoutInflater inflater) {
			View v = inflater.inflate(R.layout.coupon_detail_viewpager_item_company, null);
			setViewHolder(v);
			return v;
		}

		CouponDetailViewHolder setViewHolder(View v) {
			CouponDetailViewHolder holder = new CouponDetailViewHolder();
			holder.photo = (ImageView) v.findViewById(R.id.photo);
			holder.name = (TextView) v.findViewById(R.id.name);
			holder.datetime = (TextView) v.findViewById(R.id.datetime);
			holder.couponLayout = v.findViewById(R.id.coupon_layout);
			holder.coupon = (ImageView) v.findViewById(R.id.coupon_image);
			holder.title0 = (TextView) v.findViewById(R.id.title0);
			holder.title1 = (TextView) v.findViewById(R.id.title1);
			holder.title2 = (TextView) v.findViewById(R.id.title2);
			holder.title3 = (TextView) v.findViewById(R.id.title3);
			holder.positions = (TextView) v.findViewById(R.id.positions);
			holder.valid = (TextView) v.findViewById(R.id.coupon_valid);
			holder.state = (ImageView) v.findViewById(R.id.request_state); // [20130620 hyoungjoon.lee] OWL No109 사용 쿠폰 상세 화면에 만료, 사용 아이콘 추가
			holder.use = v.findViewById(R.id.use);
			v.setTag(holder);
			return holder;
		}

		class CouponDetailViewHolder {
			ImageView	photo;
			TextView	name;
			TextView	datetime;
			View		couponLayout;
			ImageView	coupon;
			TextView	title0;
			TextView	title1;
			TextView	title2;
			TextView	title3;
			TextView	positions;
			TextView	valid;
			ImageView	state;			// [20130620 hyoungjoon.lee] OWL No109 사용 쿠폰 상세 화면에 만료, 사용 아이콘 추가
			View		use;
		}

	}

}
