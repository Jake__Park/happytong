package kr.wisestone.happytong.ui.cbox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.CouponInfoData;
import kr.wisestone.happytong.data.dataset.PurchaseInfoData;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.CouponProtocol;
import kr.wisestone.happytong.data.protocol.PurchaseProtocol;
import kr.wisestone.happytong.data.protocol.UserProtocol;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.plugin.FacebookLinker;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.MenuInfoView;
import kr.wisestone.happytong.ui.widget.SubTitleView;
import kr.wisestone.happytong.ui.widget.SubTitleView.SubTitleViewListener;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CouponBoxActivity extends BaseActivity {

	protected static final int	MSG_FACEBOOK_COMPLETE	= 0x24;
	protected static final int	MSG_FACEBOOK_FAIL		= 0x25;

	Context						mContext;

	TitleInfoView				mTitleInfoView;
	SubTitleView				mSubTitleView;

	TextView					mVLeftReceiveCBox;
	TextView					mVLeftSendCBox;
	TextView					mVLeftUseCBox;
	TextView					mVLeftCompanyCBox;
	TextView[]					mVLeftBoxes;
	ColorStateList				mColorStateListForLeftMenus;

	View						mVLeftFacebook;

	View						mHelpLayout;

	ArrayList<CouponInfoData2>	mReceivedCouponList;
	ArrayList<CouponInfoData2>	mSentCouponList;
	ArrayList<CouponInfoData2>	mUsedCouponList;
	ArrayList<CouponInfoData2>	mCompanyCouponList;

	Map<String, String>			mFriendMap;

	View						mReceivedLayout;
	View						mSentLayout;
	View						mUsedLayout;
	View						mCompanyLayout;

	ListView					mReceivedCouponListView;
	ListView					mSentCouponListView;
	ListView					mUsedCouponListView;
	ListView					mCompanyCouponListView;

	CouponListAdapter			mReceivedCouponListAdapter;
	CouponListAdapter			mSentCouponListAdapter;
	UsedCouponListAdapter		mUsedCouponListAdapter;
	CompanyCouponBoxListAdapter	mCompanyCouponListAdapter;

	private String				ID;

	// private enum EditModeState {
	// NORMAL, EDIT
	// }
	//
	// private EditModeState mUsedCouponBoxState = EditModeState.NORMAL;
	// private EditModeState mCompanyCouponBoxState = EditModeState.NORMAL;

	private enum CouponBoxState {
		RECEIVE, SENT, USED, USED_EDIT, COMPANY, COMPANY_EDIT
	}

	private CouponBoxState	mCouponBoxState	= CouponBoxState.RECEIVE;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		decideVisibleHelpLayout();

		mReceivedCouponList = new ArrayList<CouponInfoData2>();
		mSentCouponList = new ArrayList<CouponInfoData2>();
		mUsedCouponList = new ArrayList<CouponInfoData2>();
		mCompanyCouponList = new ArrayList<CouponInfoData2>();
		mFriendMap = getFriendIdNameMap();

		ID = SharedPrefManager.getLoginEmail(this);

		new MenuInfoView(this, (View) findViewById(R.id.menu_tab), CommonData.ENTRY_PONINT_CBOX);
		initTitleView();
		initSubTitleView();
		init();
		initColorStateListForLeftMenus();

		setListView();

		changeCouponBoxList(CouponBoxType.getTypeMatchedIntValue(getCouponBoxTypeFromPref()));
		getServerData();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, R.string.update).setIcon(android.R.drawable.ic_popup_sync);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Logg.d(item.getItemId());
		getServerData();
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 쿠폰함 안내 뷰 보일지 말지 결정
	 */
	private void decideVisibleHelpLayout() {
		mHelpLayout = findViewById(R.id.couponbox_help);

		if (SharedPrefManager.getInstance(this).getShownCouponBoxHelp() > 0) {
			mHelpLayout.setVisibility(View.GONE);
			return;
		}

		findViewById(R.id.couponbox_help_img).setOnClickListener(mHelpClickListener);
		mHelpLayout.setVisibility(View.VISIBLE);
		mHelpLayout.setOnClickListener(mHelpClickListener);
	}

	View.OnClickListener	mHelpClickListener	= new View.OnClickListener() {
													@Override
													public void onClick(View v) {
														mHelpLayout.setVisibility(View.GONE);
														SharedPrefManager.getInstance(CouponBoxActivity.this).setShownCouponBoxHelp(1);
													}
												};

	/**
	 * Preference에 저장된 쿠폰박스타입 문자열을 정수형으로 반환
	 * 
	 * @return
	 */
	private int getCouponBoxTypeFromPref() {
		String type = SharedPrefManager.getInstance(this).getMoveCBoxType();

		if (TextUtils.isEmpty(type)) {
			return 0;
		}

		int iType = 0;
		try {
			iType = Integer.parseInt(type);
		} catch (NumberFormatException e) {
			iType = 0;
		}
		return iType;
	}

	private void initSubTitleView() {
		mSubTitleView = (SubTitleView) findViewById(R.id.subtitleview);
		mSubTitleView.setListener(new SubTitleViewListener() {
			@Override
			public void onEggClickListener() {
			}
		});
		setSubTitleOwlEgg();
	}

	private void setSubTitleOwlEgg() {
		mSubTitleView.setFreeOwlCount(SharedPrefManager.getInstance(this).getFreeOwlCount());
		mSubTitleView.setPayOwlCount(String.valueOf(SharedPrefManager.getInstance(this).getPurchasedOwlCount()));
		mSubTitleView.setEggCount(String.valueOf(SharedPrefManager.getInstance(this).getGoldEggCount()));
	}

	/**
	 * 소스상에서 셀렉터 주기
	 */
	private void initColorStateListForLeftMenus() {
		try {
			mColorStateListForLeftMenus = ColorStateList.createFromXml(getResources(), getResources().getXml(R.color.selector_m3_leftmenu_textcolor));
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 전체 데이터 요청 - 순차적으로 진행하도록 처리
	 */
	private void getServerData() {
		showProgressDialog();

		new CouponProtocol(iOkCBoxReceived, iFail).couponReceived(CallBackParamWrapper.CODE_COUPON_RECEIVED, ID);
	}

	/**
	 * 받은 쿠폰 콜백 - 완료 후 - 보낸 쿠폰 데이터 요청
	 */
	final IWiseCallback	iOkCBoxReceived	= new IWiseCallback() {
											@SuppressWarnings("unchecked")
											@Override
											public boolean WiseCallback(CallbackParam param) {
												new CouponProtocol(iOkCBoxSent, iFail).couponSent(CallBackParamWrapper.CODE_COUPON_SENT, ID);
												Logg.d("iOkCBoxReceived");
												if (param.param1 != null) {
													mReceivedCouponList = convertDataListToData2List((ArrayList<CouponInfoData>) param.param1);
												} else {
													mReceivedCouponList.clear();
												}
												return false;
											}
										};

	/**
	 * 보낸 쿠폰 콜백 - 완료 후 - 사용 쿠폰 데이터 요청
	 */
	final IWiseCallback	iOkCBoxSent		= new IWiseCallback() {
											@SuppressWarnings("unchecked")
											@Override
											public boolean WiseCallback(CallbackParam param) {
												new CouponProtocol(iOkCBoxUsed, iFail).couponUsedExpired(CallBackParamWrapper.CODE_COUPON_USED_EXPIRED, ID);
												Logg.d("iOkCBoxSent");
												if (param.param1 != null) {
													mSentCouponList = convertDataListToData2List((ArrayList<CouponInfoData>) param.param1);
												} else {
													mSentCouponList.clear();
												}
												return false;
											}
										};

	/**
	 * 사용 쿠폰 콜백 - 완료 후 - 업체 쿠폰 데이터 요청
	 */
	final IWiseCallback	iOkCBoxUsed		= new IWiseCallback() {
											@SuppressWarnings("unchecked")
											@Override
											public boolean WiseCallback(CallbackParam param) {
												new CouponProtocol(iOkCBoxCompany, iFail).couponCompaniesCouponBox(CallBackParamWrapper.CODE_COUPON_COMPANY, ID);
												Logg.d("iOkCBoxUsed");
												if (param.param1 != null) {
													mUsedCouponList = convertDataListToData2List((ArrayList<CouponInfoData>) param.param1);
												} else {
													mUsedCouponList.clear();
												}
												return false;
											}
										};
	/**
	 * 사용 쿠폰 콜백 - 완료 후 - 다이얼로그 dismiss, 리스트뷰표시(좌측탭메뉴 카운트 갱신)
	 */
	final IWiseCallback	iOkCBoxCompany	= new IWiseCallback() {
											@SuppressWarnings("unchecked")
											@Override
											public boolean WiseCallback(CallbackParam param) {
												Logg.d("iOkCBoxCompany");
												if (param.param1 != null) {
													mCompanyCouponList = convertData2ListToData2List((ArrayList<CouponInfoData2>) param.param1);
													// mCompanyCouponList = (ArrayList<CouponInfoData2>) param.param1;
												} else {
													mCompanyCouponList.clear();
												}
												getServerDataComplete();
												return false;
											}
										};

	/**
	 * 리스트 요청 완료시 호출
	 */
	private void getServerDataComplete() {
		closeProgressDialog();
		setListView();
		updateBoxCount();

		changeCouponBoxList(CouponBoxType.getTypeMatchedIntValue(getCouponBoxTypeFromPref()));
		moveDetailonPush();
	}

	/**
	 * 쿠폰 상세 페이지 이동 처리 - 푸시에서 넘어온 경우
	 */
	private void moveDetailonPush() {

		String boxSeq = SharedPrefManager.getInstance(this).getMoveCBoxSeq();

		if (TextUtils.isEmpty(boxSeq)) {
			// 쿠폰 아이디가 없을 경우에는, 잘못 저장된 것이므로 Preference에 저장되어 있는 값 초기화 시킴
			SharedPrefManager.getInstance(this).setMoveCBoxSeq("");
			SharedPrefManager.getInstance(this).setMoveCBoxType("");
			return;
		}

		// 2013.06.19.jish
		// 받은쿠폰이거나 보낸쿠폰이거나 유효기간이 지나서 받은 쿠폰임에도 완료쿠폰, 보낸 쿠폰임에도 완료쿠폰으로 갈 수 있음
		// 위와 같은 상황을 처리하기 위하여 해당 쿠폰함에서 검사를 진행한후, 완료쿠폰함으로 보내는 동작을 하도록 한다.
		switch (getCouponBoxTypeFromPref()) {
		case 0:
			startCouponDetailActivityDecideItem(mReceivedCouponList, CouponBoxType.RECEIVED, boxSeq);
			break;
		case 1:
			startCouponDetailActivityDecideItem(mSentCouponList, CouponBoxType.SENT, boxSeq);
			break;
		case 2:
			startCouponDetailActivityDecideItem(mUsedCouponList, CouponBoxType.USED, boxSeq);
			break;
		case 3:
			startCouponDetailActivityDecideItem(mReceivedCouponList, CouponBoxType.RECEIVED, boxSeq);
			break;
		case 4:
			startCouponDetailActivityDecideItem(mCompanyCouponList, CouponBoxType.COMPANY, boxSeq);
			break;
		}

		SharedPrefManager.getInstance(this).setMoveCBoxSeq("");
		SharedPrefManager.getInstance(this).setMoveCBoxType("");
	}

	private void startCouponDetailActivityDecideItem(ArrayList<CouponInfoData2> couponList, CouponBoxType couponBoxType, String boxSeq) {
		int itemPosition = getCouponBoxSeqMatchListPosition(couponList, boxSeq);

		if (itemPosition < 0) {
			itemPosition = getCouponBoxSeqMatchListPosition(mUsedCouponList, boxSeq);
			if (itemPosition < 0) {
				itemPosition = 0;
			}
			changeCouponBoxList(CouponBoxType.USED);
			startCouponDetailActivity(mUsedCouponList, CouponBoxType.USED, itemPosition);
		} else {
			startCouponDetailActivity(couponList, couponBoxType, itemPosition);
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		getServerData();
	}

	/**
	 * 지정된 리스트에서 boxSeq맞는 값 찾아서, 해당 포지션 리턴
	 * 
	 * @param list
	 * @param seq
	 * @return
	 */
	private int getCouponBoxSeqMatchListPosition(ArrayList<CouponInfoData2> list, String seq) {
		for (int i = 0; i < list.size(); i++) {
			CouponInfoData2 data = list.get(i);
			if (data.m_boxSEQ.equals(seq)) {
				return i;
			}
		}
		return -1;
	}

	protected void setListView() {
		mReceivedCouponListAdapter = new ReceivedCouponListAdapter(this, mReceivedCouponList);
		mReceivedCouponListView.setAdapter(mReceivedCouponListAdapter);
		mReceivedCouponListView.setEmptyView(findViewById(R.id.received_empty));

		mSentCouponListAdapter = new SentCouponListAdapter(this, mSentCouponList);
		mSentCouponListView.setAdapter(mSentCouponListAdapter);
		mSentCouponListView.setEmptyView(findViewById(R.id.sent_empty));

		mUsedCouponListAdapter = new UsedCouponListAdapter(this, mUsedCouponList);
		mUsedCouponListView.setAdapter(mUsedCouponListAdapter);
		mUsedCouponListView.setEmptyView(findViewById(R.id.used_empty));

		mCompanyCouponListAdapter = new CompanyCouponBoxListAdapter(this, mCompanyCouponList);
		mCompanyCouponListView.setAdapter(mCompanyCouponListAdapter);
		mCompanyCouponListView.setEmptyView(findViewById(R.id.company_empty));
	}

	/**
	 * 좌측 쿠폰함 카운트 갱신
	 */
	protected void updateBoxCount() {
		mVLeftReceiveCBox.setText(getString(R.string.received_coupon_box) + "\n" + getString(R.string.round_bracket_count, String.valueOf(mReceivedCouponList.size())));
		mVLeftSendCBox.setText(getString(R.string.sent_coupon_box) + "\n" + getString(R.string.round_bracket_count, String.valueOf(mSentCouponList.size())));
		mVLeftUseCBox.setText(getString(R.string.used_coupon_box) + "\n" + getString(R.string.round_bracket_count, String.valueOf(mUsedCouponList.size())));
		mVLeftCompanyCBox.setText(getString(R.string.company_coupon_box) + "\n" + getString(R.string.round_bracket_count, String.valueOf(mCompanyCouponList.size())));
	}

	/**
	 * 서버에서 내려받은 데이터 리스트를, 실제 사용하는 데이터 리스트로 변환<br>
	 * 시간 데이터 삽입, 날짜 데이터 삽입, 카운트 세팅
	 * 
	 * @param list
	 * @return
	 */
	private ArrayList<CouponInfoData2> convertDataListToData2List(ArrayList<CouponInfoData> list) {
		ArrayList<CouponInfoData2> nList = new ArrayList<CouponInfoData2>();
		Map<String, ArrayList<CouponInfoData2>> map = new LinkedHashMap<String, ArrayList<CouponInfoData2>>();
		for (CouponInfoData data : list) {
			String patternDateString = Utils.getDateFormatStringFromServerDateFormat(data.getCreatedCouponDate(), "yyyy/MM/dd EEEE", Locale.KOREA);

			if (!map.containsKey(patternDateString)) {
				map.put(patternDateString, new ArrayList<CouponInfoData2>());
			}
			CouponInfoData2 data2 = new CouponInfoData2();
			data2.setCouponInfoData(data, patternDateString);

			// 쿠폰함에 닉뿐아니라 이름까지 보여져야함
			if (mFriendMap.containsKey(data.m_senderID)) {
				data2.mSenderNameNick = mFriendMap.get(data.m_senderID) + "(" + data.m_senderNickname + ")";
			} else {
				data2.mSenderNameNick = data.m_senderNickname;
			}
			if (mFriendMap.containsKey(data.m_receiverID)) {
				data2.mReceiverNameNick = mFriendMap.get(data.m_receiverID) + "(" + data.m_receiverNickname + ")";
			} else {
				data2.mReceiverNameNick = data.m_receiverNickname;
			}

			map.get(patternDateString).add(data2);
		}

		// 첫번째 레이블을 표시해줄 아이템에 카운트 세팅
		Iterator<String> mapKeyIter = map.keySet().iterator();
		while (mapKeyIter.hasNext()) {
			String key = mapKeyIter.next();
			ArrayList<CouponInfoData2> tmpList = map.get(key);
			tmpList.get(0).mDateLabelCount = getString(R.string.round_bracket_count, String.valueOf(tmpList.size()));
			nList.addAll(tmpList);
		}

		return nList;
	}

	/**
	 * 서버에서 내려받은 데이터 리스트를, 실제 사용하는 데이터 리스트로 변환<br>
	 * 시간 데이터 삽입, 날짜 데이터 삽입, 카운트 세팅
	 * 
	 * @param list
	 * @return
	 */
	private ArrayList<CouponInfoData2> convertData2ListToData2List(ArrayList<CouponInfoData2> list) {
		ArrayList<CouponInfoData2> nList = new ArrayList<CouponInfoData2>();
		Map<String, ArrayList<CouponInfoData2>> map = new LinkedHashMap<String, ArrayList<CouponInfoData2>>();
		for (CouponInfoData2 data : list) {
			String patternDateString = Utils.getDateFormatStringFromServerDateFormat(data.getCreatedCouponDate(), "yyyy/MM/dd EEEE", Locale.KOREA);
			
			if (!map.containsKey(patternDateString)) {
				map.put(patternDateString, new ArrayList<CouponInfoData2>());
			}
			// CouponInfoData2 data = new CouponInfoData2();
			data.setCouponInfoData(data, patternDateString);
			
			// 쿠폰함에 닉뿐아니라 이름까지 보여져야함
			if (mFriendMap.containsKey(data.m_senderID)) {
				data.mSenderNameNick = mFriendMap.get(data.m_senderID) + "(" + data.m_senderNickname + ")";
			} else {
				data.mSenderNameNick = data.m_senderNickname;
			}
			if (mFriendMap.containsKey(data.m_receiverID)) {
				data.mReceiverNameNick = mFriendMap.get(data.m_receiverID) + "(" + data.m_receiverNickname + ")";
			} else {
				data.mReceiverNameNick = data.m_receiverNickname;
			}
			
			map.get(patternDateString).add(data);
		}
		
		// 첫번째 레이블을 표시해줄 아이템에 카운트 세팅
		Iterator<String> mapKeyIter = map.keySet().iterator();
		while (mapKeyIter.hasNext()) {
			String key = mapKeyIter.next();
			ArrayList<CouponInfoData2> tmpList = map.get(key);
			tmpList.get(0).mDateLabelCount = getString(R.string.round_bracket_count, String.valueOf(tmpList.size()));
			nList.addAll(tmpList);
		}
		
		return nList;
	}
	
	/**
	 * 단말 디비를 이용하여, 이메일을 키로, 값을 이름으로 가지는 맵 반환
	 * 
	 * @return
	 */
	private Map<String, String> getFriendIdNameMap() {
		Map<String, String> map = new HashMap<String, String>();

		HappytongDBHelper dbHelper = new HappytongDBHelper(this);
		ArrayList<FriendInfo> fList = dbHelper.GetBlockDatabyFriendInfo("0", null);
		for (FriendInfo info : fList) {
			if (info.GetIsName().equals("1")) {
				map.put(info.GetEmail(), info.GetName());
			}
		}
		dbHelper.close();

		return map;
	}

	@Override
	public void onProtoColFailResponse(int eventId, CallbackParam param) {
		super.onProtoColFailResponse(eventId, param);
		closeProgressDialog();

		// XXX 강제지정되고 네트웤에러 날 경우, 해당 페이지 탈출 불가
		SharedPrefManager.getInstance(this).setMoveCBoxSeq("");
		SharedPrefManager.getInstance(this).setMoveCBoxType("");
	}

	private void initTitleView() {
		mTitleInfoView = new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL);
		mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_NORMAL);
	}

	private void init() {
		mContext = getApplicationContext();

		mVLeftReceiveCBox = (TextView) findViewById(R.id.cbox_received);
		mVLeftReceiveCBox.setOnClickListener(this);
		mVLeftSendCBox = (TextView) findViewById(R.id.cbox_sent);
		mVLeftSendCBox.setOnClickListener(this);
		mVLeftUseCBox = (TextView) findViewById(R.id.cbox_used);
		mVLeftUseCBox.setOnClickListener(this);
		mVLeftCompanyCBox = (TextView) findViewById(R.id.cbox_company);
		mVLeftCompanyCBox.setOnClickListener(this);

		mVLeftBoxes = new TextView[] { mVLeftReceiveCBox, mVLeftSendCBox, mVLeftUseCBox, mVLeftCompanyCBox };

		mVLeftFacebook = findViewById(R.id.cbox_facebook);
		mVLeftFacebook.setOnClickListener(this);
		setFacebookButtonVisibility();

		mReceivedCouponListView = (ListView) findViewById(R.id.listview_received);
		mReceivedCouponListView.setOnItemClickListener(ReceivedListViewOnItemClickListenr);

		mSentCouponListView = (ListView) findViewById(R.id.listview_sent);
		mSentCouponListView.setOnItemClickListener(SentListViewOnItemClickListenr);

		mUsedCouponListView = (ListView) findViewById(R.id.listview_used);
		mUsedCouponListView.setOnItemClickListener(UsedListViewOnItemClickListenr);

		mCompanyCouponListView = (ListView) findViewById(R.id.listview_company);
		mCompanyCouponListView.setOnItemClickListener(CompanyListViewOnItemClickListenr);

		mReceivedLayout = findViewById(R.id.received_area);
		mSentLayout = findViewById(R.id.sent_area);
		mUsedLayout = findViewById(R.id.used_area);
		mCompanyLayout = findViewById(R.id.company_area);
	}

	/**
	 * 페이스북 버튼 보임/안보임 ㅋ
	 */
	private void setFacebookButtonVisibility() {
		String facebookAdVal = SharedPrefManager.getInstance(this).getPrefFackbook();
		if (CommonData.FACEBOOK_AD_Y.equals(facebookAdVal)) {
			mVLeftFacebook.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cbox_received:
			Logg.d("onClick - cbox_received");
			changeCouponBoxList(CouponBoxType.RECEIVED);
			break;
		case R.id.cbox_sent:
			Logg.d("onClick - cbox_sent");
			changeCouponBoxList(CouponBoxType.SENT);
			break;
		case R.id.cbox_used:
			Logg.d("onClick - cbox_used");
			changeCouponBoxList(CouponBoxType.USED);
			break;
		case R.id.cbox_company:
			Logg.d("onClick - cbox_company");
			changeCouponBoxList(CouponBoxType.COMPANY);
			break;
		case R.id.cbox_facebook:
			Logg.d("onClick - cbox_facebook");
			showFacebookEventDialog();
			break;
		}
	}

	Handler	mHandler	= new Handler(new Handler.Callback() {
							@Override
							public boolean handleMessage(Message msg) {
								switch (msg.what) {
								case MSG_FACEBOOK_COMPLETE:
									DialogUtil.getConfirmDialog(CouponBoxActivity.this, "", getString(R.string.facebook_ad_complete_message), null).show();
									break;
								case MSG_FACEBOOK_FAIL:
									Toast.makeText(CouponBoxActivity.this, R.string.facebook_post_fail, Toast.LENGTH_LONG).show();
									break;
								}
								return false;
							}
						});

	private void showFacebookEventDialog() {
		Dialog dialog = DialogUtil.getConfirmCancelDialog(this, R.string.facebook_event, R.string.facebook_event_message, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					new FacebookLinker(CouponBoxActivity.this, new FacebookLinker.OnResultListener() {
						@Override
						public void onFacebookResult(int retCode) {
							switch (retCode) {
							case FacebookLinker.OnResultListener.LOGIN_FAIL:
								Logg.d("facebook write happytong message - login - fail");
								break;
							case FacebookLinker.OnResultListener.LOGIN_OK:
								Logg.d("facebook write happytong message - login - ok");
								showProgressDialog();
								break;
							case FacebookLinker.OnResultListener.WRITE_FAIL:
								closeProgressDialog();
								Logg.d("facebook write happytong message - fail");
								mHandler.sendEmptyMessage(MSG_FACEBOOK_FAIL);
								break;
							case FacebookLinker.OnResultListener.WRITE_OK:
								Logg.d("facebook write happytong message");
								requestNetworkForFacebookAdComplete();
								break;
							case FacebookLinker.OnResultListener.LOGINED:
								showProgressDialog();
								break;
							}
						}
					});
				}
			}
		});
		dialog.show();
	}

	/**
	 * 페북 글작성 성공시, 우리 서버로 데이터 보냄
	 */
	protected void requestNetworkForFacebookAdComplete() {
		// TODO 페이스북 테스트시 아래 주석 - 안그러면 서버에서 작업해주지 않는 이상 기회 = 1
		new UserProtocol(reqOkFacebookAd, iFail).getProfileFacebookAd(CallBackParamWrapper.CODE_PROFILE_FACEBOOK_AD, SharedPrefManager.getLoginEmail(mContext));
	}

	final IWiseCallback	reqOkFacebookAd	= new IWiseCallback() {
											@Override
											public boolean WiseCallback(CallbackParam param) {
												Logg.d("networkOkFacebookAd");
												// 페북 단말 저장값 변경 - 페북 아이콘 숨기기
												mVLeftFacebook.setVisibility(View.GONE);
												SharedPrefManager.getInstance(mContext).setPrefFackbook(CommonData.FACEBOOK_AD_Y);
												// 서버데이터로 황금알 갱신
												requestStampRemain();
												return false;
											}
										};

	private void requestStampRemain() {
		PurchaseProtocol protocol = new PurchaseProtocol(new IWiseCallback() {
			@Override
			public boolean WiseCallback(CallbackParam param) {

				PurchaseInfoData data = (PurchaseInfoData) param.param1;

				setGoldEggCount(data.getGoldCount());
				setOwlCount(data.getFreeStampCount(), data.getPaidStampCount());

				closeProgressDialog();

				Logg.d("CouponBoxActivity - requestStampRemain " + data.getGoldCount() + " / " + data.getFreeStampCount() + " / " + data.getPaidStampCount());
				setSubTitleOwlEgg();

				mHandler.sendEmptyMessage(MSG_FACEBOOK_COMPLETE);

				return false;
			}
		}, iFail);
		protocol.orderStampRemain(0, SharedPrefManager.getLoginEmail(this));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != Activity.RESULT_OK) {
			return;
		}
		if (requestCode == IntentDefine.REQCODE_COUPON_DETAIL) {
			getServerData();
		}
		SharedPrefManager.getInstance(this).setMoveCBoxType(String.valueOf(data.getIntExtra(IntentDefine.EXTRA_CBOX_TYPE, CouponBoxType.RECEIVED.ordinal())));
	}

	private void changeCouponBoxList(CouponBoxType type) {

		for (int i = 0; i < mVLeftBoxes.length; i++) {
			mVLeftBoxes[i].setBackgroundResource(R.drawable.selector_m3_leftmenu_btn);
			if (mColorStateListForLeftMenus != null) {
				mVLeftBoxes[i].setTextColor(mColorStateListForLeftMenus);
			}
		}

		switch (type) {
		case RECEIVED:
		case refusal:
			setCouponBoxStateMode(CouponBoxState.RECEIVE);
			mReceivedLayout.setVisibility(View.VISIBLE);
			mSentLayout.setVisibility(View.GONE);
			mUsedLayout.setVisibility(View.GONE);
			mCompanyLayout.setVisibility(View.GONE);

			mVLeftReceiveCBox.setBackgroundResource(R.drawable.m3_leftmenu_btn);
			mVLeftReceiveCBox.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
			break;
		case SENT:
			setCouponBoxStateMode(CouponBoxState.SENT);
			mReceivedLayout.setVisibility(View.GONE);
			mSentLayout.setVisibility(View.VISIBLE);
			mUsedLayout.setVisibility(View.GONE);
			mCompanyLayout.setVisibility(View.GONE);

			mVLeftSendCBox.setBackgroundResource(R.drawable.m3_leftmenu_btn);
			mVLeftSendCBox.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
			break;
		case USED:
			setCouponBoxStateMode(CouponBoxState.USED);
			mReceivedLayout.setVisibility(View.GONE);
			mSentLayout.setVisibility(View.GONE);
			mUsedLayout.setVisibility(View.VISIBLE);
			mCompanyLayout.setVisibility(View.GONE);

			mVLeftUseCBox.setBackgroundResource(R.drawable.m3_leftmenu_btn);
			mVLeftUseCBox.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
			break;
		case COMPANY:
			setCouponBoxStateMode(CouponBoxState.COMPANY);
			mReceivedLayout.setVisibility(View.GONE);
			mSentLayout.setVisibility(View.GONE);
			mUsedLayout.setVisibility(View.GONE);
			mCompanyLayout.setVisibility(View.VISIBLE);

			mVLeftCompanyCBox.setBackgroundResource(R.drawable.m3_leftmenu_btn);
			mVLeftCompanyCBox.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
			break;
		}
	}

	final OnItemClickListener	ReceivedListViewOnItemClickListenr	= new AdapterView.OnItemClickListener() {
																		@Override
																		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
																			startCouponDetailActivity(mReceivedCouponList, CouponBoxType.RECEIVED, position);
																		}
																	};
	final OnItemClickListener	SentListViewOnItemClickListenr		= new AdapterView.OnItemClickListener() {
																		@Override
																		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
																			startCouponDetailActivity(mSentCouponList, CouponBoxType.SENT, position);
																		}
																	};
	final OnItemClickListener	UsedListViewOnItemClickListenr		= new AdapterView.OnItemClickListener() {
																		@Override
																		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
																			if (mUsedCouponListAdapter.isCheckMode()) {
																				mUsedCouponListAdapter.toggleItem(position);
																			} else {
																				startCouponDetailActivity(mUsedCouponList, CouponBoxType.USED, position);
																			}
																		}
																	};
	final OnItemClickListener	CompanyListViewOnItemClickListenr	= new AdapterView.OnItemClickListener() {
																		@Override
																		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
																			if (mCompanyCouponListAdapter.isCheckMode()) {
																				mCompanyCouponListAdapter.toggleItem(position);
																			} else {
																				startCouponDetailActivity(mCompanyCouponList, CouponBoxType.COMPANY, position);
																			}
																		}
																	};

	private void startCouponDetailActivity(ArrayList<CouponInfoData2> list, CouponBoxType type, int position) {
		if (type == CouponBoxType.COMPANY) {
			Intent intent = new Intent(mContext, CompanyCouponDetailActivity.class);
			intent.putExtra(IntentDefine.EXTRA_CBOX_LIST, list);
			intent.putExtra(IntentDefine.EXTRA_CBOX_TYPE, type.ordinal());
			intent.putExtra(IntentDefine.EXTRA_CBOX_LIST_SELECT_POSITION, position);
			startActivityForResult(intent, IntentDefine.REQCODE_COUPON_DETAIL);
		} else {
			Intent intent = new Intent(mContext, CouponDetailActivity.class);
			intent.putExtra(IntentDefine.EXTRA_CBOX_LIST, list);
			intent.putExtra(IntentDefine.EXTRA_CBOX_TYPE, type.ordinal());
			intent.putExtra(IntentDefine.EXTRA_CBOX_LIST_SELECT_POSITION, position);
			startActivityForResult(intent, IntentDefine.REQCODE_COUPON_DETAIL);
		}
	}

	final View.OnClickListener	titleEditClickListener	= new View.OnClickListener() {
															@Override
															public void onClick(View v) {
																if (mCouponBoxState == CouponBoxState.USED) {
																	setCouponBoxStateMode(CouponBoxState.USED_EDIT);
																} else if (mCouponBoxState == CouponBoxState.COMPANY) {
																	setCouponBoxStateMode(CouponBoxState.COMPANY_EDIT);
																}
															}
														};

	final View.OnClickListener	titleClickListener		= new View.OnClickListener() {
															@Override
															public void onClick(View v) {
																switch (v.getId()) {
																case R.id.btn_title_left:
																	// 취소
																	if (mCouponBoxState == CouponBoxState.USED_EDIT) {
																		setCouponBoxStateMode(CouponBoxState.USED);
																	} else if (mCouponBoxState == CouponBoxState.COMPANY_EDIT) {
																		setCouponBoxStateMode(CouponBoxState.COMPANY);
																	}
																	break;
																case R.id.btn_title_right:
																	// 완료
																	if (mCouponBoxState == CouponBoxState.USED_EDIT) {
																		confirmDeleteUsedCoupon();
																	} else if (mCouponBoxState == CouponBoxState.COMPANY_EDIT) {
																		confirmDeleteCompanyCoupon();
																	}
																	break;
																}
															}
														};

	private boolean requireCancelUsedCouponBoxListEditMode() {
		// if ((mUsedCouponListView.getVisibility() == View.VISIBLE) && mUsedCouponBoxState == UsedCouponBoxState.EDIT) {
		switch (mCouponBoxState) {
		case COMPANY_EDIT:
			setCouponBoxStateMode(CouponBoxState.COMPANY);
			return true;
		case USED_EDIT:
			setCouponBoxStateMode(CouponBoxState.USED);
			return true;
		default:
			break;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		if (mHelpLayout.getVisibility() == View.VISIBLE) {
			mHelpClickListener.onClick(mHelpLayout);
			return;
		}

		if (requireCancelUsedCouponBoxListEditMode()) {
			return;
		} else {
			backKeyToExit();
		}
	}

	private void setCouponBoxStateMode(CouponBoxState state) {
		mCouponBoxState = state;
		mUsedCouponListAdapter.setCheckMode(false);
		mCompanyCouponListAdapter.setCheckMode(false);
		switch (state) {
		case RECEIVE:
		case SENT:
			mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_NORMAL);
			break;
		case USED:
			mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_COUPON_BOX, titleEditClickListener);
			break;
		case USED_EDIT:
			mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_COUPON_BOX_DEL, titleClickListener);
			mUsedCouponListAdapter.setCheckMode(true);
			break;
		case COMPANY:
			mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_COUPON_BOX, titleEditClickListener);
			break;
		case COMPANY_EDIT:
			mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_COUPON_BOX_DEL, titleClickListener);
			mCompanyCouponListAdapter.setCheckMode(true);
			break;
		}
	}

	/**
	 * 사용,만료 쿠폰 제거
	 */
	private void confirmDeleteUsedCoupon() {
		final List<CouponInfoData2> list = mUsedCouponListAdapter.getCheckedItemList();
		if (list.size() > 0) {
			// 삭제 요청
			DialogUtil.getConfirmCancelDialog(this, "", getString(R.string.delete_selected_coupon), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						requestNetworkForDeleteCoupon(CouponBoxState.USED, list);
						break;
					}
					setCouponBoxStateMode(CouponBoxState.USED);
				}
			}).show();
		} else {
			setCouponBoxStateMode(CouponBoxState.USED);
		}
	}

	/**
	 * 업체쿠폰 제거
	 */
	private void confirmDeleteCompanyCoupon() {
		final List<CouponInfoData2> list = mCompanyCouponListAdapter.getCheckedItemList();
		if (list.size() > 0) {
			// 삭제 요청
			DialogUtil.getConfirmCancelDialog(this, "", getString(R.string.delete_selected_coupon), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						requestNetworkForDeleteCoupon(CouponBoxState.COMPANY, list);
						break;
					}
					setCouponBoxStateMode(CouponBoxState.COMPANY);
				}
			}).show();
		} else {
			setCouponBoxStateMode(CouponBoxState.COMPANY);
		}
	}

	private void requestNetworkForDeleteCoupon(CouponBoxState state, List<CouponInfoData2> list) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			if (i != 0) {
				sb.append("#");
			}
			sb.append(list.get(i).m_boxSEQ);
		}

		if (state == CouponBoxState.USED) {
			new CouponProtocol(iOkUsedCouponDelete, iFailCouponDelete).couponDelete(CallBackParamWrapper.CODE_COUPON_DELETE, sb.toString(), ID);
		} else {
			new CouponProtocol(iOkCompanyCouponDelete, iFailCouponDelete).companyCouponDelete(CallBackParamWrapper.CODE_COMPANY_COUPON_DELETE, sb.toString(), ID);
		}
		showProgressDialog();
	}

	/**
	 * 받은 쿠폰 콜백 - 완료 후 - 보낸 쿠폰 데이터 요청
	 */
	final IWiseCallback	iOkUsedCouponDelete		= new IWiseCallback() {
													@Override
													public boolean WiseCallback(CallbackParam param) {
														Logg.d("iOkCouponDelete");
														Toast.makeText(mContext, R.string.coupon_deleted, Toast.LENGTH_SHORT).show();
														getServerData();
														// XXX 아.. 완료쿠폰 강제 지정
														SharedPrefManager.getInstance(CouponBoxActivity.this).setMoveCBoxType("2");
														return false;
													}
												};

	/**
	 * 받은 쿠폰 콜백 - 완료 후 - 보낸 쿠폰 데이터 요청
	 */
	final IWiseCallback	iOkCompanyCouponDelete	= new IWiseCallback() {
													@Override
													public boolean WiseCallback(CallbackParam param) {
														Logg.d("iOkCouponDelete");
														Toast.makeText(mContext, R.string.coupon_deleted, Toast.LENGTH_SHORT).show();
														getServerData();
														// XXX 아.. 업체쿠폰 강제 지정
														SharedPrefManager.getInstance(CouponBoxActivity.this).setMoveCBoxType("4");
														return false;
													}
												};

	final IWiseCallback	iFailCouponDelete		= new IWiseCallback() {
													@Override
													public boolean WiseCallback(CallbackParam param) {
														Logg.d("iFailCouponDelete");
														closeProgressDialog();
														Toast.makeText(mContext, R.string.fail_to_delete_coupon, Toast.LENGTH_SHORT).show();
														return false;
													}
												};

	@Override
	protected int setContentViewId() {
		return R.layout.activity_coupon_box;
	}

	// resres 왜 따로 호출 안하고 있는건지... 모르겠음. 2013.09.24
	// protected void requestUsedCouponList() {
	// new CouponProtocol(new IWiseCallback() {
	// @SuppressWarnings("unchecked")
	// @Override
	// public boolean WiseCallback(CallbackParam param) {
	// closeProgressDialog();
	//
	// if (param.param1 != null) {
	// mUsedCouponList = convertDataListToData2List((ArrayList<CouponInfoData>) param.param1);
	// mVLeftUseCBox.setText(getString(R.string.used_coupon_box) + "\n" + getString(R.string.round_bracket_count, String.valueOf(mUsedCouponList.size())));
	// } else {
	// mUsedCouponList.clear();
	// }
	// mUsedCouponListAdapter.notifyDataSetChanged();
	// setTitleBarSupportEditMode(true);
	//
	// return false;
	// }
	// }, iFail).couponUsedExpired(CallBackParamWrapper.CODE_COUPON_USED_EXPIRED, ID);
	// }

}
