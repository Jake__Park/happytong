package kr.wisestone.happytong.ui.cbox;

public enum CouponType {

	Center("0"), Up("1"), Left("2"), Right("3"), NA("999");

	String str;

	private CouponType(String str) {
		this.str = str;
	}

	public static CouponType getCouponType(String str) {
		if (str.equals("0")) {
			return CouponType.Center;
		} else if (str.equals("1")) {
			return CouponType.Up;
		} else if (str.equals("2")) {
			return CouponType.Left;
		} else if (str.equals("3")) {
			return CouponType.Right;
		}
		return CouponType.NA;
	}

	@Override
	public String toString() {
		return name();
	}

}
