package kr.wisestone.happytong.ui.cbox;

import java.io.Serializable;
import java.util.Locale;

import kr.wisestone.happytong.data.dataset.CouponInfoData;
import kr.wisestone.happytong.data.util.Utils;

public class CouponInfoData2 extends CouponInfoData implements Serializable {
	private static final long serialVersionUID = 2256302814522257646L;
	public String mDateLabel;
	public String mDateLabelCount;
	public String mTime;
	public boolean isChecked;
	public String mSenderNameNick;
	public String mReceiverNameNick;
	public String mCompanyID;
	public String mCompanyPhotoURL;
	public String mCompanyName;
	public String mCompanyAddress;
	public String mCompanyPhoneNumber;
	public String mCompanyCouponCreatedDate;

	public void setCouponInfoData(CouponInfoData d, String dateLabel) {
		this.setSenderPhotoURL(d.getSenderPhotoURL());
		this.setCouponCropIMGURL(d.getCouponCropIMGURL());
		this.setCouponType(d.getCouponType());
		this.setValidPeriod(d.getValidPeriod());
		this.setSenderID(d.getSenderID());
		this.setCreatedCouponDate(d.getCreatedCouponDate());
		this.setSenderNickname(d.getSenderNickname());
		this.setCouponIMGURL(d.getCouponIMGURL());
		this.setCouponText(d.getCouponText());
		this.setFlag(d.getFlag());
		this.setRequestState(d.getRequestState());
		this.setReceiverID(d.getReceiverID());
		this.setCouponSEQ(d.getCouponSEQ());
		this.setBoxSEQ(d.getBoxSEQ());
		this.setReceiverPhotoURL(d.getReceiverPhotoURL());
		this.setReceiverNickname(d.getReceiverNickname());
		this.setEndCoupon(d.getEndCoupon());
		this.mTime = Utils.getDateFormatStringFromServerDateFormat(d.getCreatedCouponDate(), "a h:mm", Locale.ENGLISH);
		this.mDateLabel = dateLabel;
	}

}