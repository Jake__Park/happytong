package kr.wisestone.happytong.ui.cbox;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.util.ImageLoader;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

abstract class CouponListAdapter extends BaseAdapter {

	Context						mContext;
	LayoutInflater				mInflater;

	ArrayList<CouponInfoData2>	mList;

	String						todayDateString;
	String						yesterdayDateString;
	final long					DayMillis			= 1000 * 60 * 60 * 24;

	ImageLoader					mImgLoader;
	Picasso						mPicasso;
	final boolean				usingImageLoader	= false;

	public CouponListAdapter(Context mContext, ArrayList<CouponInfoData2> mList) {
		super();
		this.mContext = mContext;
		this.mList = mList;
		this.mInflater = LayoutInflater.from(mContext);
		setTodayYesterdayString();

		if (usingImageLoader) {
			mImgLoader = new ImageLoader(mContext);
		} else {
			mPicasso = Picasso.with(mContext);
		}
	}

	private void setTodayYesterdayString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd EEEE", Locale.KOREA);
		long currMillis = System.currentTimeMillis();

		Date date = new Date(currMillis);
		todayDateString = sdf.format(date);

		date.setTime(currMillis - DayMillis);
		yesterdayDateString = sdf.format(date);
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public CouponInfoData2 getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.couponbox_list_item, null);
			holder = new Holder();
			holder.layoutLabel = convertView.findViewById(R.id.label_layout);
			holder.layout = convertView.findViewById(R.id.layout);
			holder.labelDate = (TextView) convertView.findViewById(R.id.label_date);
			holder.labelCount = (TextView) convertView.findViewById(R.id.label_count);
			holder.thumnail = (ImageView) convertView.findViewById(R.id.thumnail);
			holder.thumnailStatus = (ImageView) convertView.findViewById(R.id.thumnail_status);
			holder.title0 = (TextView) convertView.findViewById(R.id.title0);
			holder.title1 = (TextView) convertView.findViewById(R.id.title1);
			holder.title2 = (TextView) convertView.findViewById(R.id.title2);
			holder.title3 = (TextView) convertView.findViewById(R.id.title3);
			holder.name = (TextView) convertView.findViewById(R.id.text_name);
			holder.time = (TextView) convertView.findViewById(R.id.text_time);
			holder.chkbox = (CheckBox) convertView.findViewById(R.id.coupon_item_chkbox);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		CouponInfoData2 data = mList.get(position);
		// 쿠폰 텍스트
		holderTitleChange(holder, data.m_couponType, data.m_couponText);
		holder.time.setText(data.mTime);

		// 쿠폰 이미지
		if (!TextUtils.isEmpty(data.m_couponCropIMGURL)) {
			if (usingImageLoader) {
				mImgLoader.DisplayImage(data.m_couponCropIMGURL, holder.thumnail);
			} else {
				mPicasso.load(data.m_couponCropIMGURL).placeholder(R.drawable.sub3_default).into(holder.thumnail);
			}
		}

		setViewBoxType(holder, data);

		if (TextUtils.isEmpty(data.mDateLabelCount)) {
			// 일반
			holder.layoutLabel.setVisibility(View.GONE);
		} else {
			// 헤더
			holder.layoutLabel.setVisibility(View.VISIBLE);
			holder.layoutLabel.setOnClickListener(NothingOnClick);

			if (data.mDateLabel.equals(todayDateString)) {
				holder.labelDate.setText(mContext.getString(R.string.today));
			} else if (data.mDateLabel.equals(yesterdayDateString)) {
				holder.labelDate.setText(mContext.getString(R.string.yesterday));
			} else {
				holder.labelDate.setText(data.mDateLabel);
			}
			holder.labelCount.setText(data.mDateLabelCount);
		}

		return convertView;
	}

	abstract void setViewBoxType(Holder holder, CouponInfoData2 data);

	final OnClickListener	NothingOnClick	= new OnClickListener() {
												@Override
												public void onClick(View v) {
													return;
												}
											};

	/**
	 * 쿠폰 타입에 맞는 텍스트뷰에만 메시지 표시하도록 함
	 * 
	 * @param holder
	 * @param typeString
	 * @param title
	 */
	private void holderTitleChange(Holder holder, String typeString, String title) {
		switch (CouponType.getCouponType(typeString)) {
		case NA:
		case Center:
			holder.title0.setText(title);
			holder.title1.setText("");
			holder.title2.setText("");
			holder.title3.setText("");
			break;
		case Up:
			holder.title0.setText("");
			holder.title1.setText(title);
			holder.title2.setText("");
			holder.title3.setText("");
			break;
		case Left:
			holder.title0.setText("");
			holder.title1.setText("");
			holder.title2.setText(title);
			holder.title3.setText("");
			break;
		case Right:
			holder.title0.setText("");
			holder.title1.setText("");
			holder.title2.setText("");
			holder.title3.setText(title);
			break;
		}

	}

	class Holder {
		View		layoutLabel;
		View		layout;
		TextView	labelDate;
		TextView	labelCount;
		ImageView	thumnail;
		ImageView	thumnailStatus;
		TextView	title0;
		TextView	title1;
		TextView	title2;
		TextView	title3;
		TextView	name;
		TextView	time;
		CheckBox	chkbox;
	}

}

class ReceivedCouponListAdapter extends CouponListAdapter {

	public ReceivedCouponListAdapter(Context mContext, ArrayList<CouponInfoData2> mList) {
		super(mContext, mList);
	}

	@Override
	void setViewBoxType(Holder holder, CouponInfoData2 data) {
		holder.chkbox.setVisibility(View.GONE);
		holder.thumnailStatus.setVisibility(View.GONE);
		holder.name.setText(data.mSenderNameNick);
	}

}

class SentCouponListAdapter extends CouponListAdapter {

	public SentCouponListAdapter(Context mContext, ArrayList<CouponInfoData2> mList) {
		super(mContext, mList);
	}

	@Override
	void setViewBoxType(Holder holder, CouponInfoData2 data) {
		holder.chkbox.setVisibility(View.GONE);
		holder.thumnailStatus.setVisibility(View.GONE);
		holder.name.setText(data.mReceiverNameNick);
	}

}

class UsedCouponListAdapter extends CouponListAdapter {

	private boolean	isCheckMode	= false;
	String			mMyId;

	public UsedCouponListAdapter(Context mContext, ArrayList<CouponInfoData2> mList) {
		super(mContext, mList);
		mMyId = SharedPrefManager.getLoginEmail(mContext);
	}

	public void setCheckMode(boolean mode) {
		isCheckMode = mode;
		if (!isCheckMode) {
			clearCheckedItem();
		}
		notifyDataSetChanged();
	}

	private void clearCheckedItem() {
		for (CouponInfoData2 data : mList) {
			data.isChecked = false;
		}
	}

	public boolean isCheckMode() {
		return isCheckMode;
	}

	public void toggleItem(int position) {
		CouponInfoData2 data = mList.get(position);
		data.isChecked = !data.isChecked;
		notifyDataSetChanged();
	}

	@Override
	void setViewBoxType(Holder holder, CouponInfoData2 data) {
		if (isCheckMode) {
			holder.chkbox.setVisibility(View.VISIBLE);
			holder.chkbox.setChecked(data.isChecked);
		} else {
			holder.chkbox.setVisibility(View.GONE);
		}

		// 쿠폰상태아이콘은, 완료-부엉이, 기간만료-달력
		// flag “1” or “2” (1: 쿠폰이 보내진상태, 2: 쿠폰이 사용된 상태)
		// requestState “0” or “1” (0: 쿠폰 사용 요청 x, 1: 쿠폰 사용 요청 있음)
		// endCoupon “0” or “1” (만료쿠폰 0: 쿠폰이 만료되지않음, 1: 쿠폰이 만료됨)
		holder.thumnailStatus.setVisibility(View.VISIBLE);
		// 여기 데이터는 사용/완료 쿠폰들뿐. 따라서 미사용기간만료 or 사용
		if ("0".equals(data.m_requestState)) {
			holder.thumnailStatus.setImageResource(R.drawable.sub3_01_period);
		} else {
			holder.thumnailStatus.setImageResource(R.drawable.sub3_01_complete);
		}

		if (data.m_senderID.equals(mMyId)) {
			holder.name.setText(data.mReceiverNameNick);
		} else {
			holder.name.setText(data.mSenderNameNick);
		}

	}

	public List<CouponInfoData2> getCheckedItemList() {
		List<CouponInfoData2> list = new ArrayList<CouponInfoData2>();
		for (CouponInfoData2 data : mList) {
			if (data.isChecked) {
				list.add(data);
			}
		}
		return list;
	}

}

class CompanyCouponBoxListAdapter extends CouponListAdapter {

	private boolean	isCheckMode	= false;

	public CompanyCouponBoxListAdapter(Context mContext, ArrayList<CouponInfoData2> mList) {
		super(mContext, mList);
	}

	public void setCheckMode(boolean mode) {
		isCheckMode = mode;
		if (!isCheckMode) {
			clearCheckedItem();
		}
		notifyDataSetChanged();
	}

	private void clearCheckedItem() {
		for (CouponInfoData2 data : mList) {
			data.isChecked = false;
		}
	}

	public boolean isCheckMode() {
		return isCheckMode;
	}

	public void toggleItem(int position) {
		CouponInfoData2 data = mList.get(position);
		data.isChecked = !data.isChecked;
		notifyDataSetChanged();
	}

	@Override
	void setViewBoxType(Holder holder, CouponInfoData2 data) {
		if (isCheckMode) {
			holder.chkbox.setVisibility(View.VISIBLE);
			holder.chkbox.setChecked(data.isChecked);
		} else {
			holder.chkbox.setVisibility(View.GONE);
		}

		// 쿠폰상태아이콘은, 완료-부엉이, 기간만료-달력
		// flag // 1:쿠폰이 보내진 상태. 2:쿠폰이사용된상태
		// endCoupon // 만료쿠폰 : 0:쿠폰만료되지않음, 1:쿠폰만료
		if ("1".equals(data.m_flag)) {
			// 쿠폰 보내진 상태 - 사용하지 않았음
			if ("0".equals(data.m_endCoupon)) {
				// 쿠폰 사용 가능 상태
				holder.thumnailStatus.setVisibility(View.GONE);
				holder.thumnailStatus.setImageResource(0);
			} else {
				// 쿠폰만료 사용불가
				holder.thumnailStatus.setVisibility(View.VISIBLE);
				holder.thumnailStatus.setImageResource(R.drawable.sub3_01_period);
			}
		} else {
			// 쿠폰 사용 상태
			holder.thumnailStatus.setVisibility(View.VISIBLE);
			holder.thumnailStatus.setImageResource(R.drawable.sub3_01_complete);
		}

		holder.name.setText(data.mCompanyName);

		holder.time.setText(data.mTime);
	}

	public List<CouponInfoData2> getCheckedItemList() {
		List<CouponInfoData2> list = new ArrayList<CouponInfoData2>();
		for (CouponInfoData2 data : mList) {
			if (data.isChecked) {
				list.add(data);
			}
		}
		return list;
	}

}