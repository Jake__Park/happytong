package kr.wisestone.happytong.ui;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

public class GuideViewActivity extends BaseActivity {

	private static final int LEFT_TO_RIGHT = 0; // 왼쪽에서 오른쪽으로
	private static final int RIGHT_TO_LEFT = 1; // 오른쪽에서 왼쪽으로
	private static final int SWIPE_MIN_DISTANCE = 120; // 최소 이동 거리
	private static final int SWIPE_THRESHOLD_VELOCITY = 200; // 최소 속도

	Context mContext;

	/** 가이드 이미지를 보여주는 레이아웃 */
	private LinearLayout mLl_guide;
	/** 다시 보지 않기 레이아웃 */
	private LinearLayout mLl_guide_check;
	/** Start 버튼 */
	private Button mBt_guide_start;
	/** 다시 보지 않기 체크박스 */
	private CheckBox mCb_notShow;
	private GestureDetector mGestureDetector;
	private int mGuideNum = 1;

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected int setContentViewId() {
		// TODO Auto-generated method stub
		return R.layout.activity_guide_view;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = this;
		onInit();

	}

	private void onInit() {
		onFindView();
		setListener();
	}

	private void onFindView() {
		mLl_guide = (LinearLayout) findViewById(R.id.ll_guide);
		mLl_guide_check = (LinearLayout) findViewById(R.id.ll_guide_check);
		mBt_guide_start = (Button) findViewById(R.id.bt_guide_start);
		mCb_notShow = (CheckBox) findViewById(R.id.cb_guide_no_show);		
	}

	private void setListener() {
		mGestureDetector = new GestureDetector(this, onGestureDetector);
		mBt_guide_start.setOnClickListener(onCheckClickListener);
		mLl_guide_check.setOnClickListener(onCheckClickListener);
	}

	private OnClickListener onCheckClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch(v.getId()){
				case R.id.ll_guide_check:					
					if (mCb_notShow.isChecked()) {
						mCb_notShow.setChecked(false);
					} else {
						mCb_notShow.setChecked(true);
					}
					break;
				case R.id.bt_guide_start:
					if(mCb_notShow.isChecked()){
						SharedPrefManager.getInstance(mContext).setPrefGuideNoShow(true);
					}
					Intent intent = new Intent(GuideViewActivity.this, DummyMainActivity.class);
					startActivity(intent);
					finish();
					break;	
			}
		}

	};

	@Override
	public boolean onTouchEvent(MotionEvent me) {
		return mGestureDetector.onTouchEvent(me);
	}

	private GestureDetector.OnGestureListener onGestureDetector = new GestureDetector.OnGestureListener() {

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onShowPress(MotionEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onLongPress(MotionEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {

			try {
				// 오른쪽에서 왼쪽으로
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					showGuideView(mGuideNum, RIGHT_TO_LEFT);
				}
				// 왼쪽에서 오른쪽으로
				else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					showGuideView(mGuideNum, LEFT_TO_RIGHT);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return false;
		}

		@Override
		public boolean onDown(MotionEvent e) {
			// TODO Auto-generated method stub
			return false;
		}
	};

	private void showGuideView(int step, int lr) {
		switch (step) {
			case 1:
				if (lr == 0) {

				} else if (lr == 1) {
					mLl_guide.setBackgroundResource(R.drawable.workthrough02);
					mGuideNum = 2;
				}
				break;

			case 2:
				if (lr == 0) {
					mLl_guide.setBackgroundResource(R.drawable.workthrough01);
					mGuideNum = 1;
				} else if (lr == 1) {
					mLl_guide.setBackgroundResource(R.drawable.workthrough03);
					mGuideNum = 3;
				}
				break;

			case 3:
				if (lr == 0) {
					mLl_guide.setBackgroundResource(R.drawable.workthrough02);
					mGuideNum = 2;
				} else if (lr == 1) {
					mLl_guide.setBackgroundResource(R.drawable.workthrough04);
					mGuideNum = 4;
				}
				break;

			case 4:
				if (lr == 0) {
					mLl_guide.setBackgroundResource(R.drawable.workthrough03);
					mGuideNum = 3;
				} else if (lr == 1) {
					mLl_guide.setBackgroundResource(R.drawable.workthrough05);
					mGuideNum = 5;
				}
				break;
			case 5:
				if (lr == 0) {
					mLl_guide.setBackgroundResource(R.drawable.workthrough04);
					mGuideNum = 4;
				} else if (lr == 1) {
					mLl_guide.setBackgroundResource(R.drawable.workthrough06);
					mGuideNum = 6;
				}
				break;
			case 6:
				if (lr == 0) {
					mLl_guide.setBackgroundResource(R.drawable.workthrough05);
					mGuideNum = 5;
				} else if (lr == 1) {
					mBt_guide_start.setVisibility(View.VISIBLE);
					mLl_guide_check.setVisibility(View.VISIBLE);					
					mLl_guide.setBackgroundResource(R.drawable.workthrough07);
					mGuideNum = 7;
				}
				break;
			case 7:
				if (lr == 0) {
					mBt_guide_start.setVisibility(View.GONE);
					mLl_guide_check.setVisibility(View.GONE);					
					mLl_guide.setBackgroundResource(R.drawable.workthrough06);
					mGuideNum = 6;
				} else if (lr == 1) {
				}
				break;
		}
	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(GuideViewActivity.this,
				DummyMainActivity.class);
		startActivity(intent);
		finish();
	}

}
