package kr.wisestone.happytong.ui.auth;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.dataset.AuthInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.AuthProtocol;
import kr.wisestone.happytong.data.protocol.UserProtocol;
import kr.wisestone.happytong.data.util.FriendSynchronizer;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class AgreementActivity extends BaseActivity {

	AuthInfoData aiData;
	
	ImageView mIvProvision;
    ImageView mIvPersonal;
    
	boolean bProvisionCheck = false;
    boolean bPersonalCheck = false;
    
    /**
     * 이용약관 조회
     */
    final IWiseCallback iOkAuthProvision = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkAuthProvision");
            
            if(param.param1 != null)
                ((TextView)findViewById(R.id.tv_agreement)).setText( (CharSequence)param.param1 );
            
            mIvProvision.setEnabled(true);
    	    new AuthProtocol(iOkAuthPersonal, iFail).authPersonal(CallBackParamWrapper.CODE_AUTH_PERSONAL);
            return false;
        }
    };
    
    /**
     * 개인정보 취급내용 조회
     */
    final IWiseCallback iOkAuthPersonal = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkAuthPersonal");
            closeProgressDialog();
            
            if(param.param1 != null)
                ((TextView)findViewById(R.id.tv_agreement2)).setText( (CharSequence)param.param1 );
            
            mIvPersonal.setEnabled(true);
            return false;
        }
    };
    
    /**
     * 로그인
     */
    final IWiseCallback iOkAuthLogin = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkAuthLogin");

			closeProgressDialog();

			if (param.param1 != null && param.param1.equals(JsonKeyWrapper.success)) {

				SharedPrefManager spm = SharedPrefManager.getInstance(AgreementActivity.this);
				spm.setLoginEmail(aiData.getId());
				spm.setPrefLoginPwd(aiData.getPassword());
				spm.setPrefProfileNickName(aiData.getNickName());
				
				// XXX 새 회원 가입 후 진입시, 무료 부엉이 카운트 지정. 
				spm.setFreeOwlCount( 5 );

				new FriendSynchronizer(AgreementActivity.this, new FriendSynchronizer.SyncListener() {
                    @Override
                    public void onStartLoading() {
                        showProgressDialog();
                    }
                    
					@Override
					public void onEndLoading() {
					    closeProgressDialog();
					}

					@Override
					public void onEndSynchronize(int retValue) {
					    DialogUtil.getConfirmDialog( AgreementActivity.this, R.string.info, R.string.txt_noti_login_ok, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick( DialogInterface dialog, int which )
                            {
                            	mIsClickable = true;
                            	// 20130611.등록페이지잔류문제로 인해 메인호출은 등록페이지에서 처리하도록 함
                            	// 등록페이지 잔류문제 - 약관동의까지 완료 후 메인진입. 이후 종료. 등록페이지 남아있음 
                                // startActivity(new Intent(AgreementActivity.this, DummyMainActivity.class));
                                setResult(Activity.RESULT_OK);
                                finish();
                            }
                        } ).show();
					}
				});
			} else {
				mIsClickable = true;
			}
			return false;
        }
    };
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (!isRightIntentValues(getIntent())) {
			finish();
		}
		
		findViewData();
		commonAreaSet();
		requestInfoData();

	}
	
    void commonAreaSet(){
        TitleInfoView tiv = new TitleInfoView( this, (View)findViewById( R.id.title_lay ), TitleInfoView.TITLE_TYPE_NORMAL);
        tiv.updateView( R.string.title_agreement, TitleInfoView.TITLE_MODE_AGREEMENT);
    }
	
	private void findViewData(){
	    mIvProvision = (ImageView)findViewById(R.id.iv_agree1);
	    mIvPersonal = (ImageView)findViewById(R.id.iv_agree2);
	    
	    mIvProvision.setOnClickListener( this );	    
	    mIvPersonal.setOnClickListener( this );
	    
	    mIvProvision.setEnabled(false);
	    mIvPersonal.setEnabled(false);
	}
	
	private void requestInfoData(){
	   showProgressDialog();
	    
       new AuthProtocol(iOkAuthProvision, iFail).authProvision(CallBackParamWrapper.CODE_AUTH_PROVISION);
	}
	
   private void requestLogin(){
       showProgressDialog();
        
       new UserProtocol(iOkAuthLogin, iFail).getProfileAdd(CallBackParamWrapper.CODE_PROFILE_ADD, 
               aiData.getId(),
               aiData.getPassword(), 
               aiData.getDeviceId(), 
               aiData.getPhoneNumber(),
               aiData.getDeviceType(),
               aiData.getNickName(),
               aiData.getLocationSmallCode());
    }

	private boolean isRightIntentValues(Intent intent) {
		aiData = (AuthInfoData) intent.getSerializableExtra(IntentDefine.EXTRA_AUTH_INFO_DATA);
		if (aiData == null) {
			return false;
		}
		Logg.d(aiData);
		return true;
	}

	private void setCheckState(int id){
        switch (id) {
        case R.id.iv_agree1:
            if(bProvisionCheck){
                mIvProvision.setBackgroundResource( R.drawable.sub1_02_agree2_btn );
            } else {
                mIvProvision.setBackgroundResource( R.drawable.sub1_02_agree_btn );
            }
            bProvisionCheck = !bProvisionCheck;
            break;
            
        case R.id.iv_agree2:
            if(bPersonalCheck){
                mIvPersonal.setBackgroundResource( R.drawable.sub1_02_agree4_btn );
            } else {
                mIvPersonal.setBackgroundResource( R.drawable.sub1_02_agree3_btn );
            }
            bPersonalCheck = !bPersonalCheck;
            break;
        }
        
        
        if(bProvisionCheck && bPersonalCheck) {
        	mIsClickable = false;
        	Logg.d("if(bProvisionCheck && bPersonalCheck) ");
        	requestLogin();
        }
	}
	
	boolean mIsClickable = true;

	@Override
	public void onClick(View v) {
		if(mIsClickable) {
			setCheckState( v.getId() );
		}
	}
	
	@Override
	public void onProtoColFailResponse(int eventId, CallbackParam param) {
		mIsClickable = true;
		super.onProtoColFailResponse(eventId, param);
	}

	@Override
	protected int setContentViewId() {
		return R.layout.activity_agreement;
	}

}
