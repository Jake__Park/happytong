package kr.wisestone.happytong.ui.auth;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.AuthProtocol;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.CheckValidation;
import kr.wisestone.happytong.util.Logg;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class PasswordActivity extends BaseActivity
{
    static final int CERTIFICATION_MODE   = 0;
    static final int CHANGE_PASSWORD_MODE = 1;
    
    static final int SEND_PW_BTN          = 0;
    static final int CONFIRM_BTN          = 1;
    static final int CHANGE_PW_BTN        = 2;

    final int MINIUM_LENGTH = 4;

    LinearLayout mCertiLayout;
    LinearLayout mChangePwLayout;

    String mAuthNumber;
    String mUserEmail;

    EditText        m_etEmail;
    EditText        m_etAuthNumber;
    EditText        m_etPassword1;
    EditText        m_etPassword2;
    
    final IWiseCallback iOkAuthEmail = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            closeProgressDialog();
            DialogUtil.getConfirmDialog( PasswordActivity.this, R.string.info, R.string.send_certi_number_ok, null ).show();
            return false;
        }
    };
    
    final IWiseCallback iOkChangePw = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            closeProgressDialog();
            DialogUtil.getConfirmDialog( PasswordActivity.this, R.string.info, R.string.change_password_ok_login_again, new OnClickListener()
            {
                
                @Override
                public void onClick( DialogInterface dialog, int which )
                {
                	// 2013.05.07.18:12. jish. 로그인 화면 중복 생성. finish()만 호출하도록
                    // startActivity(new Intent(PasswordActivity.this, LoginActivity.class));
                    finish();
                }
            } ).show();
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        findAllViews();
        commonAreaSet();

        changeLayout(CERTIFICATION_MODE);
    }
    
    private void commonAreaSet(){
        TitleInfoView tiv = new TitleInfoView( this, (View)findViewById( R.id.title_lay ), TitleInfoView.TITLE_TYPE_NORMAL );
        tiv.updateView( R.string.change_password, TitleInfoView.TITLE_MODE_NORMAL );
    }

    
    private void findAllViews() {
        mCertiLayout = (LinearLayout)findViewById( R.id.certify_lay );
        mChangePwLayout = (LinearLayout)findViewById( R.id.password_lay );
        
        m_etEmail = (EditText)findViewById( R.id.email_et );
        m_etAuthNumber = (EditText)findViewById( R.id.certi_num_et );
        m_etPassword1 = (EditText)findViewById( R.id.password_et_1 );
        m_etPassword2 = (EditText)findViewById( R.id.password_et_2 );
        
        ((Button)findViewById( R.id.certify_send_btn )).setOnClickListener( this );
        ((Button)findViewById( R.id.certify_confirm_btn )).setOnClickListener( this );
        ((Button)findViewById( R.id.password_ok_btn )).setOnClickListener( this );
        
        ((Button)findViewById( R.id.certify_send_btn )).setTag( SEND_PW_BTN );
        ((Button)findViewById( R.id.certify_confirm_btn )).setTag( CONFIRM_BTN );
        ((Button)findViewById( R.id.password_ok_btn )).setTag( CHANGE_PW_BTN );
        
        m_etPassword1.setFilters( new InputFilter[] {new PasswordInputFilter()});
        m_etPassword2.setFilters( new InputFilter[] {new PasswordInputFilter()});
        
        m_etEmail.addTextChangedListener( mTwEmail );
        m_etAuthNumber.addTextChangedListener( mTwAuthNum );
        m_etPassword1.addTextChangedListener( mTwPassword );
        m_etPassword2.addTextChangedListener( mTwPassword );
    }
    
    private void changeLayout(int mode){
        switch(mode){
            case CERTIFICATION_MODE:
                mCertiLayout.setVisibility( View.VISIBLE );
                mChangePwLayout.setVisibility( View.GONE );
                
                break;
            case CHANGE_PASSWORD_MODE:
                mCertiLayout.setVisibility( View.GONE );
                mChangePwLayout.setVisibility( View.VISIBLE );
                break;
        }
    }
    
    private void requestAuthNumber(){
        mAuthNumber = Integer.toString( (int)(Math.random()*10000));
        
        mUserEmail = AESEncryption.encrypt( m_etEmail.getText().toString() );
        Logg.d( "[userEmail[[" + mUserEmail );

        AuthProtocol mAp = new AuthProtocol( iOkAuthEmail, iFail );
        mAp.authEmail( CallBackParamWrapper.CODE_AUTH_EMAIL, mUserEmail, AESEncryption.encrypt(mAuthNumber) );
        showProgressDialog();
    }
    
    private void certiAuthNumberCheck(){
        String certiNum = m_etAuthNumber.getText().toString();
        Logg.e( "[certiNum[[" + certiNum );

        if(certiNum != null && certiNum.length() > 0){
            if(certiNum.equals( mAuthNumber )){
                changeLayout( CHANGE_PASSWORD_MODE );
            } else {
                DialogUtil.getConfirmDialog( this, R.string.info, R.string.error_certi_number, null ).show();
            }
        }
    }
    
    private void changePassword(){
        String password1 = AESEncryption.encrypt( m_etPassword1.getText().toString() );
        
        AuthProtocol mAp = new AuthProtocol( iOkChangePw, iFail );
        mAp.authResetPassword( CallBackParamWrapper.CODE_AUTH_EMAIL, mUserEmail, password1 );
        showProgressDialog();
    }
    
    @Override
    public void onClick(View v) {
        Logg.e( "[[[[[[[[[[[[[[" + v.getTag() );
        if(v.getTag().equals( SEND_PW_BTN )){
            requestAuthNumber();
        } else if (v.getTag().equals( CONFIRM_BTN )){
            certiAuthNumberCheck();
        } else if (v.getTag().equals( CHANGE_PW_BTN )){
            changePassword();
        }
    }

    @Override
    protected int setContentViewId() {
        return R.layout.activity_change_pw;
    }
    
    TextWatcher mTwEmail = new TextWatcher() {
        
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            
        }
        
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        
        @Override
        public void afterTextChanged(Editable s) {
            mUserEmail = m_etEmail.getText().toString();
            
            if(CheckValidation.checkTstoreEmailValidation( mUserEmail ) == CheckValidation.CHECK_SUCCESS){
                ((Button)findViewById( R.id.certify_send_btn )).setEnabled( true );
            }  else {
                ((Button)findViewById( R.id.certify_send_btn )).setEnabled( false );
            }
        }
    };
    
    TextWatcher mTwAuthNum = new TextWatcher() {
        
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
        
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        
        @Override
        public void afterTextChanged(Editable s) {
            String certiNum = ((EditText)findViewById( R.id.certi_num_et )).getText().toString();
            
            if(certiNum.length() > 0){
                ((Button)findViewById( R.id.certify_confirm_btn )).setEnabled( true );
            }  else {
                ((Button)findViewById( R.id.certify_confirm_btn )).setEnabled( false );
            }

        }
    };

    TextWatcher mTwPassword = new TextWatcher() {
        
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
        
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        
        @Override
        public void afterTextChanged(Editable s) {
            if(m_etPassword1.getText().length() < MINIUM_LENGTH || m_etPassword2.getText().length() < MINIUM_LENGTH) {
                ((Button)findViewById( R.id.password_ok_btn )).setEnabled( false );    
            } else if(m_etPassword1.getText().toString().equals( m_etPassword2.getText().toString() )) {
                ((Button)findViewById( R.id.password_ok_btn )).setEnabled( true );    
            } else {
                ((Button)findViewById( R.id.password_ok_btn )).setEnabled( false );    
            }            
        }
    };

}
