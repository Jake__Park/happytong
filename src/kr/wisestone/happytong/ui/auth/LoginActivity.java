package kr.wisestone.happytong.ui.auth;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.dataset.AuthInfoData;
import kr.wisestone.happytong.data.dataset.UserInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.AuthProtocol;
import kr.wisestone.happytong.data.protocol.UserProtocol;
import kr.wisestone.happytong.data.util.FriendSynchronizer;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.GuideViewActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends BaseActivity {

	Context mContext;

	EditText mEtEmail;
	EditText mEtPass;

	View mVLogin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = this;

		initTitle();
		init();
	}

	private void initTitle() {
		new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL).updateView(R.string.login, TitleInfoView.TITLE_MODE_NORMAL);
	}

	private void init() {
		mEtEmail = (EditText) findViewById(R.id.email_et);
		mEtEmail.addTextChangedListener(watcher);
		mEtPass = (EditText) findViewById(R.id.password_et);
		mEtPass.addTextChangedListener(watcher);
		mEtPass.setFilters(new InputFilter[] { new PasswordInputFilter() });

		mVLogin = findViewById(R.id.login_v);
		mVLogin.setOnClickListener(this);

		findViewById(R.id.change_password_v).setOnClickListener(this);
		findViewById(R.id.register_v).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login_v:
			if (isValid()) {
				requestLogin();
			}
			break;
		case R.id.change_password_v:
			Logg.d("change_password_view - click");
			// 20130611. jish. 비밀번호 페이지에서는 아래와 같이 호출할 필요 없음.
			// startActivityForResult(new Intent(this, PasswordActivity.class), IntentDefine.REQCODE_CHANGE_PASSWORD);
			startActivity(new Intent(this, PasswordActivity.class));
			break;
		case R.id.register_v:
			Logg.d("register_view - click");
			startActivityForResult(new Intent(this, RegisterActivity.class), IntentDefine.REQCODE_REGISTER);
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			finish();
			// 20130611. jish. 약관페이지까지 완료가 떨어지면 메인으로 이동하도록 처리
//			startActivity(new Intent(this, DummyMainActivity.class));			
			startActivity(new Intent(this, GuideViewActivity.class));
		}
	}

	private boolean isValid() {
		String email = mEtEmail.getText().toString();
		if (!Utils.isStringMatchEmail(email)) {
			DialogUtil.getConfirmDialog(this, null, getString(R.string.register_fail_wrong_email), null).show();
			return false;
		}
		return true;
	}

	private void requestLogin() {
		AuthInfoData aiData = new AuthInfoData();
		aiData.setId(AESEncryption.encrypt(mEtEmail.getText().toString()));
		aiData.setPassword(AESEncryption.encrypt(mEtPass.getText().toString()));
		aiData.setDeviceId(Utils.getEncryptDeviceIdForGCM(this));
		aiData.setPhoneNumber(Utils.getPhoneNumber(this));

		new AuthProtocol(networkLoginOk, iFail).authLogin(CallBackParamWrapper.CODE_AUTH_LOGIN, aiData.getId(), aiData.getPassword(), aiData.getPhoneNumber(),
				aiData.getDeviceId(), aiData.getDeviceType());
		showProgressDialog();
	}

	final IWiseCallback networkLoginOk = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			Logg.d("onProtocolResponse");

			if (param.param1 != null) {
				AuthInfoData mData = (AuthInfoData) param.param1;

				setOwlCount(mData.getFreeStampCount(), mData.getPaidStampCount());
				setGoldEggCount(mData.getRemainGoldEgg());

				SharedPrefManager.getInstance(mContext).setPrefProfileNickName(mData.getNickName());
				Logg.d("networkLoginOk - " + mData.isFreeStampReceive());
				SharedPrefManager.getInstance(mContext).setPrefShowFreeOwlReceivePopup(mData.isFreeStampReceive());
				SharedPrefManager.getInstance(mContext).setPrefLoginCompanyState(Integer.parseInt(mData.getCompanyState()));
				if(SharedPrefManager.getInstance(mContext).getPrefLoginCompanyState() == 1){
					SharedPrefManager.getInstance(mContext).setPrefCompanyOneTimeCoupon(mData.getOneTimeCoupon());
					SharedPrefManager.getInstance(mContext).setPrefCompanyNoLimitTimeCoupon(mData.getNoLimitTimeCoupon());
					SharedPrefManager.getInstance(mContext).setPrefCompanyBusinessNumber(mData.getBusinessNumber());
					
					Logg.d("getOneTimeCoupon============== " + mData.getOneTimeCoupon());
					Logg.d("getNoLimitTimeCoupon============== " + mData.getNoLimitTimeCoupon());
					Logg.d("getBusinessNumber ================= " + mData.getBusinessNumber());
				}
			}

			SharedPrefManager.getInstance(mContext).setLoginEmail(AESEncryption.encrypt(mEtEmail.getText().toString()));
			SharedPrefManager.getInstance(mContext).setPrefLoginPwd(AESEncryption.encrypt(mEtPass.getText().toString()));

			requestProfileGetForSettingFacebookAd();

			return false;
		}
	};

	// final IWiseCallback networkLoginFail = new IWiseCallback() {
	// @Override
	// public boolean WiseCallback(CallbackParam param) {
	// Logg.d("onProtoColFailResponse");
	// closeProgressDialog();
	//
	// Toast.makeText(mContext, R.string.txt_fail_login, Toast.LENGTH_SHORT).show();
	// // Utils.ToastNetworkError(this);
	// return false;
	// }
	// };

	/**
	 * 페이스북에 앱홍보했는지 여부 체크
	 */
	protected void requestProfileGetForSettingFacebookAd() {
		new UserProtocol(networkProfileGetOk, networkProfileGetFail).getProfileGet(CallBackParamWrapper.CODE_PROFILE_GET, SharedPrefManager.getLoginEmail(this));
	}

	final IWiseCallback networkProfileGetOk = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			closeProgressDialog();

			if (param.param1 != null) {
				UserInfoData userInfoData = (UserInfoData) param.param1;
				SharedPrefManager.getInstance(mContext).setPrefFackbook(userInfoData.getFbAd());
			}

			new FriendSynchronizer(mContext, syncListener);
			return false;
		}
	};

	// XXX USE Customized NetworkFail
	final IWiseCallback networkProfileGetFail = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			closeProgressDialog();
			new FriendSynchronizer(mContext, syncListener);
			return false;
		}
	};

	final FriendSynchronizer.SyncListener syncListener = new FriendSynchronizer.SyncListener() {
		@Override
		public void onStartLoading() {
			showProgressDialog();
		}

		@Override
		public void onEndLoading() {
			closeProgressDialog();
		}

		@Override
		public void onEndSynchronize(int retValue) {
			closeProgressDialog();

			Intent intent = new Intent(mContext, GuideViewActivity.class);
			startActivity(intent);
			finish();
		}
	};

	@Override
	protected int setContentViewId() {
		return R.layout.activity_login;
	}

	private void setEnableLoginButton(boolean b) {
		mVLogin.setEnabled(b);
	}

	final TextWatcher watcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			if (mEtEmail.getText().toString().length() > 0 && mEtPass.getText().toString().length() >= 4) {
				setEnableLoginButton(true);
			} else {
				setEnableLoginButton(false);
			}
		}
	};

}
