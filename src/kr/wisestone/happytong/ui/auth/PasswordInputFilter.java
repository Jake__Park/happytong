package kr.wisestone.happytong.ui.auth;

import java.util.regex.Pattern;

import android.text.InputFilter;
import android.text.Spanned;

public class PasswordInputFilter implements InputFilter {

	final int MAX_LENGTH = 16;

	@Override
	public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
		if (dend >= MAX_LENGTH) {
			return "";
		}
		Pattern mPattern = Pattern.compile("^[a-zA-Z0-9-_.()-]+$");
		if (!mPattern.matcher(source).matches()) {
			return "";
		}
		return null;
	}

}
