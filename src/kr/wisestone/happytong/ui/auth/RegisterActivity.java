package kr.wisestone.happytong.ui.auth;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.AuthInfoData;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.AuthProtocol;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.setting.LocationListViewActivity;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

public class RegisterActivity extends BaseActivity {

	EditText mEtNick;
	EditText mEtEmail;
	EditText mEtPass1;
	EditText mEtPass2;

	View mVComplete;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initTitle();
		init();
	}

	private void initTitle() {
		new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL).updateView(R.string.register, TitleInfoView.TITLE_MODE_NORMAL);
	}

	private void init() {
		mEtNick = (EditText) findViewById(R.id.nickname_et);
		mEtEmail = (EditText) findViewById(R.id.email_et);
		mEtPass1 = (EditText) findViewById(R.id.password1_et);
		mEtPass1.setFilters(new InputFilter[]{new PasswordInputFilter()});
		mEtPass2 = (EditText) findViewById(R.id.password2_et);
		mEtPass2.setFilters(new InputFilter[]{new PasswordInputFilter()});

		EditText[] ets = { mEtNick, mEtEmail, mEtPass1, mEtPass2 };
		EnableWatcher enableWatcher = new EnableWatcher(ets);

		mEtNick.addTextChangedListener(enableWatcher);
		mEtEmail.addTextChangedListener(enableWatcher);
		mEtPass1.addTextChangedListener(enableWatcher);
		mEtPass2.addTextChangedListener(enableWatcher);
		mVComplete = findViewById(R.id.complete);
	}

	private void setCompleteButtonEnabled(boolean b) {
		mVComplete.setEnabled(b);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.complete:
			if (isValid()) {
				requestNetworkForCheckDuplicateId();
			}
			break;
		}
	}

	/**
	 * 중복된 아이디인지 확인하고, 이후 절차 진행하도록 처리
	 */
	private void requestNetworkForCheckDuplicateId() {
		String encryptId = AESEncryption.encrypt(mEtEmail.getText().toString());
		new AuthProtocol(networkForCheckDuplicatedIdOk, networkForCheckDuplicatedIdFail).authCheckId(CallBackParamWrapper.CODE_AUTH_CHECK_ID, encryptId);
		showProgressDialog();
	}

	final IWiseCallback networkForCheckDuplicatedIdOk = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			closeProgressDialog();
			finishMoveAgreeTerms();
			return false;
		}
	};

	final IWiseCallback networkForCheckDuplicatedIdFail = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			closeProgressDialog();
			DialogUtil.getConfirmDialog(RegisterActivity.this, null, getString(R.string.register_fail_duplicated_id), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// 이메일 초기화 제거
					// mEtEmail.setText("");
				}
			}).show();
			return false;
		}
	};

	private boolean isValid() {
		String nick = mEtNick.getText().toString();
		if (TextUtils.isEmpty(nick) || nick.length() < 2) {
			DialogUtil.getConfirmDialog(this, null, getString(R.string.register_fail_nickname_length), null).show();
			return false;
		}

		String email = mEtEmail.getText().toString();
		if (!Utils.isStringMatchEmail(email)) {
			DialogUtil.getConfirmDialog(this, null, getString(R.string.register_fail_wrong_email), null).show();
			return false;
		}
		String pass1 = mEtPass1.getText().toString();
		String pass2 = mEtPass2.getText().toString();
		if (!pass1.equals(pass2)) {
			DialogUtil.getConfirmDialog(this, null, getString(R.string.register_fail_notequals_passwords), null).show();
			mEtPass1.requestFocus();
			return false;
		}
		return true;
	}

	private void finishMoveAgreeTerms() {
		Logg.d("move AgreeTermsActivity");
		AuthInfoData aiData = new AuthInfoData();
		aiData.setId(AESEncryption.encrypt(mEtEmail.getText().toString()));
		aiData.setPassword(AESEncryption.encrypt(mEtPass1.getText().toString()));
		aiData.setNickName(mEtNick.getText().toString());
		aiData.setDeviceId(Utils.getEncryptDeviceIdForGCM(this));
		aiData.setPhoneNumber(Utils.getPhoneNumber(this));

		Intent intent = new Intent(this, LocationListViewActivity.class);
		intent.putExtra(IntentDefine.EXTRA_AUTH_INFO_DATA, aiData);
		intent.putExtra(IntentDefine.EXTRA_LOCATION_LIST_TYPE, CommonData.LOCATION_LIST_TYPE_JOIN);
		startActivityForResult(intent, IntentDefine.REQCODE_REGISTER);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			setResult(Activity.RESULT_OK);
			finish();
		}
	}

	@Override
	protected int setContentViewId() {
		return R.layout.activity_register;
	}

	class EnableWatcher implements TextWatcher {

		EditText[] mEditTexts;

		public EnableWatcher(EditText[] mEditTexts) {
			super();
			this.mEditTexts = mEditTexts;
		}

		@Override
		public void afterTextChanged(Editable s) {
			setCompleteButtonEnabled(isValidInputDatas());
		}

		private boolean isValidInputDatas() {
			for (EditText editText : mEditTexts) {
				switch (editText.getId()) {
				case R.id.nickname_et:
					if (editText.getText().length() < 2) {
						return false;
					}
					break;
				case R.id.email_et:
					if (!Utils.isStringMatchEmail(editText.getText().toString())) {
						return false;
					}
					break;
				case R.id.password1_et:
					if (editText.getText().length() < 4) {
						return false;
					}
					break;
				case R.id.password2_et:
					if (editText.getText().length() < 4) {
						return false;
					}
					break;
				}
			}
			return true;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
	}

}
