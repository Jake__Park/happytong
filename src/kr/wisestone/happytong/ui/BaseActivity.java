package kr.wisestone.happytong.ui;

import java.util.List;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.util.ErrorCode;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.util.Logg;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.widget.Toast;

public abstract class BaseActivity extends Activity implements android.view.View.OnClickListener {
	public static final int REQUEST_GROUP_EDIT = 100;
	public static final int REQUEST_FRIEND_EDIT = 200;
	public static final int REQUEST_FRIEND_BLOCK = 300;

	protected ProgressDialog mProgressDialog;

	// back key
	private boolean mB_Close = false;
	private Handler m_close_handler = null;

	protected IWiseCallback iOK = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			Logg.d("RootActivity SUCCESS");

			// TODO Auto-generated method stub
			onProtocolResponse((Integer) param.eventId, param);
			return false;
		}
	};

	protected IWiseCallback iFail = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			closeProgressDialog();
			Logg.e("BaseActivity iFail!!");
			onProtoColFailResponse((Integer) param.errorCode, param);
			return false;
		}
	};

	/**
	 * onProtocolResponse : Response Pass
	 * 
	 * @param eventId
	 * @param param
	 */
	public void onProtocolResponse(int eventId, CallbackParam param) {
	}

	/**
	 * onProtocolResponse : Response Fail
	 * 
	 * @param eventId
	 * @param param
	 */
	public void onProtoColFailResponse(int eventId, CallbackParam param) {
		showCommonErrorPopup(param.errorCode);
	}

	public void showCommonErrorPopup(int errorCode) {
		int messageId = ErrorCode.getErrorMsg(errorCode);

		showPopupDone(R.string.info, messageId);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		int contentViewId = setContentViewId();
		if (contentViewId != 0) {
			setContentView(setContentViewId());
		}

		baseActivityInit();

		// 무료부엉이 받음 팝업
		showFreeOwlReceivePopup();
	}

	private void showFreeOwlReceivePopup() {
		SharedPrefManager spm = SharedPrefManager.getInstance(this);
		if (spm.getPrefShowFreeOwlReceivePopup()) {
			DialogUtil.getConfirmDialog(this, "", getString(R.string.free_owl_receive_popup_message), null).show();
			spm.setPrefShowFreeOwlReceivePopup(false);
		}
	}

	public void setOwlCount(String strFreeCount, String strPaidCount) {
		int freeCount = Integer.parseInt(strFreeCount);
		int paidCount = Integer.parseInt(strPaidCount);

		setOwlCount(freeCount, paidCount);
	}

	public void setOwlCount(int freeCount, int paidCount) {
		SharedPrefManager.getInstance(this).setFreeOwlCount(freeCount);
		SharedPrefManager.getInstance(this).setPurchasedOwlCount(paidCount);
	}

	public void setGoldEggCount(String strGoldCount) {
		int goldCount = Integer.parseInt(strGoldCount);

		setGoldEggCount(goldCount);
	}

	public void setGoldEggCount(int goldCount) {
		SharedPrefManager.getInstance(this).setGoldEggCount(goldCount);
	}

	public void showPopupDone(int titleId, final int messageId) {
		DialogUtil.getConfirmDialog(this, titleId, messageId, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (messageId == R.string.error_code_305) {
					// 타단말 로그인일경우 처리
					Utils.deleteUserDataAndFinishCuFunU(BaseActivity.this, false);
				}
			}
		}).show();
	}

	public void showPopupBack(int titleId, int messageId) {
		DialogUtil.getConfirmDialog(this, titleId, messageId, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		}).show();
	}

	@SuppressLint("HandlerLeak")
	private void baseActivityInit() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setCanceledOnTouchOutside(false);
		mProgressDialog.setMessage(getString(R.string.loading_message));

		// back key
		m_close_handler = new Handler() {
			public void handleMessage(Message msg) {
				mB_Close = false;
			}
		};
	}

	protected abstract int setContentViewId();

	public final void showProgressDialog() {
		mProgressDialog.show();
	}

	public final void closeProgressDialog() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
	}

	@Override
	protected void onDestroy() {
		// 20130613.jish.errlog_stack --> 06-12 18:11:01.617: E/AndroidRuntime(22416): java.lang.IllegalArgumentException: View not attached to window manager
		closeProgressDialog();
		super.onDestroy();
	}

	public void clearApplicationFileCache(java.io.File dir) {
		if (dir == null)
			dir = getCacheDir();
		else
			;

		if (dir == null)
			return;
		else
			;

		java.io.File[] children = dir.listFiles();
		try {
			for (int i = 0; i < children.length; i++) {
				if (children[i].isDirectory())
					clearApplicationFileCache(children[i]);
				else
					children[i].delete();
			}
		} catch (Exception e) {
		}
	}

	public void requestKillProcess() {
		setResult(Activity.RESULT_OK);
		finish();

//		TODO 무한GC발생 (갤럭시S : 앱진입, 종료, 앱진입, 종료 - 재현) - 확인 후 정리 필요. 2013.09.24.jish		
//		new Thread(new Runnable() {
//			public void run() {
//				ActivityManager actMng = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
//				String strProcessName = getApplicationInfo().processName;
//				while (true) {
//					List<RunningAppProcessInfo> list = actMng.getRunningAppProcesses();
//					for (RunningAppProcessInfo rap : list) {
//						if (rap.processName.equals(strProcessName)) {
//							if (rap.importance >= RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {
//								actMng.restartPackage(getPackageName());
//							}
//							Thread.yield();
//							break;
//						}
//					}
//				}
//			}
//		}, "Process Killer").start();
	}

	/**
	 * Clear key
	 * 
	 * @Method : backKeyToExit
	 */
	public void backKeyToExit() {
		if (mB_Close == false) {

			Toast.makeText(this, getResources().getText(R.string.txt_end_str).toString(), Toast.LENGTH_SHORT).show();

			mB_Close = true;

			m_close_handler.sendEmptyMessageDelayed(0, 2500);

		} else {
			clearApplicationFileCache(null);
			requestKillProcess();
		}
	}
}
