package kr.wisestone.happytong.ui.dialog;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.util.Logg;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MenuDialog extends Dialog implements View.OnClickListener{
    public static final int MENU_NONE_TYPE            = -1;
    public static final int MENU_FRIEND_TYPE_BY_GRUOP = 0;
    public static final int MENU_FRIEND_TYPE_BY_ALL   = 1;
    public static final int MENU_FRIEND_TYPE_ADD_ONLY = 2;
    public static final int MENU_FRIEND_TYPE_DEL_ONLY = 3;
    
    public static final int CLICK_TYPE_BLOCK          = 100;
    public static final int CLICK_TYPE_ADD            = 200;
    public static final int CLICK_TYPE_DELETE         = 300;
    
    View.OnClickListener m_clListener;
    int m_nType;
    
    public MenuDialog(Context context) {
        super( context, android.R.style.Theme_Translucent_NoTitleBar);
    }

    public MenuDialog(Context context, int nType, View.OnClickListener clListener) {
        super( context, android.R.style.Theme_Translucent_NoTitleBar);
        m_clListener = clListener;
        m_nType = nType;
    }

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView(R.layout.menu_option);
        
        initView();
    }
    
    protected void initView()
    {
        ((LinearLayout)findViewById( R.id.menu_ll_first )).setVisibility( View.VISIBLE );
        ((LinearLayout)findViewById( R.id.menu_ll_second )).setVisibility( View.VISIBLE );
        
        ((LinearLayout)findViewById( R.id.menu_ll_first )).setOnClickListener( this );
        ((LinearLayout)findViewById( R.id.menu_ll_second )).setOnClickListener( this );

        switch(m_nType)
        {
            case MENU_FRIEND_TYPE_BY_ALL:
                ((LinearLayout)findViewById( R.id.menu_ll_second )).setVisibility( View.GONE );
                ((TextView)findViewById( R.id.menu_tv_first )).setBackgroundResource( R.drawable.sub2_04_block_menu );
                
                ((LinearLayout)findViewById( R.id.menu_ll_first )).setTag( CLICK_TYPE_BLOCK );
                break;
            case MENU_FRIEND_TYPE_BY_GRUOP:
                ((TextView)findViewById( R.id.menu_tv_first )).setBackgroundResource( R.drawable.sub2_02_groupadd_menu );
                ((TextView)findViewById( R.id.menu_tv_second )).setBackgroundResource( R.drawable.sub2_02_groupdel_menu );

                ((LinearLayout)findViewById( R.id.menu_ll_first )).setTag( CLICK_TYPE_ADD );
                ((LinearLayout)findViewById( R.id.menu_ll_second )).setTag( CLICK_TYPE_DELETE );
                break;
            case MENU_FRIEND_TYPE_ADD_ONLY:
                ((LinearLayout)findViewById( R.id.menu_ll_second )).setVisibility( View.GONE );
                ((TextView)findViewById( R.id.menu_tv_first )).setBackgroundResource( R.drawable.sub2_02_groupadd_menu );
                
                ((LinearLayout)findViewById( R.id.menu_ll_first )).setTag( CLICK_TYPE_ADD );
                break;
            case MENU_FRIEND_TYPE_DEL_ONLY:
                ((LinearLayout)findViewById( R.id.menu_ll_second )).setVisibility( View.GONE );
                ((TextView)findViewById( R.id.menu_tv_first )).setBackgroundResource( R.drawable.sub2_02_groupdel_menu );
                
                ((LinearLayout)findViewById( R.id.menu_ll_first )).setTag( CLICK_TYPE_DELETE );
                break;
        }
        
        findViewById( R.id.option_space ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                dismiss();
            }
        } );
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_MENU) {
            dismiss();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onClick( View v )
    {
        if(m_clListener != null) m_clListener.onClick( v );
        Logg.d( v.getTag() );
        dismiss();
    }
}
