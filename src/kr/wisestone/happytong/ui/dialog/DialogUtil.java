package kr.wisestone.happytong.ui.dialog;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.text.InputFilter;
import android.text.TextUtils;
import android.widget.EditText;

public class DialogUtil {

	/**
	 * 통신이 안될 때
	 * 
	 * @param context
	 */
	public static void showNetworkNotAvailableDialog(Context context) {
		getConfirmDialog(context, context.getString(android.R.string.dialog_alert_title), context.getString(R.string.network_not_available), null).show();
	}

	/**
	 * 서버 통신 오류
	 * 
	 * @param context
	 */
	public static void showNetworkErrorDialog(Context context) {
		getConfirmDialog(context, context.getString(android.R.string.dialog_alert_title), context.getString(R.string.network_error), null).show();
	}

	/**
	 * 확인 다이얼로그
	 * 
	 * @param context
	 * @param titleId
	 *            String Resource Id
	 * @param messageId
	 *            String Resource Id
	 * @param l
	 * @return
	 */
	public static Dialog getConfirmDialog(Context context, int titleId, int messageId, DialogInterface.OnClickListener l) {
		return getDialog(context, context.getString(titleId), context.getString(messageId), context.getString(android.R.string.ok), null, l);
	}

	/**
	 * 확인 다이얼로그
	 * 
	 * @param context
	 * @param title
	 *            String
	 * @param message
	 *            String
	 * @param l
	 * @return
	 */
	public static Dialog getConfirmDialog(Context context, String title, String message, DialogInterface.OnClickListener l) {
		return getDialog(context, title, message, context.getString(android.R.string.ok), null, l);
	}

	/**
	 * 확인 다이얼로그
	 * 
	 * @param context
	 * @param titleId
	 *            String Resource Id
	 * @param messageId
	 *            String Resource Id
	 * @param positiveId
	 *            String Resource Id
	 * @param l
	 * @return
	 */
	public static Dialog getConfirmDialog(Context context, int titleId, int messageId, int positiveId, DialogInterface.OnClickListener l) {
		return getDialog(context, context.getString(titleId), context.getString(messageId), context.getString(positiveId), null, l);
	}

	/**
	 * 확인 다이얼로그
	 * 
	 * @param context
	 * @param title
	 *            String
	 * @param message
	 *            String
	 * @param positive
	 *            String
	 * @param l
	 * @return
	 */
	public static Dialog getConfirmDialog(Context context, String title, String message, String positive, DialogInterface.OnClickListener l) {
		return getDialog(context, title, message, positive, null, l);
	}

	/**
	 * 확인 취소 다이얼로그
	 * 
	 * @param context
	 * @param title
	 *            String
	 * @param message
	 *            String
	 * @param l
	 * @return
	 */
	public static Dialog getConfirmCancelDialog(Context context, String title, String message, DialogInterface.OnClickListener l) {
		return getDialog(context, title, message, context.getString(android.R.string.ok), context.getString(android.R.string.cancel), l);
	}

	/**
	 * 확인 취소 다이얼로그
	 * 
	 * @param context
	 * @param titleId
	 *            String Resource Id
	 * @param messageId
	 *            String Resource Id
	 * @param l
	 * @return
	 */
	public static Dialog getConfirmCancelDialog(Context context, int titleId, int messageId, DialogInterface.OnClickListener l) {
		return getDialog(context, context.getString(titleId), context.getString(messageId), context.getString(android.R.string.ok), context.getString(android.R.string.cancel), l);
	}

	/**
	 * 확인 취소 다이얼로그
	 * 
	 * @param context
	 * @param title
	 *            String
	 * @param message
	 *            String
	 * @param positive
	 *            String
	 * @param negative
	 *            String
	 * @param l
	 * @return
	 */
	public static Dialog getConfirmCancelDialog(Context context, String title, String message, String positive, String negative, DialogInterface.OnClickListener l) {
		return getDialog(context, title, message, positive, negative, l);
	}

	/**
	 * 확인 취소 다이얼로그
	 * 
	 * @param context
	 * @param titleId
	 *            String Resource Id
	 * @param messageId
	 *            String Resource Id
	 * @param positiveId
	 *            String Resource Id
	 * @param negativeId
	 *            String Resource Id
	 * @param l
	 * @return
	 */
	public static Dialog getConfirmCancelDialog(Context context, int titleId, int messageId, int positiveId, int negativeId, DialogInterface.OnClickListener l) {
		return getDialog(context, context.getString(titleId), context.getString(messageId), context.getString(positiveId), context.getString(negativeId), l);
	}

	private static Dialog getDialog(Context context, String title, String message, String positive, String negative, final DialogInterface.OnClickListener l) {
		AlertDialog.Builder adb = new AlertDialog.Builder(context);
		if (!TextUtils.isEmpty(title)) {
			adb.setTitle(title);
		}
		if (!TextUtils.isEmpty(message)) {
			adb.setMessage(message);
		}
		if (!TextUtils.isEmpty(positive)) {
			adb.setPositiveButton(positive, l);
		}
		if (!TextUtils.isEmpty(negative)) {
			adb.setNegativeButton(negative, l);
		}
		adb.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				if (l != null) {
					l.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
				}
			}
		});
		return adb.create();
	}

	/**
	 * EditTextDialog Listener
	 */
	public interface OnEditTextDialogListener {
		/**
		 * EditTextDialog - ok Button - return EditText values
		 * 
		 * @param text
		 */
		void onEditTextDialogOk(String text);

		/**
		 * EditTextDialog - cancel Button or cancel key
		 */
		void onEditTextDialogCancel();
	}

	/**
	 * 편집텍스트 다이얼로그
	 * 
	 * @param context
	 * @param titleId
	 *            다이얼로그 타이틀 아이디
	 * @param defaultText
	 *            기본 텍스트
     * @param hintId
     *            다이얼로그 힌트 아이디
	 * @param l
	 *            OnEditTextDialogListener
	 * @return
	 */
	public static Dialog getEditTextDialog(Context context, int titleId, String defaultText, int hintId, final OnEditTextDialogListener l) {
		return getEditTextDialog(context, context.getString(titleId), defaultText, context.getString(hintId),CommonData.EDIT_TEXT_DIALOG_TEXT_MAX_LENGTH, l);
	}

	/**
	 * 편집텍스트 다이얼로그
	 * 
	 * @param context
	 * @param title
	 *            다이얼로그 타이틀
	 * @param defaultText
	 *            기본 텍스트
	 * @param l
	 *            OnEditTextDialogListener
	 * @return
	 */
	public static Dialog getEditTextDialog(Context context, String title, String defaultText, final OnEditTextDialogListener l) {
		return getEditTextDialog(context, title, defaultText, "", CommonData.EDIT_TEXT_DIALOG_TEXT_MAX_LENGTH, l);
	}

	/**
	 * 편집텍스트 다이얼로그
	 * 
	 * @param context
	 * @param title
	 *            타이틀
	 * @param defaultText
	 *            기본텍스트
     * @param hint
     *            EditText Hint String
	 * @param maxLength
	 *            길이제한
	 * @param l
	 * @return
	 */
	public static Dialog getEditTextDialog(Context context, String title, String defaultText, String hint, final int maxLength, final OnEditTextDialogListener l) {
		final EditText editText = new EditText(context.getApplicationContext());
		InputFilter[] filters = { new InputFilter.LengthFilter(maxLength) };
		editText.setFilters(filters);
		editText.setSingleLine(true);
		editText.setHint(hint);
		if (!TextUtils.isEmpty(defaultText)) {
			editText.setText(defaultText);
			if (defaultText.length() > 0) {
				editText.setSelection(defaultText.length());
			}
		}

		DialogInterface.OnClickListener r = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (l == null) {
					return;
				}
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					l.onEditTextDialogOk(editText.getText().toString());
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					l.onEditTextDialogCancel();
					break;
				}
			}
		};

		AlertDialog.Builder adb = new AlertDialog.Builder(context);
		adb.setTitle(title);
		adb.setView(editText);
		adb.setPositiveButton(android.R.string.ok, r);
		adb.setNegativeButton(android.R.string.cancel, r);
		adb.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				if (l != null) {
					l.onEditTextDialogCancel();
				}
			}
		});
		return adb.create();
	}

}
