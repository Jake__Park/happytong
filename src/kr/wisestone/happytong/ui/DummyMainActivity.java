package kr.wisestone.happytong.ui;

import kr.wisestone.happytong.AllActivityListActivity;
import kr.wisestone.happytong.company.csend.CompanyCouponSendActivity;
import kr.wisestone.happytong.company.history.CompanySendCouponHistoryActivity;
import kr.wisestone.happytong.company.setting.CompanySettingActivity;
import kr.wisestone.happytong.company.shop.CompanyShopActivity;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.ui.auth.LoginActivity;
import kr.wisestone.happytong.ui.cbox.CouponBoxActivity;
import kr.wisestone.happytong.ui.csend.CouponSendActivity;
import kr.wisestone.happytong.ui.cshop.CouponShopActivity;
import kr.wisestone.happytong.ui.friend.FriendMainActivity;
import kr.wisestone.happytong.ui.setting.SettingActivity;
import kr.wisestone.happytong.util.IntentDefine;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

public class DummyMainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		movePage(SharedPrefManager.getInstance(this).getBeforeEntryPoint());
		// testActivityList();
	}

	private void testActivityList() {
		Intent aal = new Intent(this, AllActivityListActivity.class);
		aal.putExtra(Intent.EXTRA_INITIAL_INTENTS, false);
		startActivity(aal);
		finish();
	}

	private void movePage(int point) {
		Context context = this;

		// GCM으로부터 실행된것인지 확인하여, GCM이동값있을 경우, 쿠폰함으로 이동시킴
		if (!TextUtils.isEmpty(SharedPrefManager.getInstance(this).getMoveCBoxType())) {
			point = CommonData.ENTRY_PONINT_CBOX;
		}
		
		overridePendingTransition(0, 0);
		switch (point) {
		case CommonData.ENTRY_PONINT_FRIEND:
			startActivityForResult(new Intent(context, FriendMainActivity.class), IntentDefine.REQCODE_MAIN);
			break;
		case CommonData.ENTRY_PONINT_CBOX:
			startActivityForResult(new Intent(context, CouponBoxActivity.class), IntentDefine.REQCODE_MAIN);
			break;
		case CommonData.ENTRY_PONINT_CSEND:
			startActivityForResult(new Intent(context, CouponSendActivity.class), IntentDefine.REQCODE_MAIN);
			break;
		case CommonData.ENTRY_PONINT_CSHOP:
			startActivityForResult(new Intent(context, CouponShopActivity.class), IntentDefine.REQCODE_MAIN);
			break;
		case CommonData.ENTRY_PONINT_SETTING:
			startActivityForResult(new Intent(context, SettingActivity.class), IntentDefine.REQCODE_MAIN);
			break;
			
        case CommonData.ENTRY_PONINT_COMPAMY_SEND_LIST:
            startActivityForResult(new Intent(context, CompanySendCouponHistoryActivity.class), IntentDefine.REQCODE_MAIN);
            break;
        case CommonData.ENTRY_PONINT_COMPAMY_SEND:
            startActivityForResult(new Intent(context, CompanyCouponSendActivity.class), IntentDefine.REQCODE_MAIN);
            break;
        case CommonData.ENTRY_PONINT_COMPAMY_CHARGE:
            startActivityForResult(new Intent(context, CompanyShopActivity.class), IntentDefine.REQCODE_MAIN);
            break;
        case CommonData.ENTRY_PONINT_COMPAMY_SETTING:
            startActivityForResult(new Intent(context, CompanySettingActivity.class), IntentDefine.REQCODE_MAIN);
            break;            
			
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Utils.loggingIntentData(intent);

		if (intent.getBooleanExtra(IntentDefine.EXTRA_MAIN_FINISH, false)) {
			// 로그인 호출
			finish();
			if (intent.getBooleanExtra(IntentDefine.EXTRA_MAIN_FINISH_START_LOGIN, false)) {
				startActivity(new Intent(this, LoginActivity.class));
			}
		} else {
			movePage(intent.getIntExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_FRIEND));
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			finish();
		}
	}

}
