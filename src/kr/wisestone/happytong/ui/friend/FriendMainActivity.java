package kr.wisestone.happytong.ui.friend;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.HappytongDB.GroupInfo;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.util.ImageDownloader;
import kr.wisestone.happytong.data.util.RecycleUtils;
import kr.wisestone.happytong.plugin.InviteHelper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.csend.CouponSendActivity;
import kr.wisestone.happytong.ui.dialog.MenuDialog;
import kr.wisestone.happytong.ui.friend.FriendListAdapter.ViewHolder;
import kr.wisestone.happytong.ui.widget.MenuInfoView;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

public class FriendMainActivity extends BaseActivity
{
    public static final int       DISPLAY_STATE_NORMAL      = 1;
    public static final int       DISPLAY_STATE_SELECT_MODE = 2;
    
    public static final int       SELECT_ALL_FRIEND      = 1;
    public static final int       SELECT_GROUP_FRIEND    = 2;
    
    TitleInfoView                 mTitleInfoView;

    private ListView              mGroupListView;                                         // group list view.
    private ListView              mFriendListView;                                        // friend list view.
    private ImageView             mIvNodata;

    private RatingBar             mRbFreeOwl;
    private TextView              mTvChargedOwl;
    private EditText              mEtSearchName;

    private Button                mSendCoupon               = null;                       // 쿠폰 보내기

    private MainHandler           mMainHandler              = new MainHandler();          // UI 처리 Handler
    private FriendHandler         mFriendtHandler           = null;                       // 공용 DB 처리 Handler

    private int                   mSelectedListItemId       = 0;                          // 선택된 list item 의 index
    private ImageDownloader       mImageLoader              = null;                       // image loader

    private ArrayList<GroupInfo>  mArrayGroupList           = new ArrayList<GroupInfo>(); // 그룹 리스트 data
    private ArrayList<FriendInfo> mArrayFriendList          = new ArrayList<FriendInfo>(); // 친구 리스트 data
    private ArrayList<FriendInfo> mArrayFriendCheckList     = new ArrayList<FriendInfo>(); // 친구 체크 리스트 data

    private FriendListAdapter     mGroupAdapter;
    private FriendListAdapter     mFriendAdapter;
    
    private int                   mSelectGroupPoisition     = 0;
    private int                   mDisplayState             = DISPLAY_STATE_NORMAL;
    private int                   mFriendViewMode           = SELECT_ALL_FRIEND;
    private PreGroupSelect        mPreGroupSelect           = new PreGroupSelect();       // 이전 선택된 그룹 정보 저장
    
    boolean                       bGroupAddMode             = false;	//SDJ[130812] 개선사항 적용
    
    TextWatcher mTwSearch = new TextWatcher() {
        
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        	// if("+".equals( mArrayGroupList.get(mSelectGroupPoisition).GetGroupName() ) == false){
        	// 친구메인 - 그룹리스트 - 하단 구분 문자열 "+"에서 "..."로 변경. 20130806.jish
			//[SDJ 131016]Array Exception 방지용
            if(mArrayGroupList.size() > mSelectGroupPoisition){
                if(CommonData.GROUP_LIST_FOOTER_STRING.equals( mArrayGroupList.get(mSelectGroupPoisition).GetGroupName() ) == false){
                    changeFriendList(mSelectGroupPoisition);
                }
            }
        }
        
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        
        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    OnItemClickListener mGroupClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick( AdapterView<?> v, View arg1, int position, long arg3 ) {
            if(mSelectGroupPoisition != position){
                mSelectGroupPoisition = position;
                changeFriendList(position);
                
                changeSelectImage(arg1, position);
                clearEditStr();
            }
        }
    };
    
    // 친구메인에서 친구선택시 바로 쿠폰발송 페이지로 이동하도록 수정. 20130806.jish
    OnItemClickListener mFriendItemClickListener = new OnItemClickListener() {
    	@Override
    	public void onItemClick( AdapterView<?> parent, View view, int position, long id) {
    		mArrayFriendCheckList.add(mArrayFriendList.get(position));
            Intent intent = new Intent(FriendMainActivity.this, CouponSendActivity.class);
            intent.putExtra( IntentDefine.EXTRA_FRIEND_SELECT_LIST, mArrayFriendCheckList );
            startActivity(intent);
            setResult(Activity.RESULT_OK);
            finish();
    	}
    };
    
    OnClickListener mCheckBoxClickListener = new OnClickListener() {
        @Override
        public void onClick( View v ) {
            if( mFriendListView==null) {
                return;
            }
            
            int position = mFriendListView.getPositionForView(v);
            if( position<0 ) {
                Logg.d("can not find item from view");
                return;
            }
            
            FriendInfo info = mArrayFriendList.get(position);
            
            setCheckState(info, position, false);
        }        

    };
    
    OnClickListener mTitleClickListener = new OnClickListener() {
        @Override
        public void onClick( View v ) {
            if(v.getTag().equals( TitleInfoView.TITLE_RIGHT_BTN )){
                startOtherActivity();
            }
        }        
    };
    
    /***
     * onCreate 
     * @param context : Bundle savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Logg.d( "onCreate" );
        super.onCreate(savedInstanceState);
        
        mImageLoader=new ImageDownloader();
        
        findAllViews();
        setEventes();
        commonAreaSet();
        
        makeListStart();
        changeLayout(mDisplayState);
    }
    
    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        Logg.d("requestCode = " + requestCode);
        Logg.d("resultCode = " + resultCode);

        if(requestCode == REQUEST_GROUP_EDIT || requestCode == REQUEST_FRIEND_BLOCK) {
            if(resultCode == RESULT_OK) {
                mSelectGroupPoisition = 0;
            } else if(resultCode == CommonData.GROUP_ADD_OK){
				//SDJ[130812] 개선사항 적용
                bGroupAddMode = true;
                mSelectGroupPoisition = data.getIntExtra(String.class.toString(), 0);
            }
			//SDJ[130812] 개선사항 적용			
            changeFriendList(0);
        } else if(requestCode == REQUEST_FRIEND_EDIT){
            if(resultCode == RESULT_OK) {
                Logg.d("mSelectGroupPoisition = " + mSelectGroupPoisition);
                sendMsg(FriendHandler.MSG_CONTROL_DOING, FriendHandler.DB_GET_GROUP_LIST, 0, null);

                changeFriendList(mSelectGroupPoisition);
            }
        }
        super.onActivityResult( requestCode, resultCode, data );
    }
    
    private void commonAreaSet(){
        new MenuInfoView( this, (View)findViewById( R.id.menu_tab ), CommonData.ENTRY_PONINT_FRIEND );
        mTitleInfoView = new TitleInfoView( this, (View)findViewById( R.id.title_lay ), TitleInfoView.TITLE_TYPE_NORMAL );
    }

    /***
     * view load.
     */
    private void findAllViews() {
        mSendCoupon = (Button)findViewById(R.id.ib_sendCoupon);
        
        mGroupListView = (ListView)findViewById( R.id.groupList );
        mFriendListView = (ListView)findViewById( R.id.list );
        mIvNodata = (ImageView)findViewById( R.id.ivNodata );
        
        mRbFreeOwl = (RatingBar)findViewById( R.id.owl_free_rating );
        mTvChargedOwl = (TextView)findViewById( R.id.owl_charged_rating );
        mEtSearchName = (EditText)findViewById( R.id.et_search_name );
        
        mEtSearchName.addTextChangedListener( mTwSearch );
        
        mRbFreeOwl.setRating( SharedPrefManager.getInstance(this).getFreeOwlCount() );
        
        mGroupListView.setDivider( null );
        mFriendListView.setDivider( null );
        
        int count = SharedPrefManager.getInstance(this).getPurchasedOwlCount();
        mTvChargedOwl.setText( "+" + count );
        
//      안드로이드를 아이폰 처럼 "+0" 문구 추가. 20130806.jish
//        if(count > 0) 
//            mTvChargedOwl.setText( "+" + count );
//        else
//            mTvChargedOwl.setText( "" );
            
    }
    
    /***
     * View에 event 등록
     */
    private void setEventes() {
        mSendCoupon.setOnClickListener(setContactAddBtnListener());
    }
    
    @Override
    protected void onStart() {
        Logg.d("onStart");
        super.onStart();

    }
    
    @Override
    protected void onResume() {
        Logg.d("onResume");
        
        super.onResume();
    }
    
    @Override
    protected void onPause() {
        Logg.d("onPause");
        
        super.onPause();
    }
    
    @Override
    protected void onDestroy() {
    	
		if (mFriendtHandler != null && mFriendtHandler.mDbHelper != null) {
			mFriendtHandler.mDbHelper.close();
		}
    	
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
        System.gc();
        super.onDestroy();
    }
        
    private OnClickListener setContactAddBtnListener() {
        return new OnClickListener() {
            public void onClick(View v) {
                if(mDisplayState == DISPLAY_STATE_SELECT_MODE){
                    for(int nIndex =0; nIndex < mArrayFriendList.size(); nIndex++) {
                        if(mArrayFriendList.get( nIndex ).isChecked() == false){
                            selectAll(true);
                            return;
                        }
                    }
                    selectAll(false);
                } else {
                    if(mArrayFriendList.size() > 0){
                        mDisplayState = DISPLAY_STATE_SELECT_MODE;
                        changeLayout(mDisplayState);
						//SDJ[130812] 개선사항 적용
                        mArrayGroupList.remove(mArrayGroupList.size()-1);
                        mFriendAdapter.notifyDataSetChanged();
						//SDJ[130812] 개선사항 적용
                        mGroupAdapter.notifyDataSetChanged();
                    }
                }
            }
        };
    }
    
    private void selectAll(boolean flag){
        int mode = FriendInfo.CHECK_FLAG_UNCHECK ;
        
        mArrayFriendCheckList.clear();
        
        if(flag) {
            mode = FriendInfo.CHECK_FLAG_CHECKED ;
            mSendCoupon.setBackgroundResource( R.drawable.sub4_02_select2_btn );
        } else {
            mSendCoupon.setBackgroundResource( R.drawable.sub4_02_select_btn );
        }
    
        for(int nIndex =0; nIndex < mArrayFriendList.size(); nIndex++)
        {
            mArrayFriendList.get( nIndex ).setCheckBoxFlag( mode );
            setCheckState(mArrayFriendList.get( nIndex ), nIndex , true);
        }
        mFriendAdapter.notifyDataSetChanged();
    }
    
    @Override
    public void onBackPressed() {
        if(mDisplayState == DISPLAY_STATE_SELECT_MODE){
            clearResource();
            return;
        } else {
            backKeyToExit();
        }
    }
    
    private void clearResource(){
        clearEditStr();
        selectAll(false);
        
        mDisplayState = DISPLAY_STATE_NORMAL;

		//SDJ[130812] 개선사항 적용
        GroupInfo groupList = new GroupInfo(""+mArrayGroupList.size()+1, CommonData.GROUP_LIST_FOOTER_STRING);
        mArrayGroupList.add(groupList);
        
        changeLayout(DISPLAY_STATE_NORMAL);
        mFriendAdapter.notifyDataSetChanged();
    }
    
    private void startOtherActivity() {
        if(mDisplayState == DISPLAY_STATE_NORMAL){
            registerForContextMenu(findViewById( R.id.btn_title_right));
            openContextMenu(findViewById( R.id.btn_title_right));
        } else {
            if(mArrayFriendCheckList.size() > 0){
                Intent intent = new Intent(FriendMainActivity.this, CouponSendActivity.class);
                intent.putExtra( IntentDefine.EXTRA_FRIEND_SELECT_LIST, mArrayFriendCheckList );
                startActivity(intent);
                setResult(Activity.RESULT_OK);
                finish();
            } else {
                clearResource();
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        menu.add(0, CommonData.INVITE_SMS, 0, R.string.invite_by_sms);
        menu.add(0, CommonData.INVITE_KAKAO, 0, R.string.invite_by_kakao);
//    	안드로이드를 아이폰 처럼 "친구초대" 문구 추가. 20130806.jish 
//      menu.add(0, CommonData.INVITE_SMS, 0, R.string.sms);
//      menu.add(0, CommonData.INVITE_KAKAO, 0, R.string.kakao);

    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case CommonData.INVITE_SMS:
            //친구 선택 화면 이동 
            Intent intent = new Intent(FriendMainActivity.this, FriendInviteActivity.class);
            startActivity(intent);
            break;
        case CommonData.INVITE_KAKAO:
        	InviteHelper.getInstance(this).sendKakaoTextMessage();
            break;
        }
        return super.onContextItemSelected(item);
    }
    
    private void setCheckState(FriendInfo info, int position, boolean isAll){
        if(isAll){
            if(info.isChecked()){
                mArrayFriendCheckList.add( info );
            }
            else{
                mArrayFriendCheckList.remove( info );
            }            
        } else {
            if(info.isChecked()){
                info.setCheckBoxFlag( FriendInfo.CHECK_FLAG_UNCHECK);
                
                for(FriendInfo temp : mArrayFriendCheckList){
                    if(info.GetEmail().equals( temp.GetEmail() )){
                        mArrayFriendCheckList.remove( temp );
                        break;
                    }
                }
                mArrayFriendCheckList.remove( info );
            }
            else{
                info.setCheckBoxFlag( FriendInfo.CHECK_FLAG_CHECKED );
                mArrayFriendCheckList.add( info );
            }
            mArrayFriendList.set( position, info );
        }

        mFriendAdapter.notifyDataSetChanged();
    }
    
    /***
     * @Brief 화면 전환.
     */
    private void changeLayout(int mode) {
        switch(mDisplayState) {
        case DISPLAY_STATE_NORMAL:
            mRbFreeOwl.setVisibility( View.VISIBLE );
            mTvChargedOwl.setVisibility( View.VISIBLE );
            mEtSearchName.setVisibility( View.GONE );
            
            mSendCoupon.setBackgroundResource( R.drawable.selector_sub2_01_send );
            // 메뉴하고 상단 타이틀 제목 맞추기(친구리스트:친구, 쿠폰발송:쿠폰보내기) - 20130806.jish
            // mTitleInfoView.updateView( R.string.menu_friend, TitleInfoView.TITLE_MODE_FRIEND_MAIN, mTitleClickListener );
            mTitleInfoView.updateView( R.string.menu_friend_list, TitleInfoView.TITLE_MODE_FRIEND_MAIN, mTitleClickListener );
            break;
            
        case DISPLAY_STATE_SELECT_MODE:
            mRbFreeOwl.setVisibility( View.GONE );
            mTvChargedOwl.setVisibility( View.GONE );
            mEtSearchName.setVisibility( View.VISIBLE );
            
            mSendCoupon.setBackgroundResource( R.drawable.sub4_02_select_btn );
            mTitleInfoView.updateView( R.string.friend_title_select, TitleInfoView.TITLE_MODE_FRIEND_SELECT, mTitleClickListener );
            break;
        }
    }
    
    private void changeFriendList(int position) {
        if(position == 0){
            //친구 전체 리스트
            mFriendViewMode = SELECT_ALL_FRIEND;
            makeListStart();

        } else {
            // if("+".equals( mArrayGroupList.get(position).GetGroupName() )){
        	// 친구메인 - 그룹리스트 - 하단 구분 문자열 "+"에서 "..."로 변경. 20130806.jish
            if(CommonData.GROUP_LIST_FOOTER_STRING.equals( mArrayGroupList.get(position).GetGroupName() )){
                startActivityForResult( new Intent(FriendMainActivity.this, GroupEditActivity.class), REQUEST_GROUP_EDIT);
            } else {
                //그룹별 친구 리스트
                mFriendViewMode = SELECT_GROUP_FRIEND;
                makeGroupFriendList(mArrayGroupList.get(position).GetGroupID());
                Logg.d( "GetGroupID = " +  mArrayGroupList.get(position).GetGroupID());
            }
        }
    }
    
	//SDJ[130812] 개선사항 적용
    private void selectGroupFriendList(int position) {
        //그룹별 친구 리스트
        mFriendViewMode = SELECT_GROUP_FRIEND;
        makeGroupFriendList(mArrayGroupList.get(position).GetGroupID());
        Logg.d( "GetGroupID = " +  mArrayGroupList.get(position).GetGroupID());
    }    
    
    /***
     * @Brief 리스트 생성.
     */
    private void makeListStart() {
        Logg.d( "makeListStart" );
        
        if( mFriendtHandler==null) {
            mFriendtHandler = new FriendHandler(this, mMainHandler) {
                @Override
                public String getSearchString() {
                    String searchStr = mEtSearchName.getText().toString();
                    return (searchStr.length()>0)?searchStr:null;
                }
            };
        }

        sendMsg(FriendHandler.MSG_CONTROL_DOING, FriendHandler.DB_CONTROL_START, 0, null);
    }
    
    /***
     * @Brief 리스트 생성.
     */
    private void makeGroupFriendList(String groupID) {
        Logg.d( "makeListStart groupName = " + groupID );
        
        if( mFriendtHandler==null) {
            mFriendtHandler = new FriendHandler(this, mMainHandler){
                @Override
                public String getSearchString() {
                    String searchStr = mEtSearchName.getText().toString();
                    return (searchStr.length()>0)?searchStr:null;
                }
            };
        }
        sendMsg(FriendHandler.MSG_CONTROL_DOING, FriendHandler.DB_GET_FRIEND_LIST_GROUP, 0, groupID);
    }
    
    /***
     * @Brief 선택되어 있는 group 가져오기.
     * 
     * @return GroupList class.
     */
    public GroupInfo getSelectedGroup() {
        if( mFriendtHandler==null) return null;
        
        GroupInfo groupList = getFocusedItem();
        
        if( groupList==null) {
            return null;
        }
        
        return groupList;
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }
    
    /***
     * @Brief message 전송
     * @param int what  : message 처리 handler id
     * @param int arg1  : action id
     * @param int arg2  : parameter1
     * @param Object obj: parameter2 
     */
    private void sendMsg(int what, int arg1, int arg2, Object obj) {
        Message msg = new Message();
        msg.what = what;
        msg.arg1 = arg1;
        msg.arg2 = arg2;
        msg.obj = obj;
        
        mFriendtHandler.sendMessage( msg );
    }

    @SuppressLint( "HandlerLeak" )
    private class MainHandler extends Handler{
        
        /***
         * @Brief hander 분기.
         */
        @SuppressWarnings( "unchecked" )
        public void handleMessage(Message msg) {
            Logg.d( "Main Handler = " + msg.arg1);
            
            switch(msg.arg1) {
                case FriendHandler.MSG_COMPLETE_GROUP_LIST:
                    mArrayGroupList = (ArrayList<GroupInfo>)msg.obj;
                    initGroupList();
                    break;
                case FriendHandler.MSG_COMPLETE_FRIEND_LIST:
                    if(mArrayFriendList != null) mArrayFriendList.clear();
                    
                    if(msg.obj != null)
                        mArrayFriendList = (ArrayList<FriendInfo>)msg.obj;
                    
					//SDJ[130812] 개선사항 적용
                    if(bGroupAddMode){
                        bGroupAddMode = false;
                        selectGroupFriendList(mSelectGroupPoisition);
                    } else {
                        listViewInit();
                    }
                    break;
            }
        }
    }
    
    /***
     * @Brief 그룹 리스트 초기화. 
     */
    private void initGroupList() {
        mGroupAdapter = new FriendListAdapter(this, mArrayGroupList, R.layout.group_list, FriendListAdapter.LIST_TYPE_GROUP) {
            @Override
            public View setView(int position, View convertView, ViewGroup parent, ViewHolder holder) {
                setGroupViewData(position, (GroupInfo)getItem(position), holder);
                return convertView;
            }
        };
        
        mGroupListView.setOnItemClickListener(mGroupClickListener);
        mGroupListView.setAdapter(mGroupAdapter);
        
        mGroupListView.post(setSelectInit());

    }
    
    /***
     * @Brief 리스트 생성 완료 후 첫번째 item 에 select focus 를 주기 위해 재귀
     */
    private Runnable setSelectInit() {
        return new Runnable() {
            public void run() {
                View v = mGroupListView.getChildAt(mSelectGroupPoisition);
                if( v!=null) {
                    changeSelectImage(v, mSelectGroupPoisition);
                } else {
                    mGroupListView.post(setSelectInit());
                }
            }
        };
    }
    
    private void setGroupViewData(int position, GroupInfo item, ViewHolder holder) {
        String groupName = item.GetGroupName();
        Logg.e( "SDJ setGroupViewData= " +  groupName);

        if( mPreGroupSelect.position==position ) {
            holder.mllGroupName.setBackgroundResource(R.drawable.m3_leftmenu_btn );
            holder.mGroupName.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
            holder.mGroupCount.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
        } else {
            holder.mllGroupName.setBackgroundResource(R.drawable.m3_leftmenu2_btn );
            holder.mGroupName.setTextColor(getResources().getColor(R.color.rgb_666));
            holder.mGroupCount.setTextColor(getResources().getColor(R.color.rgb_666));
        }
            
        holder.mGroupName.setText(groupName);
        
        // if("+".equals( groupName )){
        // 친구메인 - 그룹리스트 - 하단 구분 문자열 "+"에서 "..."로 변경. 20130806.jish
        if(CommonData.GROUP_LIST_FOOTER_STRING.equals( groupName )){
            holder.mGroupCount.setVisibility( View.GONE );
        } else {
            holder.mGroupCount.setVisibility( View.VISIBLE );
            holder.mGroupCount.setText("["+getGroupCount(item)+"]");
        }
        
        holder.mllGroupName.setVisibility(View.VISIBLE);
    }
    
    /***
     * @Brief 그룹 리스트의 선택 된 image 변경.
     * @param1 View : Click event 가 발생한 view
     * @param2 int  : click event가 가 발생한 item의 그룹 리스트내의 index
     */
    private void changeSelectImage(View clickedView, int position) {

        mPreGroupSelect.backToUnSelect();
        mPreGroupSelect.position = mSelectedListItemId = position;
        mPreGroupSelect.preView = clickedView;
        
        Logg.d("changeSelectImage :"+position);
        if( clickedView!=null && clickedView.getTag()!= null ) {
            ViewHolder holder = (ViewHolder)clickedView.getTag();
            holder.mllGroupName.setBackgroundResource(R.drawable.m3_leftmenu_btn );
            holder.mGroupName.setTextColor(getResources().getColor(R.color.rgb_25a4b8));
            holder.mGroupCount.setTextColor(getResources().getColor(R.color.rgb_25a4b8));
        }
        ((FriendListAdapter)mGroupListView.getAdapter()).notifyDataSetChanged();
    }
    
    
    private void clearEditStr(){
        if(mDisplayState == DISPLAY_STATE_SELECT_MODE)
            mEtSearchName.setText( "" );
    }
    /***
     * @Brief 해당 그룹의 Child 수를 반환.
     * @param1 GroupList : 연락처의 개수를 알고자 하는 그룹 item 
     * @return int : 그룹에 포함 된 연락처의 개수.
     */
    private int getGroupCount(GroupInfo item) {
        int nCount = 0;
        HappytongDBHelper db = new HappytongDBHelper(this);
        
        if( item == null ||item.GetGroupName().compareTo(getString(R.string.all))==0 ) {
            nCount = db.getUnBlockFriendCount();
        } else {
            nCount = db.getGroupFriendCount( item.GetGroupID() );
        }
        
        db.close();
        return nCount;
    }
    
    public GroupInfo getFocusedItem() {
        int position = 0;
        position = mSelectedListItemId;
                
        if( position==-1 ) {
            position = 0;
        }
        
        return mArrayGroupList.get(position);
    }
    
    /***
     * @Brief 연락처 리스트 초기화 및 생성 
     */
    private void listViewInit() {
        if(mDisplayState == DISPLAY_STATE_SELECT_MODE){
            for(int i=0; i<mArrayFriendCheckList.size(); i++){
                String friendId = mArrayFriendCheckList.get( i ).GetID();
                for(int j=0; j<mArrayFriendList.size(); j++){
                    if(friendId.equals( mArrayFriendList.get( j ).GetID() )){
                        mArrayFriendList.get( j ).setCheckBoxFlag( FriendInfo.CHECK_FLAG_CHECKED );
                        break;
                    }
                }
            }
        }
        
        mFriendAdapter = new FriendListAdapter(this, mArrayFriendList, R.layout.friend_list_item, FriendListAdapter.LIST_TYPE_FRIEND) {
            public View setView(int position, View convertView, ViewGroup parent, ViewHolder holder) {
                setFriendViewData(holder, (FriendInfo)getItem(position));
                
                return convertView;
            }
        };
        if(mFriendViewMode == SELECT_ALL_FRIEND) {
            mIvNodata.setVisibility( View.VISIBLE );
            mFriendListView.setEmptyView( mIvNodata );
        } else {
            mIvNodata.setVisibility( View.GONE );
            mFriendListView.setEmptyView( null );
        }        
        mFriendListView.setAdapter( mFriendAdapter );
        // 친구메인에서 친구선택시 바로 쿠폰발송 페이지로 이동하도록 수정. 20130806.jish
        mFriendListView.setOnItemClickListener(mFriendItemClickListener);
    }
    
    private void setViewVisible(ViewHolder holder) {
        holder.mContactListItem.setVisibility( View.VISIBLE );
        holder.mListHeader.setVisibility( View.GONE );
        holder.mButton.setVisibility( View.GONE );
        
        switch(mDisplayState) {
        case DISPLAY_STATE_NORMAL:
            holder.mCheckBox.setVisibility(View.GONE);
            break;
            
        case DISPLAY_STATE_SELECT_MODE:
            holder.mCheckBox.setVisibility(View.VISIBLE);
            break;
        }
    }
    
    private void setFriendViewData(ViewHolder holder, FriendInfo info) {
        setViewVisible(holder);

        if( info.GetIsFirst() ){
            setHeadLine(holder, ""+info.GetChosung());
        } else {
            holder.mListHeader.setVisibility(View.GONE);
        }
        
        String displayName;
        if(info.GetIsName().equals("1")){
            displayName = info.GetName() + "(" + info.GetNickName() + ")";
        } else {
            displayName = info.GetName();
        }
        
        mImageLoader.DisplayImage(info.GetUserImgUrl(), holder.mPhoto);
        holder.mPhoto.setScaleType(ScaleType.FIT_XY);
        holder.mName.setText(displayName);
        
        if(info.isChecked())
            holder.mCheckBox.setChecked( true );
        else
            holder.mCheckBox.setChecked( false );
        
        if(mDisplayState == DISPLAY_STATE_SELECT_MODE)
            holder.mContactListItem.setOnClickListener( mCheckBoxClickListener );
        
    }
    
    private void setHeadLine(ViewHolder holder, String headLine) {
        holder.mListHeader.setVisibility(View.VISIBLE);
        
        holder.mHeadLine.setVisibility(View.VISIBLE);
        holder.mHeadLine.setText(headLine);
    }
    
    private void showMenuDialog(){
        int menuMode = MenuDialog.MENU_NONE_TYPE;
        
        if(mFriendViewMode == SELECT_ALL_FRIEND) {
            if(mArrayFriendList.size() > 0){
                menuMode = MenuDialog.MENU_FRIEND_TYPE_BY_ALL;
            }
        } else if(mFriendViewMode == SELECT_GROUP_FRIEND) {
            if(getGroupCount( mArrayGroupList.get(mSelectGroupPoisition) ) > 0){
                if(getGroupCount(mArrayGroupList.get(mSelectGroupPoisition)) < getGroupCount(null)){
                    menuMode = MenuDialog.MENU_FRIEND_TYPE_BY_GRUOP;
                } else {
                    menuMode = MenuDialog.MENU_FRIEND_TYPE_DEL_ONLY;
                }
            } else {
                menuMode = MenuDialog.MENU_FRIEND_TYPE_ADD_ONLY;
            }
        }
        
        if(menuMode != MenuDialog.MENU_NONE_TYPE){
            MenuDialog menuDialog = new MenuDialog(this, menuMode, new OnClickListener() {
                @Override
                public void onClick( View v ) {
                    Logg.d( "MenuDialog click = " + v.getTag() );
                    
                    if(v.getTag().equals( MenuDialog.CLICK_TYPE_BLOCK )){
                        startActivityForResult( new Intent(FriendMainActivity.this, FriendBlockActivity.class), REQUEST_FRIEND_BLOCK);
                    } else if(v.getTag().equals( MenuDialog.CLICK_TYPE_ADD ) || v.getTag().equals( MenuDialog.CLICK_TYPE_DELETE)){
                        Intent intent = new Intent(FriendMainActivity.this, FriendEditActivity.class);
                        intent.putExtra( IntentDefine.EXTRA_FRIEND_GROUP_ID, mArrayGroupList.get(mSelectGroupPoisition).GetGroupID() );
                        
                        if(v.getTag().equals( MenuDialog.CLICK_TYPE_ADD )){
                            intent.putExtra( IntentDefine.EXTRA_FRIEND_EDIT_TYPE, "add" );
                        } else if (v.getTag().equals( MenuDialog.CLICK_TYPE_DELETE)){
                            intent.putExtra( IntentDefine.EXTRA_FRIEND_EDIT_TYPE, "delete");
                        }
                        startActivityForResult( intent, REQUEST_FRIEND_EDIT);
                    } 
                }
            });
            menuDialog.show();
        }
        
    }
    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_MENU && mDisplayState == DISPLAY_STATE_NORMAL) {
            showMenuDialog();
            return true;
        }

        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected int setContentViewId()
    {
        return R.layout.activity_friend_main;
    }

    @Override
    public void onClick( View v )
    {
        
    }
    
    private class PreGroupSelect {
        int position=0;  // 그룹리스트에서의 index
        View preView = null; // 그룹 리스트의 row.
        
        /***
         * @Brief Select image를 변경.
         */
        public void backToUnSelect() {
            if( preView==null ) {
                return;
            }
            ViewHolder holder = (ViewHolder)preView.getTag();
            holder.mllGroupName.setBackgroundResource(R.drawable.m3_leftmenu2_btn );
            holder.mGroupName.setTextColor(getResources().getColor(R.color.rgb_255_255_255));
            holder.mGroupCount.setTextColor(getResources().getColor(R.color.rgb_255_255_255));
        }
    }
}
