package kr.wisestone.happytong.ui.friend;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.HappytongDB.GroupInfo;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

public abstract class FriendHandler extends Handler
{

    public static final int  FOCUSE_CONTACT_LIST             = 1;
    public static final int  FOCUSE_GROUP_LIST               = 2;

    public static final int  MSG_CONTROL_COMPLETE            = 10;
    public static final int  MSG_CONTROL_DOING               = 20;

    public static final int  MSG_COMPLETE_GROUP_LIST         = 100;
    public static final int  MSG_COMPLETE_FRIEND_LIST        = 101;
    public static final int  MSG_COMPLETE_UNFRIEND_LIST      = 102;
    
    public static final int  DB_CONTROL_START                = 200;
    public static final int  DB_GET_GROUP_LIST_EDIT          = 201;
    public static final int  DB_GET_GROUP_LIST               = 202;
    public static final int  DB_GET_FRIEND_LIST_ALL          = 203;
    public static final int  DB_GET_FRIEND_LIST_GROUP        = 204;
    public static final int  DB_GET_FRIEND_LIST_EXCEPT_GROUP = 205;
    public static final int  DB_GET_INVITE_FRIEND_LIST       = 206;

    public HappytongDBHelper mDbHelper;
    private Context          mContext;
    private Handler          mResHandler;

    ArrayList<FriendInfo>    mFriendList                      = new ArrayList<FriendInfo>();
    ArrayList<GroupInfo>     mGroupList                       = new ArrayList<GroupInfo>();

    private int              mSelectMode                      = 0;

    /***
     * @Brief 생성자
     * @param context
     * @param resHandler
     */
    public FriendHandler(Context context, Handler handler) {
        Logg.d( "FriendHandler" );
        this.mContext = context;
        this.mResHandler = handler;
        
        initListData();
    }
    
    /***
     * @Brief 연락처 리스트 & 그룹 리스트 생성 및 초기화. 
     */
    private void initListData() {
        mDbHelper = new HappytongDBHelper(this.mContext);
    }
    
    public void handleMessage(Message msg) {
        Logg.d( "handleMessage = " + msg.arg1);

        if( msg.what==MSG_CONTROL_COMPLETE) {
            Message newMsg = new Message();
            newMsg.copyFrom(msg);
            mResHandler.sendMessage(newMsg);            
        }
        
        switch(msg.arg1) {
            case DB_CONTROL_START:
                getInitListData();
                break;
            case DB_GET_GROUP_LIST:
            case DB_GET_GROUP_LIST_EDIT:
                executeGetDBGroupList(msg.arg1);
                break;
            case DB_GET_FRIEND_LIST_ALL:
                executeAllFriend();           
                break;
            case DB_GET_FRIEND_LIST_GROUP:
                executeSelectGroupFriend((String)msg.obj);           
                break;
            case DB_GET_FRIEND_LIST_EXCEPT_GROUP:
                executeFriendExceptGroup((String)msg.obj);           
                break;
            case DB_GET_INVITE_FRIEND_LIST:
                executeInviteFriendList();           
                break;
                                
        }
    }
    
    /***
     * @Brief 연락처 & 그룹 리스트의 초기화 
     */
    private void getInitListData() {
        initGroupList();
        initContactList();
    }
    
    /***
     * @Brief 그룹 리스트 초기화 event
     */
    private void initGroupList() {
        sendMsg(MSG_CONTROL_DOING, DB_GET_GROUP_LIST, 0, null);
    }
    
    /***
     * @Brief 연락처 리스트 초기화 event
     */
    private void initContactList() {
        sendMsg(MSG_CONTROL_DOING, DB_GET_FRIEND_LIST_ALL, 0, null);
    }
    
    private void sendMsg(int what, int arg1, int arg2, Object o) {
        Message msg = new Message();
        msg.what = what;
        msg.arg1 = arg1;
        msg.arg2 = arg2;
        msg.obj  = o;
        
        sendMessage(msg);
    }
    
    /***
     * @Brief 전체 친구 리스트 요청    
     */
    private void executeAllFriend() {
        contactListUpdateForNormal();
        sendMsg(MSG_CONTROL_COMPLETE, MSG_COMPLETE_FRIEND_LIST, 0, (Object)mFriendList);
    }
    
    /***
     * @Brief 선택한 그룹의 멤버(연락처) 요청    
     */
    private void executeSelectGroupFriend(String groupID) {
        contactListUpdateGroupFriend(groupID);
        sendMsg(MSG_CONTROL_COMPLETE, MSG_COMPLETE_FRIEND_LIST, 0, (Object)mFriendList);
    }
    
    /***
     * @Brief 선택한 그룹 제외한 친구 리스트 요청    
     */
    private void executeFriendExceptGroup(String groupID) {
        contactListUpdateExceptGroup(groupID);
        sendMsg(MSG_CONTROL_COMPLETE, MSG_COMPLETE_FRIEND_LIST, 0, (Object)mFriendList);
    }
    
    /***
     * @Brief 선택한 그룹 제외한 친구 리스트 요청    
     */
    private void executeInviteFriendList() {
        Logg.d("contactListUpdateExceptGroup");
        
        if(mFriendList != null) mFriendList.clear();
        
        mFriendList = mDbHelper.getInvitationFriendInfo("1", getSearchString());
        
        sendMsg(MSG_CONTROL_COMPLETE, MSG_COMPLETE_FRIEND_LIST, 0, (Object)mFriendList);
    }    

    /***
     * @Brief GroupList 요청
     * @param target
     */
    private void executeGetDBGroupList(int mode) {
        ArrayList<GroupInfo> list = mDbHelper.GetGroupInfoList();
        
        if( mGroupList.size()!=0 ) {
            mGroupList.clear();
        }
        
        if(mode == DB_GET_GROUP_LIST){
            GroupInfo groupList = new GroupInfo("0", mContext.getString(R.string.all));
            mGroupList.add(groupList);
        }
        
        if(list!=null) mGroupList.addAll(list);
        
        if(mode == DB_GET_GROUP_LIST){
        	// GroupInfo groupList = new GroupInfo(""+mGroupList.size()+1,"+");
        	// 친구메인 - 그룹리스트 - 하단 구분 문자열 "+"에서 "..."로 변경. 20130806.jish
            GroupInfo groupList = new GroupInfo(""+mGroupList.size()+1, CommonData.GROUP_LIST_FOOTER_STRING);
            mGroupList.add(groupList);
        }
        
        sendMsg(MSG_CONTROL_COMPLETE, MSG_COMPLETE_GROUP_LIST, 0, (Object)mGroupList);
    }
   
    /***
     * @Brief 기본 연락처 리스트
     * @param Cursor c : 연락처 select query 에 대한 cursor
     */
    private void contactListUpdateForNormal( ) {
        Logg.d("contactListUpdateForNormal");
        
        if(mFriendList != null) mFriendList.clear();
        
        mFriendList = mDbHelper.GetBlockDatabyFriendInfo("0", getSearchString());
        
    }
    
    /***
     * @Brief 선택한 그룹 친구 리스트
     * @param Cursor groupID : 그룹 ID
     */
    private void contactListUpdateGroupFriend( String groupID) {
        Logg.d("contactListUpdateGroupFriend");
        
        if(mFriendList != null) mFriendList.clear();
        
        mFriendList = mDbHelper.GetFriendInfoByGroup(groupID, getSearchString());
        
    }
    
    /***
     * @Brief 선택한 그룹 제외한 친구 리스트
     * @param Cursor groupID : 그룹 ID
     */
    private void contactListUpdateExceptGroup( String groupID) {
        Logg.d("contactListUpdateExceptGroup");
        
        if(mFriendList != null) mFriendList.clear();
        
        mFriendList = mDbHelper.getFriendInfoUnSeleGroup(groupID, getSearchString());
        
    }
    
    public int getSelectMode() {
        return mSelectMode;
    }

    public void setSelectMode(int selectMode) {
        this.mSelectMode = selectMode;
    }
    
    /***
     * @Brief  검색.
     * @return String 검색 하고자 하는 스트링.
     */
    public abstract String getSearchString();
}
