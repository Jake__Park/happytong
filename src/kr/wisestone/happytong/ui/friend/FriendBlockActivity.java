package kr.wisestone.happytong.ui.friend;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.FriendProtocol;
import kr.wisestone.happytong.data.util.ErrorCode;
import kr.wisestone.happytong.data.util.ImageDownloader;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.friend.FriendListAdapter.ViewHolder;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.Logg;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.Toast;

public class FriendBlockActivity extends BaseActivity
{

    private ListView              mFriendListView;
    private FriendListAdapter     mFriendAdapter;

    private FriendHandler         mFriendHandler;
    private BlockHandler          mBlockHandler       = new BlockHandler();
    
    private ArrayList<FriendInfo> mArrayFriendList    = new ArrayList<FriendInfo>(); // 전체 친구 리스트 data
    private FriendInfo   mSelectedFriendInfo;

    private final ImageDownloader imageDownloader = new ImageDownloader();
    
    OnClickListener mBlockClickListener = new OnClickListener() {

        @Override
        public void onClick( View v ) {
            if( mFriendListView==null) {
                return;
            }
            
            int position = mFriendListView.getPositionForView(v);
            if( position<0 ) {
                Logg.d("can not find item from view");
                return;
            }
            
            mSelectedFriendInfo = mArrayFriendList.get(position);
            
            showBlockPopup();
            
        }        

    };
    
    final IWiseCallback iOkFriendBlock = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkFriendBlock");
            //[SDJ 130712] 차단 완료 팝업 추가
            showCompletePopup(R.string.friend_block_complete);
            return false;
        }
    };

    //[SDJ 130712] 차단 에러 처리 루틴 추가
    protected IWiseCallback iFailFriendBlock = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            closeProgressDialog();
            Logg.e("RootActivity iFail!!");
            onProtoColFailResponse((Integer) param.errorCode, param);
            return false;
        }
    };    
    
    @Override
    public void onProtoColFailResponse(int eventId, CallbackParam param) {
        if(param.errorCode == ErrorCode.ERR_HTTP_SERVER_202){
            showCompletePopup(R.string.error_code_202);
        } else {
            showCommonErrorPopup(param.errorCode);
        }
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        findAllViews();

        TitleInfoView tiv = new TitleInfoView( this, (View)findViewById( R.id.title_lay ), TitleInfoView.TITLE_TYPE_NORMAL );
        tiv.updateView( R.string.friend_title_block, TitleInfoView.TITLE_MODE_NORMAL );
		
        //[SDJ 130712] 위치 이동 resume -> create 
        friendListStart();
        
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }
    
    private void friendListStart() {
        if( mFriendHandler==null) {
            mFriendHandler = new FriendHandler(this, mBlockHandler){
                @Override
                public String getSearchString()
                {
                    // TODO Auto-generated method stub
                    return null;
                }
            };
        }
        sendMsg(FriendHandler.MSG_CONTROL_DOING, FriendHandler.DB_GET_FRIEND_LIST_ALL, 0, null);
    }
    
    private void refreshFriendInfo(){
        final HappytongDBHelper hdb = new HappytongDBHelper( this );

        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground( Void... params ) {
                mSelectedFriendInfo.SetName( mSelectedFriendInfo.GetName());
                return hdb.UpdateFriendInfo(mSelectedFriendInfo);
            }

            @Override
            protected void onPostExecute( Integer ret ) {
                if(ret > 0){
                    mArrayFriendList.remove( mSelectedFriendInfo );
                    mFriendAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText( FriendBlockActivity.this, "DB ERORR...", Toast.LENGTH_SHORT ).show();
                }
            }
        }.execute();        
    }
    
    private void sendMsg(int what, int arg1, int arg2, Object obj) {
        Message msg = new Message();
        msg.what = what;
        msg.arg1 = arg1;
        msg.arg2 = arg2;
        msg.obj = obj;

        mFriendHandler.sendMessage( msg );
    }
    
    private void showBlockPopup(){
        DialogUtil.getConfirmCancelDialog(this, R.string.friend_title_block, R.string.friend_message_block, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                   requestFriendBlock(mSelectedFriendInfo);
                   break;
                }
            }
        }).show();
    }

    //[SDJ 130712] 확인 팝업 추가
    private void showCompletePopup(int messageId){
        DialogUtil.getConfirmDialog(this, R.string.friend_title_block, messageId, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mSelectedFriendInfo.Setblock( "1" );
                refreshFriendInfo();
            }
        }).show();        
    }
    
    /***
     * layout에 포함 된 모든 view load.
     */
    private void findAllViews() {
        mFriendListView = (ListView)findViewById( R.id.list );
        mFriendListView.setDivider( null );
    }
    
    @SuppressLint( "HandlerLeak" )
    private class BlockHandler extends Handler {
        
        @SuppressWarnings( "unchecked" )
        public void handleMessage(Message msg) {
            Logg.d( "BlockHandler = " + msg.arg1);
            
            switch(msg.arg1) {
                case FriendHandler.MSG_COMPLETE_FRIEND_LIST:
                    if(mArrayFriendList != null) mArrayFriendList.clear();
                    mArrayFriendList = (ArrayList<FriendInfo>)msg.obj;
                    Logg.d( "SDJ = " +  mArrayFriendList.size());
                    
                    listViewInit();
                    break;
            }
        }
    }
    
    private void listViewInit() {
        mFriendAdapter = new FriendListAdapter(this, mArrayFriendList, R.layout.friend_list_item, FriendListAdapter.LIST_TYPE_FRIEND) {
            public View setView(int position, View convertView, ViewGroup parent, ViewHolder holder) {
                setFriendViewData(holder, (FriendInfo)getItem(position), position);
                
                return convertView;
            }
        };
            
        mFriendListView.setAdapter( mFriendAdapter );
              
    }
    
    private void setFriendViewData(ViewHolder holder, FriendInfo info, int position) {
        setViewVisible(holder);
        
        if( position == 0 ||info.GetChosung() != mArrayFriendList.get( position -1).GetChosung()){
            setHeadLine(holder, ""+info.GetChosung());
        } else {
            holder.mListHeader.setVisibility(View.GONE);
        }
        
        String displayName;
        if(info.GetIsName().equals("1")){
            displayName = info.GetName() + "(" + info.GetNickName() + ")";
        } else {
            displayName = info.GetName();
        }
        
        imageDownloader.DisplayImage( info.GetUserImgUrl(), holder.mPhoto );

        holder.mPhoto.setScaleType(ScaleType.FIT_XY);
        holder.mName.setText(displayName);
        
        holder.mButton.setOnClickListener( mBlockClickListener );
    }
    
    private void setHeadLine(ViewHolder holder, String headLine) {
        holder.mListHeader.setVisibility(View.VISIBLE);
        
        holder.mHeadLine.setVisibility(View.VISIBLE);
        holder.mHeadLine.setText(headLine);
    }

    /***
     * @Brief display state에 따른 view item의 visible 조정
     * @param1 ViewHolder : 리스트 item의 모든 view 값을 가진 class
     * @param2 int : 현 display state 값.
     */
    private void setViewVisible(ViewHolder holder) {
        holder.mContactListItem.setVisibility( View.VISIBLE );
        holder.mListHeader.setVisibility( View.GONE );

        holder.mCheckBox.setVisibility( View.GONE );
        holder.mButton.setVisibility( View.VISIBLE );
    }
    
    private void requestFriendBlock(FriendInfo info){
        Logg.e( " requestFriendBlock GetEmail =  " + info.GetEmail() );
        Logg.e( " requestFriendBlock id =  " + info.GetID() );
        
        FriendProtocol mFriendProtocol = new FriendProtocol( iOkFriendBlock, iFailFriendBlock );
        mFriendProtocol.friendBlock( CallBackParamWrapper.CODE_FRIEND_FRIEND_BLOCK, SharedPrefManager.getLoginEmail(this), info.GetEmail() );
    }
    
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult( RESULT_OK, intent );
        finish();
    }
    
    @Override
    public void onClick( View arg0 ) {
    }

    @Override
    protected int setContentViewId() {
        return R.layout.activity_friend_block;
    }
}
