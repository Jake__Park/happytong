package kr.wisestone.happytong.ui.friend;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.plugin.InviteHelper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.friend.FriendListAdapter.ViewHolder;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.Logg;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FriendInviteActivity extends BaseActivity
{
    final int MAX_INVITE_COUNT = 10;
    private InviteHandler         mBlockHandler         = new InviteHandler();
    private FriendHandler         mFriendHandler        = null;

    private EditText              mEtSearchName;
    private ListView              mFriendListView;                                       
    private FriendListAdapter     mFriendAdapter;
    private TextView			  mTvNodata;

    private ArrayList<FriendInfo> mArrayFriendList      = new ArrayList<FriendInfo>(); // 실제 data
    private ArrayList<FriendInfo> mArrayFriendCheckList = new ArrayList<FriendInfo>(); // 체크 리스트
    
    TextWatcher mTwSearch = new TextWatcher() {
        
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            friendListStart();
        }
        
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        
        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    OnClickListener mCheckClickListener = new OnClickListener() {

        @Override
        public void onClick( View v ) {
            if( mFriendListView==null) {
                return;
            }
            
            int position = mFriendListView.getPositionForView(v);
            if( position<0 ) {
                Logg.d("can not find item from view");
                return;
            }
            
            FriendInfo info = mArrayFriendList.get(position);
            
            setCheckState(info, position);
        }        
    };
    
    OnClickListener mTitleClick = new OnClickListener() {

        @Override
        public void onClick( View v ) {
			Context context = FriendInviteActivity.this;
			if ( mArrayFriendList.size() <= 0 )
			{
				Logg.d( "mTitleClick invite nodata" );
                Toast.makeText( FriendInviteActivity.this, R.string.no_select_friend, Toast.LENGTH_SHORT ).show();
				return;
			}
			if (mArrayFriendCheckList != null && mArrayFriendCheckList.size() == 0) {
				DialogUtil.getConfirmDialog(context, R.string.notification, R.string.error_not_selected_invite_members, null).show();
			} else {
				InviteHelper.getInstance(context).sendSmsMessage(getFriendCheckPhoneNumberList());
			}
        }        
    };
    
    /**
     * 초대 대상 전화번호 복호화하여 반환
     * @return 초대 대상 전화번호들
     */
	private String[] getFriendCheckPhoneNumberList() {
		String[] phoneNumbers = new String[mArrayFriendCheckList.size()];
		for (int i = 0; i < phoneNumbers.length; i++) {
			phoneNumbers[i] = AESEncryption.decrypt(mArrayFriendCheckList.get(i).GetPhoneNumber());
			Logg.d(phoneNumbers[i]);
		}
		return phoneNumbers;
	}
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        commonAreaSet();
        findAllViews();
        friendListStart();
        
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        
    }
    
    private void commonAreaSet(){
        TitleInfoView tiv = new TitleInfoView( this, (View)findViewById( R.id.title_lay ), TitleInfoView.TITLE_TYPE_NORMAL );
        tiv.updateView( R.string.friend_title_invite, TitleInfoView.TITLE_MODE_FRIEND_SELECT, mTitleClick );

    }
    
    private void setCheckState(FriendInfo info, int position){
        if(info.isChecked()){
            info.setCheckBoxFlag( FriendInfo.CHECK_FLAG_UNCHECK);
            mArrayFriendCheckList.remove( info );
        }
        else{
            if(mArrayFriendCheckList.size() >= MAX_INVITE_COUNT){
                Toast.makeText( FriendInviteActivity.this, R.string.error_max_invite_count, Toast.LENGTH_SHORT ).show();
                return;
            } else {
                info.setCheckBoxFlag( FriendInfo.CHECK_FLAG_CHECKED );
                mArrayFriendCheckList.add( info );
            }
        }
        
        mArrayFriendList.set( position, info );

        mFriendAdapter.notifyDataSetChanged();
    }
    
    private void friendListStart() {
        if( mFriendHandler==null){
            mFriendHandler = new FriendHandler(this, mBlockHandler){
                @Override
                public String getSearchString() {
                    String searchStr = mEtSearchName.getText().toString();
                    return (searchStr.length()>0)?searchStr:null;
                }
            };
        }
        
        sendMsg(FriendHandler.MSG_CONTROL_DOING, FriendHandler.DB_GET_INVITE_FRIEND_LIST, 0, 0);

    }
    
    private void sendMsg(int what, int arg1, int arg2, Object obj) {
        Message msg = new Message();
        msg.what = what;
        msg.arg1 = arg1;
        msg.arg2 = arg2;
        msg.obj = obj;

        mFriendHandler.sendMessage( msg );
    }


    
    /***
     * layout에 포함 된 모든 view load.
     */
    private void findAllViews() 
    {
        mEtSearchName = (EditText)findViewById( R.id.et_search_name );
        mEtSearchName.addTextChangedListener( mTwSearch );

        mFriendListView = (ListView)findViewById( R.id.list );
        mFriendListView.setDivider( null );
        
        mTvNodata = (TextView)findViewById( R.id.tvInviteNodata );
    }
    
    @SuppressLint( "HandlerLeak" )
    private class InviteHandler extends Handler{
        
        @SuppressWarnings( "unchecked" )
        public void handleMessage(Message msg) {
            Logg.d( "BlockHandler = " + msg.arg1);
            
            switch(msg.arg1) {
                case FriendHandler.MSG_COMPLETE_FRIEND_LIST:
                    if(mArrayFriendList != null) mArrayFriendList.clear();
                    
                    mArrayFriendList = (ArrayList<FriendInfo>)msg.obj;
                    Logg.d( "mArrayFriendList.size() = " +  mArrayFriendList.size());
                    
                    if ( mArrayFriendList.size() <= 0 ) {
                    	Logg.d( "InviteHandler invite nodata" );
                    	mTvNodata.setVisibility( View.VISIBLE );
                    	mFriendListView.setVisibility( View.GONE );
                    	break;
                    } else  {
                        //[SDJ 130702] 검색어 삭제 시 처리 추가 
                        mTvNodata.setVisibility( View.GONE );
                        mFriendListView.setVisibility( View.VISIBLE );
                    }
                    listViewInit();
                    break;
            }
        }
    }
    
    /***
     * @Brief 연락처 리스트 초기화 및 생성 
     */
    private void listViewInit() {
        // set contact list adapter
        mFriendAdapter = new FriendListAdapter(this, mArrayFriendList, R.layout.friend_list_item, FriendListAdapter.LIST_TYPE_FRIEND) {
            public View setView(int position, View convertView, ViewGroup parent, ViewHolder holder) {
                setFriendViewData(holder, (FriendInfo)getItem(position));
                
                return convertView;
            }
        };
            
        mFriendListView.setAdapter( mFriendAdapter );
              
    }
    
    /***
     * @Brief 기본 형태의 연락처 리스트  setFriendViewData.
     * @param1 ViewHolder : 리스트 item의 모든 view 값을 가진 class.
     * @param2 FriendInfo : 연락처 기본 정보 table.
     */
    private void setFriendViewData(ViewHolder holder, FriendInfo info) {
        setViewVisible(holder);
        
        if( info.GetIsFirst() ){
            setHeadLine(holder, ""+info.GetChosung());
        } else {
            holder.mListHeader.setVisibility(View.GONE);
        }

        holder.mCheckBox.setVisibility(View.VISIBLE);
        if(info.isChecked())
            holder.mCheckBox.setChecked( true );
        else
            holder.mCheckBox.setChecked( false );

        holder.mName.setText(info.GetName());
        
        //[SDJ 130702] 기존 선택된 리스트 Check 활성화
        if(mArrayFriendCheckList != null && mArrayFriendCheckList.size() > 0){
            for(int idx = 0; idx < mArrayFriendCheckList.size(); idx++){
                if(info.GetPhoneNumber().equals( mArrayFriendCheckList.get( idx ).GetPhoneNumber() )){
                    holder.mCheckBox.setChecked( true );
                }
            }
        }
        
        holder.mContactListItem.setOnClickListener( mCheckClickListener );
    }
    
    private void setHeadLine(ViewHolder holder, String headLine) {
        holder.mListHeader.setVisibility(View.VISIBLE);
        
        holder.mHeadLine.setVisibility(View.VISIBLE);
        holder.mHeadLine.setText(headLine);
    }
    
    /***
     * @Brief display state에 따른 view item의 visible 조정
     * @param1 ViewHolder : 리스트 item의 모든 view 값을 가진 class
     * @param2 int : 현 display state 값.
     */
    private void setViewVisible(ViewHolder holder) {
        holder.mContactListItem.setVisibility( View.VISIBLE );
        holder.mListHeader.setVisibility( View.GONE );

        holder.mCheckBox.setVisibility( View.VISIBLE );
        holder.mButton.setVisibility( View.GONE );
    }
    
    @Override
    public void onClick( View v ) {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected int setContentViewId() {
        // TODO Auto-generated method stub
        return R.layout.activity_friend_invite;
    }

}
