package kr.wisestone.happytong.ui.friend;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.data.util.ImageDownloader;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.friend.FriendListAdapter.ViewHolder;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.TextView;

public class FriendEditActivity extends BaseActivity
{
    private BlockHandler          mBlockHandler         = new BlockHandler();
    private FriendHandler         mFriendHandler        = null;

    private ListView              mFriendListView;                                       
    private FriendListAdapter     mFriendAdapter;

    private ArrayList<FriendInfo> mArrayFriendList      = new ArrayList<FriendInfo>(); // 실제 data
    private ArrayList<FriendInfo> mArrayFriendCheckList = new ArrayList<FriendInfo>(); // 체크 리스트
    
    int                           m_nCheckCount         = 0;

    boolean                       bAddMode              = false;
    String                        mGroupId;

    OnClickListener mCheckClickListener = new OnClickListener()
    {

        @Override
        public void onClick( View v )
        {
            if( mFriendListView==null) {
                return;
            }
            
            int position = mFriendListView.getPositionForView(v);
            if( position<0 ) {
                Logg.d("can not find item from view");
                return;
            }
            
            FriendInfo info = mArrayFriendList.get(position);
            
            setCheckState(info, position);
        }        
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        initData();
        findAllViews();
        
        TitleInfoView tiv = new TitleInfoView( this, (View)findViewById( R.id.title_lay ), TitleInfoView.TITLE_TYPE_NORMAL );
        tiv.updateView( R.string.friend_title_select, TitleInfoView.TITLE_MODE_FRIEND_SELECT, this );
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        
        friendListStart();
    }
    
    private void initData(){
        String mMode = getIntent().getStringExtra( IntentDefine.EXTRA_FRIEND_EDIT_TYPE );
        mGroupId = getIntent().getStringExtra( IntentDefine.EXTRA_FRIEND_GROUP_ID );
        
        Logg.d( "mMode = " + mMode );
        Logg.d( "mGroupId = " + mGroupId );
        
        if(mMode.equals( "add" )){
            bAddMode = true;
        } else {
            bAddMode = false;
        }

    }
    
    private void setCheckState(FriendInfo info, int position){
        if(info.isChecked()){
            info.setCheckBoxFlag( FriendInfo.CHECK_FLAG_UNCHECK);
            mArrayFriendCheckList.remove( info );
        }
        else{
            info.setCheckBoxFlag( FriendInfo.CHECK_FLAG_CHECKED );
            mArrayFriendCheckList.add( info );
        }
        
        mArrayFriendList.set( position, info );

        TextView tvSelectName = (TextView)findViewById( R.id.tv_select_name );
        int size = mArrayFriendCheckList.size();
        String mNameText;
        if(size > 0 ){
            mNameText = mArrayFriendCheckList.get( 0 ).GetName();
            
            if(size > 1)
            	mNameText = getString(R.string.coupon_receivers_formatted_text_with_receiver, mNameText, String.valueOf(size-1));
                // mNameText = mNameText + "외 " + ""+ (size-1) + "명";
        } else {
            mNameText = "";
        }
        
        tvSelectName.setText( mNameText );
        mFriendAdapter.notifyDataSetChanged();
    }
    
    private void friendListStart() {
        if( mFriendHandler==null) 
            mFriendHandler = new FriendHandler(this, mBlockHandler){
            @Override
            public String getSearchString() {
                // TODO Auto-generated method stub
                return null;
            }
        };
        if(bAddMode){
            sendMsg(FriendHandler.MSG_CONTROL_DOING, FriendHandler.DB_GET_FRIEND_LIST_EXCEPT_GROUP, 0, mGroupId);
        } else {
            sendMsg(FriendHandler.MSG_CONTROL_DOING, FriendHandler.DB_GET_FRIEND_LIST_GROUP, 0, mGroupId);
        }

    }
    
    private void sendMsg(int what, int arg1, int arg2, Object obj) {
        Message msg = new Message();
        msg.what = what;
        msg.arg1 = arg1;
        msg.arg2 = arg2;
        msg.obj = obj;

        mFriendHandler.sendMessage( msg );
    }


    
    /***
     * layout에 포함 된 모든 view load.
     */
    private void findAllViews() 
    {
        mFriendListView = (ListView)findViewById( R.id.list );
        mFriendListView.setDivider( null );
    }
    
    @SuppressLint( "HandlerLeak" )
    private class BlockHandler extends Handler{
        
        @SuppressWarnings( "unchecked" )
        public void handleMessage(Message msg) {
            Logg.d( "BlockHandler = " + msg.arg1);
            
            switch(msg.arg1) {
                case FriendHandler.MSG_COMPLETE_FRIEND_LIST:
                    if(mArrayFriendList != null) mArrayFriendList.clear();
                    mArrayFriendList = (ArrayList<FriendInfo>)msg.obj;
                    Logg.d( "SDJ = " +  mArrayFriendList.size());
                    listViewInit();
                    break;
            }
        }
    }
    
    /***
     * @Brief 연락처 리스트 초기화 및 생성 
     */
    private void listViewInit() {
        // set contact list adapter
        mFriendAdapter = new FriendListAdapter(this, mArrayFriendList, R.layout.friend_list_item, FriendListAdapter.LIST_TYPE_FRIEND) {
            public View setView(int position, View convertView, ViewGroup parent, ViewHolder holder) {
                setFriendViewData(holder, (FriendInfo)getItem(position));
                
                return convertView;
            }
        };
            
        mFriendListView.setAdapter( mFriendAdapter );
              
    }
    
    /***
     * @Brief 기본 형태의 연락처 리스트  setFriendViewData.
     * @param1 ViewHolder : 리스트 item의 모든 view 값을 가진 class.
     * @param2 FriendInfo : 연락처 기본 정보 table.
     */
    private void setFriendViewData(ViewHolder holder, FriendInfo info) {
        setViewVisible(holder);
        ImageDownloader imageLoader = new ImageDownloader();
        
        if( info.GetIsFirst() ){
            setHeadLine(holder, ""+info.GetChosung());
        } else {
            holder.mListHeader.setVisibility(View.GONE);
        }
        
        String displayName;
        if(info.GetIsName().equals("1")){
            displayName = info.GetName() + "(" + info.GetNickName() + ")";
        } else {
            displayName = info.GetName();
        }

        imageLoader.DisplayImage(info.GetUserImgUrl(), holder.mPhoto);
        holder.mPhoto.setScaleType(ScaleType.FIT_XY);
        holder.mName.setText(displayName);
        
        if(info.isChecked())
            holder.mCheckBox.setChecked( true );
        else
            holder.mCheckBox.setChecked( false );
        
        holder.mContactListItem.setOnClickListener( mCheckClickListener );
    }
    
    private void setHeadLine(ViewHolder holder, String headLine) {
        holder.mListHeader.setVisibility(View.VISIBLE);
        
        holder.mHeadLine.setVisibility(View.VISIBLE);
        holder.mHeadLine.setText(headLine);
    }
    
    /***
     * @Brief display state에 따른 view item의 visible 조정
     * @param1 ViewHolder : 리스트 item의 모든 view 값을 가진 class
     * @param2 int : 현 display state 값.
     */
    private void setViewVisible(ViewHolder holder) {
        holder.mContactListItem.setVisibility( View.VISIBLE );
        holder.mListHeader.setVisibility( View.GONE );

        holder.mCheckBox.setVisibility( View.VISIBLE );
        holder.mButton.setVisibility( View.GONE );
    }
    
    private void refreshFriendData(){
        final HappytongDBHelper hdb = new HappytongDBHelper( this );
        
        new AsyncTask<Void, Void, Integer>()
        {
            @Override
            protected Integer doInBackground( Void... params )
            {
                if(bAddMode){
                    return hdb.InsertGroupList(mArrayFriendCheckList, mGroupId);
                } 
                return hdb.DeleteGroupList( mArrayFriendCheckList, mGroupId);
            }

            @Override
            protected void onPostExecute( Integer ret )
            {
                Logg.d( "onPostExecute!@!!!!!! =  "+ ret);
                if(ret > 0){
                    Intent intent = new Intent();
                    setResult( RESULT_OK, intent );
                    finish();
                } else {
                    
                }
            }
        }.execute();
    }

    @Override
    public void onClick( View v )
    {
        if(v.getTag().equals(TitleInfoView.TITLE_RIGHT_BTN )){
            Logg.d( " mArrayFriendCheckList size = " + mArrayFriendCheckList.size() );
            
            refreshFriendData();
        }
    }

    @Override
    protected int setContentViewId()
    {
        return R.layout.activity_friend_edit;
    }
}
