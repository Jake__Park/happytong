package kr.wisestone.happytong.ui.friend;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.db.HappytongDB.GroupInfo;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.cbox.CouponBoxType;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.dialog.DialogUtil.OnEditTextDialogListener;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.Logg;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class GroupEditActivity extends BaseActivity
{
    private EditMainHandler      mEditMainHandler     = new EditMainHandler();
    private FriendHandler        mFriendHandler       = null;

    private ListView             mGroupListView;
    private TextView             mTvNodata;
    private GroupListAdapter     mFriendAdapter;

    private ArrayList<GroupInfo> mArrGroupList        = new ArrayList<GroupInfo>();

    GroupInfo                    mTempGroup;

    boolean                      bEditNameMode        = false;    
   
    OnClickListener mEditClickListener = new OnClickListener()
    {
        @Override
        public void onClick( View v )
        {
            int position = mGroupListView.getPositionForView(v);
            mTempGroup = mArrGroupList.get(position);
            
            bEditNameMode = true;
            DialogUtil.getEditTextDialog( GroupEditActivity.this, 
                    R.string.group_edit_popup_title,
                    mTempGroup.GetGroupName(),
                    R.string.group_add_message,
                    mEtListener)
                    .show();
        }        
    };
    
    OnClickListener mDeleteClickListener = new OnClickListener()
    {

        @Override
        public void onClick( View v )
        {
            int position = mGroupListView.getPositionForView(v);
            mArrGroupList.get(position);
            
            deleteGroup(mArrGroupList.get(position));
        }        
    };
    
    OnEditTextDialogListener mEtListener = new OnEditTextDialogListener()
    {
        @Override
        public void onEditTextDialogOk( String text )
        {
            
            if(bEditNameMode){
                //그룹명 수정
                editGroupAsync(text);
            } else {
                //그룹 생성
                if(text.length() > 0)
                    createGroupAsync(text);
            }
        }
        
        @Override
        public void onEditTextDialogCancel()
        {
        }
    };
    
    OnClickListener mTitleClickListener = new OnClickListener()
    {
        @Override
        public void onClick( View v )
        {
            if(v.getTag().equals( TitleInfoView.TITLE_RIGHT_BTN )){
                bEditNameMode = false;

                DialogUtil.getEditTextDialog( GroupEditActivity.this, R.string.group_add, "", R.string.group_add_message, mEtListener).show();
            }
        }        

    };
    
    private void createGroupAsync(final String groupname){
        
        final HappytongDBHelper hdb = new HappytongDBHelper( this );
        
        new AsyncTask<Void, Void, Integer>()
        {
            @Override
            protected Integer doInBackground( Void... params )
            {
                return hdb.InsertGroupInfo(groupname);
            }

            @Override
            protected void onPostExecute( Integer ret )
            {
                if(ret > 0){
					//SDJ[130812] 개선사항 적용
                    showAddCompletePopup(groupname);

                    friendListStart();
                } else {
                    Toast.makeText( GroupEditActivity.this, R.string.fail_create_group, Toast.LENGTH_SHORT ).show();
                }
            }
        }.execute();
    }
    
	//SDJ[130812] 개선사항 적용
    private void showAddCompletePopup(final String groupName){
        DialogUtil.getConfirmCancelDialog(GroupEditActivity.this, 
                                     getResources().getString( R.string.info ), 
                                     String.format( getString(R.string.txt_add_group_success), groupName), 
                                     new DialogInterface.OnClickListener() {
                                        
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if(which != DialogInterface.BUTTON_POSITIVE){
                                                return;
                                            }
                                            Intent intent =  new Intent();
                                            intent.putExtra( String.class.toString(), mArrGroupList.size() );
                                            setResult(CommonData.GROUP_ADD_OK, intent);
                                            finish();
                                        }
                                    }).show();
    }
    
    private void editGroupAsync(final String groupname){
        
        final HappytongDBHelper hdb = new HappytongDBHelper( this );
        
        new AsyncTask<Void, Void, Integer>()
        {
            @Override
            protected Integer doInBackground( Void... params )
            {
                if(TextUtils.isEmpty( groupname )){
                    return -1;
                } else {
                    for(GroupInfo info : mArrGroupList){
                        if(groupname.equals( info.GetGroupName() )){
                            return -1;
                        }
                    }
                    mTempGroup.SetGroupName( groupname );
                    Logg.e( "mTempGroup! =  "+ mTempGroup.GetGroupID());
                    Logg.e( "mTempGroup!!! =  "+ mTempGroup.GetGroupName());
                    
                }
                return hdb.UpdateGroupInfo(mTempGroup);
            }

            @Override
            protected void onPostExecute( Integer ret )
            {
                Logg.e( "onPostExecute!@!!!!!! =  "+ ret);
                if(ret > 0){
                    DialogUtil.getConfirmDialog( GroupEditActivity.this, 
                            getResources().getString( R.string.info ), 
                            String.format( getString(R.string.txt_edit_group_success), groupname), 
                            null)
                            .show();

                    friendListStart();
                } else {
                    Toast.makeText( GroupEditActivity.this, R.string.fail_edit_group, Toast.LENGTH_SHORT ).show();
                }
            }
        }.execute();
    }
    
    private void deleteGroupAsync(final GroupInfo groupinfo){
        
        Logg.e( "deleteGroupAsync GetGroupID = "+groupinfo.GetGroupID());
        Logg.e( "deleteGroupAsync GetGroupName = "+groupinfo.GetGroupName());
        
        new AsyncTask<Void, Void, Integer>()
        {
            @Override
            protected Integer doInBackground( Void... params )
            {
                
                return new HappytongDBHelper( GroupEditActivity.this ).DeleteGroupInfo(groupinfo);
            }

            @Override
            protected void onPostExecute( Integer ret )
            {
                Logg.e( "onPostExecute = " +ret );
                if(ret > 0){
                    DialogUtil.getConfirmDialog( GroupEditActivity.this, 
                            getResources().getString( R.string.info ), 
                            String.format( getString(R.string.txt_delete_group_success), groupinfo.GetGroupName()), 
                            null)
                            .show();

                    friendListStart();

                } else {
                    
                }
            }
        }.execute();
    }
    
    private void deleteGroup(final GroupInfo groupinfo){
        DialogUtil.getConfirmCancelDialog( GroupEditActivity.this, 
                getResources().getString( R.string.group_delete ),
                String.format( getString(R.string.txt_delete_group_ask), groupinfo.GetGroupName()),
                getResources().getString( R.string.confirm ),
                getResources().getString( R.string.cancel ),
                new DialogInterface.OnClickListener()
                {
                    
                    @Override
                    public void onClick( DialogInterface dialog, int which )
                    {
                        if(which == DialogInterface.BUTTON_POSITIVE) {
                            deleteGroupAsync( groupinfo );
                        }
                    }
                }).show();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        findAllViews();
        
        TitleInfoView tiv = new TitleInfoView( this, (View)findViewById( R.id.title_lay ), TitleInfoView.TITLE_TYPE_NORMAL );
        tiv.updateView( R.string.group_edit, TitleInfoView.TITLE_MODE_GROUP_EDIT, mTitleClickListener );
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        
        friendListStart();
    }
    
    private void friendListStart() {
        mFriendHandler = new FriendHandler(this, mEditMainHandler){
            @Override
            public String getSearchString() {
                return null;
            }
        };
        sendMsg(FriendHandler.MSG_CONTROL_DOING, FriendHandler.DB_GET_GROUP_LIST_EDIT, 0, null);
    }
    
    private void sendMsg(int what, int arg1, int arg2, Object obj) {
        Message msg = new Message();
        msg.what = what;
        msg.arg1 = arg1;
        msg.arg2 = arg2;
        msg.obj = obj;

        mFriendHandler.sendMessage( msg );
    }


    
    /***
     * layout에 포함 된 모든 view load.
     */
    private void findAllViews() 
    {
        mTvNodata = (TextView)findViewById(R.id.tvNodata);
        
        mGroupListView = (ListView)findViewById( R.id.lv_groupList );
        mGroupListView.setDivider( null );
    }
    
    @SuppressLint( "HandlerLeak" )
    private class EditMainHandler extends Handler{
        
        /***
         * @Brief hander 분기.
         * @return Bitmap : default photo
         */
        @SuppressWarnings( "unchecked" )
        public void handleMessage(Message msg) {
            Logg.d( "BlockHandler = " + msg.arg1);
            
            switch(msg.arg1) {
                case FriendHandler.MSG_COMPLETE_GROUP_LIST:
                    if(mArrGroupList != null) mArrGroupList.clear();
                    mArrGroupList = (ArrayList<GroupInfo>)msg.obj;
                    Logg.d( "SDJ = " +  mArrGroupList.size());
                    listViewInit();
                    break;
            }
        }
    }
    
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult( RESULT_OK, intent );
        finish();
    }
    
    /***
     * @Brief 연락처 리스트 초기화 및 생성 
     */
    private void listViewInit() {
        // set contact list adapter
        mFriendAdapter = new GroupListAdapter(this, mArrGroupList);
        mGroupListView.setAdapter( mFriendAdapter );
        
		//[SDJ 131015]리스트 없을 경우 안내 문구 추가		
        if(mArrGroupList == null || mArrGroupList.size() == 0){
            mTvNodata.setVisibility( View.VISIBLE );
            mGroupListView.setEmptyView( mTvNodata );
        } else {
            mTvNodata.setVisibility( View.GONE );
            mGroupListView.setEmptyView( null );
        }
    }
    
    @Override
    public void onClick( View v )
    {
    }

    @Override
    protected int setContentViewId()
    {
        // TODO Auto-generated method stub
        return R.layout.activity_group_edit;
    }
    
    class GroupListAdapter extends BaseAdapter {

        Context mContext;
        LayoutInflater mInflater;
        ArrayList<GroupInfo> mArrGroupInfo;

        CouponBoxType mCouponBoxType;

        public GroupListAdapter(Context mContext, ArrayList<GroupInfo> arrGroupInfo) {
            super();
            this.mContext = mContext;
            this.mArrGroupInfo = arrGroupInfo;
            this.mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mArrGroupInfo.size();
        }

        @Override
        public GroupInfo getItem(int position) {
            return mArrGroupInfo.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.group_edit_list_item, null);
                
                holder = new Holder();
                // holder.btnEdit = (Button) convertView.findViewById(R.id.item_btn_edit);
                // 연필이미지, 텍스트 영역시에도 그룹 이름 편집가능하도록 수정. 20130806.jish
                holder.btnEdit = convertView.findViewById(R.id.item_btn_edit);
                holder.btnDelete = (Button) convertView.findViewById(R.id.item_btn_delete);
                holder.name = (TextView) convertView.findViewById(R.id.item_text);
                
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            GroupInfo data = mArrGroupInfo.get(position);
            
            holder.name.setText(data.GetGroupName());

            holder.btnEdit.setOnClickListener( mEditClickListener );
            holder.btnDelete.setOnClickListener( mDeleteClickListener );
            
            return convertView;
        }

        class Holder {
        	// Button btnEdit;
        	// 연필이미지, 텍스트 영역시에도 그룹 이름 편집가능하도록 수정. 20130806.jish
            View btnEdit;
            Button btnDelete;
            TextView name;
        }
    }

}
