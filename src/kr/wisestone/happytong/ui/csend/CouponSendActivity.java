package kr.wisestone.happytong.ui.csend;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.OrderCouponPurchased;
import kr.wisestone.happytong.data.dataset.PurchaseInfoData;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.CouponProtocol;
import kr.wisestone.happytong.data.protocol.PurchaseProtocol;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.DummyMainActivity;
import kr.wisestone.happytong.ui.cbox.CouponType;
import kr.wisestone.happytong.ui.csend.CouponSendSideMenu.CouponSendSideMenuAnimationListener;
import kr.wisestone.happytong.ui.csend.CouponSendSideMenu.CouponSendSideMenuListener;
import kr.wisestone.happytong.ui.cshop.CouponShopActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.MenuInfoView;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CouponSendActivity extends BaseActivity implements CouponSendSideMenuListener {

	static final int		DATE_DIALOG_ID	= 0;

	Context					mContext;

	CouponSendSideMenu		mSideMenu;

	TextView				mTextViewFriends;
	TextView				mTextViewValid;
	TextView				mTextViewCouponCount;

	EditText				mEditTextCouponContents0;
	EditText				mEditTextCouponContents1;
	EditText				mEditTextCouponContents2;
	EditText				mEditTextCouponContents3;

	TextWatcher				mEditTextWatcher0;
	TextWatcher				mEditTextWatcher1;
	TextWatcher				mEditTextWatcher2;
	TextWatcher				mEditTextWatcher3;

	ImageView				mImageViewCoupon;

	Calendar				mCalendar;

	OrderCouponPurchased	mCouponData;

	ImageLoader				mImageLoader;

	ArrayList<FriendInfo>	mCouponReceivableFriendList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = this;

		initTitleView();
		init();
		initCoupon();

		setCouponCount();

		mSideMenu = new CouponSendSideMenu(this, findViewById(R.id.coupon_send_sidemenu), findViewById(R.id.coupon_send_sidemenu_content), findViewById(R.id.cover), this, iFail);
		new MenuInfoView(this, (View) findViewById(R.id.menu_tab), CommonData.ENTRY_PONINT_CSEND);

		setCouponReceivableFriendList(getIntent());
	}

	private void setCouponCount() {
		final int cnt = getCouponCountFromPreference();
		mTextViewCouponCount.setText(String.valueOf(cnt));
	}

	private int getCouponCountFromPreference() {
		SharedPrefManager spm = SharedPrefManager.getInstance(mContext);
		int owlFree = spm.getFreeOwlCount();
		int owlPaid = spm.getPurchasedOwlCount();
		return owlFree + owlPaid;
	}

	@SuppressWarnings("unchecked")
	private void setCouponReceivableFriendList(Intent intent) {

		mCouponReceivableFriendList = (ArrayList<FriendInfo>) intent.getSerializableExtra(IntentDefine.EXTRA_FRIEND_SELECT_LIST);
		if (mCouponReceivableFriendList == null) {
			mCouponReceivableFriendList = new ArrayList<FriendInfo>();
		} else {
			setFriendsTextView();
		}

	}

	private void setFriendsTextView() {
		if (mCouponReceivableFriendList.size() > 0) {
			StringBuilder sb = new StringBuilder();
			FriendInfo info = mCouponReceivableFriendList.get(0);
			sb.append(info.GetName());
			sb.append("(").append(info.GetNickName()).append(")");
			if (mCouponReceivableFriendList.size() > 1) {
				sb.append(getString(R.string.coupon_receivers_formatted_text, String.valueOf(mCouponReceivableFriendList.size() - 1)));
			}
			mTextViewFriends.setText(sb.toString());
		} else {
			mTextViewFriends.setText("");
		}
	}

	private void init() {
		mTextViewFriends = (TextView) findViewById(R.id.receive_friend_list);

		mTextViewValid = (TextView) findViewById(R.id.coupon_valid);
		mTextViewValid.setOnClickListener(this);

		findViewById(R.id.receive_friend_select).setOnClickListener(this);
		findViewById(R.id.coupon_send).setOnClickListener(this);

		mImageViewCoupon = (ImageView) findViewById(R.id.coupon_image);
		mImageViewCoupon.setOnClickListener(this);

		mEditTextCouponContents0 = (EditText) findViewById(R.id.title0);
		mEditTextCouponContents1 = (EditText) findViewById(R.id.title1);
		mEditTextCouponContents2 = (EditText) findViewById(R.id.title2);
		mEditTextCouponContents3 = (EditText) findViewById(R.id.title3);

		mEditTextWatcher0 = new CouponTextWatcher(mEditTextCouponContents1, mEditTextCouponContents2, mEditTextCouponContents3);
		mEditTextWatcher1 = new CouponTextWatcher(mEditTextCouponContents0, mEditTextCouponContents2, mEditTextCouponContents3);
		mEditTextWatcher2 = new CouponTextWatcher(mEditTextCouponContents0, mEditTextCouponContents1, mEditTextCouponContents3);
		mEditTextWatcher3 = new CouponTextWatcher(mEditTextCouponContents0, mEditTextCouponContents1, mEditTextCouponContents2);

		mCalendar = Calendar.getInstance();

		mImageLoader = new ImageLoader(this);

		mTextViewCouponCount = (TextView) findViewById(R.id.coupon_count);
	}

	private void initCoupon() {
		SharedPrefManager spMgr = SharedPrefManager.getInstance(this);
		String savedOcp = spMgr.getRecentSelectedCoupon();

		if (TextUtils.isEmpty(savedOcp)) {
			// 쿠폰 보내기 처음 - 디폴트 이미지, 유효기간 없음. 쿠폰 보내기 비활성화. 알림 필요
			mTextViewValid.setVisibility(View.GONE);
			activeEditTextByCouponType(CouponType.NA);
		} else {
			// 안드로이드를 아이폰처럼 선택한 쿠폰 이미지 유지하도록 수정. 20130806.jish
			onCouponItemSelected(OrderCouponPurchased.convertStringToData(savedOcp));
		}
	}

	/**
	 * 쿠폰 타입에 맞게 쿠폰내용입력란 처리
	 * 
	 * @param couponType
	 *            (쿠폰의 타입 0:가운데, 1:왼쪽, 2:오른쪽)
	 */
	private void activeEditTextByCouponType(CouponType couponType) {
		mEditTextCouponContents0.setVisibility(View.GONE);
		mEditTextCouponContents1.setVisibility(View.GONE);
		mEditTextCouponContents2.setVisibility(View.GONE);
		mEditTextCouponContents3.setVisibility(View.GONE);

		// 재귀호출 되므로 Wathcer제거후 보여지는 EditText에만 Watcher추가
		mEditTextCouponContents0.removeTextChangedListener(mEditTextWatcher0);
		mEditTextCouponContents1.removeTextChangedListener(mEditTextWatcher1);
		mEditTextCouponContents2.removeTextChangedListener(mEditTextWatcher2);
		mEditTextCouponContents3.removeTextChangedListener(mEditTextWatcher3);
		switch (couponType) {
		case Center:
			mEditTextCouponContents0.setVisibility(View.VISIBLE);
			mEditTextCouponContents0.addTextChangedListener(mEditTextWatcher0);
			Utils.showKeypad(this, mEditTextCouponContents0);
			break;
		case Up:
			mEditTextCouponContents1.setVisibility(View.VISIBLE);
			mEditTextCouponContents1.addTextChangedListener(mEditTextWatcher1);
			Utils.showKeypad(this, mEditTextCouponContents1);
			break;
		case Left:
			mEditTextCouponContents2.setVisibility(View.VISIBLE);
			mEditTextCouponContents2.addTextChangedListener(mEditTextWatcher2);
			Utils.showKeypad(this, mEditTextCouponContents2);
			break;
		case Right:
			mEditTextCouponContents3.setVisibility(View.VISIBLE);
			mEditTextCouponContents3.addTextChangedListener(mEditTextWatcher3);
			Utils.showKeypad(this, mEditTextCouponContents3);
			break;
		default:
			break;
		}
	}

	private boolean isEmptyEditTexts() {
		if (TextUtils.isEmpty(mEditTextCouponContents1.getText().toString())) {
			return true;
		}
		if (TextUtils.isEmpty(mEditTextCouponContents2.getText().toString())) {
			return true;
		}
		if (TextUtils.isEmpty(mEditTextCouponContents3.getText().toString())) {
			return true;
		}
		return false;
	}

	private void initTitleView() {
		TitleInfoView titleInfoView = new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL);
		// 메뉴하고 상단 타이틀 제목 맞추기(친구리스트:친구, 쿠폰발송:쿠폰보내기) - 20130806.jish
		titleInfoView.updateView(R.string.coupon_send, TitleInfoView.TITLE_MODE_NORMAL);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.coupon_image:
			sideMenuOpenClose();
			break;
		case R.id.coupon_send_sidemenu_handle:
			Logg.d("R.id.coupon_send_sidemenu_handle - onClick");
			sideMenuOpenClose();
			break;
		case R.id.coupon_valid:
			showDialog(DATE_DIALOG_ID);
			break;
		case R.id.receive_friend_select:
			startFriendSelectActivityForResult();
			break;
		case R.id.coupon_send:
			if (isValid()) {
				requestNetworkForCouponSend();
			}
			break;
		}
	}

	private void sideMenuOpenClose() {
		if (mSideMenu.isAnimating()) {
			return;
		}
		if (mSideMenu.isOpen()) {
			mSideMenu.closeSideMenu();
		} else {
			mSideMenu.openSideMenu();
		}
	}

	private void startFriendSelectActivityForResult() {
		// Intent intent = new Intent(this, CouponSendFriendSelectActivity.class);
		Intent intent = new Intent(this, CouponSendFriendSearchSelectActivity.class);
		intent.putExtra(IntentDefine.EXTRA_FRIEND_SELECT_LIST, mCouponReceivableFriendList);
		startActivityForResult(intent, IntentDefine.REQCODE_GET_FRIENDS);
	}

	@Override
	public void onCouponItemSelected(OrderCouponPurchased data) {
		mCouponData = data;

		Logg.d("onCouponItemSelected : " + data.couponType + " / " + data.couponSEQ + " / " + data.couponImageURL + " / " + data.productName);
		// 쿠폰 이미지 변경
		mImageLoader.DisplayImage(data.couponImageURL, mImageViewCoupon);
		// 유효기간 영역 활성화
		mTextViewValid.setVisibility(View.VISIBLE);
		// 쿠폰 타입에 맞게 입력란 활성화
		activeEditTextByCouponType(CouponType.getCouponType(data.couponType));
	}

	@Override
	public void onCouponShopButtonClick() {
		mSideMenu.closeSideMenu(sideMenuCloseL);
	}

	final CouponSendSideMenuAnimationListener	sideMenuCloseL	= new CouponSendSideMenuAnimationListener() {
																	@Override
																	public void onCloseAnimationEnd() {
																		startCouponShopActivityForResult();
																	}
																};

	private void startCouponShopActivityForResult() {
		Intent shopIntent = new Intent(this, CouponShopActivity.class);
		shopIntent.putExtra(IntentDefine.EXTRA_CSEND_STARTED, true);
		startActivityForResult(shopIntent, IntentDefine.REQCODE_MOVE_SHOP);
	}

	@Override
	public void onCouponImageUpdated() {
		// 우측 사이드메뉴 쿠폰 네트워크 완료 후 콜백
		closeProgressDialog();
	}

	private void requestNetworkForCouponSend() {
		Logg.d("requestNetworkForCouponSend()");

		String[] receiverIds = new String[mCouponReceivableFriendList.size()];
		for (int i = 0; i < mCouponReceivableFriendList.size(); i++) {
			receiverIds[i] = mCouponReceivableFriendList.get(i).GetEmail();
		}

		new CouponProtocol(couponSendingOk, iFail).couponSending(CallBackParamWrapper.CODE_COUPON_SENDING, mCouponData.couponSEQ, SharedPrefManager.getLoginEmail(mContext),
				getFriendIdsForServerString(receiverIds), mEditTextCouponContents1.getText().toString(), mTextViewValid.getTag().toString());
		showProgressDialog();
	}

	private String getFriendIdsForServerString(String... args) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			if (i != 0) {
				sb.append("#");
			}
			sb.append(args[i]);
		}
		return sb.toString();
	}

	final IWiseCallback	couponSendingOk	= new IWiseCallback() {
											@Override
											public boolean WiseCallback(CallbackParam param) {

												// 안드로이드를 아이폰처럼 선택한 쿠폰 이미지 유지하도록 수정. 20130806.jish
												SharedPrefManager.getInstance(mContext).setRecentSelectedCoupon(OrderCouponPurchased.convertDataToString(mCouponData));

												// 부분실패 여부를 조사하고, 다음 페이지로 이동을 하거나 팝업 생성후 다음 페이지 이동 처리
												if (param.param1 != null) {
													@SuppressWarnings("unchecked")
													List<BasicNameValuePair> list = (List<BasicNameValuePair>) param.param1;
													String totalCount = String.valueOf(mCouponReceivableFriendList.size());
													String failCount = String.valueOf(list.size());

													DialogUtil.getConfirmDialog(mContext, "", getString(R.string.coupon_send_part_fail, totalCount, failCount),
															new DialogInterface.OnClickListener() {
																@Override
																public void onClick(DialogInterface dialog, int which) {
																	requestStampRemain();
																}
															}).show();
												} else {
													requestStampRemain();
												}
												return false;
											}
										};

	private void requestStampRemain() {
		PurchaseProtocol protocol = new PurchaseProtocol(new IWiseCallback() {
			@Override
			public boolean WiseCallback(CallbackParam param) {
				PurchaseInfoData data = (PurchaseInfoData) param.param1;

				setGoldEggCount(data.getGoldCount());
				setOwlCount(data.getFreeStampCount(), data.getPaidStampCount());

				closeProgressDialog();

				Toast.makeText(mContext, R.string.complete_send_coupon, Toast.LENGTH_SHORT).show();

				SharedPrefManager.getInstance(mContext).setMoveCBoxType("1");

				Intent mainIntent = new Intent(mContext, DummyMainActivity.class);
				mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				mainIntent.putExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_CBOX);
				mContext.startActivity(mainIntent);

				return false;
			}
		}, iFail);
		protocol.orderStampRemain(0, SharedPrefManager.getLoginEmail(this));
	}

	private boolean isValid() {
		if (mCouponReceivableFriendList.size() == 0) {
			// 받는 친구 선택되지 않음
			DialogUtil.getConfirmDialog(this, "", getString(R.string.coupon_receiver_select), null).show();
			return false;
		}
		if (mCouponData == null) {
			// 쿠폰 데이터 설정 안됐음. 쿠폰 이미지 선택하라는 메시지 표시
			DialogUtil.getConfirmDialog(this, "", getString(R.string.coupon_image_not_setted), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mSideMenu.openSideMenu();
				}
			}).show();
			return false;
		}
		if (isEmptyEditTexts()) {
			// 내용 입력 안됨
			DialogUtil.getConfirmDialog(this, "", getString(R.string.coupon_text_not_setted), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			}).show();
			return false;
		}
		if (mTextViewValid.getTag() == null) {
			// 유효기간 설정안됨
			DialogUtil.getConfirmDialog(this, "", getString(R.string.input_valid_date), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			}).show();
			return false;
		}
		if (mCouponReceivableFriendList.size() > getCouponCountFromPreference()) {
			// 부엉이 부족할 경우 -- 쿠폰대상친구들 > 부엉이
			DialogUtil.getConfirmCancelDialog(this, R.string.shortage_owl, R.string.want_to_move_shoppage_for_buy_owl, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (which == DialogInterface.BUTTON_POSITIVE) {
						startCouponShopActivityForResult();
					}
				}
			}).show();
			return false;
		}

		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_CANCELED) {
			return;
		}

		if (requestCode == IntentDefine.REQCODE_GET_FRIENDS) {
			// 친구리스트 반환
			setCouponReceivableFriendList(data);
			return;
		}

		if (requestCode == IntentDefine.REQCODE_BUY_OWL) {
			// 쿠폰샵 이동
			setCouponCount();
			setRecentPage();
			return;
		}

		if (requestCode == IntentDefine.REQCODE_MOVE_SHOP) {
			// 쿠폰샵 이동
			setCouponCount();
			showProgressDialog();
			mSideMenu.updateMyCouponImages();
			setRecentPage();
			return;
		}
	}

	/**
	 * 쿠폰샵, 업체샵으로 이동하고 발송페이지에서 종료시, 재진입시 쿠폰샵이나 업체샵으로 페이지 열림. 이를 해결하기 위한 최신 접근 페이지 갱신
	 */
	private void setRecentPage() {
		SharedPrefManager.getInstance(mContext).setBeforeEntryPoint(CommonData.ENTRY_PONINT_CSEND);
	}

	@Override
	public void onBackPressed() {
		if (mSideMenu.isOpen()) {
			mSideMenu.closeSideMenu();
		} else {
			backKeyToExit();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle args) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, onDateSetListener, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
		}
		return super.onCreateDialog(id, args);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog, Bundle args) {
		switch (id) {
		case DATE_DIALOG_ID:
			((DatePickerDialog) dialog).updateDate(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
			break;
		}
		super.onPrepareDialog(id, dialog, args);
	}

	final DatePickerDialog.OnDateSetListener	onDateSetListener	= new DatePickerDialog.OnDateSetListener() {
																		@Override
																		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

																			Calendar cToday = Calendar.getInstance();

																			Calendar cSelected = Calendar.getInstance();
																			cSelected.set(year, monthOfYear, dayOfMonth);

																			if (cToday.after(cSelected)) {
																				Toast.makeText(mContext, R.string.setting_date_after_today, Toast.LENGTH_LONG).show();
																				showDialog(DATE_DIALOG_ID);
																				return;
																			}

																			mCalendar.set(year, monthOfYear, dayOfMonth);
																			Logg.d("showSelectValidDateDialog - onDateSetListener : " + mCalendar);
																			Date date = new Date(mCalendar.getTimeInMillis());
																			String convertedServerDateString = Utils.getServerDateFormatString(date);
																			mTextViewValid.setText(getDateStringForValidDateText(date));
																			mTextViewValid.setTag(convertedServerDateString);
																		}
																	};

	private String getDateStringForValidDateText(Date date) {
		return new SimpleDateFormat(getString(R.string.until_yyyy_mm_dd), Locale.KOREA).format(date);
	};

	@Override
	protected int setContentViewId() {
		return R.layout.activity_coupon_send;
	}

}

// EditText 텍스트 변경시, 보이지 않는 다른 EditText에도 변화를 주도록 함
class CouponTextWatcher implements TextWatcher {

	EditText[]	mEditTexts;

	public CouponTextWatcher(EditText... editTexts) {
		super();
		this.mEditTexts = editTexts;
	}

	@Override
	public void afterTextChanged(Editable s) {
		for (EditText et : mEditTexts) {
			et.setText(s);
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

}