package kr.wisestone.happytong.ui.csend;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.HappytongDB.GroupInfo;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.data.util.ImageDownloader;
import kr.wisestone.happytong.data.util.RecycleUtils;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.friend.FriendHandler;
import kr.wisestone.happytong.ui.friend.FriendListAdapter;
import kr.wisestone.happytong.ui.friend.FriendListAdapter.ViewHolder;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;

public class CouponSendFriendSearchSelectActivity extends BaseActivity {
	public static final int DISPLAY_STATE_SELECT_MODE = 2;

	TitleInfoView mTitleInfoView;

	private ListView mGroupListView; // group list view.
	private ListView mFriendListView; // friend list view.

	private EditText mEtSearchName;

	private Button mSendCoupon = null; // 쿠폰 보내기

	private MainHandler mMainHandler = new MainHandler(); // UI 처리 Handler
	private FriendHandler mFriendtHandler = null; // 공용 DB 처리 Handler

	private int mSelectedListItemId = 0; // 선택된 list item 의 index
	private ImageDownloader mImageLoader = null; // image loader

	private ArrayList<GroupInfo> mArrayGroupList = new ArrayList<GroupInfo>(); // 그룹 리스트 data
	private ArrayList<FriendInfo> mArrayFriendList = new ArrayList<FriendInfo>(); // 친구 리스트 data
	private ArrayList<FriendInfo> mArrayFriendCheckList = new ArrayList<FriendInfo>(); // 친구 체크 리스트 data

	private FriendListAdapter mGroupAdapter;
	private FriendListAdapter mFriendAdapter;

	private int mSelectGroupPoisition = 0;
	private int mDisplayState = DISPLAY_STATE_SELECT_MODE;
	private PreGroupSelect mPreGroupSelect = new PreGroupSelect(); // 이전 선택된 그룹 정보 저장

	TextWatcher mTwSearch = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// if ("+".equals(mArrayGroupList.get(mSelectGroupPoisition).GetGroupName()) == false) {
			// 친구메인 - 그룹리스트 - 하단 구분 문자열 "+"에서 "..."로 변경. 20130806.jish
			if (CommonData.GROUP_LIST_FOOTER_STRING.equals(mArrayGroupList.get(mSelectGroupPoisition).GetGroupName()) == false) {
				changeFriendList(mSelectGroupPoisition);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	OnItemClickListener mGroupClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> v, View arg1, int position, long arg3) {
			if (mSelectGroupPoisition != position) {
				mSelectGroupPoisition = position;
				changeFriendList(position);

				changeSelectImage(arg1, position);
				clearEditStr();
			}
		}
	};

	OnClickListener mCheckBoxClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (mFriendListView == null) {
				return;
			}

			int position = mFriendListView.getPositionForView(v);
			if (position < 0) {
				Logg.d("can not find item from view");
				return;
			}

			FriendInfo info = mArrayFriendList.get(position);

			setCheckState(info, position, false);
		}

	};

	OnClickListener mTitleClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v.getTag().equals(TitleInfoView.TITLE_RIGHT_BTN)) {
				setDataResultOkFinish();
			}
		}
	};

	/***
	 * onCreate
	 * 
	 * @param context
	 *            : Bundle savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Logg.d("onCreate");
		super.onCreate(savedInstanceState);

		mImageLoader = new ImageDownloader();

		findAllViews();
		setEventes();
		commonAreaSet();

		makeListStart();
		changeLayout(mDisplayState);
	}

	private void commonAreaSet() {
		mTitleInfoView = new TitleInfoView(this, (View) findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL);
	}

	/***
	 * view load.
	 */
	private void findAllViews() {
		mSendCoupon = (Button) findViewById(R.id.ib_sendCoupon);

		mGroupListView = (ListView) findViewById(R.id.groupList);
		mFriendListView = (ListView) findViewById(R.id.list);

		mEtSearchName = (EditText) findViewById(R.id.et_search_name);
		mEtSearchName.addTextChangedListener(mTwSearch);

	}

	/***
	 * View에 event 등록
	 */
	private void setEventes() {
		mSendCoupon.setOnClickListener(setContactAddBtnListener());
	}

	@Override
	protected void onStart() {
		Logg.d("onStart");
		super.onStart();

	}

	@Override
	protected void onResume() {
		Logg.d("onResume");

		super.onResume();
	}

	@Override
	protected void onPause() {
		Logg.d("onPause");

		super.onPause();
	}

	@Override
	protected void onDestroy() {

		if (mFriendtHandler != null && mFriendtHandler.mDbHelper != null) {
			mFriendtHandler.mDbHelper.close();
		}

		RecycleUtils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		super.onDestroy();
	}

	private OnClickListener setContactAddBtnListener() {
		return new OnClickListener() {
			public void onClick(View v) {
				if (mDisplayState == DISPLAY_STATE_SELECT_MODE) {
					for (int nIndex = 0; nIndex < mArrayFriendList.size(); nIndex++) {
						if (mArrayFriendList.get(nIndex).isChecked() == false) {
							selectAll(true);
							return;
						}
					}
					selectAll(false);
				} else {
					if (mArrayFriendList.size() > 0) {
						mDisplayState = DISPLAY_STATE_SELECT_MODE;
						changeLayout(mDisplayState);
						mFriendAdapter.notifyDataSetChanged();
					}
				}
			}
		};
	}

	/**
	 * 기존에 선택된 친구 리스트를 유지하기위함?
	 */
	@SuppressWarnings("unchecked")
	private void setSelectDefaultListItems(Intent data) {

		ArrayList<FriendInfo> list = (ArrayList<FriendInfo>) data.getSerializableExtra(IntentDefine.EXTRA_FRIEND_SELECT_LIST);
		if (list == null || list.size() == 0) {
			return;
		}

		if (list.size() == mArrayFriendList.size()) {
			// 전체선택 이미지 활성화
			mSendCoupon.setBackgroundResource(R.drawable.sub4_02_select2_btn);
		} else {
			mSendCoupon.setBackgroundResource(R.drawable.sub4_02_select_btn);
		}

		for (FriendInfo info : mArrayFriendList) {
			int wantRemovePosition = -1;
			for (int i = 0; i < list.size(); i++) {
				FriendInfo info2 = list.get(i);
				if (info.GetID().equals(info2.GetID())) {
					wantRemovePosition = i;
					info.setCheckBoxFlag(FriendInfo.CHECK_FLAG_CHECKED);
					// XXX 0 ㅠㅜ
					setCheckState(info, 0, true);
					break;
				}
			}
			if (wantRemovePosition != -1) {
				list.remove(wantRemovePosition);
			}
		}
		mFriendAdapter.notifyDataSetChanged();

		Logg.d("resres - " + mArrayFriendCheckList.size() + " / " + list.size());
	}

	private void selectAll(boolean flag) {
		int mode = FriendInfo.CHECK_FLAG_UNCHECK;

		mArrayFriendCheckList.clear();

		if (flag) {
			mode = FriendInfo.CHECK_FLAG_CHECKED;
			mSendCoupon.setBackgroundResource(R.drawable.sub4_02_select2_btn);
		} else {
			mSendCoupon.setBackgroundResource(R.drawable.sub4_02_select_btn);
		}

		for (int nIndex = 0; nIndex < mArrayFriendList.size(); nIndex++) {
			mArrayFriendList.get(nIndex).setCheckBoxFlag(mode);
			setCheckState(mArrayFriendList.get(nIndex), nIndex, true);
		}
		mFriendAdapter.notifyDataSetChanged();
	}

	private void setDataResultOkFinish() {
		Intent resultIntent = new Intent();
		resultIntent.putExtra(IntentDefine.EXTRA_FRIEND_SELECT_LIST, mArrayFriendCheckList);
		setResult(Activity.RESULT_OK, resultIntent);
		finish();
	}

	private void setCheckState(FriendInfo info, int position, boolean isAll) {
		if (isAll) {
			if (info.isChecked()) {
				mArrayFriendCheckList.add(info);
			} else {
				mArrayFriendCheckList.remove(info);
			}
		} else {
			if (info.isChecked()) {
				info.setCheckBoxFlag(FriendInfo.CHECK_FLAG_UNCHECK);

				for (FriendInfo temp : mArrayFriendCheckList) {
					if (info.GetEmail().equals(temp.GetEmail())) {
						mArrayFriendCheckList.remove(temp);
						break;
					}
				}
				mArrayFriendCheckList.remove(info);
			} else {
				info.setCheckBoxFlag(FriendInfo.CHECK_FLAG_CHECKED);
				mArrayFriendCheckList.add(info);
			}
			mArrayFriendList.set(position, info);
		}

		mFriendAdapter.notifyDataSetChanged();
	}

	/***
	 * @Brief 화면 전환.
	 */
	private void changeLayout(int mode) {
		mEtSearchName.setVisibility(View.VISIBLE);

		mSendCoupon.setBackgroundResource(R.drawable.sub4_02_select_btn);
		mTitleInfoView.updateView(R.string.friend_title_select, TitleInfoView.TITLE_MODE_FRIEND_SELECT, mTitleClickListener);
	}

	private void changeFriendList(int position) {
		if (position == 0) {
			// 친구 전체 리스트
			makeListStart();

		} else {
			// 그룹별 친구 리스트
			makeGroupFriendList(mArrayGroupList.get(position).GetGroupID());
			Logg.d("GetGroupID = " + mArrayGroupList.get(position).GetGroupID());
		}
	}

	/***
	 * @Brief 리스트 생성.
	 */
	private void makeListStart() {
		Logg.d("makeListStart");

		if (mFriendtHandler == null) {
			mFriendtHandler = new FriendHandler(this, mMainHandler) {
				@Override
				public String getSearchString() {
					String searchStr = mEtSearchName.getText().toString();
					return (searchStr.length() > 0) ? searchStr : null;
				}
			};
		}

		sendMsg(FriendHandler.MSG_CONTROL_DOING, FriendHandler.DB_CONTROL_START, 0, null);
	}

	/***
	 * @Brief 리스트 생성.
	 */
	private void makeGroupFriendList(String groupID) {
		Logg.d("makeListStart groupName = " + groupID);

		if (mFriendtHandler == null) {
			mFriendtHandler = new FriendHandler(this, mMainHandler) {
				@Override
				public String getSearchString() {
					String searchStr = mEtSearchName.getText().toString();
					return (searchStr.length() > 0) ? searchStr : null;
				}
			};
		}
		sendMsg(FriendHandler.MSG_CONTROL_DOING, FriendHandler.DB_GET_FRIEND_LIST_GROUP, 0, groupID);
	}

	/***
	 * @Brief 선택되어 있는 group 가져오기.
	 * 
	 * @return GroupList class.
	 */
	public GroupInfo getSelectedGroup() {
		if (mFriendtHandler == null)
			return null;

		GroupInfo groupList = getFocusedItem();

		if (groupList == null) {
			return null;
		}

		return groupList;
	}

	/***
	 * @Brief message 전송
	 * @param int what : message 처리 handler id
	 * @param int arg1 : action id
	 * @param int arg2 : parameter1
	 * @param Object
	 *            obj: parameter2
	 */
	private void sendMsg(int what, int arg1, int arg2, Object obj) {
		Message msg = new Message();
		msg.what = what;
		msg.arg1 = arg1;
		msg.arg2 = arg2;
		msg.obj = obj;

		mFriendtHandler.sendMessage(msg);
	}

	@SuppressLint("HandlerLeak")
	private class MainHandler extends Handler {

		/***
		 * @Brief hander 분기.
		 */
		@SuppressWarnings("unchecked")
		public void handleMessage(Message msg) {
			Logg.d("Main Handler = " + msg.arg1);

			switch (msg.arg1) {
			case FriendHandler.MSG_COMPLETE_GROUP_LIST:
				mArrayGroupList = (ArrayList<GroupInfo>) msg.obj;
				initGroupList();
				break;
			case FriendHandler.MSG_COMPLETE_FRIEND_LIST:
				if (mArrayFriendList != null) {
					mArrayFriendList.clear();
				}

				if (msg.obj != null)
					mArrayFriendList = (ArrayList<FriendInfo>) msg.obj;

				listViewInit();
				break;
			}
		}
	}

	/***
	 * @Brief 그룹 리스트 초기화.
	 */
	private void initGroupList() {
		mGroupAdapter = new FriendListAdapter(this, mArrayGroupList, R.layout.group_list, FriendListAdapter.LIST_TYPE_GROUP) {
			@Override
			public int getCount() {
				// 그룹 리스트에서 [+]영역 삭제하기위함
				return super.getCount() - 1;
			}

			@Override
			public View setView(int position, View convertView, ViewGroup parent, ViewHolder holder) {
				setGroupViewData(position, (GroupInfo) getItem(position), holder);
				return convertView;
			}
		};

		mGroupListView.setOnItemClickListener(mGroupClickListener);
		mGroupListView.setAdapter(mGroupAdapter);

		mGroupListView.post(setSelectInit());

	}

	/***
	 * @Brief 리스트 생성 완료 후 첫번째 item 에 select focus 를 주기 위해 재귀
	 */
	private Runnable setSelectInit() {
		return new Runnable() {
			public void run() {
				View v = mGroupListView.getChildAt(mSelectGroupPoisition);
				if (v != null) {
					changeSelectImage(v, mSelectGroupPoisition);
				} else {
					mGroupListView.post(setSelectInit());
				}
			}
		};
	}

	private void setGroupViewData(int position, GroupInfo item, ViewHolder holder) {
		String groupName = item.GetGroupName();
		Logg.e("SDJ setGroupViewData= " + groupName);

		if (mPreGroupSelect.position == position) {
			holder.mllGroupName.setBackgroundResource(R.drawable.m3_leftmenu_btn);
            holder.mGroupName.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
            holder.mGroupCount.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
		} else {
			holder.mllGroupName.setBackgroundResource(R.drawable.m3_leftmenu2_btn);
            holder.mGroupName.setTextColor(getResources().getColor(R.color.rgb_666));
            holder.mGroupCount.setTextColor(getResources().getColor(R.color.rgb_666));
		}

		holder.mGroupName.setText(groupName);

		// if ("+".equals(groupName)) {
		// 친구메인 - 그룹리스트 - 하단 구분 문자열 "+"에서 "..."로 변경. 20130806.jish
		if (CommonData.GROUP_LIST_FOOTER_STRING.equals(groupName)) {
			holder.mGroupCount.setVisibility(View.GONE);
		} else {
			holder.mGroupCount.setVisibility(View.VISIBLE);
			holder.mGroupCount.setText("[" + getGroupCount(item) + "]");
		}

		holder.mllGroupName.setVisibility(View.VISIBLE);
	}

	/***
	 * @Brief 그룹 리스트의 선택 된 image 변경.
	 * @param1 View : Click event 가 발생한 view
	 * @param2 int : click event가 가 발생한 item의 그룹 리스트내의 index
	 */
	private void changeSelectImage(View clickedView, int position) {

		mPreGroupSelect.backToUnSelect();
		mPreGroupSelect.position = mSelectedListItemId = position;
		mPreGroupSelect.preView = clickedView;

		Logg.d("changeSelectImage :" + position);
		if (clickedView != null && clickedView.getTag() != null) {
			ViewHolder holder = (ViewHolder) clickedView.getTag();
			holder.mllGroupName.setBackgroundResource(R.drawable.m3_leftmenu_btn);
			holder.mGroupName.setTextColor(getResources().getColor(R.color.rgb_25a4b8));
			holder.mGroupCount.setTextColor(getResources().getColor(R.color.rgb_25a4b8));
		}
		((FriendListAdapter) mGroupListView.getAdapter()).notifyDataSetChanged();
	}

	private void clearEditStr() {
		if (mDisplayState == DISPLAY_STATE_SELECT_MODE)
			mEtSearchName.setText("");
	}

	/***
	 * @Brief 해당 그룹의 Child 수를 반환.
	 * @param1 GroupList : 연락처의 개수를 알고자 하는 그룹 item
	 * @return int : 그룹에 포함 된 연락처의 개수.
	 */
	private int getGroupCount(GroupInfo item) {
		int nCount = 0;
		HappytongDBHelper db = new HappytongDBHelper(this);

		if (item == null || item.GetGroupName().compareTo(getString(R.string.all)) == 0) {
			nCount = db.getUnBlockFriendCount();
		} else {
			nCount = db.getGroupFriendCount(item.GetGroupID());
		}

		db.close();
		return nCount;
	}

	public GroupInfo getFocusedItem() {
		int position = 0;
		position = mSelectedListItemId;

		if (position == -1) {
			position = 0;
		}

		return mArrayGroupList.get(position);
	}

	/***
	 * @Brief 연락처 리스트 초기화 및 생성
	 */
	private void listViewInit() {
		if (mDisplayState == DISPLAY_STATE_SELECT_MODE) {
			for (int i = 0; i < mArrayFriendCheckList.size(); i++) {
				String friendId = mArrayFriendCheckList.get(i).GetID();
				for (int j = 0; j < mArrayFriendList.size(); j++) {
					if (friendId.equals(mArrayFriendList.get(j).GetID())) {
						mArrayFriendList.get(j).setCheckBoxFlag(FriendInfo.CHECK_FLAG_CHECKED);
						break;
					}
				}
			}
		}

		mFriendAdapter = new FriendListAdapter(this, mArrayFriendList, R.layout.friend_list_item, FriendListAdapter.LIST_TYPE_FRIEND) {
			public View setView(int position, View convertView, ViewGroup parent, ViewHolder holder) {
				setFriendViewData(holder, (FriendInfo) getItem(position));

				return convertView;
			}
		};

		mFriendListView.setAdapter(mFriendAdapter);
		
		setSelectDefaultListItems(getIntent());
	}

	private void setViewVisible(ViewHolder holder) {
		holder.mContactListItem.setVisibility(View.VISIBLE);
		holder.mListHeader.setVisibility(View.GONE);
		holder.mButton.setVisibility(View.GONE);

		switch (mDisplayState) {
		case DISPLAY_STATE_SELECT_MODE:
			holder.mCheckBox.setVisibility(View.VISIBLE);
			break;
		}
	}

	private void setFriendViewData(ViewHolder holder, FriendInfo info) {
		setViewVisible(holder);

		if (info.GetIsFirst()) {
			setHeadLine(holder, "" + info.GetChosung());
		} else {
			holder.mListHeader.setVisibility(View.GONE);
		}

		String displayName;
		if (info.GetIsName().equals("1")) {
			displayName = info.GetName() + "(" + info.GetNickName() + ")";
		} else {
			displayName = info.GetName();
		}

		mImageLoader.DisplayImage(info.GetUserImgUrl(), holder.mPhoto);
		holder.mPhoto.setScaleType(ScaleType.FIT_XY);
		holder.mName.setText(displayName);

		if (info.isChecked())
			holder.mCheckBox.setChecked(true);
		else
			holder.mCheckBox.setChecked(false);

		if (mDisplayState == DISPLAY_STATE_SELECT_MODE)
			holder.mContactListItem.setOnClickListener(mCheckBoxClickListener);

	}

	private void setHeadLine(ViewHolder holder, String headLine) {
		holder.mListHeader.setVisibility(View.VISIBLE);

		holder.mHeadLine.setVisibility(View.VISIBLE);
		holder.mHeadLine.setText(headLine);
	}

	@Override
	protected int setContentViewId() {
		return R.layout.activity_coupon_send_friend_select_search;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private class PreGroupSelect {
		int position = 0; // 그룹리스트에서의 index
		View preView = null; // 그룹 리스트의 row.

		/***
		 * @Brief Select image를 변경.
		 */
		public void backToUnSelect() {
			if (preView == null) {
				return;
			}
			ViewHolder holder = (ViewHolder) preView.getTag();
			holder.mllGroupName.setBackgroundResource(R.drawable.m3_leftmenu2_btn);
			holder.mGroupName.setTextColor(getResources().getColor(R.color.rgb_255_255_255));
			holder.mGroupCount.setTextColor(getResources().getColor(R.color.rgb_255_255_255));

		}
	}
}
