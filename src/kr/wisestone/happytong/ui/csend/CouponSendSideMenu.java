package kr.wisestone.happytong.ui.csend;

import java.util.ArrayList;
import java.util.List;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.dataset.OrderCouponPurchased;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.PurchaseProtocol;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

public class CouponSendSideMenu implements OnClickListener, OnItemClickListener {

	public interface CouponSendSideMenuListener {
		void onCouponItemSelected(OrderCouponPurchased orderCouponPurchasedData);

		void onCouponShopButtonClick();

		void onCouponImageUpdated();
	}

	public interface CouponSendSideMenuAnimationListener {
		void onCloseAnimationEnd();
	}

	private static final int			SLIDE_ANI_DURATION	= 500;

	private Activity					mActivity;
	private Context						mContext;

	private ListView					mListView;
	private List<OrderCouponPurchased>	mList;
	private MyCouponImageAdapter		mAdapter;

	private View						mViewAniLayout;
	private View						mViewContent;
	private View						mViewCover;

	private CouponSendSideMenuListener	mListener;

	private boolean						isAnimating;
	private boolean						isOpen;

	private IWiseCallback				iFail;

	public CouponSendSideMenu(Activity activity, View viewAniLayout, View viewContent, View viewCover, CouponSendSideMenuListener listener, IWiseCallback iFail) {
		super();
		this.mActivity = activity;
		this.mContext = activity.getApplicationContext();
		this.mViewAniLayout = viewAniLayout;
		this.mViewContent = viewContent;
		this.mViewCover = viewCover;
		this.mListener = listener;
		this.iFail = iFail;

		init();
		requestNetworkForMyCouponImage();
	}

	public void updateMyCouponImages() {
		requestNetworkForMyCouponImage();
	}

	private void requestNetworkForMyCouponImage() {
		new PurchaseProtocol(iOK, iFail).orderCouponPurchased(CallBackParamWrapper.CODE_ORDER_COUPON_PURCHASED, SharedPrefManager.getLoginEmail(mContext));
	}

	final IWiseCallback	iOK	= new IWiseCallback() {
								@SuppressWarnings("unchecked")
								@Override
								public boolean WiseCallback(CallbackParam param) {
									mList.clear();
									if (param.param1 != null) {
										mList.addAll((List<OrderCouponPurchased>) param.param1);
									}
									mAdapter.notifyDataSetChanged();

									if (mListener != null) {
										mListener.onCouponImageUpdated();
									}

									return false;
								}
							};

	private void init() {
		mViewCover.setOnClickListener(this);

		mViewContent.findViewById(R.id.go_couponshop).setOnClickListener(this);

		mListView = (ListView) mViewContent.findViewById(R.id.my_coupon_listview);
		mListView.setOnItemClickListener(this);

		mList = new ArrayList<OrderCouponPurchased>();
		mAdapter = new MyCouponImageAdapter(mActivity, mList);
		mListView.setAdapter(mAdapter);
	}

	public void setGoButtonImageResource(int res) {
		mViewContent.findViewById(R.id.go_couponshop).setBackgroundResource(res);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.go_couponshop:
			Logg.d("R.id.go_couponshop - click");
			if (mListener != null) {
				mListener.onCouponShopButtonClick();
			}
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		mListener.onCouponItemSelected(mAdapter.getItem(position));
		closeSideMenu();
	}

	public void openSideMenu() {
		Animation ani = new TranslateAnimation(0, mViewContent.getWidth() * -1, 0, 0);
		ani.setDuration(SLIDE_ANI_DURATION);
		ani.setFillEnabled(true);
		ani.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				((RelativeLayout.MarginLayoutParams) mViewAniLayout.getLayoutParams()).rightMargin = 0;
				mViewAniLayout.requestLayout();
				isAnimating = false;
				isOpen = true;
				mViewCover.setVisibility(View.VISIBLE);
			}
		});
		mViewAniLayout.startAnimation(ani);
		isAnimating = true;
	}

	public void closeSideMenu() {
		closeSideMenu(null);
	}

	public void closeSideMenu(final CouponSendSideMenuAnimationListener l) {
		Animation ani = new TranslateAnimation(0, mViewContent.getWidth(), 0, 0);
		ani.setDuration(SLIDE_ANI_DURATION);
		ani.setFillEnabled(true);
		ani.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				((RelativeLayout.MarginLayoutParams) mViewAniLayout.getLayoutParams()).rightMargin = mViewContent.getWidth() * -1;
				mViewAniLayout.requestLayout();
				isAnimating = false;
				isOpen = false;
				mViewCover.setVisibility(View.GONE);
				if (l != null) {
					l.onCloseAnimationEnd();
				}
			}
		});
		mViewAniLayout.startAnimation(ani);
		isAnimating = true;
	}

	public boolean isAnimating() {
		return isAnimating;
	}

	public boolean isOpen() {
		return isOpen;
	}

}

class MyCouponImageAdapter extends BaseAdapter {

	List<OrderCouponPurchased>	mList;
	LayoutInflater				mInflater;
	Picasso						mPicasso;

	public MyCouponImageAdapter(Context context, List<OrderCouponPurchased> mList) {
		super();
		this.mList = mList;
		mInflater = LayoutInflater.from(context);
		mPicasso = Picasso.with(context);
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public OrderCouponPurchased getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ImageView imgView;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.coupon_send_side_listview_item, null);
			imgView = (ImageView) convertView.findViewById(R.id.coupon_image);
			convertView.setTag(imgView);
		} else {
			imgView = (ImageView) convertView.getTag();
		}
		// mImgLoader.DisplayImage(mList.get(position).couponImageURL, imgView);
		mPicasso.load(mList.get(position).couponImageURL).placeholder(R.drawable.m3_coupon_defult).into(imgView);

		return convertView;
	}

}
