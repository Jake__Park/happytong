package kr.wisestone.happytong.ui.widget;

import kr.wisestone.happytong.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SubTitleView extends RelativeLayout implements OnClickListener {

	final int TYPE_FRIEND_MAIN = 0;
	final int TYPE_COUPON_BOX = 1;

	RatingBar mRatingBar;
	TextView mTextViewPayCount;
	TextView mTextViewEgg;
	View mViewCSend;

	public interface SubTitleViewListener {
		/**
		 * 에그영역 클릭
		 */
		void onEggClickListener();
	}

	SubTitleViewListener mListener;

	Context mContext;

	ImageButton[] imgBtns;

	public SubTitleView(Context context) {
		this(context, null);
	}

	public SubTitleView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SubTitleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		mContext = context;
		inflate(mContext, R.layout.sub_title_view, this);

		init();

		setTypedArray(attrs);
	}

	private void init() {
		mRatingBar = (RatingBar) findViewById(R.id.subtitle_owl_free_ratingbar);
		mTextViewPayCount = (TextView) findViewById(R.id.subtitle_owl_pay_count);
		mTextViewEgg = (TextView) findViewById(R.id.subtitle_egg);
		mTextViewEgg.setOnClickListener(this);
		mViewCSend = findViewById(R.id.subtitle_csend);
		mViewCSend.setOnClickListener(this);
	}

	private void setTypedArray(AttributeSet attrs) {
		TypedArray typedArray = mContext.getTheme().obtainStyledAttributes(attrs, R.styleable.SubTitleView, 0, 0);
		int type = typedArray.getInt(0, -1);
		typedArray.recycle();

		switch (type) {
		case TYPE_FRIEND_MAIN:
			mTextViewEgg.setVisibility(View.GONE);
			mViewCSend.setVisibility(View.VISIBLE);
			break;
		case TYPE_COUPON_BOX:
			mTextViewEgg.setVisibility(View.VISIBLE);
			mViewCSend.setVisibility(View.GONE);
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.subtitle_egg:
			if (mListener != null) {
				mListener.onEggClickListener();
			}
			break;
		case R.id.subtitle_csend:
			break;
		}
	}

	public void setListener(SubTitleViewListener l) {
		mListener = l;
	}

	/**
	 * 좌측 부엉이 갯수 지정
	 * 
	 * @param rating
	 */
	public void setFreeOwlCount(int rating) {
		mRatingBar.setRating(rating);
	}

	/**
	 * 좌측 부엉이 +영역 카운트 지정. (+자동으로 붙임)
	 * 
	 * @param count
	 */
	public void setPayOwlCount(String count) {
//		안드로이드를 아이폰 처럼 "+0" 문구 추가. 20130806.jish
//		if (TextUtils.isEmpty(count) || count.equals("0")) {
//			mTextViewPayCount.setText("");
//		} else {
//			mTextViewPayCount.setText("+" + count);
//		}
		mTextViewPayCount.setText("+" + count);
	}

	/**
	 * 금달걀 갯수 지정
	 * 
	 * @param count
	 */
	public void setEggCount(String count) {
		mTextViewEgg.setText(count);
	}

}
