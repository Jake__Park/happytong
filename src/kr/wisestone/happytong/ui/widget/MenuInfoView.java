package kr.wisestone.happytong.ui.widget;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.ui.DummyMainActivity;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class MenuInfoView
{
    private              Context       mContext;
    
    private              View          mView;
    
    public MenuInfoView(Context context, View parent, int nTabIndex)
    {
        mContext = context;
        mView = parent;
        
        SharedPrefManager.getInstance(mContext).setBeforeEntryPoint( nTabIndex );

        init(nTabIndex);
    }
    
    private void init(int nTabIndex)
    {
        Logg.d( "init TAB index = " + nTabIndex );
        
        setDisableButton(nTabIndex);
    }
    
    public View.OnClickListener mBaseClListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        	Intent mainIntent = new Intent(mContext, DummyMainActivity.class);
        	mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            switch(v.getId())
            {
            case R.id.btn_friend:
            	mainIntent.putExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_FRIEND);
                break;
            case R.id.btn_cbox:
            	mainIntent.putExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_CBOX);
                break;
            case R.id.btn_csend:
            	mainIntent.putExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_CSEND);
                break;
            case R.id.btn_cshop:
            	mainIntent.putExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_CSHOP);
                break;
            case R.id.btn_setting:
            	mainIntent.putExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_SETTING);
                break;
            }
            mContext.startActivity(mainIntent);
            // ( (Activity)mContext ).finish();
        }
    };
    
    private void setDisableButton(int nIndex)
    {
        int[] m_arnTabId = 
            {
                R.id.btn_friend,
                R.id.btn_cbox,
                R.id.btn_csend,
                R.id.btn_cshop,
                R.id.btn_setting
            };
        
        int[] m_arnResId = 
            {
                R.drawable.m2_navi_01,
                R.drawable.m2_navi_02,
                R.drawable.m2_navi_03,
                R.drawable.m2_navi_04,
                R.drawable.m2_navi_05
            };
        
        for(int idx = 0; idx < m_arnTabId.length; idx++){
            ((Button)mView.findViewById(m_arnTabId[idx])).setOnClickListener(mBaseClListener);

            ((Button)mView.findViewById(m_arnTabId[idx])).setEnabled(true);
            ((Button)mView.findViewById(m_arnTabId[idx])).setBackgroundResource(m_arnResId[idx]);
            
        }
        
        switch(nIndex)
        {
        case CommonData.ENTRY_PONINT_FRIEND:
            ((Button)mView.findViewById(R.id.btn_friend)).setEnabled(false);
            ((Button)mView.findViewById(R.id.btn_friend)).setBackgroundResource(R.drawable.m2_navi_01_over);
            break;
        case CommonData.ENTRY_PONINT_CBOX:
            ((Button)mView.findViewById(R.id.btn_cbox)).setEnabled(false);
            ((Button)mView.findViewById(R.id.btn_cbox)).setBackgroundResource(R.drawable.m2_navi_02_over);
            break;
        case CommonData.ENTRY_PONINT_CSEND:
            ((Button)mView.findViewById(R.id.btn_csend)).setEnabled(false);
            ((Button)mView.findViewById(R.id.btn_csend)).setBackgroundResource(R.drawable.m2_navi_03_over);
            break;
        case CommonData.ENTRY_PONINT_CSHOP:
            ((Button)mView.findViewById(R.id.btn_cshop)).setEnabled(false);
            ((Button)mView.findViewById(R.id.btn_cshop)).setBackgroundResource(R.drawable.m2_navi_04_over);
            break;
        case CommonData.ENTRY_PONINT_SETTING:
            ((Button)mView.findViewById(R.id.btn_setting)).setEnabled(false);
            ((Button)mView.findViewById(R.id.btn_setting)).setBackgroundResource(R.drawable.m2_navi_05_over);
            break;
        }
    }
}
