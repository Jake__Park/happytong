package kr.wisestone.happytong.ui.widget;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.util.Utils;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TitleInfoView implements View.OnClickListener
{
    public static final int      TITLE_MODE_NORMAL         = 0;
    public static final int      TITLE_MODE_FRIEND_MAIN    = 1;
    public static final int      TITLE_MODE_FRIEND_SELECT  = 2;
    public static final int      TITLE_MODE_GROUP_EDIT     = 3;
    public static final int      TITLE_MODE_COUPON_BOX     = 4;
    public static final int      TITLE_MODE_COUPON_BOX_DEL = 5;
    public static final int      TITLE_MODE_COUPON_SEND    = 6;
    public static final int      TITLE_MODE_AGREEMENT      = 7;

    public static final int      TITLE_MODE_AREA_SELECT    = 8;  //지역 설정 화면
    public static final int      TITLE_COUPON_MODE_COMPLETE= 9;  //쿠폰함->완료쿠폰함
    public static final int      TITLE_STAMP_MODE_RECEIVE  = 10; //스템프함->받은스템프함
    public static final int      TITLE_COUPON_MODE         = 11; //쿠폰함 모드
    public static final int      TITLE_STAMP_MODE          = 12; //스템프함 모드
    public static final int      TITLE_COUPON_MANAGE       = 13; //쿠폰 관리
    public static final int      TITLE_BUSINESS_COUPON_SEND_NUMBER       = 14; //업체 쿠폰 발송 건수
    public static final int      TITLE_MODE_COMPANY_REG    = 15; //사업자 등록
    
    public static final int      TITLE_LEFT_BTN            = 100;
    public static final int      TITLE_RIGHT_BTN           = 200;
    
    public static final int      TITLE_TYPE_NORMAL         = 300;
    public static final int      TITLE_TYPE_COMPANY        = 400;
    
    Context                      mContext;

    private View                 mView;
    private View.OnClickListener m_clButton;
    
    int                          mType                     = TITLE_TYPE_NORMAL;

    /**
     * 일반 모드, 업체모드 Type param 추가 [SDJ 131002]
     * @param context 
     * @param parent Title View
     * @param type 일반: TITLE_TYPE_NORMAL / 업체: TITLE_TYPE_COMPANY
     */
    public TitleInfoView(Context context, View parent, int type)
    {
        mView = parent;
        mContext = context;
        
        mType = type;
    }
    
    public void updateView(int titleStr, int mode)
    {
        updateView(titleStr, mode, null);
    }
    
    public void updateView(int titleStr, int mode, View.OnClickListener listener)
    {
        int resID = 0;
        
        m_clButton = listener;
        
        ((TextView)mView.findViewById(R.id.tv_title)).setText( titleStr );
        
        Button mLeftButton = (Button)mView.findViewById( R.id.btn_title_left );
        Button mRightButton = (Button)mView.findViewById( R.id.btn_title_right );
        
        mLeftButton.setTag( TITLE_LEFT_BTN );
        mRightButton.setTag( TITLE_RIGHT_BTN );
        
        mLeftButton.setOnClickListener( this );
        mRightButton.setOnClickListener( this );

        mLeftButton.setVisibility( View.INVISIBLE );
        mRightButton.setVisibility( View.INVISIBLE ); 
        
        if(mType == TITLE_TYPE_COMPANY){
            resID = R.drawable.pattern_m4_titlebar;
        } else {
            resID = R.drawable.pattern_m1_titlebar;
        }
        ((LinearLayout)mView.findViewById(R.id.title_ll)).setBackgroundDrawable(Utils.getPatternDrawable(mContext, resID));
        
        switch(mode)
        {
            case TITLE_MODE_NORMAL:
            case TITLE_MODE_COMPANY_REG:
                break;
            case TITLE_MODE_FRIEND_MAIN:
                mRightButton.setVisibility( View.VISIBLE );
                
                mRightButton.setBackgroundResource( R.drawable.selector_m1_group );
                break;
            case TITLE_MODE_GROUP_EDIT:
                mRightButton.setVisibility( View.VISIBLE );
                mRightButton.setBackgroundResource( R.drawable.selector_m1_add );
                break;
            case TITLE_MODE_COUPON_BOX:
                mRightButton.setVisibility( View.VISIBLE );
                mRightButton.setBackgroundResource( R.drawable.selector_m1_del);
                break;
            case TITLE_MODE_COUPON_BOX_DEL:
                mLeftButton.setVisibility( View.VISIBLE );
                mLeftButton.setBackgroundResource( R.drawable.selector_m1_cancel);
                mRightButton.setVisibility( View.VISIBLE );
                mRightButton.setBackgroundResource( R.drawable.selector_m1_complete );
                break;
            case TITLE_MODE_COUPON_SEND:
            case TITLE_MODE_FRIEND_SELECT:
            case TITLE_MODE_AREA_SELECT:
            case TITLE_BUSINESS_COUPON_SEND_NUMBER:
                mRightButton.setVisibility( View.VISIBLE );
                mRightButton.setBackgroundResource( R.drawable.selector_m1_complete);
                break;
            case TITLE_MODE_AGREEMENT:
                mLeftButton.setVisibility( View.GONE );
                mRightButton.setVisibility( View.GONE );
                break;
            case TITLE_COUPON_MODE_COMPLETE:
                //TODO 쿠폰함 완료 쿠폰 모드   삭제 <TITLE> 스템프함
                mLeftButton.setVisibility( View.VISIBLE );
                mLeftButton.setBackgroundResource( R.drawable.selector_m1_del);
                mRightButton.setVisibility( View.VISIBLE );
                mRightButton.setText("스템프모드");
                break;                
            case TITLE_STAMP_MODE_RECEIVE:
                //스템프함->받은스템프함   삭제 <TITLE> 쿠폰함 
                mLeftButton.setVisibility( View.VISIBLE );
                mLeftButton.setBackgroundResource( R.drawable.selector_m1_del);                
                mRightButton.setVisibility( View.VISIBLE );
                mRightButton.setText("스템프모드");
                break;                
            case TITLE_COUPON_MODE:
                //쿠폰함 모드   X <TITLE> 스템프함 
                mRightButton.setVisibility( View.VISIBLE );
                mRightButton.setText("스템프모드");
                break;
            case TITLE_STAMP_MODE:
                //스템프함 모드  X <TITLE> 쿠폰함
                mRightButton.setVisibility( View.VISIBLE );
                mRightButton.setText("쿠폰모드");
                break;
            case TITLE_COUPON_MANAGE:
                //쿠폰 관리 모드   X <TITLE> 사진등록
                mRightButton.setVisibility( View.VISIBLE );
                break;
        }
    }
    
    public void updateTitle(String titleStr) {
    	((TextView)mView.findViewById(R.id.tv_title)).setText( titleStr );
    }
    
    @Override
    public void onClick( View v )
    {
        if(m_clButton != null) m_clButton.onClick( v );
    }
}
