package kr.wisestone.happytong.ui.cshop;

import kr.wisestone.happytong.data.billing.IabHelper;
import kr.wisestone.happytong.data.billing.IabResult;
import kr.wisestone.happytong.data.billing.Purchase;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.PurchaseProtocol;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class IAPController
{
    public final static int IAP_NORMAL_REQUEST = 0;
    public final static int IAP_COMPANY_REQUEST = 1;
    
    public final static int PURCHASE_SUCCESS = 10;
    public final static int PURCHASE_ERROR = 100;
    
    public interface PurchaseListener {
        void onStartPurchase();

        void onEndPurchase(int retValue);
    }
    
    Context mContext;
    
    IabHelper mHelper;
    PurchaseListener mListener;
    int mRequestMode;

    // (arbitrary) request code for the purchase flow
    public static final int RC_REQUEST = 10001;
    
    final IWiseCallback iOKSignature = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOKSignature");

            return false;
        }
    };
    
    // XXX USE Customized NetworkFail
    final IWiseCallback iFailSignature = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iFailSignature");
            mListener.onEndPurchase( PURCHASE_ERROR );
            return false;
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        
        @Override
        public void onConsumeFinished( Purchase purchase, IabResult result ) {
            Logg.d("Consumption finished. Purchase: " + purchase + ", result: " + result);

            if (result.isSuccess()) {
                if(mRequestMode == IAP_NORMAL_REQUEST){
                    checkSignatureNoraml(purchase);
                } else {
                    checkSignatureCompany(purchase);
                }

            } else {
                mListener.onEndPurchase( PURCHASE_ERROR );
            }
        }
    };
    
    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase, String signature) {
            Logg.d("Purchase finished: " + result + ", purchase: " + purchase);
            Logg.d("Purchase signature: " + signature );
            
            if (result.isFailure()) {
                mListener.onEndPurchase( PURCHASE_ERROR );
                return;
            }
            Logg.d("Purchase successful.");
            mHelper.consumeAsync(purchase, mConsumeFinishedListener);

        }
    };
    
    public IAPController(Context ctx, String key, int requestMode, PurchaseListener listener) {
        mContext = ctx;
        mRequestMode = requestMode;
        mListener = listener;
        
        initHelper(key);
    }
    
    private void initHelper(String publicKey){
        mHelper = new IabHelper(mContext, publicKey);
        
        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        Logg.d("initHelper");
        
        try{
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                public void onIabSetupFinished(IabResult result) {
                    Logg.d("Setup finished.");

                    if (!result.isSuccess()) {
                        Logg.d(result.getMessage());
                        
                        mListener.onEndPurchase( PURCHASE_ERROR );
                        return;
                    }
                    Logg.d("Setup successful. Querying inventory.");
                }
            });
        } catch (IllegalStateException e){
            mListener.onEndPurchase( PURCHASE_ERROR );
        }
        
    }
    
    //구매 요청
    public void buyProduct(String item) {
        
        String decryptItem = AESEncryption.decrypt( item );
        Logg.d("buyProduct decryptItem = " + decryptItem);

        String payload = ""; 
        mListener.onStartPurchase();
        
        mHelper.launchPurchaseFlow((Activity)mContext, decryptItem, RC_REQUEST, mPurchaseFinishedListener, payload);
    }
    
    public void handleActivityResult(int requestCode, int resultCode, Intent data){
        
        if(!mHelper.handleActivityResult( requestCode, resultCode, data )){
            mListener.onEndPurchase(PURCHASE_ERROR);
        } 
    }
    
    public void destoryData(){
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }
    
    private void checkSignatureNoraml(final Purchase purchase){
        SharedPrefManager.getInstance( mContext ).setPurchaseData( purchase.getOriginalJson() );
        SharedPrefManager.getInstance( mContext ).setSignatureData( purchase.getSignature() );

        Logg.d( "SDJ_TEST  purchaseData = " + purchase.getOriginalJson());
        Logg.d( "SDJ_TEST  signatureData = " + purchase.getSignature());

        PurchaseProtocol protocol = new PurchaseProtocol( new IWiseCallback() {
            
            @Override
            public boolean WiseCallback( CallbackParam param ) {
                SharedPrefManager.getInstance( mContext ).setPurchaseData( "" );
                SharedPrefManager.getInstance( mContext ).setSignatureData( "" );
                
                mListener.onEndPurchase( PURCHASE_SUCCESS );
                return false;
            }
        }, 
        // XXX USE Customized NetworkFail
        new IWiseCallback() {
            @Override
            public boolean WiseCallback( CallbackParam param ) {
                mListener.onEndPurchase( PURCHASE_ERROR );
                return false;
            }
        } );
        protocol.checkSignature( 0, SharedPrefManager.getLoginEmail( mContext ), purchase.getOriginalJson(), purchase.getSignature() );
    }
    
    private void checkSignatureCompany(final Purchase purchase){
        Logg.d( "checkSignatureCompany = " + purchase.getOriginalJson());
        
        SharedPrefManager.getInstance( mContext ).setCompanyPurchaseData( purchase.getOriginalJson() );
        SharedPrefManager.getInstance( mContext ).setCompanySignatureData( purchase.getSignature() );

        PurchaseProtocol protocol = new PurchaseProtocol( new IWiseCallback() {
            
            @Override
            public boolean WiseCallback( CallbackParam param ) {
                SharedPrefManager.getInstance( mContext ).setCompanyPurchaseData( "" );
                SharedPrefManager.getInstance( mContext ).setCompanySignatureData( "" );
                
                mListener.onEndPurchase( PURCHASE_SUCCESS );
                return false;
            }
        }, 
        // XXX USE Customized NetworkFail
        new IWiseCallback() {
            @Override
            public boolean WiseCallback( CallbackParam param ) {
                mListener.onEndPurchase( PURCHASE_ERROR );
                return false;
            }
        } );
        protocol.setCompaniesMarketBuyValidity( 0, SharedPrefManager.getLoginEmail( mContext ), purchase.getOriginalJson(), purchase.getSignature() );
    }    
}
