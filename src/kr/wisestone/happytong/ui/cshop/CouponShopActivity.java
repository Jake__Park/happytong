package kr.wisestone.happytong.ui.cshop;

import java.io.IOException;
import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.ProductInfoData;
import kr.wisestone.happytong.data.dataset.PurchaseInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.AuthProtocol;
import kr.wisestone.happytong.data.protocol.ProductProtocol;
import kr.wisestone.happytong.data.protocol.PurchaseProtocol;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.MenuInfoView;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CouponShopActivity extends BaseActivity
{
    final int REQEUST_SHOP_DETAIL = 100;
    
    final int TYPE_CSHOP_ALL = 0;
    final int TYPE_CSHOP_POPULAR = 10;
    final int TYPE_CSHOP_PURCHASED = 20;
    
    View mViewGoldList;
    View mViewOwlList;
    
    LinearLayout mLeftListLay;
    LinearLayout mRightListLay;
    LinearLayout mPurchasedListLay;

    TextView mTvNodata;
    
    TextView m_tvLeftAll;
    TextView m_tvLeftPopular;
    TextView m_tvLeftPurchased;
    TextView[] mVLeftBoxes;
    ColorStateList mColorStateListForLeftMenus;
    
    ArrayList<ProductInfoData> mArrGoldListData;
    ArrayList<ProductInfoData> mArrOwlListData;
    ArrayList<ProductInfoData> mArrCouponListData;

    ArrayList<PurchaseInfoData> mArrPurchaseListData;
    
    IAPController mIAPcontroller;
    
    boolean mStartedByCSend = false;
    
    OnClickListener mClkGoldListener = new OnClickListener() {
        
        @Override
        public void onClick( View v )
        {
            int index = (Integer)v.getTag();
            
            showPurchasePopup(index);
        }
    };
    
    OnClickListener mClkOwlListener = new OnClickListener()
    {
        
        @Override
        public void onClick( View v )
        {
            int index = (Integer)v.getTag();
            requestPurchaseAwl(mArrOwlListData.get(index));
        }
    };

    /**
     * 일반 상품 요청 
     */
    final IWiseCallback iOkCouponList = new IWiseCallback() {
        @SuppressWarnings("unchecked")
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkCouponList");
            if (param.param1 != null) {
                if(mArrGoldListData != null) mArrGoldListData.clear();

                mArrGoldListData = (ArrayList<ProductInfoData>)param.param1;
            }
            if (param.param2 != null) {
                if(mArrOwlListData != null) mArrOwlListData.clear();

                mArrOwlListData = (ArrayList<ProductInfoData>)param.param2; 
            }
            if (param.param3 != null) {
                if(mArrCouponListData != null) mArrCouponListData.clear();
                
                mArrCouponListData = (ArrayList<ProductInfoData>)param.param3; 
            }
            drawListData(TYPE_CSHOP_ALL);
            closeProgressDialog();
            return false;
        }
    };
    
    /**
     * 인기 상품 요청
     */
    final IWiseCallback iOkPopularList = new IWiseCallback() {
        @SuppressWarnings("unchecked")
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkPopularList");
            if (param.param1 != null) {
                if(mArrCouponListData != null) mArrCouponListData.clear();
                
                mArrCouponListData = (ArrayList<ProductInfoData>)param.param1;
            }
            drawListData(TYPE_CSHOP_POPULAR);
            closeProgressDialog();
            return false;
        }
    };
    
    /**
     * 구매 목록 요청
     */
    final IWiseCallback iOkPurchasedList = new IWiseCallback() {
        @SuppressWarnings("unchecked")
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkPurchasedList");
            if (param.param1 != null) {
                if(mArrPurchaseListData != null) mArrPurchaseListData.clear();
                
                mArrPurchaseListData = (ArrayList<PurchaseInfoData>)param.param1;
            }
            
            drawListData(TYPE_CSHOP_PURCHASED);
            closeProgressDialog();
            return false;
        }
    };
    
    /**
     * 부엉이 구매 요청
     */
    final IWiseCallback iOkBuyOwl = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkBuyOwl");
            if (param.param1 != null) {
                checkBuyOwlState((PurchaseInfoData)param.param1);
            }
            return false;
        }
    };
    
    /**
     * IAP Public key 요청
     */
    final IWiseCallback iOkPublickey = new IWiseCallback() {
        @Override
        public boolean WiseCallback(CallbackParam param) {
            Logg.d("iOkPublickey");
            if (param.param1 != null) {
                if(initIAPState((String)param.param1)){
                    requestCouponData(TYPE_CSHOP_ALL);
                }
            }
            return false;
        }
    };
    
    final IAPController.PurchaseListener purchaseListener = new IAPController.PurchaseListener(){

        @Override
        public void onStartPurchase() {
            showProgressDialog();
        }

        @Override
        public void onEndPurchase( int retValue ) {
            closeProgressDialog();
            
            switch(retValue){
                case IAPController.PURCHASE_SUCCESS:
                    requestStampRemain();
                    break;
                case IAPController.PURCHASE_ERROR:
                    break;
            }
        }
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mStartedByCSend = getIntent().getBooleanExtra(IntentDefine.EXTRA_CSEND_STARTED, false);

        initColorStateListForLeftMenus();
        findAllView();
        commonAreaSet();
        
        requestPublicKey();
    }
    
    @Override
    protected void onDestroy() {
        if(mIAPcontroller != null){
            mIAPcontroller.destoryData();
        }
        
        super.onDestroy();
    }
    
    @Override
    public void onBackPressed() {
		if (mStartedByCSend) {
			setResult(Activity.RESULT_OK);
			super.onBackPressed();
			return;
		}
        backKeyToExit();
    }
    
    private void requestPublicKey(){
        showProgressDialog();
        
        AuthProtocol mProtocol = new AuthProtocol(iOkPublickey, iFail);
        mProtocol.authGetPublicKey( 0 );
    }
    
    private boolean initIAPState(String key){
        String oriKey = null;
        
        oriKey = AESEncryption.decrypt( key );
        
        if(oriKey != null){
            mIAPcontroller = new IAPController( this, oriKey, IAPController.IAP_NORMAL_REQUEST, purchaseListener);
        } else {
            DialogUtil.getConfirmDialog( this, R.string.info, R.string.txt_fail_get_coupon_info, new DialogInterface.OnClickListener() {
                
                @Override
                public void onClick( DialogInterface dialog, int which ) {
                    finish();
                }
            } ).show();
            return false;
        }
        return true;
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logg.d( "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        
        if( requestCode == IAPController.RC_REQUEST){
            mIAPcontroller.handleActivityResult(requestCode, resultCode, data);
        } else if(resultCode == Activity.RESULT_OK &&requestCode == REQEUST_SHOP_DETAIL){
            requestCouponData(TYPE_CSHOP_ALL);
        } 
    }
    
    private void initColorStateListForLeftMenus() {
        try {
            mColorStateListForLeftMenus = ColorStateList.createFromXml(getResources(), getResources().getXml(R.color.selector_m3_leftmenu_textcolor));
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void findAllView(){
        m_tvLeftAll = (TextView) findViewById(R.id.tv_all);
        m_tvLeftAll.setOnClickListener(this);
        m_tvLeftPopular = (TextView) findViewById(R.id.tv_popular);
        m_tvLeftPopular.setOnClickListener(this);
        m_tvLeftPurchased = (TextView) findViewById(R.id.tv_purchased);
        m_tvLeftPurchased.setOnClickListener(this);

        mVLeftBoxes = new TextView[3];
        mVLeftBoxes[0] = m_tvLeftAll;
        mVLeftBoxes[1] = m_tvLeftPopular;
        mVLeftBoxes[2] = m_tvLeftPurchased;
        
        mLeftListLay = (LinearLayout)findViewById( R.id.coupon_list_left );
        mRightListLay = (LinearLayout)findViewById( R.id.coupon_list_right );
        mPurchasedListLay = (LinearLayout)findViewById( R.id.purchased_list );
        
        mViewGoldList = (View)findViewById( R.id.gold_item_list );
        mViewOwlList = (View)findViewById( R.id.owl_item_list );

        mTvNodata = (TextView)findViewById(R.id.tvNodata);
    }
    
    private void changeLeftBoxList(int type) {

        for (int i = 0; i < mVLeftBoxes.length; i++) {
            mVLeftBoxes[i].setBackgroundResource(R.drawable.selector_m3_leftmenu_btn);
            if (mColorStateListForLeftMenus != null) {
                mVLeftBoxes[i].setTextColor(mColorStateListForLeftMenus);
            }
        }
        mViewGoldList.setVisibility(View.VISIBLE);
        mViewOwlList.setVisibility(View.VISIBLE);

        mPurchasedListLay.setVisibility( View.VISIBLE );
        mLeftListLay.setVisibility( View.VISIBLE );
        mRightListLay.setVisibility( View.VISIBLE );

        switch (type) {
        case TYPE_CSHOP_ALL:
            mPurchasedListLay.setVisibility( View.GONE );
            
            m_tvLeftAll.setBackgroundResource(R.drawable.m3_leftmenu_btn);
            m_tvLeftAll.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
            break;
            
        case TYPE_CSHOP_POPULAR:
            mPurchasedListLay.setVisibility( View.GONE );

            m_tvLeftPopular.setBackgroundResource(R.drawable.m3_leftmenu_btn);
            m_tvLeftPopular.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
            break;
            
        case TYPE_CSHOP_PURCHASED:
            mViewGoldList.setVisibility(View.GONE);
            mViewOwlList.setVisibility(View.GONE);

            mLeftListLay.setVisibility( View.GONE );
            mRightListLay.setVisibility( View.GONE );
            
            mViewGoldList.setVisibility(View.GONE);
            mViewOwlList.setVisibility(View.GONE);
            
            m_tvLeftPurchased.setBackgroundResource(R.drawable.m3_leftmenu_btn);
            m_tvLeftPurchased.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
            break;
        }
    }
    
    void commonAreaSet(){
        TitleInfoView tiv = new TitleInfoView( this, (View)findViewById( R.id.title_lay ), TitleInfoView.TITLE_TYPE_NORMAL );
        tiv.updateView( R.string.menu_coupon_shop, TitleInfoView.TITLE_MODE_NORMAL);
 
        new MenuInfoView(this, (View) findViewById(R.id.menu_tab), CommonData.ENTRY_PONINT_CSHOP);

        //[SDJ 131008] 보내기에서 쿠폰함가기로 진입 시 하단 Tab 숨김 처리
        if(mStartedByCSend){
            ((View) findViewById(R.id.menu_tab)).setVisibility(View.GONE);
            
            RelativeLayout layout1 = (RelativeLayout) findViewById(R.id.main_lay);
            RelativeLayout.LayoutParams plControl = (RelativeLayout.LayoutParams) layout1.getLayoutParams();
            plControl.bottomMargin = 0;
            layout1.setLayoutParams(plControl);
        }
    }
    
    private void startCShopDetailActivity(ArrayList<ProductInfoData> list, int position) {
        Intent intent = new Intent(this, CouponShopDetailActivity.class);
        intent.putExtra(IntentDefine.EXTRA_CSHOP_LIST, list);
        intent.putExtra(IntentDefine.EXTRA_CSHOP_SELECT_POSITION, position);
        startActivityForResult(intent, REQEUST_SHOP_DETAIL);
    }
    
    private void requestCouponData(int mode){
        showProgressDialog();
        
        changeLeftBoxList(mode);
        
        switch (mode) {
        case TYPE_CSHOP_ALL:
            ProductProtocol ptAll = new ProductProtocol(iOkCouponList, iFail);
            ptAll.itemsGetNormal(CallBackParamWrapper.CODE_ITEMS_GET_NORMAL, "0", "2", SharedPrefManager.getLoginEmail(this));
            break;
        case TYPE_CSHOP_POPULAR:
            ProductProtocol ptPopular = new ProductProtocol(iOkPopularList, iFail);
            ptPopular.itemsGetPop(CallBackParamWrapper.CODE_ITEMS_GET_POP, SharedPrefManager.getLoginEmail(this));
            break;
        case TYPE_CSHOP_PURCHASED:
            PurchaseProtocol ptPurchased = new PurchaseProtocol(iOkPurchasedList, iFail);
            ptPurchased.orderGoldeggPurchaseHistory(0, SharedPrefManager.getLoginEmail(this), "-1");
            break;
        }
    }
    
    private void drawListData(int mode){
        new ShopItemController( this, mViewGoldList, mArrGoldListData, ShopItemController.LIST_MODE_GOLD, mClkGoldListener );
        new ShopItemController( this, mViewOwlList, mArrOwlListData, ShopItemController.LIST_MODE_OWL, mClkOwlListener );
        
        Logg.d( "drawListData size ="  + mArrCouponListData.size() );
        
        mTvNodata.setVisibility(View.GONE);

        if(mode == TYPE_CSHOP_ALL || mode == TYPE_CSHOP_POPULAR){
            if(mLeftListLay.getChildCount() > 0) mLeftListLay.removeAllViews();
            if(mRightListLay.getChildCount() > 0) mRightListLay.removeAllViews();
            
            for (int i = 0; i < mArrCouponListData.size(); i++) {
                if(i%2 == 0)
                    mLeftListLay.addView(CouponListView(mArrCouponListData.get( i ), i));
                else
                    mRightListLay.addView(CouponListView(mArrCouponListData.get( i ), i));
                    
            }
        } else {
            if(mPurchasedListLay.getChildCount() > 0) mPurchasedListLay.removeAllViews();
            
            if(mArrPurchaseListData != null && mArrPurchaseListData.size() > 0){
				//[SDJ 131015]리스트 없을 경우 안내 문구 추가
                mPurchasedListLay.setVisibility(View.VISIBLE);

                for (int i = 0; i < mArrPurchaseListData.size(); i++) {
                    mPurchasedListLay.addView(purchasedListView(mArrPurchaseListData.get( i ), i));
                }
                
            } else {
				//[SDJ 131015]리스트 없을 경우 안내 문구 추가
				mPurchasedListLay.setVisibility(View.GONE);
                mTvNodata.setVisibility(View.VISIBLE);
            }
        }
        
    }
    
    private View CouponListView(ProductInfoData data, final int index){
        ShopListAdapter adpater = null;                
        adpater = new ShopListAdapter(this);          
        
        View view = adpater.getView(data, index);
        view.setOnClickListener( new OnClickListener()
        {
            
            @Override
            public void onClick( View v )
            {
                startCShopDetailActivity(mArrCouponListData, index);
            }
        } );
        
        return view;
    }
    
    private View purchasedListView(PurchaseInfoData data, int index){
        PurchasedListAdapter adpater = null;                
        adpater = new PurchasedListAdapter(this);          
        
        View view = adpater.getView(data, index);
        
        return view;
    }
    
    private void showPurchasePopup(int nIndex){
        final ProductInfoData mData = mArrGoldListData.get( nIndex );
        
        String strTitle = getResources().getString(R.string.info);
        String strMessage = getString( R.string.txt_gold ) + " "
        					+ String.format( getResources().getString( R.string.txt_ask_buy_owl ), mData.getGoldCount() );
        DialogUtil.getConfirmCancelDialog( this, strTitle, strMessage, new DialogInterface.OnClickListener() {
            
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                if(which == DialogInterface.BUTTON_POSITIVE){
                    mIAPcontroller.buyProduct( mData.getProductID());        
                }
            }
        } ).show();
        
    }
    
    private void requestPurchaseAwl(final ProductInfoData data){
        int count = Integer.parseInt( data.getGoldCount() );
        
        if(count > SharedPrefManager.getInstance( this ).getGoldEggCount()){
            showPopupDone( R.string.info, R.string.txt_recommand_buy_gold_egg );
        } else {
            String title = getResources().getString( R.string.info );
            String message = getString( R.string.txt_owl ) + " "
            				+ String.format( getResources().getString( R.string.txt_ask_buy_owl ), data.getOwlCount() );
            DialogUtil.getConfirmCancelDialog(CouponShopActivity.this, title, message, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        showProgressDialog();
                        
                        PurchaseProtocol protocol = new PurchaseProtocol(iOkBuyOwl, iFail);
                        protocol.buyingRequestOwl(0, SharedPrefManager.getLoginEmail(CouponShopActivity.this), data.getOwlID());
                       break;
                    }
                }
            }).show();
        }
    }
    
    private void checkBuyOwlState(PurchaseInfoData data){
        Logg.d( "checkBuyOwlState" + data.getPurchasedCount() );
        
        int resultCode = Integer.parseInt( data.getPurchasedCount() );
        if(resultCode > 0){
            //구매 성공
            requestStampRemain();
        } else {
            //구매 실패
            closeProgressDialog();
            showPopupDone( R.string.info, R.string.txt_error_buy_owl );
        }
    }
    
    private void requestStampRemain(){
        PurchaseProtocol protocol = new PurchaseProtocol(new IWiseCallback() {
            
            @Override
            public boolean WiseCallback( CallbackParam param ) {
                PurchaseInfoData data = (PurchaseInfoData)param.param1;
                
                setGoldEggCount( data.getGoldCount() );
                setOwlCount( data.getFreeStampCount(), data.getPaidStampCount() );

                closeProgressDialog();
                Toast.makeText( CouponShopActivity.this, R.string.txt_complete_buy, Toast.LENGTH_SHORT ).show();

                return false;
            }
        }, iFail);
        protocol.orderStampRemain(0, SharedPrefManager.getLoginEmail(this));
    }
    
    @Override
    public void onClick( View v )
    {
        switch (v.getId()) {
        case R.id.tv_all:
            requestCouponData(TYPE_CSHOP_ALL);
            break;
        case R.id.tv_popular:
            requestCouponData(TYPE_CSHOP_POPULAR);
            break;
        case R.id.tv_purchased:
            requestCouponData(TYPE_CSHOP_PURCHASED);
            break;
        }
    }

    @Override
    protected int setContentViewId()
    {
        // TODO Auto-generated method stub
        return R.layout.activity_coupon_shop;
    }

}
