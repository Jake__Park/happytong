package kr.wisestone.happytong.ui.cshop;

import java.util.ArrayList;
import java.util.List;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.dataset.ProductInfoData;
import kr.wisestone.happytong.data.dataset.PurchaseInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.PurchaseProtocol;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.cshop.CouponDetailViewFactory.CouponDetailViewHolder;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CouponShopDetailActivity extends BaseActivity
{

    ViewPager mViewPager;
    CShopPagerAdapter mAdapter;

    ArrayList<ProductInfoData> mList;
    int mListSelectPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Logg.d("onCreated");

        if (!isRightIntentValues(getIntent())) {
            finish();
            return;
        }
        init();
        setCallback();
    }

    @SuppressWarnings("unchecked")
    private boolean isRightIntentValues(Intent intent) {
        mList = (ArrayList<ProductInfoData>) intent.getSerializableExtra(IntentDefine.EXTRA_CSHOP_LIST);
        mListSelectPosition = intent.getIntExtra(IntentDefine.EXTRA_CSHOP_SELECT_POSITION, 0);
        if (mList == null) {
            return false;
        }
        Logg.d("isRightIntentValues - true - receivedListSize : " + mList.size());
        return true;
    }

    private void init() {
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new CShopPagerAdapter(this, mList);

        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(mListSelectPosition);
    }

    private void setCallback() {
        mAdapter.setUsewNetworkCallback(new IWiseCallback() {
            @Override
            public boolean WiseCallback(CallbackParam param) {
                requestStampRemain();
                return false;
            }
        }, new IWiseCallback() {
            
            @Override
            public boolean WiseCallback( CallbackParam param ) {
                
                closeProgressDialog(); // [20130620 hyoungjoon.lee] OWL No.110 : 서버 요청 시 프로그래스바 추가
                
                Toast.makeText( CouponShopDetailActivity.this, R.string.txt_download_complete, Toast.LENGTH_SHORT ).show();
                setResult(RESULT_OK);
                finish();
                return false;
            }
        }, iFail, new DialogInterface.OnClickListener() {
            
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                
                closeProgressDialog(); // [20130620 hyoungjoon.lee] OWL No.110 : 서버 요청 시 프로그래스바 추가
                
                finish();
            }
        });
    }

    private void requestStampRemain(){
        PurchaseProtocol protocol = new PurchaseProtocol(new IWiseCallback() {
            
            @Override
            public boolean WiseCallback( CallbackParam param ) {
                PurchaseInfoData data = (PurchaseInfoData)param.param1;
                
                setGoldEggCount( data.getGoldCount() );
                setOwlCount( data.getFreeStampCount(), data.getPaidStampCount() );

                closeProgressDialog();
                showCompletePopup();
                return false;
            }
        }, iFail);
        protocol.orderStampRemain(0, SharedPrefManager.getLoginEmail(this));
    }
    
    private void showCompletePopup(){
        DialogUtil.getConfirmDialog( this, R.string.info, R.string.txt_complete_buy, new DialogInterface.OnClickListener() {
            
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                setResult(RESULT_OK);
                finish();
            }
        } ).show();
    }

    @Override
    public void onClick( View v )
    {
    }

    @Override
    protected int setContentViewId()
    {
        return R.layout.activity_coupon_detail;
    }

}

class CShopPagerAdapter extends PagerAdapter {

    Context mContext;
    List<ProductInfoData> mList;

    LayoutInflater mInflater;
    ImageLoader mImgLoader;

    IWiseCallback useDownOk, useBuyOk, useFail;
    DialogInterface.OnClickListener userClose;

    public CShopPagerAdapter(Context mContext, List<ProductInfoData> mList) {
        super();
        this.mContext = mContext;
        this.mList = mList;

        this.mInflater = LayoutInflater.from(mContext);
        this.mImgLoader = new ImageLoader(mContext);
    }

    public void setUsewNetworkCallback(IWiseCallback buyOk, IWiseCallback downOk, IWiseCallback fail, DialogInterface.OnClickListener close) {
        useBuyOk = buyOk;
        useDownOk = downOk;
        useFail = fail;
        userClose = close; 
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View v, Object o) {
        return v == o;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = CouponDetailViewFactory.create(mInflater);
        CouponDetailViewHolder holder = (CouponDetailViewFactory.CouponDetailViewHolder) v.getTag();

        ProductInfoData data = mList.get(position);
        
        holder.valid.setVisibility( View.GONE );
        
        holder.name.setText(data.getProductName());

        String mPrice = data.getPrice();
        
        if(mPrice.equals( "0" )){
            holder.allow.setImageResource(R.drawable.selector_sub5_03_down);
            holder.allow.setTag( 0 );
            holder.allow.setOnClickListener(OnClickListenerFactory.create(mContext, data, useDownOk, useFail, userClose));
            
			//SDJ[130812] 개선사항 적용
            holder.price.setText(R.string.free);
            holder.price.setTextColor(mContext.getResources().getColor(R.color.rgb_255_255_255));
        } else {
            holder.allow.setImageResource(R.drawable.selector_sub5_03_buy);
            holder.allow.setTag( 1 );
            holder.allow.setOnClickListener(OnClickListenerFactory.create(mContext, data, useBuyOk, useFail, userClose));
            
			//SDJ[130812] 개선사항 적용
            // holder.price.setText("황금알 " + mPrice + "개");
            holder.price.setText(mContext.getString(R.string.goldegg_count, mPrice));
            holder.price.setTextColor(mContext.getResources().getColor(R.color.rgb_6c2813));
        }

        if(data.getIsOwned().equals( "1" )){
            //보유 쿠폰
            holder.allow.setEnabled( false );
        } else {
            holder.allow.setEnabled( true );
        }
        
        holder.explain.setText(data.getCouponExplanation());
        holder.positions.setText((position + 1) + " / " + getCount());

        this.mImgLoader.DisplayImage(data.getImageURL(), holder.coupon);

        ((ViewPager) container).addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
        // TODO recycle
    }

}

class CouponDetailViewFactory {

    public static View create(LayoutInflater inflater) {
        View v = inflater.inflate(R.layout.cshop_coupon_viewpager_item, null);
        setViewHolder(v);
        return v;
    }

    private static CouponDetailViewHolder setViewHolder(View v) {
        CouponDetailViewHolder holder = new CouponDetailViewHolder();
        holder.name = (TextView) v.findViewById(R.id.tv_name);
        holder.price = (TextView) v.findViewById(R.id.tv_price);
        holder.explain = (TextView) v.findViewById(R.id.tv_explain);
        holder.coupon = (ImageView) v.findViewById(R.id.coupon_image);
        holder.title1 = (TextView) v.findViewById(R.id.title1);
        holder.title2 = (TextView) v.findViewById(R.id.title2);
        holder.title3 = (TextView) v.findViewById(R.id.title3);
        holder.positions = (TextView) v.findViewById(R.id.positions);
        holder.valid = (TextView) v.findViewById(R.id.coupon_valid);
        holder.allow = (ImageView) v.findViewById(R.id.iv_btn);
        v.setTag(holder);
        return holder;
    }

    static class CouponDetailViewHolder {
        TextView name;
        TextView price;
        TextView explain;
        
        ImageView coupon;
        TextView title1;
        TextView title2;
        TextView title3;
        TextView positions;
        TextView valid;
        ImageView allow;
    }

}

class OnClickListenerFactory {
    public static View.OnClickListener create(Context context, ProductInfoData data, IWiseCallback ok, IWiseCallback fail, DialogInterface.OnClickListener close) {
        return new UseClickListener(context, data, ok, fail, close);
    }

}

class UseClickListener implements View.OnClickListener {
    Context mContext;
    ProductInfoData data;
    IWiseCallback mOk, mFail;
    DialogInterface.OnClickListener mClose;
    
    public interface SyncListener {
        void onStartSynchronize();

        void onEndSynchronize(int retValue);
    }

    public UseClickListener(Context context, ProductInfoData data, IWiseCallback ok, IWiseCallback fail, DialogInterface.OnClickListener close) {
        super();
        mContext = context;
        this.data = data;
        
        mOk = ok;
        mFail = fail;
        mClose = close;
    }

    @Override
    public void onClick(View v) {
        Logg.e( "SDJTEST __ " + v.getTag() );
        if(v.getTag().equals( 0 )){
            showFreePopup();
        } else {
            checkBuyState();
        }
    }
    
    private void showFreePopup(){
        String title  = mContext.getResources().getString( R.string.info );
        String message = String.format( mContext.getResources().getString( R.string.ask_coupon_download ), data.getProductName() );
        
        DialogUtil.getConfirmCancelDialog( mContext, title, message, new DialogInterface.OnClickListener() {
            
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                if (which != DialogInterface.BUTTON_POSITIVE) {
                    return;
                }
                PurchaseProtocol mProtocol = new PurchaseProtocol(mOk, mFail);
                mProtocol.buyingRequestCoupon(0, SharedPrefManager.getLoginEmail(mContext), data.getCouponSEQ());

                ((BaseActivity)mContext).showProgressDialog(); // [20130620 hyoungjoon.lee] OWL No.110 : 서버 요청 시 프로그래스바 추가
            }
        } ).show();
    }
    
    private void checkBuyState(){
        int myGoldCount = SharedPrefManager.getInstance(mContext).getGoldEggCount();
        int productPrice = Integer.parseInt( data.getPrice() );
        
        if(myGoldCount < productPrice){
            DialogUtil.getConfirmDialog( mContext, R.string.info, R.string.txt_recommand_buy_gold_egg, mClose).show();
        } else {
            String title  = mContext.getResources().getString( R.string.info );
            String message = String.format( mContext.getResources().getString( R.string.txt_buy_coupon_question ), ""+productPrice, data.getProductName() );
            
            DialogUtil.getConfirmCancelDialog( mContext, title, message, new DialogInterface.OnClickListener() {
                
                @Override
                public void onClick( DialogInterface dialog, int which ) {
                    if (which != DialogInterface.BUTTON_POSITIVE) {
                        return;
                    }
                    PurchaseProtocol mProtocol = new PurchaseProtocol(mOk, mFail);
                    mProtocol.buyingRequestCoupon(0, SharedPrefManager.getLoginEmail(mContext), data.getCouponSEQ());
                    
                    ((BaseActivity)mContext).showProgressDialog(); // [20130620 hyoungjoon.lee] OWL No.110 : 서버 요청 시 프로그래스바 추가
                }
            } ).show();
        }
    }
}
