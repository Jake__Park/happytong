package kr.wisestone.happytong.ui.cshop;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.dataset.ProductInfoData;
import kr.wisestone.happytong.data.util.ImageLoader;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ShopListAdapter
{
    OnClickListener mClListener;

    private Context mCtx;
    private int mType;
    ImageLoader mImageLoader;
    private ViewHolder mHolder = null;

    public ShopListAdapter( Context context){
        mCtx = context;
        
        mImageLoader = new ImageLoader(mCtx);

    }

    public LinearLayout getLayout(){
        if(mHolder!=null){
            return mHolder.getTwoLayout();
        }
        return null;
    }

    class ViewHolder {
        int      nType;
        private LinearLayout twoLayout = null;

        ImageView mainImage;
        ImageView newImage;

        TextView headerText;
        TextView couponName;
        TextView myCoupon;
        TextView couponPrice;
        
        private View base;

        public ViewHolder(View base) {
            this.base = base;
        }

        void setViewHolder(int nType){

            mainImage       = (ImageView)base.findViewById(R.id.item_img);
            newImage       = (ImageView)base.findViewById(R.id.item_new);
            
            headerText     = (TextView)base.findViewById(R.id.header_text);
            couponName       = (TextView)base.findViewById(R.id.item_coupon_name);
            myCoupon        = (TextView)base.findViewById(R.id.item_my);
            couponPrice        = (TextView)base.findViewById(R.id.item_price);
            
        }

        public ImageView getMainImage() {
            if(mainImage == null){
                mainImage = (ImageView)base.findViewById(R.id.item_img);
            }
            return mainImage;
        }
        
        public ImageView getNewImage() {
            if(newImage == null){
                newImage = (ImageView)base.findViewById(R.id.item_new);
            }
            return newImage;
        }
        
        public TextView getHeaderText() {
            if(headerText == null){
                headerText = (TextView)base.findViewById(R.id.header_text);
            }
            return headerText;
        }
        
        public TextView getCouponName() {
            if(couponName == null){
                couponName = (TextView)base.findViewById(R.id.item_coupon_name);
            }
            return couponName;
        }
        public TextView getmyCoupon() {
            if(myCoupon == null){
                myCoupon = (TextView)base.findViewById(R.id.item_my);
            }
            return myCoupon;
        }
        
        public TextView getCouponPrice() {
            if(couponPrice == null){
                couponPrice = (TextView)base.findViewById(R.id.item_price);
            }
            return couponPrice;
        }

        public LinearLayout getTwoLayout() {
            if(twoLayout==null){
                twoLayout = (LinearLayout)base.findViewById(R.id.list_lay);
            }
            return twoLayout;
        }
    }

    int getResourceId(){
        int res = 0;

        res = R.layout.coupon_shop_list_item;

        return res;
    }

    public void setClickListener(OnClickListener listener){
        mClListener = listener;
    }

    public View getView(ProductInfoData data, int index) {

        View view = null;

        LayoutInflater inflater = ((Activity) mCtx).getLayoutInflater();
        view = inflater.inflate(getResourceId(), null);
        mHolder = new ViewHolder(view);
        view.setTag(mHolder);
        mHolder.setViewHolder(mType);


        if(mHolder!=null){
            if(data.getImageURL()!=null && data.getImageURL() != "" && !(data.getImageURL().equalsIgnoreCase("null"))){
                mImageLoader.DisplayImage(data.getImageURL(), mHolder.getMainImage());
            }
            
            if(data.getIsNew().equals( "1" )){
                mHolder.getNewImage().setVisibility( View.VISIBLE );
            } else {
                mHolder.getNewImage().setVisibility( View.GONE );
            }
            
            if(index == 0){
                mHolder.getHeaderText().setVisibility( View.VISIBLE );
            } else {
                mHolder.getHeaderText().setVisibility( View.INVISIBLE );
                
            }
            
            //text
            mHolder.getCouponName().setText(data.getProductName());
            
            if(data.getIsOwned().equals( "1" )){
                mHolder.getCouponPrice().setText(R.string.my_coupon);
                mHolder.getCouponPrice().setTextColor( mCtx.getResources().getColor(R.color.rgb_cf0000));
            } else {
                if(data.getPrice().equals( "0" )){
                    mHolder.getCouponPrice().setText(R.string.free);
                } else {
                	// "아이폰 - 황금알 20개, 안드로이드 - 20개" - 안드로이드를 아이폰 처럼 문구 수정. 20130807.jish
                	// mHolder.getCouponPrice().setText(mCtx.getString(R.string.txt_count, data.getPrice()));
                	mHolder.getCouponPrice().setText(mCtx.getString(R.string.goldegg_count, data.getPrice()));
                }
                mHolder.getCouponPrice().setTextColor( mCtx.getResources().getColor(R.color.rgb_666));
            }
        }            

        return view;
    }
}
