package kr.wisestone.happytong.ui.cshop;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.dataset.ProductInfoData;
import kr.wisestone.happytong.data.util.ImageLoader;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ShopItemController 
{
    final int                            MAX_COUNT      = 4;
    
    public static final int              LIST_MODE_GOLD = 10;
    public static final int              LIST_MODE_OWL  = 20;
    protected View                       m_vParent;
    protected int[]                      m_arnType;
    protected OnClickListener            m_clItem;
    protected ArrayList<ProductInfoData> m_Data;

    protected String[]                   m_arTitle;
    protected int                        m_nListType;
    protected int                        m_nMaxCount = 0;
    
    protected Context					 mContext;

    ImageLoader mImageLoader;
    public static int[] m_arnViewId = 
        {
            R.id.grid_common_item_1,
            R.id.grid_common_item_2,
            R.id.grid_common_item_3,
            R.id.grid_common_item_4
        };

    public ShopItemController(Context context, View parent, ArrayList<ProductInfoData> data, int mType, OnClickListener clItem)
    {
    	mContext = context;
        mImageLoader = new ImageLoader( context ); 
        setData(parent, data, mType, clItem);
        updateView();
    }
    
    public void setData(View parent, ArrayList<ProductInfoData> data, int mType, OnClickListener clItem)
    {
        m_vParent           = parent;
        m_clItem            = clItem;
        m_nListType         = mType;
        m_Data              = data;
        
        if(m_Data != null )
            m_nMaxCount = m_Data.size();
        
        if(m_nMaxCount > MAX_COUNT) 
            m_nMaxCount = MAX_COUNT;
    }
    
    public void updateView()
    {
        if(m_nListType == LIST_MODE_GOLD) {
            ((TextView)m_vParent.findViewById( R.id.grid_title_txt )).setText(R.string.txt_gold);
        } else {
            ((TextView)m_vParent.findViewById( R.id.grid_title_txt )).setText(R.string.txt_owl);
        }
        
        for(int nIndex = 0; nIndex < m_nMaxCount; nIndex++)
            updateView(nIndex);
    }
    
    public void updateView(int nIndex)
    {
        ProductInfoData mData = m_Data.get( nIndex );
        
        View view = m_vParent.findViewById( m_arnViewId[nIndex]);

        view.setPadding( 0, 0, 0, 0 );
        view.setVisibility( View.VISIBLE );
        view.setOnClickListener( m_clItem );
        
        if(m_nListType == LIST_MODE_GOLD) {
            ((TextView)view.findViewById( R.id.item_text )).setText(mData.getPrice());
            
        } else {
            ((TextView)view.findViewById( R.id.item_text )).setText(mContext.getString(R.string.goldegg_count, mData.getGoldCount()));
        }
        
        if(mData.getImageURL()!=null && mData.getImageURL() != "" && !(mData.getImageURL().equalsIgnoreCase("null"))){
            mImageLoader.DisplayImage(mData.getImageURL(), ((ImageView)view.findViewById( R.id.item_img )));
        }

        view.setTag( nIndex );
        view.setEnabled( true );
    }
}
