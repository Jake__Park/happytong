package kr.wisestone.happytong.ui.cshop;

import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.dataset.PurchaseInfoData;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.data.util.Utils;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PurchasedListAdapter
{
    OnClickListener mClListener;

    private Context mCtx;
    ImageLoader mImageLoader;
    private ViewHolder mHolder = null;

    public PurchasedListAdapter( Context context){
        mCtx = context;
        
        mImageLoader = new ImageLoader(mCtx);

    }

    public LinearLayout getLayout(){
        if(mHolder!=null){
            return mHolder.getTwoLayout();
        }
        return null;
    }

    class ViewHolder {
        private LinearLayout twoLayout = null;

        ImageView mainImage;

        TextView tvName;
        TextView tvPrice;
        TextView tvDate;
        
        private View base;

        public ViewHolder(View base) {
            this.base = base;
        }

        void setViewHolder(){

            mainImage       = (ImageView)base.findViewById(R.id.iv_photo);
            
            tvName     = (TextView)base.findViewById(R.id.tv_name);
            tvPrice       = (TextView)base.findViewById(R.id.tv_price);
            tvDate        = (TextView)base.findViewById(R.id.tv_date);
        }

        public ImageView getMainImage() {
            if(mainImage == null){
                mainImage = (ImageView)base.findViewById(R.id.iv_photo);
            }
            return mainImage;
        }
        
        public TextView getName() {
            if(tvName == null){
                tvName = (TextView)base.findViewById(R.id.tv_name);
            }
            return tvName;
        }
        public TextView getPrice() {
            if(tvPrice == null){
                tvPrice = (TextView)base.findViewById(R.id.tv_price);
            }
            return tvPrice;
        }
        
        public TextView getDate() {
            if(tvDate == null){
                tvDate = (TextView)base.findViewById(R.id.tv_date);
            }
            return tvDate;
        }

        public LinearLayout getTwoLayout() {
            if(twoLayout==null){
                twoLayout = (LinearLayout)base.findViewById(R.id.purchase_list_lay);
            }
            return twoLayout;
        }
    }

    int getResourceId(){
        int res = 0;

        res = R.layout.purchased_list_item;

        return res;
    }

    public void setClickListener(OnClickListener listener){
        mClListener = listener;
    }

    public View getView(PurchaseInfoData data, int index) {

        View view = null;

        LayoutInflater inflater = ((Activity) mCtx).getLayoutInflater();
        view = inflater.inflate(getResourceId(), null);
        mHolder = new ViewHolder(view);
        view.setTag(mHolder);
        mHolder.setViewHolder();


        if(mHolder!=null){
            if(data.getImageURL()!=null && data.getImageURL() != "" && !(data.getImageURL().equalsIgnoreCase("null"))){
                mImageLoader.DisplayImage(data.getImageURL(), mHolder.getMainImage());
            }
            
            //name
            mHolder.getName().setText(data.getProductName());
            
            //price
            mHolder.getPrice().setText( data.getPrice());
            
            //date
            String patternDateString = Utils.getDateFormatStringFromServerDateFormat(data.getPruchasedDate(), "yyyy-MM-dd", Locale.KOREA);

            mHolder.getDate().setText(patternDateString);

        }            

        return view;
    }
}
