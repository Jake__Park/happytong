package kr.wisestone.happytong.ui.sbox;

public enum StampBoxType {
	RECEIVED, SENT;

	public static int getIntValue(StampBoxType type) {
		return type.ordinal();
	}

	public static StampBoxType getTypeMatchedIntValue(int i) {
		switch (i) {
		case 0:
			return StampBoxType.RECEIVED;
		case 1:
			return StampBoxType.SENT;
		}
		return StampBoxType.RECEIVED;
	}

}
