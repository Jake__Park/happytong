package kr.wisestone.happytong.ui.sbox;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.util.ImageLoader;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

abstract class StampListAdapter extends BaseAdapter {

	Context						mContext;
	LayoutInflater				mInflater;

	ArrayList<StampInfoData>	mList;

	String						todayDateString;
	String						yesterdayDateString;
	final long					DayMillis			= 1000 * 60 * 60 * 24;

	ImageLoader					mImgLoader;
	Picasso						mPicasso;
	final boolean				usingImageLoader	= false;

	public StampListAdapter(Context mContext, ArrayList<StampInfoData> mList) {
		super();
		this.mContext = mContext;
		this.mList = mList;
		this.mInflater = LayoutInflater.from(mContext);
		setTodayYesterdayString();

		if (usingImageLoader) {
			mImgLoader = new ImageLoader(mContext);
		} else {
			mPicasso = Picasso.with(mContext);
		}
	}

	private void setTodayYesterdayString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd EEEE", Locale.KOREA);
		long currMillis = System.currentTimeMillis();

		Date date = new Date(currMillis);
		todayDateString = sdf.format(date);

		date.setTime(currMillis - DayMillis);
		yesterdayDateString = sdf.format(date);
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public StampInfoData getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.stampbox_list_item, null);
			holder = new Holder();
			holder.layout = convertView.findViewById(R.id.layout);
			holder.labelDate = (TextView) convertView.findViewById(R.id.label_date);
			holder.labelCount = (TextView) convertView.findViewById(R.id.label_count);
			holder.thumnail = (ImageView) convertView.findViewById(R.id.thumnail);
			holder.thumnailStatus = (ImageView) convertView.findViewById(R.id.thumnail_status);
			holder.name = (TextView) convertView.findViewById(R.id.text_name);
			holder.time = (TextView) convertView.findViewById(R.id.text_time);
			holder.stampInfo = (TextView) convertView.findViewById(R.id.text_stamp_count);
			holder.chkbox = (CheckBox) convertView.findViewById(R.id.coupon_item_chkbox);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		StampInfoData data = mList.get(position);
		holder.time.setText(data.mTime);

		// 쿠폰 이미지
		if (usingImageLoader) {
			mImgLoader.DisplayImage(data.m_couponCropIMGURL, holder.thumnail);
		} else {
			mPicasso.load(data.m_couponCropIMGURL).placeholder(R.drawable.sub3_default).into(holder.thumnail);
		}
		
		// TODO StampInfo
		holder.stampInfo.setText(mContext.getString(R.string.stampt_count_count, "10"));

		setViewBoxType(holder, data);

		if (data.mDateLabel.equals(todayDateString)) {
			holder.labelDate.setText(mContext.getString(R.string.today));
		} else if (data.mDateLabel.equals(yesterdayDateString)) {
			holder.labelDate.setText(mContext.getString(R.string.yesterday));
		} else {
			holder.labelDate.setText(data.mDateLabel);
		}
		holder.labelCount.setText(data.mDateLabelCount);

		return convertView;
	}

	abstract void setViewBoxType(Holder holder, StampInfoData data);

	final OnClickListener	NothingOnClick	= new OnClickListener() {
												@Override
												public void onClick(View v) {
													return;
												}
											};

	class Holder {
		View		layout;
		TextView	labelDate;
		TextView	labelCount;
		ImageView	thumnail;
		ImageView	thumnailStatus;
		TextView	name;
		TextView	time;
		TextView	stampInfo;
		CheckBox	chkbox;
	}

}

class ReceivedStampListAdapter extends StampListAdapter {

	public ReceivedStampListAdapter(Context mContext, ArrayList<StampInfoData> mList) {
		super(mContext, mList);
	}

	@Override
	void setViewBoxType(Holder holder, StampInfoData data) {
		holder.chkbox.setVisibility(View.GONE);
		holder.thumnailStatus.setVisibility(View.GONE);
		holder.name.setText(data.mSenderNameNick);
	}

}

class SentStampListAdapter extends StampListAdapter {

	public SentStampListAdapter(Context mContext, ArrayList<StampInfoData> mList) {
		super(mContext, mList);
	}

	@Override
	void setViewBoxType(Holder holder, StampInfoData data) {
		holder.chkbox.setVisibility(View.GONE);
		holder.thumnailStatus.setVisibility(View.GONE);
		holder.name.setText(data.mReceiverNameNick);
	}

}

class UsedStampListAdapter extends StampListAdapter {

	private boolean	isCheckMode	= false;
	String			mMyId;

	public UsedStampListAdapter(Context mContext, ArrayList<StampInfoData> mList) {
		super(mContext, mList);
		mMyId = SharedPrefManager.getLoginEmail(mContext);
	}

	public void setCheckMode(boolean mode) {
		isCheckMode = mode;
		if (!isCheckMode) {
			clearCheckedItem();
		}
		notifyDataSetChanged();
	}

	private void clearCheckedItem() {
		for (StampInfoData data : mList) {
			data.isChecked = false;
		}
	}

	public boolean isCheckMode() {
		return isCheckMode;
	}

	public void toggleItem(int position) {
		StampInfoData data = mList.get(position);
		data.isChecked = !data.isChecked;
		notifyDataSetChanged();
	}

	@Override
	void setViewBoxType(Holder holder, StampInfoData data) {
		if (isCheckMode) {
			holder.chkbox.setVisibility(View.VISIBLE);
			holder.chkbox.setChecked(data.isChecked);
		} else {
			holder.chkbox.setVisibility(View.GONE);
		}

		// 쿠폰상태아이콘은, 완료-부엉이, 기간만료-달력
		// flag “1” or “2” (1: 쿠폰이 보내진상태, 2: 쿠폰이 사용된 상태)
		// requestState “0” or “1” (0: 쿠폰 사용 요청 x, 1: 쿠폰 사용 요청 있음)
		// endCoupon “0” or “1” (만료쿠폰 0: 쿠폰이 만료되지않음, 1: 쿠폰이 만료됨)
		holder.thumnailStatus.setVisibility(View.VISIBLE);
		// 여기 데이터는 사용/완료 쿠폰들뿐. 따라서 미사용기간만료 or 사용 밖에 없나??
		if ("0".equals(data.m_requestState)) {
			holder.thumnailStatus.setImageResource(R.drawable.sub3_01_period);
		} else {
			holder.thumnailStatus.setImageResource(R.drawable.sub3_01_complete);
		}

		if (data.m_senderID.equals(mMyId)) {
			holder.name.setText(data.mReceiverNameNick);
		} else {
			holder.name.setText(data.mSenderNameNick);
		}

	}

	public List<StampInfoData> getCheckedItemList() {
		List<StampInfoData> list = new ArrayList<StampInfoData>();
		for (StampInfoData data : mList) {
			if (data.isChecked) {
				list.add(data);
			}
		}
		return list;
	}

}