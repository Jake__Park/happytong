package kr.wisestone.happytong.ui.sbox;

public enum StampType {

	Center("0"), Up("1"), Left("2"), Right("3"), NA("999");

	String str;

	private StampType(String str) {
		this.str = str;
	}

	public static StampType getCouponType(String str) {
		if (str.equals("0")) {
			return StampType.Center;
		} else if (str.equals("1")) {
			return StampType.Up;
		} else if (str.equals("2")) {
			return StampType.Left;
		} else if (str.equals("3")) {
			return StampType.Right;
		}
		return StampType.NA;
	}

	@Override
	public String toString() {
		return name();
	}

}
