package kr.wisestone.happytong.ui.sbox;

import java.util.ArrayList;
import java.util.List;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.dialog.DialogUtil.OnEditTextDialogListener;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.Logg;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class StampDetailActivity extends BaseActivity {

	private static final int	MAX_ONEPAGE_ITEM_COUNT	= 30;

	Context						mContext;
	LayoutInflater				mInflater;
	Picasso						mPicasso;

	TitleInfoView				mTitleInfoView;

	ViewPager					mViewPager;
	LinearLayout				mIndicatorLayout;

	StampDetailPagerAdapter		mViewPagerAdapter;

	List<StampInfoData>			mList;
	StampBoxType				mStampType;

	boolean						mIsSender;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = getApplicationContext();
		mInflater = LayoutInflater.from(mContext);

		mPicasso = Picasso.with(mContext);
		// mPicasso.load(data.m_couponCropIMGURL).placeholder(R.drawable.sub3_default).into(holder.thumnail);

		if (!isRightIntentValues(getIntent())) {
			finish();
			return;
		}

		mIsSender = true;

		initTitleView();
		init();
	}

	private void initTitleView() {
		mTitleInfoView = new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL);
		// mTitleInfoView.updateView(R.string.stamp_box, TitleInfoView.TITLE_STAMP_MODE);
		mTitleInfoView.updateTitle("이름 (닉네임)");
	}

	@SuppressWarnings("unchecked")
	private boolean isRightIntentValues(Intent intent) {
		Logg.d("isRightIntentValues - true");
		// TODO 루틴 추가 필요
		mList = new ArrayList<StampInfoData>();
		mList.addAll(getDummy());
		mStampType = StampBoxType.RECEIVED;
		return true;
	}

	private List<StampInfoData> getDummy() {
		List<StampInfoData> list = new ArrayList<StampInfoData>();
		for (int i = 0; i < 42; i++) {
			if (i == 20) {
				list.add(new StampInfoData(i, "비행기", ""));
			} else {
				list.add(new StampInfoData(i, "", ""));
			}
		}

		return list;
	}

	private void init() {
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPagerAdapter = new StampDetailPagerAdapter(mList, mStampType);

		mViewPager.setAdapter(mViewPagerAdapter);
		initIndicator();

		mViewPager.setCurrentItem(mViewPagerAdapter.getLastPage());
	}

	private void initIndicator() {
		mIndicatorLayout = (LinearLayout) findViewById(R.id.indicator);
		final View vs[] = new View[mViewPagerAdapter.getCount()];

		for (int i = 0; i < vs.length; i++) {
			// mInflater.inflate(R.layout., mIndicatorLayout);
			vs[i] = new View(getApplicationContext());
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(10, 10);
			lp.leftMargin = 10;
			vs[i].setLayoutParams(lp);
			vs[i].setBackgroundColor(Color.YELLOW);
			mIndicatorLayout.addView(vs[i]);
		}

		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageScrollStateChanged(int state) {
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
				for (View v : vs) {
					v.setBackgroundColor(Color.YELLOW);
				}
				vs[position].setBackgroundColor(Color.RED);
			}

		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		}
	}

	@Override
	protected int setContentViewId() {
		return R.layout.activity_stamp_detail;
	}

	/**
	 * 스탬프 클릭 - 보낸 사람일 경우
	 * 
	 * @param data
	 */
	private void onItemClickBySender(StampInfoData data) {
		
		boolean b = TextUtils.isEmpty(data.mark);

		getEditTextDialog(this, getString(R.string.register_mark), "", 10, new OnEditTextDialogListener() {
			@Override
			public void onEditTextDialogOk(String text) {
			}

			@Override
			public void onEditTextDialogDelete() {
				DialogUtil.getConfirmDialog(StampDetailActivity.this, "", getString(R.string.complete_delete_mark), null).show();
			}

			@Override
			public void onEditTextDialogCancel() {
			}
		}, b).show();

		if (data != null) {
			// 스탬프 있는 영역 선택
			// 쿠폰 발송 가능
		} else {
			// 스탬프 받지 않은 영역 선택
			// 비어있을 경우, 목표 추가 다이얼로그 (확인, 취소)
			// 내용있을 경우, 수정 다이얼로그 (삭제, 확인, 취소)
		}
	}

	/**
	 * 스탬프 클릭 - 받은 사람일 경우
	 * 
	 * @param data
	 */
	public void onItemClickByReceiver(StampInfoData data) {
		if (data != null) {
			// 스탬프 있는 영역 선택 - 조르기
		} else {
			// 스탬프 없는 영역 선택 - 목표 보여주기
		}
	}

	/**
	 * 뷰페이저. 주어진 리스트를 바탕으로 페이징을 처리하기 위함
	 */
	class StampDetailPagerAdapter extends PagerAdapter implements OnItemClickListener {

		List<StampInfoData>			mList;
		String						mMyId;
		StampDetailGridViewAdapter	mGridViewAdapter;

		int							pageCount;

		public StampDetailPagerAdapter(List<StampInfoData> mList, StampBoxType type) {
			super();
			this.mList = mList;

			this.mMyId = SharedPrefManager.getLoginEmail(mContext);

			// pageCount = (mList.size() / MAX_ONEPAGE_ITEM_COUNT) + 1;
			pageCount = (mList.size() / MAX_ONEPAGE_ITEM_COUNT);

			if (mList.size() == 0) {
				pageCount = 0;
			} else {
				int quota = mList.size() / MAX_ONEPAGE_ITEM_COUNT;
				int remainder = mList.size() % MAX_ONEPAGE_ITEM_COUNT;

				pageCount = quota + (remainder > 0 ? 1 : 0);
			}

		}

		public int getLastPage() {
			return pageCount - 1;
		}

		@Override
		public int getCount() {
			return pageCount;
		}

		@Override
		public boolean isViewFromObject(View v, Object o) {
			return v == o;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			View v = mInflater.inflate(R.layout.stamp_detail_viewpager_item, null);
			GridView gridView = (GridView) v;

			mGridViewAdapter = new StampDetailGridViewAdapter(position, getSubList(position));
			gridView.setAdapter(mGridViewAdapter);
			gridView.setOnItemClickListener(this);
			((ViewPager) container).addView(v);
			return v;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			StampInfoData data = mGridViewAdapter.getItem(position);
			if (mIsSender) {
				onItemClickBySender(data);
			} else {
				onItemClickByReceiver(data);
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
			// TODO recycle
		}

		private List<StampInfoData> getSubList(int position) {
			int start, end, maxCount;
			maxCount = mList.size();

			start = position * MAX_ONEPAGE_ITEM_COUNT;
			end = start + MAX_ONEPAGE_ITEM_COUNT;

			if (end > maxCount) {
				end = start + (maxCount % MAX_ONEPAGE_ITEM_COUNT);
			}

			return mList.subList(start, end);
		}
	}

	/**
	 * Pager에 표시되는 GridView Adapter
	 */
	class StampDetailGridViewAdapter extends BaseAdapter {

		List<StampInfoData>	mList;
		int					mPageCount;

		public StampDetailGridViewAdapter(int page, List<StampInfoData> mList) {
			super();
			this.mList = mList;
			this.mPageCount = page * MAX_ONEPAGE_ITEM_COUNT;
		}

		@Override
		public int getCount() {
			return MAX_ONEPAGE_ITEM_COUNT;
		}

		@Override
		public StampInfoData getItem(int position) {
			if (position < mList.size()) {
				return mList.get(position);
			}
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder holder;

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.stamp_detail_gridview_item, null);
				holder = new Holder();
				holder.img = (ImageView) convertView.findViewById(R.id.img);
				holder.text = (TextView) convertView.findViewById(R.id.text);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}

			if (position < mList.size()) {
				StampInfoData data = mList.get(position);
				holder.img.setImageResource(R.drawable.sub3_01_complete);
				holder.img.setBackgroundColor(Color.BLACK);
				if (TextUtils.isEmpty(data.mark)) {
					holder.text.setText(String.valueOf(mPageCount + position + 1));
				} else {
					holder.text.setText(data.mark);
				}
			} else {
				holder.img.setImageResource(0);
				holder.img.setBackgroundColor(Color.BLUE);
				holder.text.setText(String.valueOf(mPageCount + position + 1));
			}

			return convertView;
		}

		class Holder {
			ImageView	img;
			TextView	text;
		}
	}

	/**
	 * 편집텍스트 다이얼로그 - 삭제, 확인, 취소
	 * 
	 * @param context
	 * @param title
	 *            타이틀
	 * @param defaultText
	 *            기본텍스트
	 * @param maxLength
	 *            길이제한
	 * @param l
	 * @return
	 */
	public static Dialog getEditTextDialog(Context context, String title, String defaultText, final int maxLength, final OnEditTextDialogListener l, boolean showDelButton) {
		final EditText editText = new EditText(context.getApplicationContext());
		InputFilter[] filters = { new InputFilter.LengthFilter(maxLength) };
		editText.setFilters(filters);
		editText.setSingleLine(true);
		if (!TextUtils.isEmpty(defaultText)) {
			editText.setText(defaultText);
			if (defaultText.length() > 0) {
				editText.setSelection(defaultText.length());
			}
		}

		DialogInterface.OnClickListener r = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (l == null) {
					return;
				}
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					l.onEditTextDialogDelete();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					l.onEditTextDialogCancel();
					break;
				case DialogInterface.BUTTON_NEUTRAL:
					l.onEditTextDialogOk(editText.getText().toString());
					break;
				}
			}
		};

		AlertDialog.Builder adb = new AlertDialog.Builder(context);
		adb.setTitle(title);
		adb.setView(editText);
		if (showDelButton) {
			adb.setPositiveButton(R.string.delete, r);
			adb.setNeutralButton(android.R.string.ok, r);
		} else {
			adb.setNeutralButton(android.R.string.ok, r);
		}
		adb.setNegativeButton(android.R.string.cancel, r);
		adb.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				if (l != null) {
					l.onEditTextDialogCancel();
				}
			}
		});
		return adb.create();
	}

	/**
	 * EditTextDialog Listener
	 */
	interface OnEditTextDialogListener {
		/**
		 * EditTextDialog - ok Button - return EditText values
		 * 
		 * @param text
		 */
		void onEditTextDialogOk(String text);

		/**
		 * EditTextDialog - cancel Button or cancel key
		 */
		void onEditTextDialogCancel();

		void onEditTextDialogDelete();

	}

}
