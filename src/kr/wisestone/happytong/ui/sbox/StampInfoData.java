package kr.wisestone.happytong.ui.sbox;

import java.io.Serializable;
import java.util.Locale;

import kr.wisestone.happytong.data.dataset.CouponInfoData;
import kr.wisestone.happytong.data.util.Utils;

class StampInfoData extends CouponInfoData implements Serializable {
	private static final long	serialVersionUID	= 6936525681949433389L;

	int							position;
	String						mark;
	String						imgUrl;

	public StampInfoData() {
		super();
	}

	public StampInfoData(int position, String mark, String imgUrl) {
		super();
		this.position = position;
		this.mark = mark;
		this.imgUrl = imgUrl;
	}

	public String	mDateLabel;
	public String	mDateLabelCount;
	public String	mTime;
	public boolean	isChecked;
	public String	mSenderNameNick;
	public String	mReceiverNameNick;

	public void setCouponInfoData(CouponInfoData d, String dateLabel) {
		this.setSenderPhotoURL(d.getSenderPhotoURL());
		this.setCouponCropIMGURL(d.getCouponCropIMGURL());
		this.setCouponType(d.getCouponType());
		this.setValidPeriod(d.getValidPeriod());
		this.setSenderID(d.getSenderID());
		this.setCreatedCouponDate(d.getCreatedCouponDate());
		this.setSenderNickname(d.getSenderNickname());
		this.setCouponIMGURL(d.getCouponIMGURL());
		this.setCouponText(d.getCouponText());
		this.setFlag(d.getFlag());
		this.setRequestState(d.getRequestState());
		this.setReceiverID(d.getReceiverID());
		this.setCouponSEQ(d.getCouponSEQ());
		this.setBoxSEQ(d.getBoxSEQ());
		this.setReceiverPhotoURL(d.getReceiverPhotoURL());
		this.setReceiverNickname(d.getReceiverNickname());
		this.setEndCoupon(d.getEndCoupon());
		this.mTime = Utils.getDateFormatStringFromServerDateFormat(d.getCreatedCouponDate(), "a h:mm", Locale.ENGLISH);
		this.mDateLabel = dateLabel;
	}

}