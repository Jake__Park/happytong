package kr.wisestone.happytong.ui.sbox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.CouponInfoData;
import kr.wisestone.happytong.data.dataset.PurchaseInfoData;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.CouponProtocol;
import kr.wisestone.happytong.data.protocol.PurchaseProtocol;
import kr.wisestone.happytong.data.protocol.UserProtocol;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.plugin.FacebookLinker;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.MenuInfoView;
import kr.wisestone.happytong.ui.widget.SubTitleView;
import kr.wisestone.happytong.ui.widget.SubTitleView.SubTitleViewListener;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class StampBoxActivity extends BaseActivity {

	protected static final int	MSG_FACEBOOK_COMPLETE	= 0x24;
	protected static final int	MSG_FACEBOOK_FAIL		= 0x25;

	Context						mContext;

	TitleInfoView				mTitleInfoView;
	SubTitleView				mSubTitleView;

	TextView					mVLeftReceiveSBox;
	TextView					mVLeftSendSBox;
	TextView[]					mVLeftBoxes;
	ColorStateList				mColorStateListForLeftMenus;

	View						mVLeftFacebook;

	View						mHelpLayout;

	ArrayList<StampInfoData>	mReceivedStampList;
	ArrayList<StampInfoData>	mSentStampList;

	Map<String, String>			mFriendMap;

	View						mReceivedLayout;
	View						mSentLayout;

	ListView					mReceivedStampListView;
	ListView					mSentStampListView;

	StampListAdapter			mReceivedStampListAdapter;
	StampListAdapter			mSentStampListAdapter;

	private String				ID;

	private enum UsedStampBoxState {
		NORMAL, EDIT
	}

	private UsedStampBoxState	mUsedStampBoxState	= UsedStampBoxState.NORMAL;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		decideVisibleHelpLayout();

		mReceivedStampList = new ArrayList<StampInfoData>();
		mSentStampList = new ArrayList<StampInfoData>();
		mFriendMap = getFriendIdNameMap();

		ID = SharedPrefManager.getLoginEmail(this);

		new MenuInfoView(this, (View) findViewById(R.id.menu_tab), CommonData.ENTRY_PONINT_CBOX);
		initTitleView();
		initSubTitleView();
		init();
		initColorStateListForLeftMenus();

		changeStampBoxList(StampBoxType.getTypeMatchedIntValue(getStampBoxTypeFromPref()));
		getServerData();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, R.string.update).setIcon(android.R.drawable.ic_popup_sync);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Logg.d(item.getItemId());
		getServerData();
		return super.onOptionsItemSelected(item);
	}

	private void decideVisibleHelpLayout() {
		mHelpLayout = findViewById(R.id.stampbox_help);

		if (SharedPrefManager.getInstance(this).getShownCouponBoxHelp() > 0) {
			mHelpLayout.setVisibility(View.GONE);
			return;
		}

		findViewById(R.id.stampbox_help_img).setOnClickListener(mHelpClickListener);
		mHelpLayout.setVisibility(View.VISIBLE);
		mHelpLayout.setOnClickListener(mHelpClickListener);
	}

	View.OnClickListener	mHelpClickListener	= new View.OnClickListener() {
													@Override
													public void onClick(View v) {
														mHelpLayout.setVisibility(View.GONE);
														SharedPrefManager.getInstance(StampBoxActivity.this).setShownStampBoxHelp(1);
													}
												};

	/**
	 * Preference에 저장된 쿠폰박스타입 문자열을 정수형으로 반환
	 * 
	 * @return
	 */
	private int getStampBoxTypeFromPref() {
		String type = SharedPrefManager.getInstance(this).getMoveCBoxType();

		if (TextUtils.isEmpty(type)) {
			return 0;
		}

		int iType = 0;
		try {
			iType = Integer.parseInt(type);
		} catch (NumberFormatException e) {
			iType = 0;
		}
		return iType;
	}

	private void initSubTitleView() {
		mSubTitleView = (SubTitleView) findViewById(R.id.subtitleview);
		mSubTitleView.setListener(new SubTitleViewListener() {
			@Override
			public void onEggClickListener() {
			}
		});
		setSubTitleOwlEgg();
	}

	private void setSubTitleOwlEgg() {
		mSubTitleView.setFreeOwlCount(SharedPrefManager.getInstance(this).getFreeOwlCount());
		mSubTitleView.setPayOwlCount(String.valueOf(SharedPrefManager.getInstance(this).getPurchasedOwlCount()));
		mSubTitleView.setEggCount(String.valueOf(SharedPrefManager.getInstance(this).getGoldEggCount()));
	}

	/**
	 * 소스상에서 셀렉터 주기
	 */
	private void initColorStateListForLeftMenus() {
		try {
			mColorStateListForLeftMenus = ColorStateList.createFromXml(getResources(), getResources().getXml(R.color.selector_m3_leftmenu_textcolor));
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 전체 데이터 요청 - 순차적으로 진행하도록 처리
	 */
	private void getServerData() {
		showProgressDialog();

		new CouponProtocol(iOkSBoxReceived, iFail).couponReceived(CallBackParamWrapper.CODE_COUPON_RECEIVED, ID);
	}

	/**
	 * 받은 스탬프 콜백 - 완료 후 - 보낸 쿠폰 데이터 요청
	 */
	final IWiseCallback	iOkSBoxReceived	= new IWiseCallback() {
											@SuppressWarnings("unchecked")
											@Override
											public boolean WiseCallback(CallbackParam param) {
												new CouponProtocol(iOkSBoxSent, iFail).couponSent(CallBackParamWrapper.CODE_COUPON_SENT, ID);
												Logg.d("iOkCBoxReceived");
												if (param.param1 != null) {
													mReceivedStampList = convertDataListToData2List((ArrayList<CouponInfoData>) param.param1);
												} else {
													mReceivedStampList.clear();
												}
												return false;
											}
										};

	/**
	 * 보낸 스탬프 콜백 - 완료 후 - 사용 쿠폰 데이터 요청
	 */
	final IWiseCallback	iOkSBoxSent		= new IWiseCallback() {
											@SuppressWarnings("unchecked")
											@Override
											public boolean WiseCallback(CallbackParam param) {
												Logg.d("iOkCBoxSent");
												if (param.param1 != null) {
													mSentStampList = convertDataListToData2List((ArrayList<CouponInfoData>) param.param1);
												} else {
													mSentStampList.clear();
												}
												getServerDataComplete();
												return false;
											}
										};

	/**
	 * 리스트 요청 완료시 호출
	 */
	private void getServerDataComplete() {
		closeProgressDialog();
		setListView();
		updateBoxCount();

		changeStampBoxList(StampBoxType.getTypeMatchedIntValue(getStampBoxTypeFromPref()));
		moveDetailonPush();
	}

	/**
	 * 스탬프 상세 페이지 이동 처리 - 푸시에서 넘어온 경우
	 */
	private void moveDetailonPush() {

		// String boxSeq = SharedPrefManager.getInstance(this).getMoveCBoxSeq();
		//
		// if (TextUtils.isEmpty(boxSeq)) {
		// // 쿠폰 아이디가 없을 경우에는, 잘못 저장된 것이므로 Preference에 저장되어 있는 값 초기화 시킴
		// SharedPrefManager.getInstance(this).setMoveCBoxSeq("");
		// SharedPrefManager.getInstance(this).setMoveCBoxType("");
		// return;
		// }
		//
		// // 2013.06.19.jish
		// // 받은쿠폰이거나 보낸쿠폰이거나 유효기간이 지나서 받은 쿠폰임에도 완료쿠폰, 보낸 쿠폰임에도 완료쿠폰으로 갈 수 있음
		// // 위와 같은 상황을 처리하기 위하여 해당 쿠폰함에서 검사를 진행한후, 완료쿠폰함으로 보내는 동작을 하도록 한다.
		// switch (getCouponBoxTypeFromPref()) {
		// case 0:
		// startCouponDetailActivityDecideItem(mReceivedStampList, StampBoxType.RECEIVED, boxSeq);
		// break;
		// case 1:
		// startCouponDetailActivityDecideItem(mSentStampList, StampBoxType.SENT, boxSeq);
		// break;
		//
		// SharedPrefManager.getInstance(this).setMoveCBoxSeq("");
		// SharedPrefManager.getInstance(this).setMoveCBoxType("");
	}

	private void startStampDetailActivityDecideItem(ArrayList<StampInfoData> couponList, StampBoxType couponBoxType, String boxSeq) {
		int itemPosition = getCouponBoxSeqMatchListPosition(couponList, boxSeq);

		// if (itemPosition < 0) {
		// itemPosition = getCouponBoxSeqMatchListPosition(mUsedCouponList, boxSeq);
		// if (itemPosition < 0) {
		// itemPosition = 0;
		// }
		// changeCouponBoxList(StampBoxType.USED);
		// startCouponDetailActivity(mUsedCouponList, StampBoxType.USED, itemPosition);
		// } else {
		// startCouponDetailActivity(couponList, couponBoxType, itemPosition);
		// }
		startStampDetailActivity(couponList, couponBoxType, itemPosition);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		getServerData();
	}

	/**
	 * 지정된 리스트에서 boxSeq맞는 값 찾아서, 해당 포지션 리턴
	 * 
	 * @param list
	 * @param seq
	 * @return
	 */
	private int getCouponBoxSeqMatchListPosition(ArrayList<StampInfoData> list, String seq) {
		for (int i = 0; i < list.size(); i++) {
			StampInfoData data = list.get(i);
			if (data.m_boxSEQ.equals(seq)) {
				return i;
			}
		}
		return -1;
	}

	protected void setListView() {
		mReceivedStampListAdapter = new ReceivedStampListAdapter(this, mReceivedStampList);
		mReceivedStampListView.setAdapter(mReceivedStampListAdapter);
		mReceivedStampListView.setEmptyView(findViewById(R.id.received_empty));

		mSentStampListAdapter = new SentStampListAdapter(this, mSentStampList);
		mSentStampListView.setAdapter(mSentStampListAdapter);
		mSentStampListView.setEmptyView(findViewById(R.id.sent_empty));
	}

	/**
	 * 좌측 쿠폰함 카운트 갱신
	 */
	protected void updateBoxCount() {
		// mVLeftReceiveSBox.setText(getString(R.string.received_stamp_box) + "\n" + getString(R.string.round_bracket_count, String.valueOf(mReceivedStampList.size())));
		// mVLeftSendSBox.setText(getString(R.string.sent_stamp_box) + "\n" + getString(R.string.round_bracket_count, String.valueOf(mSentStampList.size())));
	}

	/**
	 * 서버에서 내려받은 데이터 리스트를, 실제 사용하는 데이터 리스트로 변환<br>
	 * 시간 데이터 삽입, 날짜 데이터 삽입, 카운트 세팅
	 * 
	 * @param list
	 * @return
	 */
	private ArrayList<StampInfoData> convertDataListToData2List(ArrayList<CouponInfoData> list) {
		ArrayList<StampInfoData> nList = new ArrayList<StampInfoData>();
		Map<String, ArrayList<StampInfoData>> map = new LinkedHashMap<String, ArrayList<StampInfoData>>();
		for (CouponInfoData data : list) {
			String patternDateString = Utils.getDateFormatStringFromServerDateFormat(data.getCreatedCouponDate(), "yyyy/MM/dd EEEE", Locale.KOREA);

			if (!map.containsKey(patternDateString)) {
				map.put(patternDateString, new ArrayList<StampInfoData>());
			}
			StampInfoData data2 = new StampInfoData();
			data2.setCouponInfoData(data, patternDateString);

			// 쿠폰함에 닉뿐아니라 이름까지 보여져야함
			if (mFriendMap.containsKey(data.m_senderID)) {
				data2.mSenderNameNick = mFriendMap.get(data.m_senderID) + "(" + data.m_senderNickname + ")";
			} else {
				data2.mSenderNameNick = data.m_senderNickname;
			}
			if (mFriendMap.containsKey(data.m_receiverID)) {
				data2.mReceiverNameNick = mFriendMap.get(data.m_receiverID) + "(" + data.m_receiverNickname + ")";
			} else {
				data2.mReceiverNameNick = data.m_receiverNickname;
			}

			map.get(patternDateString).add(data2);
		}

		// 첫번째 레이블을 표시해줄 아이템에 카운트 세팅
		Iterator<String> mapKeyIter = map.keySet().iterator();
		while (mapKeyIter.hasNext()) {
			String key = mapKeyIter.next();
			ArrayList<StampInfoData> tmpList = map.get(key);
			tmpList.get(0).mDateLabelCount = getString(R.string.round_bracket_count, String.valueOf(tmpList.size()));
			nList.addAll(tmpList);
		}

		return nList;
	}

	/**
	 * 단말 디비를 이용하여, 이메일을 키로, 값을 이름으로 가지는 맵 반환
	 * 
	 * @return
	 */
	private Map<String, String> getFriendIdNameMap() {
		Map<String, String> map = new HashMap<String, String>();

		HappytongDBHelper dbHelper = new HappytongDBHelper(this);
		ArrayList<FriendInfo> fList = dbHelper.GetBlockDatabyFriendInfo("0", null);
		for (FriendInfo info : fList) {
			if (info.GetIsName().equals("1")) {
				map.put(info.GetEmail(), info.GetName());
			}
		}
		dbHelper.close();

		return map;
	}

	@Override
	public void onProtoColFailResponse(int eventId, CallbackParam param) {
		super.onProtoColFailResponse(eventId, param);
		closeProgressDialog();
	}

	private void initTitleView() {
		mTitleInfoView = new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL);
		mTitleInfoView.updateView(R.string.stamp_box, TitleInfoView.TITLE_MODE_NORMAL);
	}

	private void init() {
		mContext = getApplicationContext();

		mVLeftReceiveSBox = (TextView) findViewById(R.id.sbox_received);
		mVLeftReceiveSBox.setOnClickListener(this);
		mVLeftSendSBox = (TextView) findViewById(R.id.sbox_sent);
		mVLeftSendSBox.setOnClickListener(this);

		mVLeftBoxes = new TextView[2];
		mVLeftBoxes[0] = mVLeftReceiveSBox;
		mVLeftBoxes[1] = mVLeftSendSBox;

		mVLeftFacebook = findViewById(R.id.sbox_facebook);
		mVLeftFacebook.setOnClickListener(this);
		setFacebookButtonVisibility();

		mReceivedStampListView = (ListView) findViewById(R.id.listview_received);
		mReceivedStampListView.setOnItemClickListener(ReceivedListViewOnItemClickListenr);

		mSentStampListView = (ListView) findViewById(R.id.listview_sent);
		mSentStampListView.setOnItemClickListener(SentListViewOnItemClickListenr);

		mReceivedLayout = findViewById(R.id.received_area);
		mSentLayout = findViewById(R.id.sent_area);
	}

	/**
	 * 페이스북 버튼 보임/안보임 ㅋ
	 */
	private void setFacebookButtonVisibility() {
		String facebookAdVal = SharedPrefManager.getInstance(this).getPrefFackbook();
		if (CommonData.FACEBOOK_AD_Y.equals(facebookAdVal)) {
			mVLeftFacebook.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.sbox_received:
			Logg.d("onClick - cbox_received");
			changeStampBoxList(StampBoxType.RECEIVED);
			break;
		case R.id.sbox_sent:
			Logg.d("onClick - cbox_sent");
			changeStampBoxList(StampBoxType.SENT);
			break;
		case R.id.sbox_facebook:
			Logg.d("onClick - cbox_facebook");
			showFacebookEventDialog();
			break;
		}
	}

	Handler	mHandler	= new Handler(new Handler.Callback() {
							@Override
							public boolean handleMessage(Message msg) {
								switch (msg.what) {
								case MSG_FACEBOOK_COMPLETE:
									DialogUtil.getConfirmDialog(StampBoxActivity.this, "", getString(R.string.facebook_ad_complete_message), null).show();
									break;
								case MSG_FACEBOOK_FAIL:
									Toast.makeText(StampBoxActivity.this, R.string.facebook_post_fail, Toast.LENGTH_LONG).show();
									break;
								}
								return false;
							}
						});

	private void showFacebookEventDialog() {
		Dialog dialog = DialogUtil.getConfirmCancelDialog(this, R.string.facebook_event, R.string.facebook_event_message, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					new FacebookLinker(StampBoxActivity.this, new FacebookLinker.OnResultListener() {
						@Override
						public void onFacebookResult(int retCode) {
							switch (retCode) {
							case FacebookLinker.OnResultListener.LOGIN_FAIL:
								Logg.d("facebook write happytong message - login - fail");
								break;
							case FacebookLinker.OnResultListener.LOGIN_OK:
								Logg.d("facebook write happytong message - login - ok");
								showProgressDialog();
								break;
							case FacebookLinker.OnResultListener.WRITE_FAIL:
								closeProgressDialog();
								Logg.d("facebook write happytong message - fail");
								mHandler.sendEmptyMessage(MSG_FACEBOOK_FAIL);
								break;
							case FacebookLinker.OnResultListener.WRITE_OK:
								Logg.d("facebook write happytong message");
								requestNetworkForFacebookAdComplete();
								break;
							case FacebookLinker.OnResultListener.LOGINED:
								showProgressDialog();
								break;
							}
						}
					});
				}
			}
		});
		dialog.show();
	}

	/**
	 * 페북 글작성 성공시, 우리 서버로 데이터 보냄
	 */
	protected void requestNetworkForFacebookAdComplete() {
		// TODO 페이스북 테스트시 아래 주석 - 안그러면 서버에서 작업해주지 않는 이상 기회 = 1
		new UserProtocol(reqOkFacebookAd, iFail).getProfileFacebookAd(CallBackParamWrapper.CODE_PROFILE_FACEBOOK_AD, SharedPrefManager.getLoginEmail(mContext));
	}

	final IWiseCallback	reqOkFacebookAd	= new IWiseCallback() {
											@Override
											public boolean WiseCallback(CallbackParam param) {
												Logg.d("networkOkFacebookAd");
												// 페북 단말 저장값 변경 - 페북 아이콘 숨기기
												mVLeftFacebook.setVisibility(View.GONE);
												SharedPrefManager.getInstance(mContext).setPrefFackbook(CommonData.FACEBOOK_AD_Y);
												// 서버데이터로 황금알 갱신
												requestStampRemain();
												return false;
											}
										};

	private void requestStampRemain() {
		PurchaseProtocol protocol = new PurchaseProtocol(new IWiseCallback() {
			@Override
			public boolean WiseCallback(CallbackParam param) {

				PurchaseInfoData data = (PurchaseInfoData) param.param1;

				setGoldEggCount(data.getGoldCount());
				setOwlCount(data.getFreeStampCount(), data.getPaidStampCount());

				closeProgressDialog();

				Logg.d("CouponBoxActivity - requestStampRemain " + data.getGoldCount() + " / " + data.getFreeStampCount() + " / " + data.getPaidStampCount());
				setSubTitleOwlEgg();

				mHandler.sendEmptyMessage(MSG_FACEBOOK_COMPLETE);

				return false;
			}
		}, iFail);
		protocol.orderStampRemain(0, SharedPrefManager.getLoginEmail(this));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != Activity.RESULT_OK) {
			return;
		}
		if (requestCode == IntentDefine.REQCODE_COUPON_DETAIL) {
			getServerData();
		}
		SharedPrefManager.getInstance(this).setMoveSBoxType(String.valueOf(data.getIntExtra(IntentDefine.EXTRA_SBOX_TYPE, StampBoxType.RECEIVED.ordinal())));
	}

	private void changeStampBoxList(StampBoxType type) {

		for (int i = 0; i < mVLeftBoxes.length; i++) {
			mVLeftBoxes[i].setBackgroundResource(R.drawable.selector_m3_leftmenu_btn);
			if (mColorStateListForLeftMenus != null) {
				mVLeftBoxes[i].setTextColor(mColorStateListForLeftMenus);
			}
		}

		switch (type) {
		case RECEIVED:
			setUsedCouponBoxMode(false);
			requireCancelUsedCouponBoxListEditMode();
			mReceivedLayout.setVisibility(View.VISIBLE);
			mSentLayout.setVisibility(View.GONE);

			mVLeftReceiveSBox.setBackgroundResource(R.drawable.m3_leftmenu_btn);
			mVLeftReceiveSBox.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
			break;
		case SENT:
			setUsedCouponBoxMode(false);
			requireCancelUsedCouponBoxListEditMode();
			mReceivedLayout.setVisibility(View.GONE);
			mSentLayout.setVisibility(View.VISIBLE);

			mVLeftSendSBox.setBackgroundResource(R.drawable.m3_leftmenu_btn);
			mVLeftSendSBox.setTextColor(getResources().getColor(R.color.left_menu_selected_color));
			break;
		}
	}

	final OnItemClickListener	ReceivedListViewOnItemClickListenr	= new AdapterView.OnItemClickListener() {
																		@Override
																		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
																			startStampDetailActivity(mReceivedStampList, StampBoxType.RECEIVED, position);
																		}
																	};
	final OnItemClickListener	SentListViewOnItemClickListenr		= new AdapterView.OnItemClickListener() {
																		@Override
																		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
																			startStampDetailActivity(mSentStampList, StampBoxType.SENT, position);
																		}
																	};

	private void startStampDetailActivity(ArrayList<StampInfoData> list, StampBoxType type, int position) {
		Intent intent = new Intent(mContext, StampDetailActivity.class);
		intent.putExtra(IntentDefine.EXTRA_CBOX_LIST, list);
		intent.putExtra(IntentDefine.EXTRA_CBOX_TYPE, type.ordinal());
		intent.putExtra(IntentDefine.EXTRA_CBOX_LIST_SELECT_POSITION, position);
		startActivityForResult(intent, IntentDefine.REQCODE_COUPON_DETAIL);
	}

	private void setUsedCouponBoxMode(boolean isUsedCouponBox) {
		// if (isUsedCouponBox) {
		// if (mUsedCouponList.size() > 0) {
		// mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_COUPON_BOX, titleEditClickListener);
		// } else {
		// mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_NORMAL);
		// }
		// } else {
		// mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_NORMAL);
		// }
	}

	final View.OnClickListener	titleEditClickListener	= new View.OnClickListener() {
															@Override
															public void onClick(View v) {
																setEditCouponBoxMode(true);
															}
														};

	final View.OnClickListener	titleClickListener		= new View.OnClickListener() {
															@Override
															public void onClick(View v) {
																switch (v.getId()) {
																case R.id.btn_title_left:
																	// 취소
																	setEditCouponBoxMode(false);
																	break;
																case R.id.btn_title_right:
																	// 완료
																	confirmDeleteStamp();
																	break;
																}
															}
														};

	private boolean requireCancelUsedCouponBoxListEditMode() {
		// if ((mUsedCouponListView.getVisibility() == View.VISIBLE) && mUsedCouponBoxState == UsedCouponBoxState.EDIT) {
		// if ((mUsedLayout.getVisibility() == View.VISIBLE) && mUsedCouponBoxState == UsedCouponBoxState.EDIT) {
		// setEditCouponBoxMode(false);
		// return true;
		// }
		return false;
	}

	@Override
	public void onBackPressed() {
		if (mHelpLayout.getVisibility() == View.VISIBLE) {
			mHelpClickListener.onClick(mHelpLayout);
			return;
		}

		if (requireCancelUsedCouponBoxListEditMode()) {
			return;
		} else {
			backKeyToExit();
		}
	}

	private void setEditCouponBoxMode(boolean isEditMode) {
		// if (isEditMode) {
		// mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_COUPON_BOX_DEL, titleClickListener);
		// mUsedCouponListAdapter.setCheckMode(true);
		// mUsedCouponBoxState = UsedCouponBoxState.EDIT;
		// } else {
		// mTitleInfoView.updateView(R.string.coupon_box, TitleInfoView.TITLE_MODE_COUPON_BOX, titleEditClickListener);
		// mUsedCouponListAdapter.setCheckMode(false);
		// mUsedCouponBoxState = UsedCouponBoxState.NORMAL;
		// }
	}

	private void confirmDeleteStamp() {
		// final List<StampInfoData> list = mUsedCouponListAdapter.getCheckedItemList();
		// if (list.size() > 0) {
		// // 삭제 요청
		// DialogUtil.getConfirmCancelDialog(this, "", getString(R.string.delete_selected_coupon), new DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// switch (which) {
		// case DialogInterface.BUTTON_POSITIVE:
		// requestNetworkForDeleteCoupon(list);
		// break;
		// }
		// setEditCouponBoxMode(false);
		// }
		// }).show();
		// } else {
		// setEditCouponBoxMode(false);
		// }
	}

	private void requestNetworkForDeleteCoupon(List<StampInfoData> list) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			if (i != 0) {
				sb.append("#");
			}
			sb.append(list.get(i).m_boxSEQ);
		}

		new CouponProtocol(iOkCouponDelete, iFailCouponDelete).couponDelete(CallBackParamWrapper.CODE_COUPON_DELETE, sb.toString(), ID);
		showProgressDialog();
	}

	/**
	 * 받은 쿠폰 콜백 - 완료 후 - 보낸 쿠폰 데이터 요청
	 */
	final IWiseCallback	iOkCouponDelete		= new IWiseCallback() {
												@Override
												public boolean WiseCallback(CallbackParam param) {
													Logg.d("iOkCouponDelete");
													Toast.makeText(mContext, R.string.coupon_deleted, Toast.LENGTH_SHORT).show();
													getServerData();
													// XXX 아.. 완료쿠폰 강제 지정
													SharedPrefManager.getInstance(StampBoxActivity.this).setMoveCBoxType("2");
													// requestUsedCouponList();
													return false;
												}
											};

	final IWiseCallback	iFailCouponDelete	= new IWiseCallback() {
												@Override
												public boolean WiseCallback(CallbackParam param) {
													Logg.d("iFailCouponDelete");
													closeProgressDialog();
													Toast.makeText(mContext, R.string.fail_to_delete_coupon, Toast.LENGTH_SHORT).show();
													return false;
												}
											};

	@Override
	protected int setContentViewId() {
		return R.layout.activity_stamp_box;
	}

	protected void requestUsedCouponList() {
		// new CouponProtocol(new IWiseCallback() {
		// @SuppressWarnings("unchecked")
		// @Override
		// public boolean WiseCallback(CallbackParam param) {
		// closeProgressDialog();
		//
		// if (param.param1 != null) {
		// mUsedCouponList = convertDataListToData2List((ArrayList<CouponInfoData>) param.param1);
		// mVLeftUseCBox.setText(getString(R.string.used_coupon_box) + "\n" + getString(R.string.round_bracket_count, String.valueOf(mUsedCouponList.size())));
		// } else {
		// mUsedCouponList.clear();
		// }
		// mUsedCouponListAdapter.notifyDataSetChanged();
		// setUsedCouponBoxMode(true);
		//
		// return false;
		// }
		// }, iFail).couponUsedExpired(CallBackParamWrapper.CODE_COUPON_USED_EXPIRED, ID);
	}

}
