package kr.wisestone.happytong.ui.setting;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.LocationInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.CompanyProtocol;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CompanyRegisterActivity extends BaseActivity{
	
	private static final int REQUEST_CODE = 1;
	private static final int COMPANY_STATE = 1; // 업체 등록시 사업자로 변경 
	private static final int COMPANY_MAX_NUM = 10; // 사업자 번호 유효 자릿수
	
	Context mContext;
	
	private LocationInfoData mLocationInfoData = new LocationInfoData();
	
	TitleInfoView mTitleInfoView;
	LinearLayout mLlLocation;
	EditText mEtName;
	EditText mEtTel;
	EditText mEtAddress;
	EditText mEtBusinessNum;
	TextView mTvLocation;
	ImageView mRegOk;
	String mLocation;

	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		mContext = this;
		
		onInit();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected int setContentViewId() {
		// TODO Auto-generated method stub
		return R.layout.activity_company_register;
	}
	
	private void onInit(){
		onFindViewById();
		commonCompanyRegSet();
		setListener();
	}
	
	private void onFindViewById(){
		mLlLocation = (LinearLayout) findViewById(R.id.ll_company_reg_location);
		mEtName = (EditText) findViewById(R.id.et_company_reg_name);
		mEtTel = (EditText) findViewById(R.id.et_company_reg_tel);
		mEtAddress = (EditText) findViewById(R.id.et_company_reg_adress);
		mEtBusinessNum = (EditText) findViewById(R.id.et_company_reg_business_num);
		mTvLocation = (TextView) findViewById(R.id.tv_company_reg_location_hint);
		mRegOk = (ImageView) findViewById(R.id.iv_company_reg_ok);
	}
	
	private void setListener(){
		mLlLocation.setOnClickListener(onClickListener);
		mRegOk.setOnClickListener(onClickListener);		
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch(v.getId()){
				case R.id.ll_company_reg_location:
					Intent intent = new Intent(mContext, LocationListViewActivity.class);
					intent.putExtra(IntentDefine.EXTRA_LOCATION_LIST_TYPE, CommonData.LOCATION_LIST_TYPE_COMPANY_REG);
					intent.putExtra(IntentDefine.EXTRA_LOCATION_COMPANY_DATA, mLocationInfoData);
					startActivityForResult(intent, REQUEST_CODE);
					break;
					
				case R.id.iv_company_reg_ok:
					companyReg();
					break;
			}
		}
	};
	
	private void commonCompanyRegSet() {
		mTitleInfoView = new TitleInfoView(this,
				(View) findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL);
		mTitleInfoView
				.updateView(R.string.setting_company_reg,	TitleInfoView.TITLE_MODE_NORMAL);
	}
	
	private DialogInterface.OnClickListener completeClickListener = new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			SharedPrefManager.getInstance(mContext).setPrefLoginCompanyState(COMPANY_STATE);
			finish();
		}
	};
	
	private void companyReg(){		
		// 업체명 입력 체크
		if(mEtName.getText().toString().equals("")){
			DialogUtil.getConfirmDialog(this, "", getString(R.string.company_reg_popup_name), getString(R.string.confirm),null).show();
		}		
		// 전화번호 입력 체크
		else if(mEtTel.getText().toString().equals("")){
			DialogUtil.getConfirmDialog(this, "", getString(R.string.company_reg_popup_tel), getString(R.string.confirm),null).show();
		}
		// 주소 입력 체크
		else if(mEtAddress.getText().toString().equals("")){
			DialogUtil.getConfirmDialog(this, "", getString(R.string.company_reg_popup_address), getString(R.string.confirm),null).show();
		}
//		// 사업자 번호 입력 체크
		else if(!Utils.checkCompanyNum(mEtBusinessNum.getText().toString(), COMPANY_MAX_NUM)){
			DialogUtil.getConfirmDialog(this, "", getString(R.string.company_reg_popup_business_num), getString(R.string.confirm),null).show();			
		}
		// 지역 선택 체크
		else if(TextUtils.isEmpty(mLocation)){
			DialogUtil.getConfirmDialog(this, "", getString(R.string.company_reg_popup_location), getString(R.string.confirm),null).show();
		}
		else{
			showProgressDialog();
			setCompanyReg();
		}
		
	}
	
	void setCompanyReg(){
		
        CompanyProtocol companyProtocol = new CompanyProtocol(new IWiseCallback() {
            
            @Override
            public boolean WiseCallback(CallbackParam param) {
            	DialogUtil.getConfirmDialog(mContext, "", getString(R.string.company_reg_popup_ok), getString(R.string.confirm),completeClickListener).show();
                closeProgressDialog();
                return false;
            }
        }, iFail);
        
        companyProtocol.getCompanyAdd(0, SharedPrefManager.getLoginEmail(this), mEtName.getText().toString(),
        		mEtBusinessNum.getText().toString(), mEtTel.getText().toString(), mEtAddress.getText().toString(),
        		mLocationInfoData.getLocationSmallCode());
	}	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_OK) {
			mLocationInfoData = (LocationInfoData) data.getExtras().getSerializable(IntentDefine.EXTRA_COMPANY_INFO);
			Logg.d("mLocationInfoData ======= " + mLocationInfoData.getLocationSmallName());
			mLocation = mLocationInfoData.getLocationBigName() + " - " + mLocationInfoData.getLocationSmallName();
			mTvLocation.setText(mLocation);
			
		} else if(resultCode == Activity.RESULT_CANCELED) {
			return;
		}
		
	}

}
