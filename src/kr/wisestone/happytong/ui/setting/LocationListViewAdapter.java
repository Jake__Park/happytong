package kr.wisestone.happytong.ui.setting;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class LocationListViewAdapter extends BaseAdapter {

	private LayoutInflater mInflater; // Inflater
	private int mLayout; // ListView의 row layout id
	private ArrayList<?> mArrayLocationData;

	public LocationListViewAdapter(Context context,
			ArrayList<?> arrayLocationData, int layout) {
		this.mArrayLocationData = arrayLocationData;
		this.mLayout = layout;
		this.mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		Logg.d("getCount = " + mArrayLocationData.size());
		return mArrayLocationData.size();
	}

	@Override
	public Object getItem(int position) {
		return mArrayLocationData.get(position);

	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolderList mHolder;

		if (convertView == null) {
			convertView = mInflater.inflate(mLayout, parent, false);

			mHolder = new ViewHolderList();
			mHolder = LocationHolderData(convertView);

			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolderList) convertView.getTag();
		}

		View v = setLocationView(position, convertView, parent, mHolder);
		return v;
	}

	ViewHolderList LocationHolderData(View convertView) {
		ViewHolderList holder = new ViewHolderList();
		holder.mContactListItem = (LinearLayout) convertView
				.findViewById(R.id.ll_contactListItem);
		holder.mCbLocation = (CheckBox) convertView
				.findViewById(R.id.cb_location);
		holder.mLocationName = (TextView) convertView
				.findViewById(R.id.tv_locationName);
		holder.mLocationUserCount = (TextView) convertView
				.findViewById(R.id.tv_locationUserCount);
		holder.mListHeader = (LinearLayout) convertView
				.findViewById(R.id.ll_list_header);
		holder.mTvHeadLine = (TextView) convertView
				.findViewById(R.id.tv_head_line);
		return holder;
	}

	public class ViewHolderList {
		// Default item
		public LinearLayout mContactListItem;
		public TextView mLocationName;
		public TextView mLocationUserCount;
		public CheckBox mCbLocation;

		// Head Line
		public LinearLayout mListHeader;
		public TextView mTvHeadLine;
	}

	public abstract View setLocationView(int position, View convertView,
			ViewGroup parent, ViewHolderList holder);

}