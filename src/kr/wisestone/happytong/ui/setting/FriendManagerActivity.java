package kr.wisestone.happytong.ui.setting;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.util.FriendSynchronizer;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FriendManagerActivity extends BaseActivity {
	Context			m_Context;
	LinearLayout 	m_blockFriend;
	LinearLayout 	m_autoSync;
	LinearLayout 	m_manualSync;
	TextView	 	m_date; 
	ImageView		m_SyncImg;

	SharedPrefManager m_spm;
	String 			m_dateTime;
		
	boolean bAutoSync = false;
	boolean bManualSync = false;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
     
		init();

	}

	private void init(){
		m_Context = FriendManagerActivity.this;
		m_spm = SharedPrefManager.getInstance(this);

		m_blockFriend = (LinearLayout)findViewById(R.id.layout_block_friend);
		m_blockFriend.setOnClickListener(this);
		m_autoSync = (LinearLayout)findViewById(R.id.layout_auto_sync);
		m_autoSync.setOnClickListener(this);
		m_manualSync = (LinearLayout)findViewById(R.id.layout_manual_sync);
		m_manualSync.setOnClickListener(this);
		
		m_SyncImg = (ImageView) findViewById(R.id.autosync_img);
		m_date = (TextView) findViewById(R.id.txt_update_date);

		String strDate = getCheckManualSync();
		if(strDate.equals(""))
			m_date.setText(" ");
		else
			m_date.setText(strDate+ " " + getString(R.string.text_update) );
		
		getAutoSync();
		
		new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL).updateView(R.string.friend_mgr_title, TitleInfoView.TITLE_MODE_NORMAL);	
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.layout_block_friend://차단친구관리
			Logg.d("onClick - block_friend");
			startActivity(new Intent(m_Context, FriendBlockSettingActivity.class));
			break;
		case R.id.layout_auto_sync://자동동기화
			Logg.d("onClick - auto_sync");
			setCheckAutoSync();
			break;
		case R.id.layout_manual_sync://수동동기화
			Logg.d("onClick - manual_sync");
			if(!bManualSync)
			{
				showDateFormat();
				setCheckManualSync();
				new FriendSynchronizer(m_Context, syncListener);
				
				bManualSync = true;
			}
			
			break;
		}
	}

	final FriendSynchronizer.SyncListener syncListener = new FriendSynchronizer.SyncListener() {
        @Override
        public void onStartLoading() {
            showProgressDialog();
        }
        
        @Override
        public void onEndLoading() {
            Logg.d("onEndLoading - manual_sync");
            if(bManualSync)
            {
            	closeProgressDialog();
        		bManualSync = false;
            }
        }

		@Override
		public void onEndSynchronize(int retValue) {
			Logg.d("onEndSynchronize - manual_sync");
			switch (retValue) {
			case FriendSynchronizer.RET_NETWORK_FAIL:
				Toast.makeText(m_Context, "Sync Network Request Fail!!", Toast.LENGTH_SHORT).show();
				break;
			case FriendSynchronizer.RET_NO_DATA:
				Toast.makeText(m_Context, "NO data!!", Toast.LENGTH_SHORT).show();
				break;
			case FriendSynchronizer.RET_NORMAL:
				break;
			}
			//StatisticGraphActivity(100);
		}
	};
	
	
	@Override
	protected int setContentViewId() {
		// TODO Auto-generated method stub
		return R.layout.activity_setting_friend_manager;
	}

	public void showDateFormat(){
		SimpleDateFormat formatter = new SimpleDateFormat ( "yyyy.MM.dd a h:mm", Locale.KOREA );
		Date currentTime = new Date ( );
		m_dateTime = formatter.format ( currentTime );
		m_date.setText(m_dateTime +" "+ getString(R.string.text_update));
		Logg.d("manualSync = "+ m_dateTime);
	}

	public void setCheckAutoSync(){
		bAutoSync = !bAutoSync;
		if(bAutoSync)
		{
			m_spm.setPrefAutoSync(CommonData.AUTO_SYNC_ON);
			m_SyncImg.setBackgroundResource( R.drawable.sub6_02_sync_btn );
		}
		else
		{
			m_spm.setPrefAutoSync(CommonData.AUTO_SYNC_OFF);
			m_SyncImg.setBackgroundResource( R.drawable.sub6_02_sync2_btn );
		}
	}
	
	private void getAutoSync(){
		if(CommonData.AUTO_SYNC_ON.equals(m_spm.getPrefAutoSync()))
			m_SyncImg.setBackgroundResource( R.drawable.sub6_02_sync_btn );	
		else
			m_SyncImg.setBackgroundResource( R.drawable.sub6_02_sync2_btn );
	}
	
	// 수동동기화 날짜를 preference에 저장.
	public void setCheckManualSync(){
		m_spm.setPrefManualSync(m_dateTime);
	}
	
	public String getCheckManualSync(){
		String date = m_spm.getPrefManualSync();
		return date;
	}
}
