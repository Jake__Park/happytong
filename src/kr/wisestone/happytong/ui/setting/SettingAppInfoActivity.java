package kr.wisestone.happytong.ui.setting;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.Logg;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SettingAppInfoActivity extends BaseActivity {

	TextView mAppVer;
	TextView mAppEmail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mAppVer = (TextView)findViewById(R.id.app_ver);
		mAppVer.setText("v"+Utils.getAppVersionName(this));
		
		mAppEmail = (TextView)findViewById(R.id.app_email);
		mAppEmail.setOnClickListener(this);
		
		new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL).updateView(R.string.setting_info_title, TitleInfoView.TITLE_MODE_NORMAL);
	}

	@Override
	protected int setContentViewId() {
		// TODO Auto-generated method stub
		return R.layout.activity_setting_app_info;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.app_email://email
			//이메일연동
			sendEmail();
			break;
		}
	}
	
	private void sendEmail(){
		Logg.d("sendEmail ");
		Intent it = new Intent(Intent.ACTION_SEND);  
		it.setType("plain/text");  

		String[] tos = {mAppEmail.getText().toString()};
		it.putExtra(Intent.EXTRA_EMAIL, tos);  

		// [20130620 hyoungjoon.lee] OWL No107 메일 제녹에 "[CuFunU]문의사항" 기본 입력 추가
		String PREFIX = getResources().getString(R.string.app_name);
		String subject = getResources().getString(R.string.setting_email_default_subject);
	    String defaultSubject = "[" + PREFIX + "]" + subject;
	    
		it.putExtra(Intent.EXTRA_SUBJECT, defaultSubject);
		// [20130620 hyoungjoon.lee] OWL No107
		
		startActivity(it);
	}
	
}
