package kr.wisestone.happytong.ui.setting;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/*****************************************************************************
 * 클래스 : CustomListAdapter
 * 패키지 : nh.smart.personalNetwork.contact
 * 작성일 : 2013.01.14
 * 작성자 : 장재훈
 * 설 명  : 연락처 & 그룹 ListView의 Adapter
 * ===========================================================================
 * 변경이력:
 * DATE         AUTHOR      DESCRIPTION
 * ---------------------------------------------------------------------------
 * 
*****************************************************************************/
public abstract class FriendBlockListAdapter extends BaseAdapter {
    public final static int LIST_TYPE_GROUP = 0;
    public final static int LIST_TYPE_FRIEND = 1;
    
    int mListType;

    private LayoutInflater      mInflater;  // Inflater 
    private int                 mLayout;    // ListView의 row layout id 
    private ArrayList<?>        mItems;     // list data
    
    /***
     * @Brief 생성자
     * @param context ContactActivity.this
     * @param items list data
     * @param layout ListView의 row layout id
     */
    public FriendBlockListAdapter(Context context, ArrayList<?> items, int layout, int listType) {
        this.mItems     = items;
        this.mLayout    = layout;
        this.mInflater  = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mListType = listType;
    }

    @Override
    public int getCount() {
        Logg.d( "getCount = " + mItems.size() );
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
        
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mHolder;

        if( convertView==null) {
            convertView = mInflater.inflate(mLayout, parent, false);
            
            mHolder = new ViewHolder();
            
            if(mListType == LIST_TYPE_GROUP){
                mHolder = groupHolderData(convertView);
            } else {
                mHolder = friendHolderData(convertView);
            }

            convertView.setTag( mHolder );
        } else {
            mHolder = (ViewHolder)convertView.getTag();
        }
        
        setView(position, convertView, parent, mHolder);
        return convertView;
    }
    
    ViewHolder groupHolderData(View convertView){
        
        ViewHolder holder = new ViewHolder();
        
        holder.mllGroupName = (LinearLayout)convertView.findViewById(R.id.llGroupName);
        holder.mGroupCount = (TextView)convertView.findViewById(R.id.groupCount);
        holder.mGroupName = (TextView)convertView.findViewById(R.id.groupName);
        return holder;
    }
    
    ViewHolder friendHolderData(View convertView){
        ViewHolder holder = new ViewHolder();
        
        holder.mContactListItem = (LinearLayout)convertView.findViewById(R.id.contactListItem);
        holder.mButton = (Button)convertView.findViewById(R.id.blockButton);
        holder.mPhoto = (ImageView)convertView.findViewById(R.id.contactPhoto);
        holder.mName  = (TextView)convertView.findViewById(R.id.contactName);
        
        holder.mListHeader = (LinearLayout)convertView.findViewById(R.id.listHeader);
        holder.mHeadLine = (TextView)convertView.findViewById(R.id.HeadLine);
        return holder;
    }
     class ViewHolder {
        // Default item
        LinearLayout    mContactListItem;
        ImageView       mPhoto;
        TextView        mName;
        Button          mButton;

        // Head Line
        LinearLayout    mListHeader;
        TextView        mHeadLine;

        LinearLayout    mllGroupName;  // row Background image 변경 용.
        TextView        mGroupName;  // 그룹 이름 Text View
        TextView        mGroupCount;  // 그룹 에 포함 된 멤버의 수 Text View
    }
    
    public abstract View setView(int position, View convertView, ViewGroup parent, ViewHolder holder);

}
