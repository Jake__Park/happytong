package kr.wisestone.happytong.ui.setting;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.UserInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.protocol.UserProtocol;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.DummyMainActivity;
import kr.wisestone.happytong.ui.widget.MenuInfoView;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SettingActivity extends BaseActivity {
	private static final int NORMAL_USER = 0;
	
	Context mContext;

	ImageView mPfImage;
	TextView mAppVer;
	TextView mTvLocation;
	LinearLayout mPf_list;
	LinearLayout mPf_list2;
	LinearLayout mPf_list3;
	LinearLayout mPf_list4;
	LinearLayout mPf_list5;
    LinearLayout mPf_list6;
    LinearLayout mPf_list7;

	UserProtocol m_UserProtocol;
	UserInfoData m_UserInfoData;
	
	int checkCompanyState;

	final int PROFILE_PHOTO = 0;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
		
		Logg.d("사업자 구분: "+SharedPrefManager.getInstance(this).getPrefLoginCompanyState());

		// 임시 DB확인용 복사
		// Utils.copyDB();
	}

	protected int setContentViewId() {
		return R.layout.activity_setting_main;
	}

	private void init() {
		mContext = getApplicationContext();

		mPfImage = (ImageView) findViewById(R.id.pf_img);

		mPf_list = (LinearLayout) findViewById(R.id.pf_list);
		mPf_list.setOnClickListener(this);
		mPf_list2 = (LinearLayout) findViewById(R.id.pf_list2);
		mPf_list2.setOnClickListener(this);
		mPf_list3 = (LinearLayout) findViewById(R.id.pf_list3);
		mPf_list3.setOnClickListener(this);
		mPf_list4 = (LinearLayout) findViewById(R.id.pf_list4);
		mPf_list4.setOnClickListener(this);
		mPf_list5 = (LinearLayout) findViewById(R.id.pf_list5);
		mPf_list5.setOnClickListener(this);
        mPf_list6 = (LinearLayout) findViewById(R.id.pf_list6);
        mPf_list6.setOnClickListener(this);
        mPf_list7 = (LinearLayout) findViewById(R.id.pf_list7);
        mPf_list7.setOnClickListener(this);
		mAppVer = (TextView) findViewById(R.id.app_ver);
		mAppVer.setText("v" + Utils.getAppVersionName(this));		
		mTvLocation = (TextView) findViewById(R.id.tv_location);		

		new MenuInfoView(this, (View) findViewById(R.id.menu_tab),
				CommonData.ENTRY_PONINT_SETTING);
		new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL).updateView(R.string.setting_title, TitleInfoView.TITLE_MODE_NORMAL);
		
		setCheckCompanyState();
	}
	
    protected void onResume() {
        super.onResume();
        
        getProfileInfo();
        setCheckCompanyState();
    }
    
    private void setCheckCompanyState(){
    	checkCompanyState = SharedPrefManager.getInstance(this).getPrefLoginCompanyState();
    	if(checkCompanyState == NORMAL_USER){
    		mPf_list6.setVisibility(View.VISIBLE);
    		mPf_list7.setVisibility(View.GONE);
    	} else {    		
    		mPf_list6.setVisibility(View.GONE);
    		mPf_list7.setVisibility(View.VISIBLE);
    	}
    }
    
	private void getProfileInfo() {
		showProgressDialog();

		m_UserProtocol = new UserProtocol(iOK, iFail);
		m_UserProtocol.getProfileGet(CallBackParamWrapper.CODE_PROFILE_GET,
				SharedPrefManager.getLoginEmail(this));
	}

	public void onProtocolResponse(int eventId, CallbackParam param) {
		super.onProtocolResponse(eventId, param);
		if (param.param1 != null) {
			m_UserInfoData = (UserInfoData) param.param1;

			Logg.d("main 1 = " + m_UserInfoData.getUserImageUrl());
			Logg.d("main 2 = " + m_UserInfoData.getAddress());
			Logg.d("main 3 = " + m_UserInfoData.getNickName());
			Logg.d("main 4 = " + m_UserInfoData.getLocationBigName());
			Logg.d("main 5 = " + m_UserInfoData.getLocationBigCode());
			Logg.d("main 6 = " + m_UserInfoData.getLocationSmallName());
			Logg.d("main 7 = " + m_UserInfoData.getLocationSmallCode());
			
			if(m_UserInfoData.getLocationSmallName() != null){
				mTvLocation.setText(m_UserInfoData.getLocationBigName() + " - " + m_UserInfoData.getLocationSmallName());
			} else{
				mTvLocation.setText("없음");
			}

			showProfileImage();
			closeProgressDialog();
		}
	}

	private void showProfileImage() {
		ImageLoader ImageLoader = new ImageLoader(mContext);

		if (m_UserInfoData.getUserImageUrl().endsWith("none")) {
			mPfImage.setImageBitmap(null);
			return;
		}

		if (m_UserInfoData.getUserImageUrl() != null
				&& m_UserInfoData.getUserImageUrl() != ""
				&& !(m_UserInfoData.getUserImageUrl().equalsIgnoreCase("null"))) {
			ImageLoader
					.DisplayImage(m_UserInfoData.getUserImageUrl(), mPfImage);
		}	
		
	}

	@Override
	public void onProtoColFailResponse(int eventId, CallbackParam param) {
		super.onProtoColFailResponse(eventId, param);
		closeProgressDialog();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.pf_list:// 프로필
			Intent intent = new Intent(mContext, ProfileEditActivity.class);
			startActivityForResult(intent, PROFILE_PHOTO);
			break;
		case R.id.pf_list2:// 친구차단관리
			startActivity(new Intent(mContext, FriendManagerActivity.class));
			break;
		case R.id.pf_list3:// 도움말
			startActivity(new Intent(mContext, SettingGuideActivity.class));
			break;
		case R.id.pf_list4:// 어플정보
			startActivity(new Intent(mContext, SettingAppInfoActivity.class));
			break;
		case R.id.pf_list5:// 지역설정
			intent = new Intent(mContext, LocationListViewActivity.class);
			intent.putExtra(IntentDefine.EXTRA_LOCATION_LIST_TYPE, CommonData.LOCATION_LIST_TYPE_NORMAL);
			intent.putExtra(IntentDefine.EXTRA_LOCATION_USER_DATA, m_UserInfoData.getLocationSmallCode());
			startActivity(intent);
			break;
        case R.id.pf_list6:// 사업자 등록
            startActivity(new Intent(mContext, CompanyRegisterActivity.class));
            break;
        case R.id.pf_list7:// 업체 모드 전환
        	Intent mainIntent = new Intent(SettingActivity.this, DummyMainActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            mainIntent.putExtra(IntentDefine.EXTRA_MAIN_ENTRY, CommonData.ENTRY_PONINT_COMPAMY_SETTING);
            startActivity(mainIntent);
            finish();
            break;
        }
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Logg.d("**SettingMain requestCode = " + requestCode);
		Logg.d("**SettingMain resultCode = " + resultCode);
		if (requestCode == PROFILE_PHOTO) {
			getProfileInfo();
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		backKeyToExit();
	}

}
