package kr.wisestone.happytong.ui.setting;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SettingGuideActivity extends BaseActivity{
	Context mContext;
	
	LinearLayout mllCouponTitle;
	LinearLayout mllAccTitle;
	LinearLayout mllPushAlarmTitle;
	LinearLayout mllResignTitle;
	LinearLayout mllJoinTitle;
	LinearLayout mllExpDateTitle;
	
	TextView	mTvCouponContent;
	TextView	mTvAccContent;
	TextView	mTvPushAlarmContent;
	TextView	mTvResignContent;
	TextView	mTvJoinContent;
	TextView	mTvExpDateContent;
	
	public final static int EXPAND_TYPE_COUPON 		= 0;
	public final static int EXPAND_TYPE_ACCOUNT		= 1;
	public final static int EXPAND_TYPE_PUSH_ALARM	= 2;
    public final static int EXPAND_TYPE_JOIN		= 3;
    public final static int EXPAND_TYPE_RESIGN 		= 4;
    public final static int EXPAND_TYPE_EXP_DATE	= 5;
	
    private boolean[] mExpanded = 
	{
    		false,
    		false,
    		false,
    		false,
    		false,
    		false,
	};

    private ImageView[] mGuideImg = 
	{
    		null,
    		null,
    		null,
    		null,
    		null,
    		null,
	};
    
    private LinearLayout[] mGuideContent = 
	{
    		null,
    		null,
    		null,
    		null,
    		null,
    		null,
	};
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		init();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.guide_coupon_title:
			toggle(EXPAND_TYPE_COUPON);
			setExpanded(EXPAND_TYPE_COUPON, mExpanded[EXPAND_TYPE_COUPON] );
			break;
		case R.id.guide_account_title:
			toggle(EXPAND_TYPE_ACCOUNT);
			setExpanded(EXPAND_TYPE_ACCOUNT, mExpanded[EXPAND_TYPE_ACCOUNT] );
			break;			
		case R.id.guide_push_alarm_title:
			toggle(EXPAND_TYPE_PUSH_ALARM);
			setExpanded(EXPAND_TYPE_PUSH_ALARM, mExpanded[EXPAND_TYPE_PUSH_ALARM] );
			break;	
		case R.id.guide_resign_title:
			toggle(EXPAND_TYPE_RESIGN);
			setExpanded(EXPAND_TYPE_RESIGN, mExpanded[EXPAND_TYPE_RESIGN] );
			break;
		case R.id.guide_join_title:
			toggle(EXPAND_TYPE_JOIN);
			setExpanded(EXPAND_TYPE_JOIN, mExpanded[EXPAND_TYPE_JOIN] );
			break;
		case R.id.guide_expiration_date_title:
			toggle(EXPAND_TYPE_EXP_DATE);
			setExpanded(EXPAND_TYPE_EXP_DATE, mExpanded[EXPAND_TYPE_EXP_DATE] );
			break;
		}
	}

	
	@Override
	protected int setContentViewId() {
		// TODO Auto-generated method stub
		return R.layout.activity_setting_guide;
	}

	private void init(){
		mContext = getApplicationContext();
				
		mllCouponTitle = (LinearLayout)findViewById(R.id.guide_coupon_title);
		mllCouponTitle.setOnClickListener(this);
	
		mllAccTitle = (LinearLayout)findViewById(R.id.guide_account_title);
		mllAccTitle.setOnClickListener(this);

		mllPushAlarmTitle = (LinearLayout)findViewById(R.id.guide_push_alarm_title);
		mllPushAlarmTitle.setOnClickListener(this);
		
		mllResignTitle = (LinearLayout)findViewById(R.id.guide_resign_title);
		mllResignTitle.setOnClickListener(this);
		
		mllJoinTitle = (LinearLayout)findViewById(R.id.guide_join_title);
		mllJoinTitle.setOnClickListener(this);

		mllExpDateTitle = (LinearLayout)findViewById(R.id.guide_expiration_date_title);
		mllExpDateTitle.setOnClickListener(this);
		
		mTvCouponContent = (TextView)findViewById(R.id.guide_content);
		mTvAccContent = (TextView)findViewById(R.id.guide_account_content);
		mTvPushAlarmContent = (TextView)findViewById(R.id.guide_push_alarm_content);
		mTvResignContent = (TextView)findViewById(R.id.guide_resign_content);
		mTvJoinContent = (TextView)findViewById(R.id.guide_join_content);
		mTvExpDateContent = (TextView)findViewById(R.id.guide_expiration_date_content);
		
		mGuideImg[0] = (ImageView)findViewById(R.id.guide_img1);
		mGuideImg[1] = (ImageView)findViewById(R.id.guide_img2);
		mGuideImg[2] = (ImageView)findViewById(R.id.guide_img3);
		mGuideImg[3] = (ImageView)findViewById(R.id.guide_img4);
		mGuideImg[4] = (ImageView)findViewById(R.id.guide_img5);
		mGuideImg[5] = (ImageView)findViewById(R.id.guide_img6);
		
		mGuideContent[0] = (LinearLayout)findViewById(R.id.guide_content_ll_1);
		mGuideContent[1] = (LinearLayout)findViewById(R.id.guide_content_ll_2);
		mGuideContent[2] = (LinearLayout)findViewById(R.id.guide_content_ll_3);
		mGuideContent[3] = (LinearLayout)findViewById(R.id.guide_content_ll_4);
		mGuideContent[4] = (LinearLayout)findViewById(R.id.guide_content_ll_5);
		mGuideContent[5] = (LinearLayout)findViewById(R.id.guide_content_ll_6);
		
		new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL).updateView(R.string.setting_guide_title, TitleInfoView.TITLE_MODE_NORMAL);
	}
	
    public void toggle(int position) {
        mExpanded[position] = !mExpanded[position];
        Logg.d("toggle pos =" +position +" value= " + mExpanded[position]);
        
        if(mExpanded[position])
        	mGuideImg[position].setImageResource(R.drawable.sub6_01_list_detail_rotate);
        else
        	mGuideImg[position].setImageResource(R.drawable.sub6_01_list_detail);
     }
    
    
    public void setExpanded(int type, boolean expanded) {
    	if(type == EXPAND_TYPE_COUPON){
    		mTvCouponContent.setVisibility(expanded ? View.VISIBLE : View.GONE);
    		mGuideContent[EXPAND_TYPE_COUPON].setVisibility(expanded ? View.VISIBLE : View.GONE);
    	}
    	else if(type == EXPAND_TYPE_ACCOUNT){
    		mTvAccContent.setVisibility(expanded ? View.VISIBLE : View.GONE);
    		mGuideContent[EXPAND_TYPE_ACCOUNT].setVisibility(expanded ? View.VISIBLE : View.GONE);
    	}
    	else if(type == EXPAND_TYPE_PUSH_ALARM){
    		mTvPushAlarmContent.setVisibility(expanded ? View.VISIBLE : View.GONE);
    		mGuideContent[EXPAND_TYPE_PUSH_ALARM].setVisibility(expanded ? View.VISIBLE : View.GONE);
    	}
    	else if(type == EXPAND_TYPE_RESIGN){
    		mTvResignContent.setVisibility(expanded ? View.VISIBLE : View.GONE);
    		mGuideContent[EXPAND_TYPE_RESIGN].setVisibility(expanded ? View.VISIBLE : View.GONE);
    	}
    	else if(type == EXPAND_TYPE_EXP_DATE){
    		mTvExpDateContent.setVisibility(expanded ? View.VISIBLE : View.GONE);
    		mGuideContent[EXPAND_TYPE_EXP_DATE].setVisibility(expanded ? View.VISIBLE : View.GONE);
    	}
    	else{//회원가입
    		mTvJoinContent.setVisibility(expanded ? View.VISIBLE : View.GONE);
    		mGuideContent[EXPAND_TYPE_JOIN].setVisibility(expanded ? View.VISIBLE : View.GONE);
    	}
    }
    
 
	
}
