package kr.wisestone.happytong.ui.setting;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.protocol.UserProtocol;
import kr.wisestone.happytong.data.util.ErrorCode;
import kr.wisestone.happytong.data.util.ImageDownloader;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.setting.FriendBlockListAdapter.ViewHolder;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.Logg;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.TextView;


public class FriendBlockSettingActivity extends BaseActivity{

	
	private ListView              		mFriendListView;
	private TextView              		mTvNodata;
	
	private FriendBlockListAdapter   mFriendAdapter;
	private ArrayList<FriendInfo> 	mArrayFriendList    = new ArrayList<FriendInfo>();
	private FriendInfo   					mSelectedFriendInfo;
	public HappytongDBHelper 		mDbHelper;
	private UserProtocol				m_UserProtocol;
	private ImageDownloader 			m_ImageLoader ;
	 
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mDbHelper = new HappytongDBHelper(this);
		m_ImageLoader = new ImageDownloader();
		findAllViews();
		GetFriendBlockData();
		listViewInit();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if(mDbHelper != null)
			mDbHelper.close();
		super.onDestroy();
	}

	@Override
	protected int setContentViewId() {
		// TODO Auto-generated method stub
		return R.layout.activity_setting_friend_block;
	}
	private void findAllViews() 
	{
		
		mFriendListView = (ListView)findViewById( R.id.list );
		mTvNodata 		= (TextView)findViewById( R.id.tvNodata );
		new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL).updateView(R.string.block_friend_mgr_title, TitleInfoView.TITLE_MODE_NORMAL);
	}

	private void GetFriendBlockData() 
	{
		if(mArrayFriendList != null) mArrayFriendList.clear();
		mArrayFriendList = mDbHelper.GetBlockDatabyFriendInfo("1", null);
	}

	private void listViewInit() {
		// mFriendListView.setLongClickable(true);
	
		mFriendAdapter = new FriendBlockListAdapter(this, mArrayFriendList, R.layout.friendblock_list_item, FriendBlockListAdapter.LIST_TYPE_FRIEND) {
			public View setView(int position, View convertView, ViewGroup parent, ViewHolder holder) {
				setFriendViewData(holder, (FriendInfo)getItem(position));
				return convertView;
			}
		};

		mFriendListView.setAdapter( mFriendAdapter );
		mFriendListView.setEmptyView(mTvNodata);
	}

	private void setFriendViewData(ViewHolder holder, FriendInfo info) {
		String displayName;
		
		setViewVisible(holder);
		//ImageLoader imageLoader = new ImageLoader( this );

		if( info.GetIsFirst() ){
			setHeadLine(holder, ""+info.GetChosung());
		} else {
			holder.mListHeader.setVisibility(View.GONE);
		}

        if(info.GetIsName().equals("1")){
            displayName = info.GetName() + "(" + info.GetNickName() + ")";
        } else {
            displayName = info.GetName();
        }

		m_ImageLoader.DisplayImage(info.GetUserImgUrl(), holder.mPhoto);
		holder.mPhoto.setScaleType(ScaleType.FIT_XY);
		holder.mName.setText(displayName);

		holder.mButton.setOnClickListener( mBlockClickListener );
	}

	private void setHeadLine(ViewHolder holder, String headLine) {
		holder.mListHeader.setVisibility(View.VISIBLE);

		holder.mHeadLine.setVisibility(View.VISIBLE);
		holder.mHeadLine.setText(headLine);
	}
	private void setViewVisible(ViewHolder holder) {
		holder.mContactListItem.setVisibility( View.VISIBLE );
		holder.mListHeader.setVisibility( View.GONE );

		// holder.mCheckBox.setVisibility( View.GONE );
		holder.mButton.setVisibility( View.VISIBLE );
	}

	OnClickListener mBlockClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if( mFriendListView==null) {
				return;
			}

			int position = mFriendListView.getPositionForView(v);
			if( position<0 ) {
				Logg.d("can not find item from view");
				return;
			}

			mSelectedFriendInfo = mArrayFriendList.get(position);

			m_UserProtocol = new UserProtocol( iOK, iFail );
			m_UserProtocol.getProfileUnBlock(CallBackParamWrapper.CODE_FRIEND_FRIEND_UNBLOCK, 
											SharedPrefManager.getLoginEmail(FriendBlockSettingActivity.this), mSelectedFriendInfo.GetEmail()); 
		}
	};
	@Override
	public void onProtocolResponse(int eventId, CallbackParam param) {
		// TODO Auto-generated method stub
		super.onProtocolResponse(eventId, param);
		Logg.d("iOkFriendUnBlock");
		if (param.param1 != null) {
			drawList();
			
		}
	}

	// 팝업 창 뜬 후에 발생
	@Override
	public void onProtoColFailResponse(int eventId, CallbackParam param) {
		// 20130619
		// 차단해지 버튼-> 차단이해지되지않았습니다.팝업시 
		// 서버에서는 차단해지 성공했으나 네트웍 오류로 단말은 차단해지 안되었을경우.
		// 20130627.jish.문제되는 에러코드 반환시 다이얼로그 없이 리스트 갱신 (정상 동작과 동일처리) 
		if(param.errorCode == ErrorCode.ERR_HTTP_SERVER_203)
		{
			drawList();
		} else {
			super.onProtoColFailResponse(eventId, param);
		}
	}

	private void drawList(){
		mSelectedFriendInfo.Setblock("0");
		mSelectedFriendInfo.SetName( mSelectedFriendInfo.GetName() );
		mDbHelper.UpdateFriendInfo(mSelectedFriendInfo);
		
		mArrayFriendList.remove( mSelectedFriendInfo );
        mFriendAdapter.notifyDataSetChanged();
		
		//GetFriendBlockData();
		//listViewInit();
	}
}

