package kr.wisestone.happytong.ui.setting;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.AuthInfoData;
import kr.wisestone.happytong.data.dataset.LocationInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.LocationProtocol;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.auth.AgreementActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.setting.LocationListViewAdapter.ViewHolderList;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class LocationListViewActivity extends BaseActivity {

	public static final int LOCATION_CHECK_FLAG_DEACTIVE = -1; // 비 선택 모드
	public static final int LOCATION_CHECK_FLAG_CHECKED  = 1; // 체크 활성화
	public static final int LOCATION_CHECK_FLAG_UNCHECK  = 2; // 체크 비활성화
	
	private static final String NORMAL_MODE = "1";
	private static final String COMPANY_MODE = "2";
	
	
	private TitleInfoView mTitleInfoView;
	private LocationListViewAdapter mLocationListViewAdapter;
	private ListView mLocationListView;
	private TextView mTvLocationGuide1;
	private TextView mTvLocationGuide2;
	
	AuthInfoData aiData = new AuthInfoData();
	private LocationInfoData mLocationInfoData = new LocationInfoData();
	private ArrayList<LocationInfoData> mArrLocationData = new ArrayList<LocationInfoData>();	
	private ArrayList<Integer> mArrLocationHeaderNum = new ArrayList<Integer>();
	
	Context mContext;
	
	private int mGetLocationListType;
	private int mHeaderCount = 0;
	private int mCheck = -1;
	private String mLocationBigName = "null"; // 지역 비교 변수
	private String mLocationCode = "";
	private String mLocationSmallName;	
	private String mCheckSmallCode;
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected int setContentViewId() {
		return R.layout.activity_location_list;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		onInit();

	}

	private void onInit() {
		Logg.d("onInit");
		
		onFindViewById();
		
		getLocationListTypeData();
		locationGuideText();
		commonAreaSet();
		getLocationData();

	}

	private void onFindViewById() {
		mTvLocationGuide1 = (TextView) findViewById(R.id.tv_location_guide1);
		mTvLocationGuide2 = (TextView) findViewById(R.id.tv_location_guide2);
		mLocationListView = (ListView) findViewById(R.id.lv_location);
		mLocationListView.setDivider(null);
	}
	
	

	/**
	 * 지역 리스트 조회
	 */
	final IWiseCallback iOkLocationList = new IWiseCallback() {
		@SuppressWarnings("unchecked")
		@Override
		public boolean WiseCallback(CallbackParam param) {
			Logg.d("iOkLocationList");

			if (param.param1 != null) {
				if (mArrLocationData != null)
					mArrLocationData.clear();
				mArrLocationData = (ArrayList<LocationInfoData>) param.param1;
				Logg.d("mArrLocationData = " + mArrLocationData.size());

				listViewInit();				
			} else {
			}
			return false;
		}
	};
	
	/**
	 * 사용자 포함 지역 리스트 조회
	 */
	final IWiseCallback iOkLocationUserCountList = new IWiseCallback() {
		@SuppressWarnings("unchecked")
		@Override
		public boolean WiseCallback(CallbackParam param) {
			Logg.d("iOkLocationUserCountList");

			if (param.param1 != null) {
				if (mArrLocationData != null)
					mArrLocationData.clear();				
				mArrLocationData = (ArrayList<LocationInfoData>) param.param1;
				
				Logg.d("mArrLocationData = " + mArrLocationData.size());

				listViewInit();				
			} else {
			}
			return false;
		}
	};

	public boolean isChecked() {
		return (this.mCheck == LOCATION_CHECK_FLAG_CHECKED) ? true : false;
	}

	public void setCheckBoxFlag(int flag) {
		this.mCheck = flag;
	}

	OnClickListener mTitleClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (isChecked()) {
				
				if(mLocationCode.equals(mCheckSmallCode)){
					finish();
				} else {
					String title  = mContext.getResources().getString( R.string.info );
					
		            String message = String.format( mContext.getResources().getString( R.string.location_select_popup ), mLocationSmallName );		            
		            
		            DialogUtil.getConfirmCancelDialog( mContext, title, message, new DialogInterface.OnClickListener() {
		                
		                @Override
		                public void onClick( DialogInterface dialog, int which ) {
		                    if (which != DialogInterface.BUTTON_POSITIVE) {
		                        return;
		                    } else{
		                    		showProgressDialog();
			                    	setDataResultOkFinish();
		                    }
		                }
		            } ).show();
				}				
			} else {
				Logg.d("mTitleClick invite nodata");
				Toast.makeText(LocationListViewActivity.this,
						R.string.location_select_toast, Toast.LENGTH_SHORT)
						.show();
				return;
			}
		}
	};

	OnClickListener mCheckClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (mLocationListView == null) {
				return;
			}

			int position = mLocationListView.getPositionForView(v);
			Logg.d("position = " + position);

			if (position < 0) {
				Logg.d("can not find item from view");
				return;
			}

			LocationInfoData info = mArrLocationData.get(position);
			Logg.d("info = " + info.getLocationSmallName());

			setCheckState(info, position);
		}
	};

	private void setCheckState(LocationInfoData info, int position) {
		if (isChecked()) {
			mLocationCode = info.getLocationSmallCode();
		}
		else {
			setCheckBoxFlag(LOCATION_CHECK_FLAG_CHECKED);
			mLocationCode = info.getLocationSmallCode();
		}
		mLocationListViewAdapter.notifyDataSetChanged();
	}

	private void commonAreaSet() {
		switch(mGetLocationListType){
			case CommonData.LOCATION_LIST_TYPE_JOIN:
			case CommonData.LOCATION_LIST_TYPE_NORMAL:
			case CommonData.LOCATION_LIST_TYPE_COMPANY_REG:
				mTitleInfoView = new TitleInfoView(this, (View) findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL);				
				break;			
			case CommonData.LOCATION_LIST_TYPE_COMPANY:
			case CommonData.LOCATION_LIST_TYPE_COMPANY_SEND:				
				mTitleInfoView = new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_COMPANY);
				break;
		}	
		mTitleInfoView.updateView(R.string.location_select_title,	TitleInfoView.TITLE_MODE_AREA_SELECT, mTitleClick);
	}
	
	private void locationGuideText(){
		switch(mGetLocationListType){
			case CommonData.LOCATION_LIST_TYPE_JOIN:
			case CommonData.LOCATION_LIST_TYPE_NORMAL:
				mTvLocationGuide1.setText(getString(R.string.location_normal_select_guide1));
				mTvLocationGuide2.setVisibility(View.VISIBLE);
				mTvLocationGuide2.setText(getString(R.string.location_normal_select_guide2));
				break;
			case CommonData.LOCATION_LIST_TYPE_COMPANY_REG:
			case CommonData.LOCATION_LIST_TYPE_COMPANY:
			case CommonData.LOCATION_LIST_TYPE_COMPANY_SEND:			
				mTvLocationGuide1.setText(getString(R.string.location_company_select_guide));
				break;
		}
	}

	private void getLocationData() {
		Logg.d("getLocationData");
		
		showProgressDialog();		
		LocationProtocol locationProtocol;

		switch(mGetLocationListType){
			case CommonData.LOCATION_LIST_TYPE_JOIN:
			case CommonData.LOCATION_LIST_TYPE_NORMAL:
			case CommonData.LOCATION_LIST_TYPE_COMPANY_REG:
			case CommonData.LOCATION_LIST_TYPE_COMPANY:
				locationProtocol = new LocationProtocol(iOkLocationList, iFail);
				locationProtocol.locationData(0);
				break;			
			case CommonData.LOCATION_LIST_TYPE_COMPANY_SEND:
				locationProtocol = new LocationProtocol(iOkLocationUserCountList, iFail);
				locationProtocol.locationUserCountListData(0, SharedPrefManager.getLoginEmail(this));
				break;
		}
		
	}

	private void listViewInit() {
		mLocationListViewAdapter = new LocationListViewAdapter(this, mArrLocationData, R.layout.location_list_item) {
			public View setLocationView(int position, View convertView,	ViewGroup parent, ViewHolderList holder) {
				setLocationViewData(holder,	(LocationInfoData) getItem(position), position);

				return convertView;
			}
		};
		mLocationListView.setAdapter(mLocationListViewAdapter);
		getLocationHeaderNum();		
		closeProgressDialog();		
	}

	private void setLocationViewData(ViewHolderList holder,
			LocationInfoData info, int position) {
		
		if(position == 0 || position == mArrLocationHeaderNum.get(0)
						 || position == mArrLocationHeaderNum.get(1) 
						 || position == mArrLocationHeaderNum.get(2) 
						 || position == mArrLocationHeaderNum.get(3)
						 || position == mArrLocationHeaderNum.get(4)){
			setLocationHeadLine(holder, info.getLocationBigName());
		} else {
			holder.mListHeader.setVisibility(View.GONE);
		}
		mLocationBigName = info.getLocationBigName();		
		holder.mLocationName.setText(info.getLocationSmallName());
				
		if(mGetLocationListType == CommonData.LOCATION_LIST_TYPE_COMPANY_SEND){
			holder.mLocationUserCount.setText("(" + info.getLocationUserCount() + "명 이용 중)");
		}		
		String mSmallCode = info.getLocationSmallCode();

		if (mSmallCode.equals(mLocationCode)) {
			holder.mCbLocation.setChecked(true);
			setLocationSelectData(info);
		}
		else {
			holder.mCbLocation.setChecked(false);
		}
		holder.mContactListItem.setOnClickListener(mCheckClickListener);		
	}

	// 지역 헤더
	private void setLocationHeadLine(ViewHolderList holder, String headLine) {
		holder.mListHeader.setVisibility(View.VISIBLE);
		holder.mTvHeadLine.setVisibility(View.VISIBLE);
		holder.mTvHeadLine.setText(headLine);
	}
	
	private void getLocationHeaderNum(){
		mLocationBigName = mArrLocationData.get(0).getLocationBigName();
		for(int i=0; i<mArrLocationData.size(); i++){
			if(mLocationBigName.equals(mArrLocationData.get(i).getLocationBigName())){
				
			}else {
				mHeaderCount = i;
				mArrLocationHeaderNum.add(mHeaderCount);
				mLocationBigName = mArrLocationData.get(i).getLocationBigName();
			}
		}
	}
	
	private void getLocationListTypeData(){
		Intent intent = getIntent();
		mGetLocationListType = intent.getIntExtra(IntentDefine.EXTRA_LOCATION_LIST_TYPE, 0);
		
		switch(mGetLocationListType){
			case CommonData.LOCATION_LIST_TYPE_JOIN:
				aiData = (AuthInfoData) intent.getSerializableExtra(IntentDefine.EXTRA_AUTH_INFO_DATA);
				break;
			case CommonData.LOCATION_LIST_TYPE_NORMAL:
				mLocationCode = intent.getStringExtra((IntentDefine.EXTRA_LOCATION_USER_DATA));
				if(mLocationCode != null) {
					mCheckSmallCode = mLocationCode;
					setCheckBoxFlag(LOCATION_CHECK_FLAG_CHECKED);
				}				
				break;
			case CommonData.LOCATION_LIST_TYPE_COMPANY_REG:
				mLocationInfoData = (LocationInfoData) intent.getSerializableExtra(IntentDefine.EXTRA_LOCATION_COMPANY_DATA);
				
				if(mLocationInfoData != null){
					mLocationCode = mLocationInfoData.getLocationSmallCode();
					if(mLocationCode != null) {
						mCheckSmallCode = mLocationCode;
						setCheckBoxFlag(LOCATION_CHECK_FLAG_CHECKED);
					}
				}
				break;
			case CommonData.LOCATION_LIST_TYPE_COMPANY:
				mLocationCode = intent.getStringExtra(IntentDefine.EXTRA_LOCATION_COMPANY_DATA);
				if(mLocationCode != null) {
					mCheckSmallCode = mLocationCode;
					setCheckBoxFlag(LOCATION_CHECK_FLAG_CHECKED);
				}
				break;			
			case CommonData.LOCATION_LIST_TYPE_COMPANY_SEND:
				mLocationCode = intent.getStringExtra(IntentDefine.EXTRA_LOCATION_COMPANY_DATA);
				mCheckSmallCode = mLocationCode;
				if(mLocationCode != null) {
					setCheckBoxFlag(LOCATION_CHECK_FLAG_CHECKED);
				}
				break;
		}
	}
	
	private void setLocationSelectData(LocationInfoData info){
		mLocationSmallName = info.getLocationSmallName();
		switch(mGetLocationListType){
			case CommonData.LOCATION_LIST_TYPE_JOIN:
				aiData.setLocationSmallCode(info.getLocationSmallCode());
				break;
			case CommonData.LOCATION_LIST_TYPE_NORMAL:
			case CommonData.LOCATION_LIST_TYPE_COMPANY:
				mLocationCode = info.getLocationSmallCode();
			case CommonData.LOCATION_LIST_TYPE_COMPANY_REG:
				mLocationInfoData.setLocationBigName(info.getLocationBigName());
				mLocationInfoData.setLocationSmallName(info.getLocationSmallName());
				mLocationInfoData.setLocationSmallCode(info.getLocationSmallCode());
				break;
			case CommonData.LOCATION_LIST_TYPE_COMPANY_SEND:
				mLocationInfoData.setLocationBigName(info.getLocationBigName());
				mLocationInfoData.setLocationSmallName(info.getLocationSmallName());
				mLocationInfoData.setLocationSmallCode(info.getLocationSmallCode());
				mLocationInfoData.setLocationUserCount(info.getLocationUserCount());
				break;
		}
		
	}

	private void setDataResultOkFinish() {	    
		switch(mGetLocationListType){
            case CommonData.LOCATION_LIST_TYPE_JOIN:
                Intent intent = new Intent(mContext, AgreementActivity.class);
                intent.putExtra(IntentDefine.EXTRA_AUTH_INFO_DATA, aiData);
                startActivityForResult(intent, IntentDefine.REQCODE_REGISTER);
                closeProgressDialog();
                break;
            case CommonData.LOCATION_LIST_TYPE_NORMAL:
                setInfoData(true);
                break;
		    case CommonData.LOCATION_LIST_TYPE_COMPANY:
		        setInfoData(false);
                break;
		    case CommonData.LOCATION_LIST_TYPE_COMPANY_REG:
		    case CommonData.LOCATION_LIST_TYPE_COMPANY_SEND:
		        intent = new Intent();
		        intent.putExtra(IntentDefine.EXTRA_COMPANY_INFO, mLocationInfoData);
		        setResult(Activity.RESULT_OK, intent);
		        finish();
                break;
		}
	}
	
	void setInfoData(boolean bNormalMode){
		
		String mFlag = NORMAL_MODE;
	    
	    if(bNormalMode == false){
	        mFlag = COMPANY_MODE;
	    }	    
        LocationProtocol locationProtocol = new LocationProtocol(new IWiseCallback() {            
            @Override
            public boolean WiseCallback(CallbackParam param) {
                closeProgressDialog();
                finish();
                return false;
            }
        }, iFail);        
        locationProtocol.getLocationDataEdit(0, SharedPrefManager.getLoginEmail(this), mFlag, mLocationCode);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			setResult(Activity.RESULT_OK);
			finish();
		}
	}
	
}
