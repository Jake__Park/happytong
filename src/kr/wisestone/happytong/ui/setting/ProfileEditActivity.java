package kr.wisestone.happytong.ui.setting;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.UserInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.UserProtocol;
import kr.wisestone.happytong.data.util.ImageLoader;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.ui.BaseActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.ui.widget.TitleInfoView;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.Logg;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileEditActivity extends BaseActivity  {
	Context 		mContext;
	ImageView 		mEdit;
	EditText 		mNickName;
	TextView 		mPNumber;
	ImageView 		mEditOk;
	TextView 		mQuitMsg;
	ImageView 		mQuit;
	ImageView		mProfileView;
	UserProtocol 	m_UserProtocol;
	UserInfoData 	m_UserInfoData;
	Bitmap 		 	mPhoto = null;
	SharedPrefManager spMgr;
	String			mNameStr = null;
	Uri 			mImageCaptureUri;

	static final int TAKE_CAMERA	= 1;
	static final int PICK_ALBUM 	= 2;
	static final int CROP_FROM_CAMERA = 3;
		
	boolean isImage = false;
	boolean isMenu = false;
	boolean isProfileDel = false;//프로필 이미지 삭제 선택시 서버에 업로드 체크
	boolean isExistImage = false;//프로필 이미지가 존재여부, 이미지 있을시 카메라,앨범->취소 시 이미지삭제버튼 활성화 위해
	
	private void SetIsImage(boolean bimg){isImage = bimg;}//프로필 사진 올리기 전 완료버튼 enable/disable
	private void SetIsMenu(boolean bmenu){isMenu = bmenu;}//프로필 메뉴 이미지 여부에 따라/ 이미지 있음: true, 없으면 false
	private boolean GetIsImage(){return isImage;}
	private boolean GetIsMenu(){return isMenu;}
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		init();
		getProfileInfo();
	}
	
	private void getProfileInfo(){
		showProgressDialog();
		
		m_UserProtocol = new UserProtocol( iOK, iFail );
		m_UserProtocol.getProfileGet(CallBackParamWrapper.CODE_PROFILE_GET, SharedPrefManager.getLoginEmail(this)); 	
	}

	@Override
	public void onProtocolResponse(int eventId, CallbackParam param) {
		super.onProtocolResponse(eventId, param);
		  if (param.param1 != null) {
			  m_UserInfoData = (UserInfoData) param.param1;
			  
			  Logg.d( "pf.1 = " + m_UserInfoData.getDate() );
              Logg.d( "pf.2 = " + m_UserInfoData.getNickName() );
              Logg.d( "pf.3 = " + m_UserInfoData.getUserImageUrl() );
              Logg.d( "pf.4 = " + m_UserInfoData.getPhoneNumber() );
              Logg.d( "pf.5 = " + m_UserInfoData.getFbAd() );
			  
              if(m_UserInfoData.getUserImageUrl().endsWith("none"))
              {
            	  isExistImage = false;
            	  SetIsMenu(false);
              }
              else
              {
            	  isExistImage = true;
            	  SetIsMenu(true);
              }
              
		 	  drawProfileData();
		 	  closeProgressDialog();
		  }
	}

	@Override
	public void onProtoColFailResponse(int eventId, CallbackParam param) {
		// TODO Auto-generated method stub
		super.onProtoColFailResponse(eventId, param);
		closeProgressDialog();
	}

	private void drawProfileData(){
		String oriphone = null;
		ImageLoader ImageLoader = new ImageLoader(mContext);
		
		if(m_UserInfoData.getUserImageUrl()!=null && m_UserInfoData.getUserImageUrl() != "" && !(m_UserInfoData.getUserImageUrl().equalsIgnoreCase("null"))){
			ImageLoader.DisplayImage(m_UserInfoData.getUserImageUrl(), mProfileView);

		if(m_UserInfoData.getNickName()!=null && m_UserInfoData.getNickName().length() !=0)
		{
			mNickName.setText(m_UserInfoData.getNickName());
			mNickName.setSelection(mNickName.getText().length());
		}
			oriphone = AESEncryption.decrypt(m_UserInfoData.getPhoneNumber());
			String formatNum = PhoneNumberUtils.formatNumber(oriphone);
			mPNumber.setText(formatNum.replaceFirst("0", getResources().getString(R.string.setting_country_number)+" "));
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.profile_edit://편집
			Logg.d("onClick - profile edit");
			openContextMenu(findViewById(R.id.profile_edit));
	        break;
		case R.id.pf_nickname_edit://닉네임
			break;
		case R.id.pf_edit_ok://완료
			Logg.d("**onClick - edit_ok");
			showProfileEditOK();
			break;
		case R.id.pf_member_quit://탈퇴하기
			Logg.d("onClick - member_quit");
			showProfileQuit();
			break;
		}	
	}
	
	@Override
	protected int setContentViewId() {
		return R.layout.activity_setting_profile_edit;
	}
	
	private void init() {
		mContext = getApplicationContext();
		spMgr = SharedPrefManager.getInstance(mContext);
		
		mEdit = (ImageView) findViewById(R.id.profile_edit);
		mEdit.setOnClickListener(this);
		mPNumber = (TextView) findViewById(R.id.pf_phonenumber_edit);
		mEditOk = (ImageView) findViewById(R.id.pf_edit_ok);
		mEditOk.setOnClickListener(this);
		mQuit = (ImageView)findViewById(R.id.pf_member_quit);
		mQuit.setOnClickListener(this);
		mProfileView = (ImageView)findViewById(R.id.pf_user_img);
		mProfileView.setOnClickListener(this);
		mNickName = (EditText) findViewById(R.id.pf_nickname_edit);
		mNickName.addTextChangedListener(watcher);		
		
		new TitleInfoView(this, findViewById(R.id.title_lay), TitleInfoView.TITLE_TYPE_NORMAL).updateView(R.string.profile_title, TitleInfoView.TITLE_MODE_NORMAL);
		registerForContextMenu(findViewById(R.id.profile_edit));
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		
		if(!GetIsMenu())
		{
			menu.add(0, 0, 0, R.string.take_camera);
			menu.add(0, 1, 0, R.string.select_album);
			menu.add(0, 3, 0, R.string.setting_cancel);	
		}
		else{
			menu.add(0, 0, 0, R.string.take_camera);
			menu.add(0, 1, 0, R.string.select_album);
			menu.add(0, 2, 0, R.string.del_profile_img);
			menu.add(0, 3, 0, R.string.setting_cancel);
		}
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	private void initContextMenu() {
		findViewById(R.id.profile_edit).setOnClickListener(this);
		registerForContextMenu(findViewById(R.id.profile_edit));
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0://카메라촬영 
			takeCamera();
			break;
		case 1://앨범에서 선택 
			pickAlbum();
			break;
		case 2://프로필 이미지 삭제 
			mProfileView.setImageBitmap(null);
			SetIsMenu(false);
			isProfileDel = true;
			setUploadSrv(true);
			//UploadImageFileDel();
			break;
		}
		return super.onContextItemSelected(item);
	}
	
	private void takeCamera(){
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
		mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));
		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
		// 특정기기에서 사진을 저장못하는 문제가 있어 다음을 주석처리 합니다.  
		//intent.putExtra("return-data", true);
		startActivityForResult(intent, TAKE_CAMERA);
	}

	private void pickAlbum(){
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");
		intent.putExtra("crop", "true");
		//intent.putExtra("scale", true); 
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 170);
		intent.putExtra("outputY", 170);
		intent.putExtra("return-data", true);
	
        startActivityForResult(intent,PICK_ALBUM);
	}
	
	private void showProfileEditOK(){
		Dialog dialog = DialogUtil.getConfirmCancelDialog(this, R.string.pf_edit_title, R.string.pr_edit_popup_msg, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					String nickName = mNickName.getText().toString();
					Logg.d("showProfileEditOK:nickname : "+ nickName );
					
					showProgressDialog();

					SharedPrefManager.getInstance(mContext).setPrefProfileNickName(nickName);
					
					if(isProfileDel)
						UploadImageFileDel();
					else 
						uploadImageFile(mPhoto);
					
					new UserProtocol( profileEditOk, iFail ).getProfileEdit(CallBackParamWrapper.CODE_PROFILE_EDIT, spMgr.getPrefLoginEmail(),
											spMgr.getPrefLoginPwd(), Utils.getEncryptDeviceIdForGCM(mContext), Utils.getPhoneNumber(mContext), CommonData.DEVICE_TYPE, nickName);
	
				}
			}
		});
		dialog.show();
	}
	
	final IWiseCallback profileEditOk = new IWiseCallback() {
		
		@Override
		public boolean WiseCallback(CallbackParam param) {
			Logg.d("profileEditOk ");
			if(param.param1 != null)
			{
				Toast.makeText(mContext, R.string.pf_edit_toast, Toast.LENGTH_SHORT).show();
				setUploadSrv(false);
			}
			isProfileDel = false;
			closeProgressDialog();
			return false;
		}
	};
			
	private void showProfileQuit(){
		Dialog dialog = DialogUtil.getConfirmCancelDialog(this, R.string.pf_quit_title, R.string.pf_quit_msg, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					Logg.d("showProfileQuit - DialogInterface.BUTTON_POSITIVE");
					
				
					showProgressDialog();
					new UserProtocol( profileResignOk, iFail ).getProfileResign(CallBackParamWrapper.CODE_PROFILE_RESIGN, spMgr.getPrefLoginEmail(), spMgr.getPrefLoginPwd());
				}
			}
		});
		dialog.show();
	}
	
	final IWiseCallback profileResignOk = new IWiseCallback() {
		
		@Override
		public boolean WiseCallback(CallbackParam param) {
			closeProgressDialog();
			Utils.deleteUserDataAndFinishCuFunU(ProfileEditActivity.this, true);
			return false;
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == RESULT_CANCELED) {
			setUploadSrv(false);
						
			//카메라,앨범 -> 취소 왔을시 기존에 이미지 있는지 체크
			if(isExistImage)
				SetIsMenu(true);
			else
				SetIsMenu(false);
				
			return;
		}
		else if(resultCode == RESULT_OK)
		{
			setUploadSrv(true);
			SetIsMenu(true);
		}

		switch(requestCode){
		case TAKE_CAMERA:
		{
			Intent intent = new Intent("com.android.camera.action.CROP");
			intent.setDataAndType(mImageCaptureUri, "image/*");  

			intent.putExtra("scale", true); 
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("outputX", 155);
			intent.putExtra("outputY", 155);
			intent.putExtra("return-data", true);

			startActivityForResult(intent,CROP_FROM_CAMERA);
		}
		break;
		case PICK_ALBUM:
		{
			mPhoto =  (Bitmap)data.getParcelableExtra("data");
			mProfileView.setImageBitmap(mPhoto);
			isExistImage = true;
		}
		break;

		case CROP_FROM_CAMERA:
		{
			final Bundle extras = data.getExtras();
			if(extras != null)
			{ 
				mPhoto = extras.getParcelable("data");
				mProfileView.setImageBitmap(mPhoto);
				isExistImage = true;
			}
			// 임시 파일 삭제
			File f = new File(mImageCaptureUri.getPath());
			if(f.exists())
			{ 
				f.delete(); 
			}
		}
		break;
		}
	}
	
	//서버에 업로드 시키기
	private void uploadImageFile(Bitmap profileBitmap){
		showProgressDialog();
		
		if(profileBitmap == null) {
			return;
		}
		new UserProtocol( profilePhoteEditOk, iFail ).getProfilePhotoEdit(CallBackParamWrapper.CODE_PROFILE_PHOTO_EDIT, SharedPrefManager.getLoginEmail(mContext),
																	getDateTime() ,profileBitmap);
	}
	
	private void UploadImageFileDel(){
		showProgressDialog();
		
		new UserProtocol( profilePhoteDelOk, iFail ).getProfilePhotoDel(CallBackParamWrapper.CODE_PROFILE_PHOTO_DEL, SharedPrefManager.getLoginEmail(mContext));
				
	}
	/*
	 * 프로필사진 추가
	 */
	final IWiseCallback profilePhoteEditOk = new IWiseCallback() {
		
		@Override
		public boolean WiseCallback(CallbackParam param) {
			// TODO Auto-generated method stub
			Logg.d("**profilePhoteEditOk- SUCCESS***");
			closeProgressDialog();
			return false;
		}
	};
	
	/*
	 * 프로필사진 삭제
	 */
	final IWiseCallback profilePhoteDelOk = new IWiseCallback() {
		
		@Override
		public boolean WiseCallback(CallbackParam param) {
			// TODO Auto-generated method stub
			Logg.d("**profilePhoteDelOk- SUCCESS***");
			closeProgressDialog();
			return false;
		}
	};
	
	final TextWatcher watcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			mNameStr = 	m_UserInfoData.getNickName();
		}

		@Override
		public void afterTextChanged(Editable s) {
			if(mNameStr.equals(s.toString())){
				mEditOk.setBackgroundResource( R.drawable.sub6_01_ok2_btn );
				setEnableOKButton(false);
			}
			else{
				mEditOk.setBackgroundResource( R.drawable.sub6_01_ok_btn );
				setEnableOKButton(true);
			}
		}
	};
		

	private void setEnableOKButton(boolean b) {
		mEditOk.setEnabled(b);
	}

	public String getDateTime(){
		SimpleDateFormat formatter = new SimpleDateFormat ( "yyyy.MM.dd a h:mm", Locale.KOREA );
		Date currentTime = new Date ( );
		String m_dateTime = formatter.format ( currentTime );
		Logg.d("manualSync = "+ m_dateTime);
		return m_dateTime;
	}
	
	private void setUploadSrv(boolean bImg){
		SetIsImage(bImg);
		
		if(GetIsImage())
		{
			mEditOk.setBackgroundResource( R.drawable.sub6_01_ok_btn );
			setEnableOKButton(true);
		}
		else
		{
			mEditOk.setBackgroundResource( R.drawable.sub6_01_ok2_btn );
			setEnableOKButton(false);
		}
		
	}
	
}