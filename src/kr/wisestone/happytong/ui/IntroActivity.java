package kr.wisestone.happytong.ui;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.dataset.AuthInfoData;
import kr.wisestone.happytong.data.dataset.UserInfoData;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.network.NetworkStatus;
import kr.wisestone.happytong.data.protocol.AuthProtocol;
import kr.wisestone.happytong.data.protocol.PurchaseProtocol;
import kr.wisestone.happytong.data.protocol.UserProtocol;
import kr.wisestone.happytong.data.util.FriendSynchronizer;
import kr.wisestone.happytong.data.util.RecycleUtils;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.plugin.GCMController;
import kr.wisestone.happytong.ui.auth.LoginActivity;
import kr.wisestone.happytong.ui.dialog.DialogUtil;
import kr.wisestone.happytong.util.Logg;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

public class IntroActivity extends BaseActivity {

	private Context mContext;

	final IWiseCallback networkLoginOk = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			Logg.d("onProtocolResponse");

			if (param.param1 != null) {
				AuthInfoData mData = (AuthInfoData) param.param1;

				setOwlCount(mData.getFreeStampCount(), mData.getPaidStampCount());
				setGoldEggCount(mData.getRemainGoldEgg());
				
				Logg.d("networkLoginOk - " + mData.isFreeStampReceive());
				SharedPrefManager.getInstance(mContext).setPrefShowFreeOwlReceivePopup(mData.isFreeStampReceive());
				SharedPrefManager.getInstance(mContext).setPrefLoginCompanyState(Integer.parseInt(mData.getCompanyState()));
				
				if(SharedPrefManager.getInstance(mContext).getPrefLoginCompanyState() == 1){
					SharedPrefManager.getInstance(mContext).setPrefCompanyOneTimeCoupon(mData.getOneTimeCoupon());
					SharedPrefManager.getInstance(mContext).setPrefCompanyNoLimitTimeCoupon(mData.getNoLimitTimeCoupon());
					SharedPrefManager.getInstance(mContext).setPrefCompanyBusinessNumber(mData.getBusinessNumber());
					
					Logg.d("getOneTimeCoupon============== " + mData.getOneTimeCoupon());
					Logg.d("getNoLimitTimeCoupon============== " + mData.getNoLimitTimeCoupon());
					Logg.d("getBusinessNumber ================= " + mData.getBusinessNumber());
				}
			}
			requestProfileGetForSettingFacebookAd();
			return false;
		}
	};

	// XXX USE Customized NetworkFail
	final IWiseCallback networkLoginFail = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			closeProgressDialog();

			Logg.d("onProtoColFailResponse" + param.param1 + " / " + param.param2 + " / " + param.param3 + " / " + param.errorCode);

			if (param.errorCode == 300) {
				Utils.deleteUserData(mContext);
				DialogUtil.getConfirmDialog(mContext, "", getString(R.string.error_code_300_login_fail), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
						startActivity(new Intent(IntroActivity.this, LoginActivity.class));
					}
				}).show();
			} else {
				Toast.makeText(mContext, R.string.txt_fail_login, Toast.LENGTH_SHORT).show();
				finish();
			}
			// Utils.ToastNetworkError(this);
			return false;
		}
	};

	final IWiseCallback networkProfileGetOk = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			closeProgressDialog();

			if (param.param1 != null) {
				UserInfoData userInfoData = (UserInfoData) param.param1;
				SharedPrefManager.getInstance(mContext).setPrefFackbook(userInfoData.getFbAd());
			}

			checkSignatureData();
			return false;
		}
	};

	// XXX USE Customized NetworkFail
	final IWiseCallback networkProfileGetFail = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			closeProgressDialog();
			checkSignatureData();
			return false;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// TODO CuFunU 임시 디비 복사 및 삭제 명령
		// Utils.copyDB();
		// Utils.deleteDB();

		mContext = this;

		// GCM등록 먼저 진행하도록 함
		if (TextUtils.isEmpty(GCMRegistrar.getRegistrationId(this))) {
			requestGcmRegister();
		} else {
			controlGeneralProcess();
		}

	}

	/**
	 * 페이스북에 앱홍보했는지 여부 체크
	 */
	protected void requestProfileGetForSettingFacebookAd() {
		new UserProtocol(networkProfileGetOk, networkProfileGetFail).getProfileGet(CallBackParamWrapper.CODE_PROFILE_GET, SharedPrefManager.getLoginEmail(this));
	}

	private BroadcastReceiver mGcmRegReceiver;

	private void requestGcmRegister() {
		mGcmRegReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.getBooleanExtra(GCMController.EXTRA_ISERROR, true)) {
					Logg.d("GCM Registration - fail");
					// 처리 여부 다이얼로그 문구 추가
					DialogUtil.getConfirmDialog(mContext, "", getString(R.string.exit_message_not_available_gcm), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					}).show();
				} else {
					controlGeneralProcess();
				}
				unregisterReceiver(mGcmRegReceiver);
			}
		};
		registerReceiver(mGcmRegReceiver, new IntentFilter(GCMController.SEND_BROADCAST_REGID));
		GCMController.registerGCM(getApplicationContext());
	}

	private void controlGeneralProcess() {
		if (isValidate()) {
			checkAppVersion();
		}
	}

	private void checkAppVersion() {

		AuthProtocol protocol = new AuthProtocol(new IWiseCallback() {

			@Override
			public boolean WiseCallback(CallbackParam param) {
				if (param.param1 != null) {
					versionCheck((String) param.param1);
				} else {
					showPopupBack(R.string.info, R.string.network_error_try_again);
				}
				return false;
			}
		}, new IWiseCallback() {
			@Override
			public boolean WiseCallback(CallbackParam param) {
				showPopupBack(R.string.info, R.string.network_error_try_again);
				return false;
			}
		});
		protocol.checkVersionForAndroid(0, Utils.getAppVersionName(this));
	}

	private void checkAutoSync() {
		String autoSyncString = SharedPrefManager.getInstance(this).getPrefAutoSync();
		if (TextUtils.isEmpty(autoSyncString) || autoSyncString.equals(CommonData.AUTO_SYNC_ON)) {
			new FriendSynchronizer(mContext, syncListener);
		} else {
			StatisticGraphActivity(100);
		}
	}

	private void checkSignatureData() {
		String purchaseData = SharedPrefManager.getInstance(mContext).getPurchaseData();
		String signatureData = SharedPrefManager.getInstance(mContext).getSignatureData();

        String companyPurchaseData = SharedPrefManager.getInstance(mContext).getPurchaseData();
        String companySignatureData = SharedPrefManager.getInstance(mContext).getSignatureData();
        
		if (TextUtils.isEmpty(purchaseData) == false|| TextUtils.isEmpty(signatureData) == false) {
            checkSignature(purchaseData, signatureData);
		} else if (TextUtils.isEmpty(companyPurchaseData) == false || TextUtils.isEmpty(companySignatureData) == false) {
		    checkCompanySignature(companyPurchaseData, companySignatureData);
		} else {
            checkAutoSync();
		}
	}

	private void checkSignature(String purchase, String signature) {

		PurchaseProtocol protocol = new PurchaseProtocol(new IWiseCallback() {

			@Override
			public boolean WiseCallback(CallbackParam param) {
				SharedPrefManager.getInstance(mContext).setPurchaseData("");
				SharedPrefManager.getInstance(mContext).setSignatureData("");

				checkAutoSync();
				return false;
			}
		}, new IWiseCallback() {

			@Override
			public boolean WiseCallback(CallbackParam param) {
				checkAutoSync();
				return false;
			}
		});
		protocol.checkSignature(0, SharedPrefManager.getLoginEmail(this), purchase, purchase);
	}
	
    private void checkCompanySignature(String purchase, String signature) {

        PurchaseProtocol protocol = new PurchaseProtocol(new IWiseCallback() {

            @Override
            public boolean WiseCallback(CallbackParam param) {
                SharedPrefManager.getInstance(mContext).setCompanyPurchaseData("");
                SharedPrefManager.getInstance(mContext).setCompanySignatureData("");

                checkAutoSync();
                return false;
            }
        }, new IWiseCallback() {

            @Override
            public boolean WiseCallback(CallbackParam param) {
                checkAutoSync();
                return false;
            }
        });
        protocol.setCompaniesMarketBuyValidity(0, SharedPrefManager.getLoginEmail(this), purchase, purchase);
    }	

	final FriendSynchronizer.SyncListener syncListener = new FriendSynchronizer.SyncListener() {
		@Override
		public void onStartLoading() {
			showProgressDialog();
		}

		@Override
		public void onEndLoading() {
			closeProgressDialog();
		}

		@Override
		public void onEndSynchronize(int retValue) {
			switch (retValue) {
			case FriendSynchronizer.RET_NETWORK_FAIL:
				Toast.makeText(mContext, "Sync Network Request Fail!!", Toast.LENGTH_SHORT).show();
				break;
			case FriendSynchronizer.RET_NO_DATA:
				break;
			case FriendSynchronizer.RET_NORMAL:
				break;
			}
			StatisticGraphActivity(100);
		}
	};

	private void versionCheck(String isUpgrade) {

		if ("Y".equals(isUpgrade)) {
			DialogUtil.getConfirmDialog(this, R.string.info, R.string.txt_noti_app_upgrade, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						moveUpgradePage();
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						finish();
						break;
					}
				}
			}).show();
		} else if ("S".equals(isUpgrade)) {
			DialogUtil.getConfirmCancelDialog(this, R.string.info, R.string.txt_noti_app_upgrade, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						moveUpgradePage();
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						loginProcess();
						break;
					}
				}
			}).show();
		} else if ("N".equals(isUpgrade)) {
			loginProcess();
		}
	}

	private void moveUpgradePage() {
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=kr.wisestone.happytong"));
		startActivity(intent);
		finish();
	}

	private void loginProcess() {
		SharedPrefManager spMgr = SharedPrefManager.getInstance(this);

		if (TextUtils.isEmpty(spMgr.getPrefLoginEmail())) {
			startActivity(new Intent(this, LoginActivity.class));
			finish();
		} else {
			// requestLogin(spMgr.getPrefLoginEmail(), spMgr.getPrefLoginPwd());
			// 서버에 사용자가 존재하는지 id와 deviceID로 확인하고 로그인 프로세스 진행 - 다른단말아이디로 로그인한경우, 사용자탈퇴한경우 등 처리 위함
			requesetAuthIdDeviceCheck(spMgr.getPrefLoginEmail(), Utils.getEncryptDeviceIdForGCM(this));
		}
	}

	private void requesetAuthIdDeviceCheck(String id, String deviceId) {
		new AuthProtocol(networkAuthIdDeviceCheckOk, networkAuthIdDeviceCheckFail).authIdDeviceCheck(CallBackParamWrapper.CODE_AUTH_ID_DEVICE_CHECK, id, deviceId);
		showProgressDialog();
	}

	final IWiseCallback networkAuthIdDeviceCheckOk = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			Logg.d("networkAuthIdDeviceCheckOk");

			if (JsonKeyWrapper.success.equals(param.param1)) {
				requestLogin();
			} else {
				networkAuthIdDeviceCheckFail.WiseCallback(param);
			}
			return false;
		}
	};

	// XXX USE Customized NetworkFail
	final IWiseCallback networkAuthIdDeviceCheckFail = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			Logg.d("networkAuthIdDeviceCheckFail");
			Utils.deleteUserData(mContext);

			closeProgressDialog();

			String message;
			if (JsonKeyWrapper.notDevice.equals(param.param1)) {
				message = getString(R.string.another_mobile_login);
				// } else if(JsonKeyWrapper.KEY_AUTH_NOT_USER.equals(param.param1)) {
				// message = getString(R.string.not_exist_user_info);
			} else {
				message = getString(R.string.not_exist_user_info);
			}

			DialogUtil.getConfirmDialog(mContext, "", message, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			}).show();
			return false;
		}
	};

	private boolean isValidate() {

		// USIM 확인
		// 20130610.jish. TextUtils.isEmpty(Utils.getPhoneNumber(this) -->> 단말유심있음. 비행기모드진입. 전화번호얻어오지못함. 기존 USIM에러로 빠지도록 처리. 제외시킴
		// if (!Utils.isSimStateReady(this) || TextUtils.isEmpty(GCMRegistrar.getRegistrationId(this)) || TextUtils.isEmpty(Utils.getPhoneNumber(this))) {
		if (!Utils.isSimStateReady(this)) {
			DialogUtil.getConfirmDialog(this, R.string.exit_title, R.string.exit_message_usim, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			}).show();
			return false;
		}
		// 네트워크 확인
		NetworkStatus.setContext(this);
		if (!NetworkStatus._isConnected()) {
			DialogUtil.getConfirmDialog(this, R.string.exit_title, R.string.exit_message_connect, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			}).show();
			return false;
		}
		return true;
	}

	@Override
	public void finish() {
		super.finish();
	}

	@Override
	protected void onDestroy() {
		RecycleUtils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		super.onDestroy();

		// GCM Receiver 해제
		if (mGcmRegReceiver != null) {
			try {
				unregisterReceiver(mGcmRegReceiver);
			} catch (Exception e) {
				Logg.d("onDestroy() - unregisterReceiver(mGcmRegReceiver) - exception");
			}
		}
	}

	private void StatisticGraphActivity(int delayTime) {
		Logg.d("StatisticGraphActivity");

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Logg.d("StatisticGraphActivity RUN!!");				
				
				if(SharedPrefManager.getInstance(mContext).getPrefGuideNoShow() == true){
					Intent intent = new Intent(IntroActivity.this, DummyMainActivity.class);
					startActivity(intent);
				} else {
					Intent intent = new Intent(IntroActivity.this, GuideViewActivity.class);
					startActivity(intent);
				}
				finish();
			}
		}, delayTime);
	}

	private void requestLogin() {
		SharedPrefManager spMgr = SharedPrefManager.getInstance(this);

		AuthInfoData aiData = new AuthInfoData();
		aiData.setId(spMgr.getPrefLoginEmail());
		aiData.setPassword(spMgr.getPrefLoginPwd());
		aiData.setDeviceId(Utils.getEncryptDeviceIdForGCM(this));
		aiData.setPhoneNumber(Utils.getPhoneNumber(this));

		new AuthProtocol(networkLoginOk, networkLoginFail).authLogin(CallBackParamWrapper.CODE_AUTH_LOGIN, aiData.getId(), aiData.getPassword(), aiData.getPhoneNumber(),
				aiData.getDeviceId(), aiData.getDeviceType());
		showProgressDialog();
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	protected int setContentViewId() {
		return R.layout.activity_intro;
	}
}
