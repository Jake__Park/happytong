package kr.wisestone.happytong;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import kr.wisestone.happytong.util.IntentDefine;
import android.app.ListActivity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class AllActivityListActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getIntent().getBooleanExtra(Intent.EXTRA_INITIAL_INTENTS, true)) {
			finish();
			return;
		}

		setListAdapter(new SimpleAdapter(this, getData(), android.R.layout.simple_list_item_2, new String[] { "title", "packageName" }, new int[] { android.R.id.text1,
				android.R.id.text2 }));
		getListView().setTextFilterEnabled(true);
	}

	protected List<ActivityMap> getData() {
		List<ActivityMap> myData = new ArrayList<ActivityMap>();

		Intent mainIntent = new Intent(IntentDefine.ACTION_HAPPYTONG, null);
		mainIntent.addCategory(IntentDefine.CATEGORY_TEST);

		PackageManager pm = getPackageManager();
		List<ResolveInfo> list = pm.queryIntentActivities(mainIntent, 0);

		if (null == list)
			return myData;

		for (int i = 0, len = list.size(); i < len; i++) {
			ResolveInfo info = list.get(i);
			String name = info.activityInfo.name;
			int lastCommaIndex = name.lastIndexOf(".");
			String label = name.substring(lastCommaIndex + 1);
			String pkgName = name.substring(0, lastCommaIndex);
			myData.add(new ActivityMap(label, pkgName, activityIntent(info.activityInfo.applicationInfo.packageName, info.activityInfo.name)));
		}

		Collections.sort(myData, sDisplayNameComparator);

		return myData;
	}

	private final static Comparator<ActivityMap> sDisplayNameComparator = new Comparator<ActivityMap>() {
		private final Collator collator = Collator.getInstance();

		public int compare(ActivityMap map1, ActivityMap map2) {
			return collator.compare(map1.get("title"), map2.get("title"));
		}
	};

	protected Intent activityIntent(String pkg, String componentName) {
		Intent result = new Intent();
		result.setClassName(pkg, componentName);
		return result;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		ActivityMap map = (ActivityMap) l.getItemAtPosition(position);

		Intent intent = (Intent) map.get("intent");
		startActivity(intent);
	}

	class ActivityMap extends HashMap<String, Object> {
		private static final long serialVersionUID = 7526363663054540872L;
		String title;
		String packageName;
		Intent intent;

		public ActivityMap(String title, String packageName, Intent intent) {
			super();
			this.title = title;
			this.packageName = packageName;
			this.intent = intent;
			put("title", title);
			put("packageName", packageName);
			put("intent", intent);
		}
	}

}
