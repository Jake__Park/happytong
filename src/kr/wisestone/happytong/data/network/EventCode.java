/**
 *
 */

package kr.wisestone.happytong.data.network;
public enum EventCode
{
    INVALID_VALUE                               (-1),
    S_SUCCESS                                   (0),
    E_FAIL                                      (-1),
    E_NONE										(-2),
    E_INTERNAL_ERROR                            (-2000),
    E_UNKNOWN_ERROR                             (-3000),
    E_INVALID_DATA                              (-4000),

    //Network error or event : 0x2000
    A_NETWORK_NOT_CONNECTED                     (0x2000),
    A_NETWORK_CHANGED                           (0x2001),
    A_NETWORK_CONNECTED                         (0x2002),
    A_NETWORK_DISCONNECTED                      (0x2003),
    E_NETWORK_ERROR                             (0x2004),
    E_INTERNAL_SERVER_ERROR                     (0x2005);

    EventCode(int nId)    { this.nId = nId; }
    public int getValue() { return nId; }
    public static EventCode getEventcode(int nId)
    {
        for(EventCode code : EventCode.values())
        {
            if(code.getValue() == nId)
                return code;
        }
        return null;
    }
    int nId;
}
