package kr.wisestone.happytong.data.network;

public class CallbackParam
{
    public Object eventId;
    public Object param1;
    public Object param2;
    public Object param3;
    public int errorCode;
   
    public CallbackParam() {};

    public CallbackParam(EventCode eventId)
    {
        this.eventId = eventId;
    }
}
