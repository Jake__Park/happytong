package kr.wisestone.happytong.data.network;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Vector;

import kr.wisestone.happytong.CuFunUApplication;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.graphics.Bitmap;
import android.os.AsyncTask;


public class HttpConnection {

	HttpParams httpParameters;
	AsyncTask<?, ?, ?> m_asyncTask;
	private final int CONNECTION_TIME_OUT = 30 * 1000;
	private final int RETRY_COUNT = 3;
	private final int SOCKET_TIME_OUT = 30 * 1000;

	private EventCode m_eError = EventCode.S_SUCCESS;

	public HttpConnection(AsyncTask<?, ?, ?> asyncTask) {
		m_asyncTask = asyncTask;
		init();
	}

	public void init() {
		httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, CONNECTION_TIME_OUT);
		HttpConnectionParams.setSoTimeout(httpParameters, SOCKET_TIME_OUT);
	}

	public EventCode getError() {
		return m_eError;
	}

	public void setLastError(EventCode eError) {
		m_eError = eError;
	}

	public String openGet(String url) {
		return openGet(url, RETRY_COUNT, "utf-8");
	}

	public String openGet(String url, String strFormat) {
		Logg.d(">> URL : " + url);
		return openGet(url, RETRY_COUNT, strFormat);
	}

	public String openPost(String url, String value, File file) {
		return Post(url, RETRY_COUNT, value, file);
	}

	public String openPost(String url, List<BasicNameValuePair> value) {
		return Post(url, RETRY_COUNT, value);
	}

	public String openPost(String url) {
		return Post(url, RETRY_COUNT);
	}

    public String openPost(String url, String id, String filename, Bitmap bitmap)// jikim
    {
        return openPost(url, id, "", filename, bitmap);
    }
	   
	public String openPost(String url, String id, String businessNumber, String filename, Bitmap bitmap)// jikim
	{
		return Post(url, RETRY_COUNT, id, businessNumber, filename, bitmap);
	}

	public String openGet(String url, int nRetryCount, String strFormat) {
		String strResult = null;
		if (!NetworkStatus.isConnected()) {
			setLastError(EventCode.A_NETWORK_NOT_CONNECTED);
			return null;
		}
		Logg.d("openGet url : " + url);

		final HttpClient Client = new DefaultHttpClient(httpParameters);

		// HttpGet httpget = new HttpGet(url);
		HttpGet httpget = null;
		try {
			httpget = new HttpGet(new URI(url));
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		HttpResponse result = null;

		HttpEntity he = null;
		for (int nTry = 0; nTry < nRetryCount && !m_asyncTask.isCancelled(); nTry++) {
			try {
				result = Client.execute(httpget);
				he = result.getEntity();

				if (he != null) {
					InputStream in = he.getContent();
					InputStreamReader isr = new InputStreamReader(in, strFormat);
					BufferedReader reader = new BufferedReader(isr);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line).append("\n");
					}
					in.close();
					strResult = sb.toString().trim();
					Logg.d("openClient : " + strResult);
				}
				break;
			} catch (ClientProtocolException e) {
				setLastError(EventCode.E_NETWORK_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (SocketException e) {
				setLastError(EventCode.E_NETWORK_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (IOException e) {
				setLastError(EventCode.E_NETWORK_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} finally {
				try {
					if (he != null)
						he.consumeContent();
				} catch (IOException e) {
					Logg.e(e);
					e.printStackTrace();
				}
			}
		}

		return strResult;
	}

	private HttpPost makeHttpPost(String url) throws Exception {
		HttpPost request = new HttpPost(url);
		Vector<BasicNameValuePair> nameValue = new Vector<BasicNameValuePair>();
		
		request.setEntity(makeEntity(setTokenNationOrNot(nameValue)));
		return request;
	}

	private HttpPost makeHttpPost(String url, List<BasicNameValuePair> valueData) throws Exception {
		Logg.d("makeHttpPost");
		HttpPost request = new HttpPost(url);
		
		request.setEntity(makeEntity(setTokenNationOrNot(valueData)));
		return request;
	}

	private HttpEntity makeEntity(List<BasicNameValuePair> nameValue) throws Exception {
		HttpEntity result = null;
		try {
			result = new UrlEncodedFormEntity(nameValue, "utf-8");
		} catch (UnsupportedEncodingException e) {
			Logg.e("makeEntity ERROR!!");
			e.printStackTrace();
		}
		return result;
	}

	public String Post(String url, int nRetryCount) {
		String strResult = null;
		if (!NetworkStatus.isConnected()) {
			setLastError(EventCode.A_NETWORK_NOT_CONNECTED);
			return null;
		}
		Logg.d("Post url : " + url);

		final HttpClient Client = new DefaultHttpClient();
		HttpResponse result = null;
		HttpEntity he = null;
		for (int nTry = 0; nTry < nRetryCount && !m_asyncTask.isCancelled(); nTry++) {
			try {
				HttpPost request = makeHttpPost(url);
				result = Client.execute(request);
				he = result.getEntity();

				if (he != null) {
					InputStream in = he.getContent();
					InputStreamReader isr = new InputStreamReader(in, "utf-8");
					BufferedReader reader = new BufferedReader(isr);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line).append("\n");
					}
					in.close();
					strResult = sb.toString().trim();
					Logg.d("openClient : " + strResult);
				}
				break;
			} catch (ClientProtocolException e) {
				setLastError(EventCode.E_NETWORK_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (SocketException e) {
				setLastError(EventCode.E_NETWORK_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (IOException e) {
				setLastError(EventCode.E_NETWORK_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (Exception e) {
				setLastError(EventCode.E_INTERNAL_ERROR);
				Logg.e(e);
				e.printStackTrace();
			}
		}

		return strResult;
	}

	/**
	 * List<BasicNameValuePair> list를 받아서 아이디가 있으면 토큰삽입, 기본 네이션 코드 삽입
	 * 
	 * @param valueData
	 * @return
	 */
	private List<BasicNameValuePair> setTokenNationOrNot(List<BasicNameValuePair> valueData) {
		boolean hasId = false;
		for (BasicNameValuePair item : valueData) {
			if (item.getName().equals(JsonKeyWrapper.id)) {
				hasId = true;
				break;
			}
		}

		if (hasId) {
			valueData.add(new BasicNameValuePair(JsonKeyWrapper.token, CuFunUApplication.getToken()));
		}
		valueData.add(new BasicNameValuePair(JsonKeyWrapper.nation, CuFunUApplication.getNationCode()));

		return valueData;
	}

    /**
     * 기본 url에 id, businessNumber와 토큰, 네이션코드 추가
     * @param url
     * @param id
     * @return
     */
    private String setTokenNation(String url, String id, String businessNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(url);
        sb.append("?id=");
        sb.append(id);
        
        if(businessNumber.equals("") == false){
            sb.append("&businessNumber=");
            sb.append(businessNumber);
        }
        
        sb.append("&" + JsonKeyWrapper.token + "=");
        sb.append(CuFunUApplication.getToken());
        sb.append("&" + JsonKeyWrapper.nation + "=");
        sb.append(CuFunUApplication.getNationCode());
        return sb.toString();
    }	

	public String Post(String url, int nRetryCount, List<BasicNameValuePair> valueData) {
		String strResult = null;
		if (!NetworkStatus.isConnected()) {
			setLastError(EventCode.A_NETWORK_NOT_CONNECTED);
			return null;
		}
		Logg.d("Post url : " + url);

		final HttpClient Client = new DefaultHttpClient();
		HttpResponse result = null;
		HttpEntity he = null;

		for (int nTry = 0; nTry < nRetryCount && !m_asyncTask.isCancelled(); nTry++) {
			try {
				HttpPost request = makeHttpPost(url, valueData);
				Logg.d(valueData);
				result = Client.execute(request);
				he = result.getEntity();

				if (he != null) {
					InputStream in = he.getContent();
					InputStreamReader isr = new InputStreamReader(in, "utf-8");
					BufferedReader reader = new BufferedReader(isr);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line).append("\n");
					}
					in.close();
					strResult = sb.toString().trim();
					Logg.d("openClient : " + strResult);
				}
				break;
			} catch (ClientProtocolException e) {
				setLastError(EventCode.E_NETWORK_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (SocketException e) {
				setLastError(EventCode.E_NETWORK_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (IOException e) {
				setLastError(EventCode.E_NETWORK_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (Exception e) {
				setLastError(EventCode.E_INTERNAL_ERROR);
				Logg.e(e);
				e.printStackTrace();
			}
		}

		return strResult;
	}

	public String Post(String url, int nRetryCount, String fileKey, File file) {
		String strResult = null;
		if (!NetworkStatus.isConnected()) {
			setLastError(EventCode.A_NETWORK_NOT_CONNECTED);
			return null;
		}
		Logg.d("Post url : " + url);

		final HttpClient Client = new DefaultHttpClient(httpParameters);

		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		for (int nTry = 0; nTry < nRetryCount && !m_asyncTask.isCancelled(); nTry++) {
			try {
				HttpPost httpost = new HttpPost(url);
				MultipartEntity entity = new MultipartEntity();
				ContentBody cbFile = new FileBody(file, "image/jpeg");
				entity.addPart(fileKey, cbFile);
				httpost.setEntity(entity);
				strResult = Client.execute(httpost, responseHandler);
				strResult = strResult.trim();
				Logg.d("openClient : " + strResult);
				break;
			} catch (ClientProtocolException e) {
				setLastError(EventCode.E_INTERNAL_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (SocketException e) {
				setLastError(EventCode.E_INTERNAL_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (IOException e) {
				setLastError(EventCode.E_INTERNAL_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (Exception e) {
				setLastError(EventCode.E_INTERNAL_ERROR);
				Logg.e(e);
				e.printStackTrace();
			}
		}

		return strResult;
	}

	public String Post(String url, int nRetryCount, String id, String businessNumber, String fileKey, Bitmap bitmap) {
		String strResult = null;
		if (!NetworkStatus.isConnected()) {
			setLastError(EventCode.A_NETWORK_NOT_CONNECTED);
			return null;
		}
		Logg.d("Post url : " + url);

		final HttpClient Client = new DefaultHttpClient(httpParameters);

		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		for (int nTry = 0; nTry < nRetryCount && !m_asyncTask.isCancelled(); nTry++) {
			try {
				// String tempurl = url + "?id=" + valueData;
				String tempurl = setTokenNation(url, id, businessNumber);
				Logg.d("get id url : " + tempurl);

				HttpPost httpost = new HttpPost(tempurl);
				MultipartEntity entity = new MultipartEntity();
				ByteArrayOutputStream bao = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, bao);
				byte[] ba = bao.toByteArray();
				// edntity.addPart("id", new StringBody(valueData) );
				entity.addPart("photo", new ByteArrayBody(ba, fileKey + ".png"));

				httpost.setEntity(entity);
				strResult = Client.execute(httpost, responseHandler);
				strResult = strResult.trim();
				Logg.d("[Post]openClient : " + strResult);
				break;
			} catch (ClientProtocolException e) {
				setLastError(EventCode.E_INTERNAL_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (SocketException e) {
				setLastError(EventCode.E_INTERNAL_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (IOException e) {
				setLastError(EventCode.E_INTERNAL_ERROR);
				Logg.e(e);
				e.printStackTrace();
			} catch (Exception e) {
				setLastError(EventCode.E_INTERNAL_ERROR);
				Logg.e(e);
				e.printStackTrace();
			}
		}

		return strResult;
	}
}
