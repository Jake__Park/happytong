package kr.wisestone.happytong.data.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;


public class NetworkStatus
{
    public static final int NETWORK_DISCONNECTED = 0;
    public static final int NETWORK_CONNECTED    = 1;
    public static final int NETWORK_UNKNOWN      = 2;

    protected static int                 m_iNetworkStatus     = NETWORK_UNKNOWN;
    protected static Context             m_ctContext          = null;
    protected static WifiManager         m_wmWifiManager      = null;
    protected static ConnectivityManager m_cmConnectivity     = null;

    public static void setContext(Context context )
    {
        if(m_ctContext == null)
        {
            m_ctContext         = context;
            m_wmWifiManager     = (WifiManager)m_ctContext.getSystemService( Context.WIFI_SERVICE );
            m_cmConnectivity    = (ConnectivityManager)m_ctContext.getSystemService( Context.CONNECTIVITY_SERVICE );
        }
    }

    public static void setStatus(int iStatus)
    {
        m_iNetworkStatus = iStatus;
    }

    public static int getStatus()
    {
        return m_iNetworkStatus;
    }

    public static boolean isConnected()
    {
        return true;
    }

    public static boolean isConnectedWifi()
    {
        if(m_wmWifiManager == null) return false;

        WifiInfo wInfo = m_wmWifiManager.getConnectionInfo();

        if ( m_wmWifiManager.isWifiEnabled() && wInfo.getSSID() != null
                && m_wmWifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED )
            return true;
        else
            return false;
    }

    public static boolean isConnected3G()
    {
        if(m_cmConnectivity == null) return false;

        NetworkInfo netinfo3g = m_cmConnectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ( netinfo3g.isConnected() ) return true;
        else return false;
    }
    
    public static boolean isConnectedLTE()
    {
    	if(m_cmConnectivity == null) return false;

        NetworkInfo netinfoLTE = m_cmConnectivity.getNetworkInfo(ConnectivityManager.TYPE_WIMAX);
        if ( netinfoLTE != null && netinfoLTE.isConnected() ) return true;
        else return false;
    	
    }

    public static boolean _isConnected()
    {
        if(m_ctContext == null) return false;

        if ( isConnectedWifi() ) return true;
        else if ( isConnected3G() ) return true;
        else if ( isConnectedLTE() ) return true;
        else return false;
    }
}
