package kr.wisestone.happytong.data.wrapper;

public class CallBackParamWrapper{
	
    /* 회원 정보 */
    public static final int CODE_PROFILE_ADD        = 100; //회원 정보 추가
    public static final int CODE_PROFILE_EDIT       = 101; //회원 정보 수정
    public static final int CODE_PROFILE_RESIGN     = 102; //회원 탈퇴
    public static final int CODE_PROFILE_GET     	= 103; //회원 조회
    public static final int CODE_PROFILE_PHOTO_EDIT	= 104; //회원 사진 수정
    public static final int CODE_PROFILE_PHOTO_DEL	= 105; //회원 사진 삭제
    public static final int CODE_PROFILE_FACEBOOK_AD= 106; //페이스북 광고여부 설정
    
    /* 상품 정보 */
    public static final int CODE_ITEMS_GET_POP      = 200; //인기 상품 요청
    public static final int CODE_ITEMS_GET_STAMP    = 201; //우표 상품 요청
    public static final int CODE_ITEMS_GET_NORMAL    = 202; //일반 상품 더보기 요청

    /* 구매 관련 */
    public static final int CODE_ORDER_STAMP_REMAIN 	= 300; // 남은 우표(유료구매) 수 조회
    public static final int CODE_ORDER_COUPON_PURCHASED = 301; // 쿠폰 이미지 구매목록 조회

    /* 친구 */
    public static final int CODE_FRIEND_FRIEND_SYNC 	= 400; // 전체 친구 목록 요청(동기화, 차단 친구목록 포함)
    public static final int CODE_FRIEND_FRIEND_BLOCK 	= 401; //친구 차단
    public static final int CODE_FRIEND_FRIEND_UNBLOCK 	= 402; //친구 차단 해제하기
    
    /* 쿠폰 */
    private static int COUPON_SEQ = 500;
    public static final int CODE_COUPON_RECEIVED    	= COUPON_SEQ++; 			// 받은 쿠폰 목록
	public static final int CODE_COUPON_SENT 			= COUPON_SEQ++; 			// 보낸 쿠폰 목록
    public static final int CODE_COUPON_USED_EXPIRED	= COUPON_SEQ++;  			// 사용 쿠폰 목록
    public static final int CODE_COUPON_COMPANY			= COUPON_SEQ++;  			// 업체 쿠폰 목록
    public static final int CODE_COUPON_DELETE			= COUPON_SEQ++; 			// 쿠폰 삭제
    public static final int CODE_COMPANY_COUPON_DELETE	= COUPON_SEQ++; 			// 업체 쿠폰 삭제
    public static final int CODE_COUPON_USING_REQUEST	= COUPON_SEQ++; 			// 쿠폰 사용 요청
    public static final int CODE_COUPON_PERMIT			= COUPON_SEQ++; 			// 쿠폰 허가 (허가,거절)
    public static final int CODE_COUPON_SENDING			= COUPON_SEQ++; 			// 보내기

    public static final int CODE_COMPANY_SEND_LIST 		= COUPON_SEQ++;				// 업체쿠폰 발송내역
    public static final int CODE_DEL_COMPANY_COUPON		= COUPON_SEQ++;				// 업체쿠폰 발송내역 삭제
    public static final int CODE_COMPANY_COUPON_SEND	= COUPON_SEQ++;				// 업체쿠폰 발송

    /* 인증 관련 */
    public static final int CODE_AUTH_LOGIN         = 600; //로그인
    public static final int CODE_AUTH_EMAIL         = 601; //이메일 인증번호 전송
    public static final int CODE_AUTH_PROVISION         = 602; //이용약관 내용 조회
    public static final int CODE_AUTH_PERSONAL         = 603; //개인정보 취급 방침 조회
//    public static final int CODE_AUTH_LOGIN         = 602; //로그인
    public static final int CODE_AUTH_CHECK_ID         = 604; // ID 중복 검사
    public static final int CODE_AUTH_ID_DEVICE_CHECK	= 605;	//  사용자  존재  여부  확인
    
    public static final int CODE_COMPANY_PHOTO_EDIT = 701; //업체 사진 수정
    public static final int CODE_COMPANY_PHOTO_DEL  = 702; //업체 사진 삭제
    
}

