package kr.wisestone.happytong.data.wrapper;

import kr.wisestone.happytong.HappyTongConfig;

public class URLWrapper {

	// 개발 서버 URL
	private static final String	BASE_URL_DEV	= "http://web.wisestone.kr:8090/CuFunU";
	
	// 운영 서버 URL
	private static final String	BASE_URL_REAL	= "http://newserver.wisestone.kr:12000/CuFunU";

	private static final String	BASE_URL		= HappyTongConfig.serverType == HappyTongConfig.ServerType.REAL ? BASE_URL_REAL : BASE_URL_DEV;

	/**
	 * 회원 정보 추가 URL
	 */
	public static String getProfileAdd() {
		return BASE_URL + "/profile_add";
	}

	/**
	 * 회원 정보 수정 URL
	 */
	public static String getProfileEdit() {
		return BASE_URL + "/profile_edit";
	}

	/**
	 * 회원정보 조회 URL
	 */
	public static String getProfileGet() {
		return BASE_URL + "/profile_get";
	}

	/*
	 * 회원사진 수정 URL
	 */
	public static String getProfilePhotoEdit() {
		return BASE_URL + "/profile_photo_edit";
	}

	/**
	 * 회원사진 삭제 URL
	 */
	public static String getProfilePhotoDel() {
		return BASE_URL + "/profile_photo_delete";
	}

	/**
	 * 회원 탈퇴
	 */
	public static String getProfileResign() {
		return BASE_URL + "/profile_resign";
	}

	/**
	 * 페이스북 광고 여부 설정 페이스북을을 광고하면 서버에 광고여부 설정 (facebookAd : 0-> 1 )
	 */
	public static String getProfileFacebookAd() {
		return BASE_URL + "/profile_facebookAd";
	}

	/**
	 * 로그인 요청
	 */
	public static String getAuthLogin() {
		return BASE_URL + "/auth_login";
	}

	/**
	 * 비밀번호 찾기용 인증번호 전달
	 */
	public static String getAuthEmail() {
		return BASE_URL + "/auth_email";
	}

	/**
	 * 비밀번호 재설정
	 */
	public static String getAuthResetPassword() {
		return BASE_URL + "/auth_reset_password";
	}

	/**
	 * 이용약관 내용 조회
	 */
	public static String getAuthPovision() {
		return BASE_URL + "/auth_provision";
	}

	/**
	 * 개인정보 취급방침 조회
	 */
	public static String getAuthPersonal() {
		return BASE_URL + "/auth_personal";
	}

	/**
	 * Id 중복 검사
	 */
	public static String getAuthCheckId() {
		return BASE_URL + "/auth_check_id";
	}

	/**
	 * 버전 체크
	 */
	public static String getCheckVersionForAndroid() {
		return BASE_URL + "/check_version_for_android";
	}

	/**
	 * 사용자 존재 여부 확인 - 서버에 사용자가 존재하는지 id와 deviceID로 확인한다
	 */
	public static String getAuthIdDeviceCheck() {
		return BASE_URL + "/auth_id_device_check";
	}

	/**
	 * IAP Public key 요청
	 */
	public static String getAuthPublicKey() {
		return BASE_URL + "/auth_get_public_key";
	}

	/**
	 * 인기 상품 요청
	 */
	public static String getItemsGetPop() {
		return BASE_URL + "/items_get_pop";
	}

	/**
	 * 일반 상품 더보기 요청
	 */
	public static String getItemsGetNormal() {
		return BASE_URL + "/items_get_normal";
	}

	/**
	 * 전체 친구 미설치 리스트 요청
	 */
	public static String getFriendSync() {
		return BASE_URL + "/friend_sync";
	}

	/**
	 * 전체 친구 리스트 요청(이름 포함)
	 */
	public static String getFriendWithNameSync() {
		return BASE_URL + "/friendWithName_sync";
	}

	/**
	 * 친구 차단
	 */
	public static String getFriendBlock() {
		return BASE_URL + "/friend_block";
	}

	/**
	 * 친구 차단 해제하기
	 */
	public static String getFriendUnBlock() {
		return BASE_URL + "/friend_unblock";
	}

	/**
	 * 남은 우표(유료구매) 수 조회
	 */
	public static String getOrderStampRemain() {
		// return BASE_URL + "/order_stamp_remain";
		return BASE_URL + "/order_owl_remain";
	}

	/**
	 * 쿠폰이미지 구매목록 조회
	 */
	public static String getOrderCouponPurchased() {
		return BASE_URL + "/order_coupon_purchased";
	}

	/**
	 * 구매 내역 요청 (황금알 구매 내역)
	 */
	public static String getOrderGoldeggPurchaseHistory() {
		return BASE_URL + "/order_goldegg_purchase_history";
	}

	/**
	 * 쿠폰 구매 요청
	 */
	public static String getBuyingRequestCoupon() {
		return BASE_URL + "/buying_request_coupon";
	}

	/**
	 * 부엉이 구매 요청
	 */
	public static String getBuyingRequestOwl() {
		return BASE_URL + "/buying_request_owl";
	}

	/**
	 * 영수증 유효성 검사
	 */
	public static String getCheckSignature() {
		return BASE_URL + "/market_buy_android_validity";
	}

	/**
	 * 받은 쿠폰 목록
	 */
	public static String getCouponReceived() {
		return BASE_URL + "/coupon_received";
	}

	/**
	 * 보낸 쿠폰 목록
	 */
	public static String getCouponSent() {
		return BASE_URL + "/coupon_sent";
	}

	/**
	 * 사용, 만료 쿠폰
	 */
	public static String getCouponUsedExpired() {
		return BASE_URL + "/coupon_used_expired";
	}

	/**
	 * 업체 쿠폰함 조회
	 */
	public static String getCompaniesCouponBox() {
		return BASE_URL + "/getCompaniesCouponBox";
	}

	/**
	 * 업체모드 - 발송 내역 삭제
	 */
	public static String getDelCompaniesSendedCoupon() {
		return BASE_URL + "/delCompaniesSendedCoupon";
	}

	/**
	 * 쿠폰 삭제
	 */
	public static String getCouponDelete() {
		return BASE_URL + "/coupon_delete";
	}

	/**
	 * 업체 쿠폰 삭제
	 */
	public static String getCompanyCouponDelete() {
		return BASE_URL + "/delCompaniesCoupon";
	}

	/**
	 * 쿠폰 사용 요청
	 */
	public static String getCouponUsingRequest() {
		return BASE_URL + "/coupon_using_request";
	}

	/**
	 * 쿠폰 사용 허가
	 */
	public static String getCouponPermit() {
		return BASE_URL + "/coupon_permit";
	}

	/**
	 * 업체 쿠폰 사용 요청하기
	 */
	public static String getCompaniesCouponUse() {
		return BASE_URL + "/companiesCouponUse";
	}

	/**
	 * 쿠본 보내기
	 */
	public static String getCouponSending() {
		return BASE_URL + "/coupon_sending";
	}

	/**
	 * 업체쿠폰 발송 - 업체가 사용자들에게 쿠폰을 발송한다
	 */
	public static String getCompaniesCouponSend() {
		return BASE_URL + "/companiesCouponSend";
	}

	/**
	 * 스템프 보유목록 조회
	 */
	public static String getHasStampInfo() {
		return BASE_URL + "/hasStampInfo";
	}

	/**
	 * 스템프 전체 리스트 요청
	 */
	public static String getAllStampInfoServlet() {
		return BASE_URL + "/allStampInfo";
	}

	/**
	 * 지역 리스트 조회
	 */
	public static String getLocation() {
		return BASE_URL + "/locationInfoList";
	}

	/**
	 * 사용자 포함 지역 리스트 조회
	 */
	public static String getLocationUserCountList() {
		return BASE_URL + "/getLocationUserCountList";
	}

	/**
	 * 관심 지역 수정
	 */
	public static String setLocation() {
		return BASE_URL + "/setLocation";
	}

	/**
	 * 사업자 상품 조회
	 */
	public static String getCompaniesGoods() {
		return BASE_URL + "/getCompaniesGoods";
	}

	/**
	 * 발송내역 조회
	 */
	public static String getCompaniesSendedCouponList() {
		return BASE_URL + "/getCompaniesSendedCouponList";
	}

	/**
	 * 업체프로필 정보 조회
	 */
	public static String getCompaniesProfile() {
		return BASE_URL + "/getCompaniesProfile";
	}

	/**
	 * 상품 구매 내역 전송
	 */
	public static String setCompaniesMarketBuyValidity() {
		return BASE_URL + "/setCompaniesMarketBuyValidity";
	}

	/**
	 * 사업자 등록
	 */
	public static String setCompaniesInfoRegist() {
		return BASE_URL + "/companiesInfoRegist";
	}

	/**
	 * 사업자 정보 수정
	 */
	public static String companiesInfoUpdate() {
		return BASE_URL + "/companiesInfoUpdate";
	}

	/**
	 * 업체 이미지 등록
	 */
	public static String companiesImagePathUpdate() {
		return BASE_URL + "/companiesImagePathUpdate";
	}

	/**
	 * 남은 사업자 전용 쿠폰 갯수
	 */
	public static String companiesItemCount() {
		return BASE_URL + "/companiesItemCount";
	}

	/**
	 * 사업체 프로필 사진 삭제
	 */
	public static String companiesImagePathDelete() {
		return BASE_URL + "/companiesImagePathDelete";
	}
}
