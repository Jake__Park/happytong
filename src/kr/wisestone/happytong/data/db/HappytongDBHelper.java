package kr.wisestone.happytong.data.db;

import java.util.ArrayList;

import kr.wisestone.happytong.data.common.InitialSoundSearch;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.HappytongDB.GroupInfo;
import kr.wisestone.happytong.data.db.HappytongDB.GroupList;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class HappytongDBHelper extends SQLiteOpenHelper {
	private static final String DB_NAME = "Happytong.db";
	private static final int DB_VERSION = 1;
	
	private static final int RET_SUCCESS = 1;
	private static final int RET_FAIL = -1;
	
	private SQLiteDatabase mDB = null; // DB
	
	public HappytongDBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		this.mDB=getWritableDatabase();
		if( mDB==null ) {
			Logg.d("DB open failed:");
		}
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(GroupInfo.CreateGroupInfo());
		db.execSQL(GroupList.CreateGroupList());
		db.execSQL(FriendInfo.CreateFriendInfo());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if( newVersion>oldVersion) {
			db.execSQL("DROP TABLE "+GroupInfo.GROUPINFO_TB_NAME);
			db.execSQL("DROP TABLE "+GroupList.GROUPLIST_TB_NAME);
			db.execSQL("DROP TABLE "+FriendInfo.FRIENDINFO_TB_NAME);
			onCreate(db);
		}
	}
	
	public void deleteAllData() {
		if(mDB != null) {
			mDB.execSQL("DELETE FROM "+GroupInfo.GROUPINFO_TB_NAME);
			mDB.execSQL("DELETE FROM "+GroupList.GROUPLIST_TB_NAME);
			mDB.execSQL("DELETE FROM "+FriendInfo.FRIENDINFO_TB_NAME);
		}
	}
    /***
     * @Brief db table 초기화
     */
    public void initDB() {
        SQLiteDatabase db = getWriteableDB();
        
//        db.execSQL("DROP TABLE "+GroupInfo.GROUPINFO_TB_NAME);
//        db.execSQL("DROP TABLE "+GroupList.GROUPLIST_TB_NAME);
        db.execSQL("DROP TABLE "+FriendInfo.FRIENDINFO_TB_NAME);

        onCreate(db);
    }
    
	public SQLiteDatabase getWriteableDB() {
		if( mDB==null || !mDB.isOpen()) {
			mDB = getWritableDatabase();
		}
		return mDB;
	}
	
	@Override
	public synchronized void close() {
		if( mDB!=null ) {
			mDB.close();
		}
		super.close();
	}
	
	/**
	 * Insert FriendInfo
	 * @param Friend Class Fields
	 */
	public int InsertFriendInfo(ArrayList<FriendInfo> o) {
		ArrayList<FriendInfo> list = (ArrayList<FriendInfo>)o;
		
		for( int i=0; i<list.size(); i++) {
			FriendInfo dbData = list.get(i);	
			int ret = InsertQueryForFriendInfo(dbData.getNameValueList());
			if(ret != 1 )return RET_FAIL;
		}
		return RET_SUCCESS;
	}
	//@param groupid, friendID
	public	int InsertQueryForFriendInfo(ArrayList<NameValuePair> setValue) {
		
		if( setValue.size()<= 1) return RET_FAIL;//fail
		
		SQLiteDatabase db = getWriteableDB();

		StringBuffer buffer = new StringBuffer();
		
		buffer.append("insert into ").append(FriendInfo.FRIENDINFO_TB_NAME).append(" ");
		
		buffer.append("(");
		for(int i=0; i<setValue.size(); i++) {
			buffer.append(setValue.get(i).getName());
			if( i+1<setValue.size() ) {
				buffer.append(",");
			}
		}
		buffer.append(")");
		
		buffer.append(" values (");
		
		for(int i=0; i<setValue.size(); i++) {
			buffer.append("'");
			buffer.append(setValue.get(i).getValue());
			buffer.append("'");
			if( i+1<setValue.size() ) {
				buffer.append(",");
			}
		}
		buffer.append(")");
		
		Logg.d(buffer.toString());	
		db.execSQL(buffer.toString());
		
		return RET_SUCCESS;
	}
	
	/**
	 * Update friendInfo
	 * @param Update 하기 위한 friend 정보
	 * @return RET_SUCCESS : 성공 
	 * 		   RET_FAIL : 실패
	 */
	/*
	public int UpdateFriendInfo(ArrayList<FriendInfo> o, String id) {

		ArrayList<FriendInfo> list = (ArrayList<FriendInfo>)o;
		
		for( int i=0; i<list.size(); i++) {
			FriendInfo dbData = list.get(i);	
			int ret = UpdateQueryForFriendInfo(dbData.getNameValueList(), id);
			if(ret != 1)return RET_FAIL;
		}
		return RET_SUCCESS;
	}
*/	
	
	public int UpdateFriendInfo(FriendInfo info) {
		int ret = UpdateQueryForFriendInfo(info.getNameValueList(), info.GetID());
		return ret;
	}

	public int UpdateQueryForFriendInfo(ArrayList<NameValuePair> setValue, String id )
	{		
		if( setValue.size()<=1) return RET_FAIL;//fail
		
		SQLiteDatabase db = getWriteableDB();

		StringBuffer buffer = new StringBuffer();
		
		buffer.append("update ").append(FriendInfo.FRIENDINFO_TB_NAME).append(" ");
		buffer.append("set ");

		for(int i=0; i<setValue.size(); i++) {
			buffer.append(setValue.get(i).getName());
			buffer.append("=");
			buffer.append("'");//jikim
			buffer.append(setValue.get(i).getValue());
			buffer.append("'");//jikim
			if( i+1<setValue.size() ) {
				buffer.append(",");
			}
		}
		
		buffer.append(" where _id= "+"'"+id+"'");
		
		Logg.d(buffer.toString());
		db.execSQL(buffer.toString());
		return RET_SUCCESS;
	}
	
	/**
	 * not support delete friendinfo
	 */
	public int DeleteFriendInfo(ArrayList<FriendInfo> o/*, String id*/) {
		ArrayList<FriendInfo> list = (ArrayList<FriendInfo>)o;
		
		for( int i=0; i<list.size(); i++) {
			FriendInfo dbData = list.get(i);	
			int ret = DeleteQueryForFriendInfo(dbData.getNameValueList()/*, id*/);
			if(ret != 1)return RET_FAIL;
		}
		return RET_SUCCESS;
		}
	
	public int DeleteQueryForFriendInfo(ArrayList<NameValuePair> data/*, String id*/) {
				
		if( data.size()<=1) return RET_FAIL;
		
		SQLiteDatabase db = getWriteableDB();
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("delete from ").append(FriendInfo.FRIENDINFO_TB_NAME).append(" ");
		
		buffer.append("where").append(" ");
		for( int i=0; i<data.size(); i++) {
			buffer.append(data.get(i).getName()).append("=").append("'").append(data.get(i).getValue()).append("'");
			
			if( i+1<data.size() ) {
				buffer.append(" and ");
			}
		}
			
		Log.d("DeleteFriendInfo",buffer.toString());	
		db.execSQL(buffer.toString());
		return RET_SUCCESS;
	}


	public String GetFriendId(String str){
		SQLiteDatabase db = getWriteableDB();
		String friendID = null;
		Cursor c = db.rawQuery("SELECT "+ FriendInfo.FRIENDINFO_ID+ 
								" FROM " + FriendInfo.FRIENDINFO_TB_NAME 
								+ " WHERE "+ FriendInfo.FRIENDINFO_EMAIL_ID+ " ='"+ str+"'" , null);
		
		if(c!=null && c.getCount()!=0){
			c.moveToFirst();
			friendID = c.getString(1);	
			if(c !=null) c.close();
		}
		Log.d("GetFriendId",friendID);	
		return friendID;
	}	
		
	
	
	
//--------------------------------------------------------------------------------------------------------	
	/**
	 * @Brief insert Groupinfo 
	 * @param1 GroupName
	 * return -1 : 동일한 그룹명이 존재
	 **/	
		
	public  int InsertGroupInfo(String groupname) {
		Cursor c = null;
		String sql = "SELECT " + GroupInfo.GROUPNAME  
	            + " FROM " +GroupInfo.GROUPINFO_TB_NAME
	            + " WHERE " + GroupInfo.GROUPNAME + " = '" + groupname + "'";

		try {
			//Cursor c = mDB.query(GROUPINFO_TB_NAME, null, GROUPNAME + " = '" + groupname + "'", null,null, null, null, null);
			c = mDB.rawQuery(sql, null);
			
			if (c.getCount() == 0 || c == null){
				ContentValues value = new ContentValues();
				value.put(GroupInfo.GROUPNAME, groupname);
				mDB.insert(GroupInfo.GROUPINFO_TB_NAME, null, value);
			}
			else {
				c.close();
				return RET_FAIL;//fail duplicate groupname
			}
			c.close();
			return 1;//success
		} catch (Exception e) {
			e.printStackTrace();
			return RET_FAIL;
		}
	}	
	
	/**
	 * update groupname
	 * @param updategroupname
	 * @param groupid
	 */
/*
	public void UpdateGroupInfo(ArrayList<GroupInfo> o, String id) {
		ArrayList<GroupInfo> list = (ArrayList<GroupInfo>)o;
		
		for( int i=0; i<list.size(); i++) {
			GroupInfo dbData = list.get(i);	
			UpdateQueryForGroupInfo(dbData.getNameValueList(), id);
		}
	}
	*/
	public int UpdateGroupInfo(GroupInfo info) {
		int ret = UpdateQueryForGroupInfo(info.getNameValueList(), info.GetGroupID());
		return ret;
	}
	public int UpdateQueryForGroupInfo(ArrayList<NameValuePair> setValue, String id )
	{		
		if( setValue.size()<=0) return RET_FAIL;
		
		SQLiteDatabase db = getWriteableDB();

		StringBuffer buffer = new StringBuffer();
		
		buffer.append("update ").append(GroupInfo.GROUPINFO_TB_NAME).append(" ");
		buffer.append("set ");

		for(int i=0; i<setValue.size(); i++) {
			buffer.append(setValue.get(i).getName());
			buffer.append("=");
			buffer.append("'");
			buffer.append(setValue.get(i).getValue());
			buffer.append("'");
			if( i+1<setValue.size() ) {
				buffer.append(",");
			}
		}
		
		buffer.append(" where _id="+"'"+id+"'");
		
		Logg.d(buffer.toString());	
		db.execSQL(buffer.toString());
		return RET_SUCCESS;
	}
	
		
//delete 	
//param GroupId, friendId
//ex)delete from  GroupList where GroupListId=1 and PersonInfoId=3
//ex)delete from GroupInfo where GroupName=company
/*	
	public void DeleteGroupInfo(ArrayList<GroupInfo> o) {
		ArrayList<GroupInfo> list = (ArrayList<GroupInfo>)o;
		
		for( int i=0; i<list.size(); i++) {
			GroupInfo dbData = list.get(i);	
			DeleteQueryGroupInfo(dbData.getNameValueList());
		}
	}
*/
	public int DeleteGroupInfo(GroupInfo info) {
		int ret = DeleteQueryGroupInfo(info.getNameValueList());
		return ret;
		}
	public int DeleteQueryGroupInfo(ArrayList<NameValuePair> data) {
		if( data.size()<=0) return RET_FAIL;
		
		SQLiteDatabase db = getWriteableDB();

		StringBuffer buffer = new StringBuffer();
		
		buffer.append("delete from ").append(GroupInfo.GROUPINFO_TB_NAME).append(" ");
		
		buffer.append("where").append(" ");
		for( int i=0; i<data.size(); i++) {
			buffer.append(data.get(i).getName()).append("=").append("'").append(data.get(i).getValue()).append("'");
			if( i+1<data.size() ) {
				buffer.append(" and ");
			}
		}
		
		Logg.d(buffer.toString());	
		db.execSQL(buffer.toString());
		return RET_SUCCESS;
	}
	
//--------------------------------------------------------------------------------------------------------	
		
	/**
	 * @Brief insert GroupList
	 * @param1 GroupID
	 * @param2 PersonID
	 * return -1 : 동일한 그룹명이 존재
	 **/
	public int InsertGroupList(ArrayList<FriendInfo> o, String groupInfoId) {
		ArrayList<FriendInfo> list = (ArrayList<FriendInfo>)o;
		
		if(groupInfoId==null||groupInfoId.compareTo("null")==0)return RET_FAIL;
			
		for( int i=0; i<list.size(); i++) {
			ArrayList<NameValuePair> list1 = new ArrayList<NameValuePair>();
			FriendInfo dbData = list.get(i);	
			String friendId = dbData.GetEmail();
			
			list1.add(new BasicNameValuePair(GroupList.GROUP_INFO_ID, groupInfoId));
			list1.add(new BasicNameValuePair(GroupList.FRIEND_INFO_ID, friendId));
			InsertQueryGroupList(list1);
			list1.clear();
		}
		return RET_SUCCESS;
	}

	public	int InsertQueryGroupList(ArrayList<NameValuePair> setValue) {

		if( setValue.size()<=1) return RET_FAIL;
		
		SQLiteDatabase db = getWriteableDB();

		StringBuffer buffer = new StringBuffer();
		
		buffer.append("insert into ").append(GroupList.GROUPLIST_TB_NAME).append(" ");
		
		buffer.append("(");
		for(int i=0; i<setValue.size(); i++) {
			buffer.append(setValue.get(i).getName());
			if( i+1<setValue.size() ) {
				buffer.append(",");
			}
		}
		buffer.append(")");
		
		buffer.append(" values (");
		
		for(int i=0; i<setValue.size(); i++) {
			buffer.append("'");
			buffer.append(setValue.get(i).getValue());
			buffer.append("'");
			if( i+1<setValue.size() ) {
				buffer.append(",");
			}
		}
		buffer.append(")");
		
		Logg.d(buffer.toString());
		db.execSQL(buffer.toString());
		return RET_SUCCESS;
	}
	

	/**
	 * 그룹리스트에서 친구를 삭제할때
	 * @param 삭제할 친구 정보
	 */

	public int DeleteGroupList(ArrayList<FriendInfo> o, String groupInfoId) {
		ArrayList<FriendInfo> list = (ArrayList<FriendInfo>)o;
		
		if(groupInfoId==null||groupInfoId.compareTo("null")==0)return RET_FAIL;
			
		for( int i=0; i<list.size(); i++) {
			ArrayList<NameValuePair> list1 = new ArrayList<NameValuePair>();
			FriendInfo dbData = list.get(i);	
			String friendId = dbData.GetEmail();
			
			list1.add(new BasicNameValuePair(GroupList.GROUP_INFO_ID, groupInfoId));
			list1.add(new BasicNameValuePair(GroupList.FRIEND_INFO_ID, friendId));
			DeleteQueryForGroupList(list1);
			list1.clear();
		}
		return RET_SUCCESS;
	}
	
	
	public int  DeleteQueryForGroupList(ArrayList<NameValuePair> data) {
		if( data.size()<=1) return RET_FAIL;
		
		SQLiteDatabase db = getWriteableDB();
	
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("delete from ").append(GroupList.GROUPLIST_TB_NAME).append(" ");
		
		buffer.append("where").append(" ");
		for( int i=0; i<data.size(); i++) {
			buffer.append(data.get(i).getName()).append("=").append("'").append(data.get(i).getValue()).append("'");
			if( i+1<data.size() ) {
				buffer.append(" and ");
			}
		}
		
		Logg.d(buffer.toString());	
		db.execSQL(buffer.toString());
		return RET_SUCCESS;
	}
	
	/**
	 * Get all group list
	 * @return ArrayList<GroupInfo>
	 */
	public ArrayList<GroupInfo> GetGroupInfoList(){
		ArrayList<GroupInfo> list = null;
		
		SQLiteDatabase db = getWriteableDB();
		
		Cursor c = db.rawQuery("SELECT * FROM " + GroupInfo.GROUPINFO_TB_NAME, null);
		
		if(c!=null && c.getCount()!=0){
		    list = new ArrayList<GroupInfo>();
		    
			c.moveToFirst();
			
			do{
				GroupInfo groupList = new GroupInfo(c.getString(0),c.getString(1));
				list.add(groupList);
			}while(c.moveToNext());
			
			if(c !=null) c.close();
		}
		return list;
	}
	
	public Cursor getGroupInfoId(String groupname) {
		Cursor c = null;
		Log.d("getGroupInfoId-start",groupname);	
		SQLiteDatabase db = getWriteableDB();
		
		c = db.rawQuery("SELECT "+ GroupInfo.GROUPID + 
					 	" FROM "  + GroupInfo.GROUPINFO_TB_NAME + 
					 	" where "+ GroupInfo.GROUPNAME + " = '" + groupname + "'" ,null);
		
		//c = db.rawQuery("SELECT _id FROM GroupInfo where GroupName = 'company'", null);
		return (c==null||c.getCount()==0)?null:c;
	}
	
	public Cursor GetFriendInfoIdByGroupList(String groupListId) {
		Cursor c = null;

		SQLiteDatabase db = getWriteableDB();

		//SELECT PERSON_INFO_ID from GROUPLIST_TB_NAME where GROUP_LIST_ID = groupListId
		c = db.rawQuery("SELECT "+ GroupList.FRIEND_INFO_ID + 
						" FROM " + GroupList.GROUPLIST_TB_NAME + 
						" where "+ GroupList.GROUP_INFO_ID + " = '" + groupListId + "'",null);

		return (c==null||c.getCount()==0)?null:c;
	}
	
	
/**
 * Get friendinfo by group
 * @param groupname
 * @return
 */
//	public ArrayList<FriendInfo> makeFriendInfoList (ArrayList<Cursor> selectList){
//		ArrayList<FriendInfo> list = new ArrayList<FriendInfo>();
//		Cursor c = null;
//
//        char initialSound = 0;
//		
//		for(int i=0; i<selectList.size(); i++ ) {
//		    c = selectList.get(i);
//			
//			if(c !=null && c.getCount()!=0){
//				c.moveToFirst();
//
//				do{
//					FriendInfo friendInfo = new FriendInfo();
//					
//					friendInfo.SetID((c.getString(0)!=null&&c.getString(0).compareTo("null")==0)?null:c.getString(0));
//					friendInfo.SetEmail((c.getString(1)!=null&&c.getString(1).compareTo("null")==0)?null:c.getString(1));
//					friendInfo.SetName((c.getString(2)!=null&&c.getString(2).compareTo("null")==0)?null:c.getString(2));
//					friendInfo.SetPhoneNumber((c.getString(3)!=null&&c.getString(3).compareTo("null")==0)?null:c.getString(3));
//					friendInfo.SetNickName((c.getString(4)!=null&&c.getString(4).compareTo("null")==0)?null:c.getString(4));
//					friendInfo.SetUserImgUrl((c.getString(5)!=null&&c.getString(5).compareTo("null")==0)?null:c.getString(5));
//					friendInfo.Setblock((c.getString(6)!=null&&c.getString(6).compareTo("null")==0)?null:c.getString(6));
//					friendInfo.SetIsName((c.getString(7)!=null&&c.getString(7).compareTo("null")==0)?null:c.getString(7));
//					
//	                String DisplayName = friendInfo.GetName();
//	                
//	                char initial = InitialSoundSearch.getInitialSound(DisplayName.charAt(0));
//	                friendInfo.SetChosung(initial);
//	            
//	                if( initialSound==0 || initialSound!=initial) {
//	                    initialSound = initial;
//	                    friendInfo.SetIsFirst(true);
//	                }
//	                
//					list.add(friendInfo);
//				}while( c.moveToNext() );
//			}
//		}
//		
//		if( c!=null ) c.close();
//			
//		return list;
//	}

	/**
	 * 그룹별 친구 리스트 가져오기
	 * @param groupname(ex "company")
	 * @return ArrayList<FriendInfo> 
	 */
	public ArrayList<FriendInfo> GetFriendInfoByGroup(String groupId, String search){
		ArrayList<FriendInfo> list = new ArrayList<FriendInfo>();
		Cursor c;
        char initialSound = 0;
	
		SQLiteDatabase db = getWriteableDB();

		
		//SELECT * from FriendInfo 
		c = db.rawQuery("SELECT *"+ " FROM " + FriendInfo.FRIENDINFO_TB_NAME + 
						" where "+"(" +"FriendInfo."+FriendInfo.FRIENDINFO_EMAIL_ID+
						" IN (SELECT "+"GroupList." +GroupList.FRIEND_INFO_ID +
						" FROM " + GroupList.GROUPLIST_TB_NAME +
						" where "+"GroupList."+GroupList.GROUP_INFO_ID + " = '"+groupId +"') "+
						" AND ("+"FriendInfo."+FriendInfo.BLOCKSTATE + "= '0' ))" +
						" order by "+FriendInfo.NAME + " COLLATE LOCALIZED ASC",null);
		
		if( c!=null&&c.getCount()!=0) {
			c.moveToFirst();
			do{
				FriendInfo friendInfo = new FriendInfo();	
				
				friendInfo.SetID((c.getString(0)!=null&&c.getString(0).compareTo("null")==0)?null:c.getString(0));
				friendInfo.SetEmail((c.getString(1)!=null&&c.getString(1).compareTo("null")==0)?null:c.getString(1));
				friendInfo.SetName((c.getString(2)!=null&&c.getString(2).compareTo("null")==0)?null:c.getString(2));
				friendInfo.SetPhoneNumber((c.getString(3)!=null&&c.getString(3).compareTo("null")==0)?null:c.getString(3));
				friendInfo.SetNickName((c.getString(4)!=null&&c.getString(4).compareTo("null")==0)?null:c.getString(4));
				friendInfo.SetUserImgUrl((c.getString(5)!=null&&c.getString(5).compareTo("null")==0)?null:c.getString(5));
				friendInfo.Setblock((c.getString(6)!=null&&c.getString(6).compareTo("null")==0)?null:c.getString(6));
				friendInfo.SetIsName((c.getString(7)!=null&&c.getString(7).compareTo("null")==0)?null:c.getString(7));
				friendInfo.SetIsInvitation((c.getString(8)!=null&&c.getString(8).compareTo("null")==0)?null:c.getString(8));  //mIsInvitation
				
				String DisplayName = friendInfo.GetName();
				
                if( search!=null && search.length()!=0){
                    if( InitialSoundSearch.matchString(DisplayName, search)==false) {
                        continue;
                    }
                }
                
                char initial = InitialSoundSearch.getInitialSound(DisplayName.charAt(0));
                friendInfo.SetChosung(initial);
            
                if( initialSound==0 || initialSound!=initial) {
                    initialSound = initial;
                    friendInfo.SetIsFirst(true);
                }
				
				list.add(friendInfo);
			}while(c.moveToNext());
		}
		
		if( c!=null ) c.close();
		
		return list;	
	}

	/**
	 * 선택하지 않은 그룹의 친구 리스트 가져오기
	 * @param groupname(ex "company")
	 * @return ArrayList<FriendInfo> 
	 */
	public ArrayList<FriendInfo> getFriendInfoUnSeleGroup(String groupId, String search){
		ArrayList<FriendInfo> list = new ArrayList<FriendInfo>();
		Cursor c;
        char initialSound = 0;
	
		SQLiteDatabase db = getWriteableDB();
		
		//SELECT * FROM FriendInfo where (FriendInfo._id NOT IN 
		//(SELECT GroupList.FriendInfoId FROM GroupList where GroupList.GroupInfoId = '2') AND (FriendInfo.bBlock= '0' ))
		c = db.rawQuery("SELECT *"+ " FROM " + FriendInfo.FRIENDINFO_TB_NAME + 
						" where "+"(" +"FriendInfo."+FriendInfo.FRIENDINFO_EMAIL_ID+
						" NOT IN (SELECT "+"GroupList." +GroupList.FRIEND_INFO_ID +
						" FROM " + GroupList.GROUPLIST_TB_NAME +
						" where "+"GroupList."+GroupList.GROUP_INFO_ID + " = '"+groupId +"') "+
						" AND ("+"FriendInfo."+FriendInfo.BLOCKSTATE + "= '0'))"+
						" order by "+ FriendInfo.NAME + " COLLATE LOCALIZED ASC",null);
				
		if( c!=null&&c.getCount()!=0) {
			c.moveToFirst();
			do{
				FriendInfo friendInfo = new FriendInfo();
				
				friendInfo.SetID((c.getString(0)!=null&&c.getString(0).compareTo("null")==0)?null:c.getString(0));
				friendInfo.SetEmail((c.getString(1)!=null&&c.getString(1).compareTo("null")==0)?null:c.getString(1));
				friendInfo.SetName((c.getString(2)!=null&&c.getString(2).compareTo("null")==0)?null:c.getString(2));
				friendInfo.SetPhoneNumber((c.getString(3)!=null&&c.getString(3).compareTo("null")==0)?null:c.getString(3));
				friendInfo.SetNickName((c.getString(4)!=null&&c.getString(4).compareTo("null")==0)?null:c.getString(4));
				friendInfo.SetUserImgUrl((c.getString(5)!=null&&c.getString(5).compareTo("null")==0)?null:c.getString(5));
				friendInfo.Setblock((c.getString(6)!=null&&c.getString(6).compareTo("null")==0)?null:c.getString(6));
				friendInfo.SetIsName((c.getString(7)!=null&&c.getString(7).compareTo("null")==0)?null:c.getString(7));
				friendInfo.SetIsInvitation((c.getString(8)!=null&&c.getString(8).compareTo("null")==0)?null:c.getString(8));  //mIsInvitation
				
				String DisplayName = friendInfo.GetName();
				
                if( search!=null && search.length()!=0){
                    if( InitialSoundSearch.matchString(DisplayName, search)==false) {
                        continue;
                    }
                }
                
                char initial = InitialSoundSearch.getInitialSound(DisplayName.charAt(0));
                friendInfo.SetChosung(initial);
            
                if( initialSound==0 || initialSound!=initial) {
                    initialSound = initial;
                    friendInfo.SetIsFirst(true);
                }
				list.add(friendInfo);
			}while(c.moveToNext());
		}
		
		if( c!=null ) c.close();
		
		return list;	
	}

//	public Cursor selectGroup(ArrayList<NameValuePair> condition) {
//		ArrayList<FriendInfo> list = new ArrayList<FriendInfo>();
//		Cursor retC = null;
//			
//		retC  = SelectQuery( new String[] {	FriendInfo.FRIENDINFO_TB_NAME+"."+FriendInfo.FRIENDINFO_ID, 
//											FriendInfo.FRIENDINFO_TB_NAME+"."+FriendInfo.FRIENDINFO_EMAIL_ID,
//											FriendInfo.FRIENDINFO_TB_NAME+"."+FriendInfo.NAME,
//											FriendInfo.FRIENDINFO_TB_NAME+"."+FriendInfo.PHONENUMBER,
//											FriendInfo.FRIENDINFO_TB_NAME+"."+FriendInfo.NICK_NAME, 
//											FriendInfo.FRIENDINFO_TB_NAME+"."+FriendInfo.USER_IMG_URL,
//											FriendInfo.FRIENDINFO_TB_NAME+"."+FriendInfo.BLOCKSTATE},
//											FriendInfo.FRIENDINFO_TB_NAME, 
//											condition,
//											FriendInfo.FRIENDINFO_TB_NAME+"."+FriendInfo.NAME, true);
//		
//		return (retC==null||retC.getCount()==0)?null:retC;
//	}
	

	
	
	/***
	 * @Breif Select query
	 * @param column String[]
	 * @param from String
	 * @param conditionList ArrayList<NameValuePair>
	 * @param orderBy String
	 * @param asc Boolean
	 * @return Cursor
	 */
	public Cursor SelectQuery(String[] column, String from,ArrayList<NameValuePair> conditionList, String orderBy, boolean asc) {
		
		SQLiteDatabase db = getWriteableDB();

		StringBuffer buffer = new StringBuffer();
		
		buffer.append("select ");
		if( column!=null && column.length!=0 ) {
			for(int i=0; i<column.length; i++) {
				buffer.append(column[i]);
				if( (i+1)<column.length ) {
					buffer.append(",");
				} else {
					buffer.append(" ");
				}
			}
		} else {
			buffer.append("* ");
		}
		
		buffer.append("from").append(" ").append(from).append(" ");
		
		if( conditionList!=null && conditionList.size()>0) {
			buffer.append("where").append(" ");
			for( int i=0; i<conditionList.size(); i++) {
				buffer.append(conditionList.get(i).getName()).append("=").append("'").append(conditionList.get(i).getValue()).append("'");
				if( i+1<conditionList.size() ) {
					buffer.append(" or ");
				}
			}
		}
		
		if( orderBy!=null ) {
			buffer.append(" order by ").append(orderBy).append(" COLLATE LOCALIZED ").append((asc)?"ASC":"DESC");
		}
		
		Logg.d(buffer.toString());		
		return db.rawQuery(buffer.toString(),null);	
	}

//	public TransferObject GetAllDataByFriendInfo( ){
//		TransferObject 			transfer = TransferObject.getInstance();
//		Cursor c;
//		
//		SQLiteDatabase db = getWriteableDB();
//		
//		transfer.clean();
//		//SELECT * from FriendInfo 
//		c = db.rawQuery("SELECT *"+ " FROM " + FriendInfo.FRIENDINFO_TB_NAME ,null);
//
//		// FriendInfo
//		if( c!=null&&c.getCount()!=0) {
//			c.moveToFirst();
//			do{
//				transfer.add(new BasicNameValuePack(FriendInfo.FRIENDINFO_TB_NAME, (Object)new FriendInfo((c.getString(0)!=null&&c.getString(0).compareTo("null")==0)?null:c.getString(0),  //_id
//																							  (c.getString(1)!=null&&c.getString(1).compareTo("null")==0)?null:c.getString(1),  //email_id
//																							  (c.getString(2)!=null&&c.getString(2).compareTo("null")==0)?null:c.getString(2),  // Name
//																							  (c.getString(3)!=null&&c.getString(3).compareTo("null")==0)?null:c.getString(3),  // PhoneNumber
//																							  (c.getString(4)!=null&&c.getString(4).compareTo("null")==0)?null:c.getString(4),  // NickName
//																							  (c.getString(5)!=null&&c.getString(5).compareTo("null")==0)?null:c.getString(5),  // UserImgUrl 
//																							  (c.getString(6)!=null&&c.getString(6).compareTo("null")==0)?null:c.getString(6), // bBlock
//																							  (c.getString(7)!=null&&c.getString(7).compareTo("null")==0)?null:c.getString(7)//isName
//																			 				)));
//			}while(c.moveToNext());
//		}
//		
//		if( c!=null ) c.close();
//		
//		return transfer;
//		
//	}
	
/**
 * 모든 친구 정보 얻기. (차단,미차단 모두 포함)
 * @param 
 * @return ArrayList<FriendInfo>
 */
	public ArrayList<FriendInfo> GetAllDataArrByFriendInfo( String search){
		ArrayList<FriendInfo> list = new ArrayList<FriendInfo>();
		Cursor c;
	
		SQLiteDatabase db = getWriteableDB();
		
        char initialSound = 0;
		
		//SELECT * from FriendInfo 
		c = db.rawQuery("SELECT *"+ " FROM " + FriendInfo.FRIENDINFO_TB_NAME +
						" order by "+FriendInfo.NAME + " COLLATE LOCALIZED ASC",null);
		if( c!=null&&c.getCount()!=0) {
			c.moveToFirst();
			do{
		        FriendInfo FriendInfo = new FriendInfo();

				FriendInfo.SetID((c.getString(0)!=null&&c.getString(0).compareTo("null")==0)?null:c.getString(0));
				FriendInfo.SetEmail((c.getString(1)!=null&&c.getString(1).compareTo("null")==0)?null:c.getString(1));
				FriendInfo.SetName((c.getString(2)!=null&&c.getString(2).compareTo("null")==0)?null:c.getString(2));
				FriendInfo.SetPhoneNumber((c.getString(3)!=null&&c.getString(3).compareTo("null")==0)?null:c.getString(3));
				FriendInfo.SetNickName((c.getString(4)!=null&&c.getString(4).compareTo("null")==0)?null:c.getString(4));
				FriendInfo.SetUserImgUrl((c.getString(5)!=null&&c.getString(5).compareTo("null")==0)?null:c.getString(5));
				FriendInfo.Setblock((c.getString(6)!=null&&c.getString(6).compareTo("null")==0)?null:c.getString(6));
				FriendInfo.SetIsName((c.getString(7)!=null&&c.getString(7).compareTo("null")==0)?null:c.getString(7));
				FriendInfo.SetIsInvitation((c.getString(8)!=null&&c.getString(8).compareTo("null")==0)?null:c.getString(8));  //mIsInvitation
				
                String DisplayName = FriendInfo.GetName();
                
                if( search!=null && search.length()!=0){
                    if( InitialSoundSearch.matchString(DisplayName, search)==false) {
                        continue;
                    }
                }
                
                char initial = InitialSoundSearch.getInitialSound(DisplayName.charAt(0));
                FriendInfo.SetChosung(initial);
            
                if( initialSound==0 || initialSound!=initial) {
                    initialSound = initial;
                    FriendInfo.SetIsFirst(true);
                }

				list.add(FriendInfo);
		
			}while(c.moveToNext());
		}
		
		if( c!=null ) c.close();
		
		return list;
	}
/**
 * Get FriendInfo by FriendInfo ID
 * @param friendInfoId
 * @return
 */
	public ArrayList<FriendInfo> GetAllDataArrByFriendInfoID(String friendInfoId ){
		ArrayList<FriendInfo> list = new ArrayList<FriendInfo>();
		Cursor c;
	
		SQLiteDatabase db = getWriteableDB();
		FriendInfo FriendInfo = new FriendInfo();
		
		//SELECT * from FriendInfo 
		c = db.rawQuery("SELECT *"+ " FROM " + FriendInfo.FRIENDINFO_TB_NAME + 
						" where "+ FriendInfo.FRIENDINFO_EMAIL_ID + " ='"+ friendInfoId+"'",null);
		
		if( c!=null&&c.getCount()!=0) {
			c.moveToFirst();
			do{
				FriendInfo.SetID((c.getString(0)!=null&&c.getString(0).compareTo("null")==0)?null:c.getString(0));
				FriendInfo.SetEmail((c.getString(1)!=null&&c.getString(1).compareTo("null")==0)?null:c.getString(1));
				FriendInfo.SetName((c.getString(2)!=null&&c.getString(2).compareTo("null")==0)?null:c.getString(2));
				FriendInfo.SetPhoneNumber((c.getString(3)!=null&&c.getString(3).compareTo("null")==0)?null:c.getString(3));
				FriendInfo.SetNickName((c.getString(4)!=null&&c.getString(4).compareTo("null")==0)?null:c.getString(4));
				FriendInfo.SetUserImgUrl((c.getString(5)!=null&&c.getString(5).compareTo("null")==0)?null:c.getString(5));
				FriendInfo.Setblock((c.getString(6)!=null&&c.getString(6).compareTo("null")==0)?null:c.getString(6));
				FriendInfo.SetIsName((c.getString(7)!=null&&c.getString(7).compareTo("null")==0)?null:c.getString(7));
				FriendInfo.SetIsInvitation((c.getString(8)!=null&&c.getString(8).compareTo("null")==0)?null:c.getString(8));  //mIsInvitation
				
				list.add(FriendInfo);
			}while(c.moveToNext());
		}
		
		if( c!=null ) c.close();
		
		return list;
	}
	
/**	
	public TransferObject GetBlockDatabyFriendInfo( ){
		TransferObject 			transfer = TransferObject.getInstance();
		Cursor c;
		
		SQLiteDatabase db = getWriteableDB();
		
		transfer.clean();
		//SELECT BLOCKSTATE from FriendInfo WHERE BLOCKSTATE=1
		c = db.rawQuery("SELECT " +FriendInfo.BLOCKSTATE + " FROM " + FriendInfo.FRIENDINFO_TB_NAME + " WHERE " + FriendInfo.BLOCKSTATE +" = '1'" ,null);

		// FriendInfo
		if( c!=null&&c.getCount()!=0) {
			c.moveToFirst();
			do{
				transfer.add(new BasicNameValuePack(FriendInfo.FRIENDINFO_TB_NAME, (Object)new FriendInfo((c.getString(0)!=null&&c.getString(0).compareTo("null")==0)?null:c.getString(0),  //_id
																							  (c.getString(1)!=null&&c.getString(1).compareTo("null")==0)?null:c.getString(1),  //email_id
																							  (c.getString(2)!=null&&c.getString(2).compareTo("null")==0)?null:c.getString(2),  // Name
																							  (c.getString(3)!=null&&c.getString(3).compareTo("null")==0)?null:c.getString(3),  // PhoneNumber
																							  (c.getString(4)!=null&&c.getString(4).compareTo("null")==0)?null:c.getString(4),  // NickName
																							  (c.getString(5)!=null&&c.getString(5).compareTo("null")==0)?null:c.getString(5),  // UserImgUrl 
																							  (c.getString(6)!=null&&c.getString(6).compareTo("null")==0)?null:c.getString(6) // bBlock
																			 				)));
			}while(c.moveToNext());
		}
		
		if( c!=null ) c.close();
		
		return transfer;
		
	}
**/
	
/*
 * @brief 차단 및 미차단 친구 리스트 얻기
 * @param : "0" : unblock, "1":block
 * @return 차단 및 미차단 친구 리스트
 */
	
	public ArrayList<FriendInfo> GetBlockDatabyFriendInfo(String blockstate , String search){
		ArrayList<FriendInfo> list = new ArrayList<FriendInfo>();
		Cursor c;
	
		SQLiteDatabase db = getWriteableDB();
		
        char initialSound = 0;
		
        //SELECT BLOCKSTATE from FriendInfo WHERE BLOCKSTATE='1'
  		c = db.rawQuery("SELECT " +"*" + 
  				" FROM " + FriendInfo.FRIENDINFO_TB_NAME + 
  				" WHERE " + FriendInfo.BLOCKSTATE +" ='"+blockstate+"'" +
  				" order by "+FriendInfo.NAME + " COLLATE LOCALIZED ASC",null);
  		      		
		if( c!=null&&c.getCount()!=0) {
			c.moveToFirst();
			do{
		        FriendInfo FriendInfo = new FriendInfo();

				FriendInfo.SetID((c.getString(0)!=null&&c.getString(0).compareTo("null")==0)?null:c.getString(0));			  //_id
				FriendInfo.SetEmail((c.getString(1)!=null&&c.getString(1).compareTo("null")==0)?null:c.getString(1));         //email_id      
				FriendInfo.SetName((c.getString(2)!=null&&c.getString(2).compareTo("null")==0)?null:c.getString(2));          // Name         
				FriendInfo.SetPhoneNumber((c.getString(3)!=null&&c.getString(3).compareTo("null")==0)?null:c.getString(3));   // PhoneNumber  
				FriendInfo.SetNickName((c.getString(4)!=null&&c.getString(4).compareTo("null")==0)?null:c.getString(4));      // NickName     
				FriendInfo.SetUserImgUrl((c.getString(5)!=null&&c.getString(5).compareTo("null")==0)?null:c.getString(5));    // UserImgUrl   
				FriendInfo.Setblock((c.getString(6)!=null&&c.getString(6).compareTo("null")==0)?null:c.getString(6));         // bBlock         
				FriendInfo.SetIsName((c.getString(7)!=null&&c.getString(7).compareTo("null")==0)?null:c.getString(7));        //bName                
				FriendInfo.SetIsInvitation((c.getString(8)!=null&&c.getString(8).compareTo("null")==0)?null:c.getString(8));  //mIsInvitation
				
                String DisplayName = FriendInfo.GetName();
                
                if( search!=null && search.length()!=0){
                    if( InitialSoundSearch.matchString(DisplayName, search)==false) {
                        continue;
                    }
                }
                
                char initial = InitialSoundSearch.getInitialSound(DisplayName.charAt(0));
                FriendInfo.SetChosung(initial);
            
                if( initialSound==0 || initialSound!=initial) {
                    initialSound = initial;
                    FriendInfo.SetIsFirst(true);
                }

				list.add(FriendInfo);
		
			}while(c.moveToNext());
		}
		
		if( c!=null ) c.close();
		
		return list;
	}
	
/**
 * 그룹에 속해진 친구 count
 * @param groupId
 * @return 친구 count 수
 */
	public int getGroupFriendCount(String groupId){
		int nCnt = 0;
		SQLiteDatabase db = getWriteableDB();
		
		Cursor cursor = null;
		
		cursor = db.rawQuery("SELECT *"+ " FROM " + FriendInfo.FRIENDINFO_TB_NAME + 
				" where "+"(" +"FriendInfo."+FriendInfo.FRIENDINFO_EMAIL_ID+
				" IN (SELECT "+"GroupList." +GroupList.FRIEND_INFO_ID +
				" FROM " + GroupList.GROUPLIST_TB_NAME +
				" where "+"GroupList."+GroupList.GROUP_INFO_ID + " = '"+groupId +"') "+
				" AND ("+"FriendInfo."+FriendInfo.BLOCKSTATE + "= '0' ))" +
				" order by "+FriendInfo.NAME + " COLLATE LOCALIZED ASC",null);
		
		if( cursor!=null ){
			nCnt = cursor.getCount();
			cursor.close();
		}
		return nCnt;
	}

/*
 * @brief 미차단 친구 카운트
 * @param : void
 * @return count값
 */
	public int getUnBlockFriendCount(){
		int nCnt = 0;
		Cursor c;
	
		SQLiteDatabase db = getWriteableDB();
		
        //SELECT BLOCKSTATE from FriendInfo WHERE BLOCKSTATE=0
  		c = db.rawQuery("SELECT " +FriendInfo.BLOCKSTATE + 
  				" FROM " + FriendInfo.FRIENDINFO_TB_NAME + 
  				" WHERE " + FriendInfo.BLOCKSTATE +" = '0'" ,null);
      		
  		if( c!=null ){
			nCnt = c.getCount();
			c.close();
		}

		return nCnt;
	}
	
/*
 * @brief 초대할 친구 리스트 얻기
 * @param : 0 : 초대할 친구에 해당 없음., 1 :초대할 친구 해당
 * @return 초대할 친구 리스트
 */
	public ArrayList<FriendInfo> getInvitationFriendInfo(String invit, String search ){
		ArrayList<FriendInfo> list = new ArrayList<FriendInfo>();
		Cursor c;
	
		SQLiteDatabase db = getWriteableDB();
		
        char initialSound = 0;
		
        //SELECT * from FriendInfo WHERE IsInvitation='1' order by Name COLLATE LOCALIZED ASC
  		c = db.rawQuery("SELECT " +"*" + 
  				" FROM " + FriendInfo.FRIENDINFO_TB_NAME + 
  				" WHERE " + FriendInfo.ISINVITATION +" ='"+invit+"'" +
  				" order by "+FriendInfo.NAME + " COLLATE LOCALIZED ASC",null);
  		      		
		if( c!=null&&c.getCount()!=0) {
			c.moveToFirst();
			do{
		        FriendInfo FriendInfo = new FriendInfo();
                                                                                                                                                        
				FriendInfo.SetID((c.getString(0)!=null&&c.getString(0).compareTo("null")==0)?null:c.getString(0));                    //_id             
				FriendInfo.SetEmail((c.getString(1)!=null&&c.getString(1).compareTo("null")==0)?null:c.getString(1));                 //email_id        
				FriendInfo.SetName((c.getString(2)!=null&&c.getString(2).compareTo("null")==0)?null:c.getString(2));                  // Name           
				FriendInfo.SetPhoneNumber((c.getString(3)!=null&&c.getString(3).compareTo("null")==0)?null:c.getString(3));           // PhoneNumber    
				FriendInfo.SetNickName((c.getString(4)!=null&&c.getString(4).compareTo("null")==0)?null:c.getString(4));              // NickName       
				FriendInfo.SetUserImgUrl((c.getString(5)!=null&&c.getString(5).compareTo("null")==0)?null:c.getString(5));            // UserImgUrl     
				FriendInfo.Setblock((c.getString(6)!=null&&c.getString(6).compareTo("null")==0)?null:c.getString(6));                 // bBlock         
				FriendInfo.SetIsName((c.getString(7)!=null&&c.getString(7).compareTo("null")==0)?null:c.getString(7));                //bName           
				FriendInfo.SetIsInvitation((c.getString(8)!=null&&c.getString(8).compareTo("null")==0)?null:c.getString(8));          //mIsInvitation   
				                                                                                                                                        
                String DisplayName = FriendInfo.GetName();
                
                if( search!=null && search.length()!=0){
                    if( InitialSoundSearch.matchString(DisplayName, search)==false) {
                        continue;
                    }
                }
                
                char initial = InitialSoundSearch.getInitialSound(DisplayName.charAt(0));
                FriendInfo.SetChosung(initial);
            
                if( initialSound==0 || initialSound!=initial) {
                    initialSound = initial;
                    FriendInfo.SetIsFirst(true);
                }

				list.add(FriendInfo);
		
			}while(c.moveToNext());
		}
		
		if( c!=null ) c.close();
		
		return list;
	}
	
}
