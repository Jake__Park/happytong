package kr.wisestone.happytong.data.db;

import java.io.Serializable;
import java.util.ArrayList;

import kr.wisestone.happytong.util.AESEncryption;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;


public class HappytongDB {
	
	public static class FriendInfo implements Serializable{
        private static final long serialVersionUID = -5956877594356549598L;

        public static final String FRIENDINFO_TB_NAME 	= "FriendInfo";
        public static final int    CHECK_FLAG_DEACTIVE = -1;  // 비 선택 모드
        public static final int    CHECK_FLAG_ACTIVE   =  0;  // 선택 모드
        public static final int    CHECK_FLAG_CHECKED  =  1;  // 체크 활성화
        public static final int    CHECK_FLAG_UNCHECK  =  2;  // 체크 비활성화

		public static final String FRIENDINFO_ID 		= "_id";  ; // email
		public static final String FRIENDINFO_EMAIL_ID 	= "email_id";  ; // email
		public static final String NAME 				= "Name";
		public static final String PHONENUMBER	 		= "PhoneNumber";
		public static final String NICK_NAME 			= "NickName";
		public static final String USER_IMG_URL	 		= "UserImgUrl";
		public static final String BLOCKSTATE 			= "bBlock";
		public static final String ISNAME	 			= "IsName";
		public static final String ISINVITATION			= "IsInvitation";
		
		private String mId 				= null;
		private String mEmailId			= null;
		private String mName			= null;
		private String mPhoneNumber		= null;
		private String mNickName	    = null;
		private String mUserImgUrl		= null;
		private String mblock			= null;
		private char mChosung		    = 0;
		private boolean isFirst			= false;	
        private int    mCheckBox        = -1;  // Chcek box 
        private String mIsName			= null;//0: 이름 없다, 1:이름있다
        private String mIsInvitation	= null;//0: 초대친구해당없음 1:초대친구해당
        
		public FriendInfo(){}
		
		public FriendInfo(String friendId, String emailId, String name, String phonenumber,
				String nickname, String userImgUrl, String bBlock, String isName, String isInvitation) {
			this.mId = friendId;
			this.mEmailId = emailId;
			this.mName = name;
			this.mPhoneNumber = phonenumber;
			this.mNickName = friendId;
			this.mUserImgUrl = userImgUrl;
			this.mblock = bBlock;
			this.mIsName = isName;
			this.mIsInvitation = isInvitation;
		}
		
		public void SetID(String friendId){this.mId = friendId;}
		public void SetEmail(String emailId){this.mEmailId = emailId;}
		public void SetName(String name){this.mName = name;}
		public void SetPhoneNumber(String phonenumber){this.mPhoneNumber = phonenumber;}
		public void SetNickName(String friendId){this.mNickName = friendId;}
		public void SetUserImgUrl(String userImgUrl){this.mUserImgUrl = userImgUrl;}
		public void Setblock(String block){this.mblock = block;}
		public void SetChosung(char Chosung){this.mChosung = Chosung;}
		public void SetIsFirst(boolean bFirst){this.isFirst = bFirst;}
		public void SetIsName(String isName){this.mIsName = isName;}
		public void SetIsInvitation(String isInvitation){this.mIsInvitation = isInvitation;}
		
		public String GetID(){return this.mId;}
		public String GetEmail(){return this.mEmailId ;}
		public String GetName(){return this.mName;}
		public String GetPhoneNumber(){return this.mPhoneNumber ;}
		public String GetNickName(){return this.mNickName ;}
		public String GetUserImgUrl(){return this.mUserImgUrl;}
		public String Getblock(){return this.mblock;}
		public char GetChosung(){return mChosung;}
		public boolean GetIsFirst(){return isFirst;}
		public String GetIsName(){return mIsName;}
		public String GetIsInvitation(){return mIsInvitation;}
		
        public boolean isChecked() {
            return (this.mCheckBox==CHECK_FLAG_CHECKED)?true:false;
        }
        
        public void setCheckBoxFlag(int flag) {
            this.mCheckBox=flag;
        }
        
        public boolean isCheckBoxActive() {
            return (this.mCheckBox!=CHECK_FLAG_DEACTIVE)?true:false;
        }
				
		public static String CreateFriendInfo() {
			return "CREATE TABLE IF NOT EXISTS " + FRIENDINFO_TB_NAME + " (" 
					+"_id INTEGER PRIMARY KEY AUTOINCREMENT," 
					+ FRIENDINFO_EMAIL_ID+" text,"
					+ NAME+" text,"
					+ PHONENUMBER+" text,"
					+ NICK_NAME+" text,"
					+ USER_IMG_URL+" text,"
					+ BLOCKSTATE+" INTEGER,"
					+ ISNAME+" text,"
					+ ISINVITATION+" text);";
		}
		
		public ArrayList<NameValuePair> getNameValueList() {
			ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
			
		//	list.add(new BasicNameValuePair(TABLE,PersonInfo.TABLE_NAME));
			if(mEmailId!=null)			list.add(new BasicNameValuePair(FriendInfo.FRIENDINFO_EMAIL_ID, mEmailId));
			if(mName!=null){ 
				list.add(new BasicNameValuePair(FriendInfo.NAME, mName));
			}
			if(mPhoneNumber!=null) 		list.add(new BasicNameValuePair(FriendInfo.PHONENUMBER, mPhoneNumber));
			if(mNickName!=null)			list.add(new BasicNameValuePair(FriendInfo.NICK_NAME, mNickName));
			if(mUserImgUrl!=null)		list.add(new BasicNameValuePair(FriendInfo.USER_IMG_URL, mUserImgUrl));
			if(mblock!=null)			list.add(new BasicNameValuePair(FriendInfo.BLOCKSTATE, mblock));
			if(mIsName!=null)			list.add(new BasicNameValuePair(FriendInfo.ISNAME, mIsName));
			if(mIsInvitation!=null)		list.add(new BasicNameValuePair(FriendInfo.ISINVITATION, mIsInvitation));
			
			return list;
		}
		
	}
		
	public static class GroupInfo {
        public static final String GROUPINFO_TB_NAME 	= "GroupInfo";
		
		public static final String GROUPID   		= "_id";
		public static final String GROUPNAME 		= "GroupName"; 
		
		private String 	mGroupName	= null;
		private String 	mId      	= null;
		
		public GroupInfo(){}
		
		public GroupInfo(String id, String groupname) {
			this.mId = id;
			this.mGroupName = groupname;
		}
		
		public void SetGroupID(String id){this.mId = id;}
		public void SetGroupName(String groupname){this.mGroupName = groupname;}
		
		public String GetGroupID(){return this.mId;}
		public String GetGroupName(){return this.mGroupName ;}
		
		public static String CreateGroupInfo() {
			return "CREATE TABLE IF NOT EXISTS " + GROUPINFO_TB_NAME + " (" 
					+"_id INTEGER PRIMARY KEY AUTOINCREMENT,"  
					+ GROUPNAME+" text);";
		}
		
		public ArrayList<NameValuePair> getNameValueList() {
			ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
			
			if(mGroupName!=null)		list.add(new BasicNameValuePair(GroupInfo.GROUPNAME,mGroupName));
			
			return list;
		}
		
	}
	
	public static class GroupList{
		public static final String GROUPLIST_TB_NAME 	= "GroupList";	
		
		public static final String GROUP_INFO_ID 	= "GroupInfoId";//그룹ID
		public static final String FRIEND_INFO_ID 	= "FriendInfoId";//친구ID
				
		private String 	mGroupInfoId	= null;
		private String 	mFriendInfoId  	= null;
		
		public GroupList(){}
		
		public void SetGroupInfoId(String groupInfoId){this.mGroupInfoId = groupInfoId;}
		public void SetFriendInfoId(String friendInfoid){this.mFriendInfoId = friendInfoid;}
		
		public String GetGroupID(){return this.mGroupInfoId;}
		public String GetGroupName(){return this.mFriendInfoId ;}
				
		public static String CreateGroupList() {
			return "CREATE TABLE IF NOT EXISTS " + GROUPLIST_TB_NAME + " (" 
					+"_id INTEGER PRIMARY KEY AUTOINCREMENT," 
					+ GROUP_INFO_ID+" INTEGER,"
					+ FRIEND_INFO_ID+" text);";
		}
		
		public ArrayList<NameValuePair> getNameValueList() {
			ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
			
			if(mGroupInfoId!=null)		list.add(new BasicNameValuePair(GroupList.GROUP_INFO_ID, mGroupInfoId));
			if(mFriendInfoId!=null)		list.add(new BasicNameValuePair(GroupList.FRIEND_INFO_ID, mFriendInfoId));
			
			return list;
		}	
	}	
}
