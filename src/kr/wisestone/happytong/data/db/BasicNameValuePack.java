package kr.wisestone.happytong.data.db;

/**
 * *********************************************************
 * @className	 : BasicNameValuePack.java
 * @packageName  : nh.smart.personalNetwork.common
 * @date 		 : 2013. 1. 15.
 * @author 		 : "Minjeong Lee<mjlee@wisestone.kr>"
 * @description  : NameValuePair와 유사한 Class. Object를 대입할 수 있다.
 * =========================================================
 * Modify record
 * Date		AUTHOR			DESCRIPTION
 * ---------------------------------------------------------
 * *********************************************************
 */
public class BasicNameValuePack {
	private String 	mName;
	private Object 	mValue;
	
	private BasicNameValuePack(){}
	
	public BasicNameValuePack(String name, Object value) {
		this.mName = name;
		this.mValue = value;
	}
	
	public String getName() {
		return mName;
	}
	
	public Object getValue() {
		return mValue;
	}
	
	public Object getValueByName(String name) {
		if( name!=null && name.compareTo(mName)==0 ) {
			return mValue;
		}
		return null;
	}
	
	public String toString() {
		return "name : "+ mName;
	}
}
