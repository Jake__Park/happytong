package kr.wisestone.happytong.data.db;

import kr.wisestone.happytong.data.common.CommonData;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {
	private static SharedPrefManager instance;
	
	private SharedPreferences sPrefs;
	
	final static String sharedPreferencesName = "sp_HappyTong";
	
	//preference key name
	public final String LOGIN_EMAIL_TOKEN 		= "pref_login_email_token";
	public final String LOGIN_PWD_TOKEN 		= "pref_login_password_token";
	public final String LOGIN_COMPANY_STATE		= "pref_login_company_state";
	public final String FACEBOOK_TOKEN 			= "pref_login_fackbook_token";
	public final String AUTO_SYNC_TOKEN 		= "pref_login_sync_token";
	public final String MANUAL_SYNC_TOKEN 		= "pref_login_manual_sync_token";
	public final String BEFORE_ENTRY_ACTIVITY	= "pref_before_entry_point";
	public final String RECENT_SELECTED_COUPON	= "pref_recent_selected_coupon";
    public final String GOLD_EGG_COUNT          = "pref_gold_egg_count";
    public final String FREE_OWL_COUNT          = "pref_free_owl_count";
    public final String PURCHASED_OWL_COUNT     = "pref_purchased_owl_count";
    public final String IAP_SIGNATURE_DATA      = "pref_sig_data";
    public final String IAP_PURCHASE_DATE       = "pref_purchase_data";
    public final String MOVE_COUPONBOX_TYPE 	= "pref_want_to_move_cbox_type";
    public final String MOVE_COUPONBOX_SEQ		= "pref_want_to_move_cbox_seq";
    public final String SHOWN_COUPONBOX_HELP	= "pref_shown_couponbox_help";
    
    public final String MOVE_STAMPBOX_TYPE 		= "pref_want_to_move_sbox_type";
    public final String MOVE_STAMPBOX_SEQ		= "pref_want_to_move_sbox_seq";
    public final String SHOWN_STAMPBOX_HELP		= "pref_shown_stampbox_help";
    public final String PROFILE_NICKNAME_TOKEN	= "pref_profile_nickname_token";
    public final String SHOW_FREE_OWL_POPUP		= "pref_show_free_owl_popup";
    public final String GUIDE_NO_SHOW			= "pref_guide_no_show";

    public final String IAP_COMPANY_SIG_DATA       = "pref_company_sig_data";
    public final String IAP_COMPANY_PURCHASE_DATE  = "pref_company_purchase_data";
    
    public final String LOCATION_COMPANY_SMALL_CODE = "pref_location_company_small_code";
    public final String COMPANY_ONE_TIME_COUPON = "pref_company_one_time_coupon";
    public final String COMPANY_NO_LIMIT_TIME_COUPON = "pref_company_no_limit_time_coupon";
    public final String COMPANY_BUSINESS_NUMBER = "pref_company_business_number";
	
	public static SharedPrefManager getInstance(Context context) {
		if (instance == null) {
			instance = new SharedPrefManager(context.getApplicationContext().getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE));
		}
		return instance;
	}
	
	private SharedPrefManager(SharedPreferences prefs) {
		this.sPrefs = prefs;
	}
	
	public static String getLoginEmail(Context context) {
		return getInstance(context).getPrefLoginEmail();
	}
	   
	public String getPrefLoginEmail(){
		return sPrefs.getString(LOGIN_EMAIL_TOKEN, "");
	}
	public String getPrefLoginPwd() {
		return sPrefs.getString(LOGIN_PWD_TOKEN, "");
	}
	
	public String getPrefFackbook() {
		return sPrefs.getString(FACEBOOK_TOKEN, "0");
	}

	public String getPrefAutoSync() {
		return sPrefs.getString(AUTO_SYNC_TOKEN, "");
	}
	
	public String getPrefManualSync() {
		return sPrefs.getString(MANUAL_SYNC_TOKEN, "");
	}

	public int getBeforeEntryPoint() {
		return sPrefs.getInt(BEFORE_ENTRY_ACTIVITY, CommonData.ENTRY_PONINT_FRIEND);
	}

	public String getRecentSelectedCoupon() {
		return sPrefs.getString(RECENT_SELECTED_COUPON, "");
	}
	
	public int getGoldEggCount() {
        return sPrefs.getInt(GOLD_EGG_COUNT, 0);
    }

    public int getFreeOwlCount() {
        return sPrefs.getInt(FREE_OWL_COUNT, 0);
    }

    public int getPurchasedOwlCount() {
        return sPrefs.getInt(PURCHASED_OWL_COUNT, 0);
    }

    public String getPurchaseData() {
        return sPrefs.getString(IAP_PURCHASE_DATE, "");
    }
    public String getSignatureData() {
        return sPrefs.getString(IAP_SIGNATURE_DATA, "");
    }
    
    public String getMoveCBoxType() {
    	return sPrefs.getString(MOVE_COUPONBOX_TYPE, "");
    }
    
    public String getMoveSBoxType() {
    	return sPrefs.getString(MOVE_STAMPBOX_TYPE, "");
    }
    
    public String getMoveCBoxSeq() {
    	return sPrefs.getString(MOVE_COUPONBOX_SEQ, "");
    }
    
    public int getShownCouponBoxHelp() {
    	return sPrefs.getInt(SHOWN_COUPONBOX_HELP, 0);
    }
    
    public int getShownStampBoxHelp() {
    	return sPrefs.getInt(SHOWN_STAMPBOX_HELP, 0);
    }
    
	public String getPrefProfileNickName(){
		return sPrefs.getString(PROFILE_NICKNAME_TOKEN, "");
	}
	
	public boolean getPrefShowFreeOwlReceivePopup() {
		return sPrefs.getBoolean(SHOW_FREE_OWL_POPUP, false);
	}
	
	public boolean getPrefGuideNoShow() {
		return sPrefs.getBoolean(GUIDE_NO_SHOW, false);
	}	
	
    public String getCompanyPurchaseData(String data){
        return sPrefs.getString(IAP_COMPANY_PURCHASE_DATE, "");
    }
    
    public String getCompanySignatureData(String data){
        return sPrefs.getString(IAP_COMPANY_SIG_DATA, "");
    }    	
	
    public int getPrefLoginCompanyState() {
		return sPrefs.getInt(LOGIN_COMPANY_STATE, 0);
	}
    
    public String getLocationCompanySmallCode(){
        return sPrefs.getString(LOCATION_COMPANY_SMALL_CODE, "");
    }
    
    public String getCompanyOneTimeCoupon(){
        return sPrefs.getString(COMPANY_ONE_TIME_COUPON, "0");
    }
    
    public String getCompanyNoLimitTimeCoupon(){
        return sPrefs.getString(COMPANY_NO_LIMIT_TIME_COUPON, "0");
    }
    
    public String getCompanyBusinessNumber(){
        return sPrefs.getString(COMPANY_BUSINESS_NUMBER, "");
    }
    
    
    
	public  void setLoginEmail(String value){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(LOGIN_EMAIL_TOKEN, value);
		editor.commit();
	}
	
	public  void setPrefLoginPwd(String value){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(LOGIN_PWD_TOKEN, value);
		editor.commit();
	}
	
	public  void setPrefFackbook(String value){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(FACEBOOK_TOKEN, value);
		editor.commit();
	}
	
	public  void setPrefAutoSync(String value){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(AUTO_SYNC_TOKEN, value);
		editor.commit();
	}
	
	public  void setPrefManualSync(String value){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(MANUAL_SYNC_TOKEN, value);
		editor.commit();
	}
	
	public  void setBeforeEntryPoint(int point){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putInt(BEFORE_ENTRY_ACTIVITY, point);
		editor.commit();
	}

	public  void setRecentSelectedCoupon(String value){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(RECENT_SELECTED_COUPON, value);
		editor.commit();
	}
	
    public  void setGoldEggCount(int count){
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putInt(GOLD_EGG_COUNT, count);
        editor.commit();
    }
    
    public  void setFreeOwlCount(int count){
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putInt(FREE_OWL_COUNT, count);
        editor.commit();
    }

    public  void setPurchasedOwlCount(int count){
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putInt(PURCHASED_OWL_COUNT, count);
        editor.commit();
    }	
	
    public  void setMoveCBoxType(String type){
    	SharedPreferences.Editor editor = sPrefs.edit();
    	editor.putString(MOVE_COUPONBOX_TYPE, type);
    	editor.commit();
    }	
    
    public  void setMoveSBoxType(String type){
    	SharedPreferences.Editor editor = sPrefs.edit();
    	editor.putString(MOVE_STAMPBOX_TYPE, type);
    	editor.commit();
    }	
    
    public  void setMoveCBoxSeq(String seq){
    	SharedPreferences.Editor editor = sPrefs.edit();
    	editor.putString(MOVE_COUPONBOX_SEQ, seq);
    	editor.commit();
    }	

	public void setShownCouponBoxHelp(int val) {
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putInt(SHOWN_COUPONBOX_HELP, val);
		editor.commit();
	}

	public void setShownStampBoxHelp(int val) {
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putInt(SHOWN_STAMPBOX_HELP, val);
		editor.commit();
	}
    
	public void deletePreferenceValues(){
    	SharedPreferences.Editor editor = sPrefs.edit();
    	editor = sPrefs.edit();
    	editor.clear();
    	editor.commit();
	}

    public  void setSignatureData(String value){
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putString(IAP_SIGNATURE_DATA, value);
        editor.commit();
    }   
    
    public  void setPurchaseData(String value){
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putString(IAP_PURCHASE_DATE, value);
        editor.commit();
    } 
    
    public  void setPrefProfileNickName(String value){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(PROFILE_NICKNAME_TOKEN, value);
		editor.commit();
	}

    public  void setPrefShowFreeOwlReceivePopup(boolean isReceive){
    	SharedPreferences.Editor editor = sPrefs.edit();
    	editor.putBoolean(SHOW_FREE_OWL_POPUP, isReceive);
    	editor.commit();
    }
	
    public  void setPrefGuideNoShow(boolean isReceive){
    	SharedPreferences.Editor editor = sPrefs.edit();
    	editor.putBoolean(GUIDE_NO_SHOW, isReceive);
    	editor.commit();
    }	
    
    public  void setCompanyPurchaseData(String data){
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putString(IAP_COMPANY_PURCHASE_DATE, data);
        editor.commit();
    }
    
    public  void setCompanySignatureData(String data){
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putString(IAP_COMPANY_SIG_DATA, data);
        editor.commit();
    }
    
    public  void setPrefLoginCompanyState(int val){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putInt(LOGIN_COMPANY_STATE, val);
		editor.commit();
	}
    
    public  void setPrefLocationCompanySmallCode(String data){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(LOCATION_COMPANY_SMALL_CODE, data);
		editor.commit();
	}
    
    public  void setPrefCompanyOneTimeCoupon(String data){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(COMPANY_ONE_TIME_COUPON, data);
		editor.commit();
	}
    
    public  void setPrefCompanyNoLimitTimeCoupon(String data){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(COMPANY_NO_LIMIT_TIME_COUPON, data);
		editor.commit();
	}
    
    public  void setPrefCompanyBusinessNumber(String data){
		SharedPreferences.Editor editor = sPrefs.edit();
		editor.putString(COMPANY_BUSINESS_NUMBER, data);
		editor.commit();
	}
    
    
}
