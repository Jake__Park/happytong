package kr.wisestone.happytong.data.common;

/**
 * *********************************************************
 * @className    : InitialSoundSearch.java
 * @packageName  : nh.smart.personalNetwork.common
 * @date         : 2013. 1. 15.
 * @author       : "Minjeong Lee<mjlee@wisestone.kr>"
 * @description  : 초성검색 지원 Class 
 * =========================================================
 * Modify record
 * Date     AUTHOR          DESCRIPTION
 * ---------------------------------------------------------
 * *********************************************************
 */
public class InitialSoundSearch {
    private static final char HANGUL_BEGIN_UNICODE = 44032; // 한글 시작 유니코드  '가'
    private static final char HANGUL_LAST_UNICODE = 55203; // 한글 끝 유니코드 '힣'
    private static final char HANGUL_BASE_UNIT = 588;// 한글 기본 유닛
    private static final int  INIT_SOUND_MAX = 19;
    private static final char[] INITIAL_SOUND = { 'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ','ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ'}; 
    
    private static final char[] INITIAL_SOUND_ENG = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    
    /**
     * 생성자
     */
    public InitialSoundSearch() { } 
    
    /**
     *initial sound 검사
     * @param searchar
     * @return
     */
    public static boolean isInitialSound(char searchar){ 
        for(char c:INITIAL_SOUND){ 
            if(c == searchar){ 
                return true; 
            } 
        } 
        return false; 
    } 
    
    /**
     * 초성 뽑아 내기
     *  
     * @param c char
     * @return
     */
    public static char getInitialSound(char c) {
        char init = 0;
        if( c>=97&&c<122 ) {
            init = (char)(c-32);
        } else { 
            int hanBegin = (c - HANGUL_BEGIN_UNICODE); 
            int index = hanBegin / HANGUL_BASE_UNIT;
            if( index<INIT_SOUND_MAX&&index>=0 ) {
                init = INITIAL_SOUND[index];
            } else {
                init = c;
            }
        }
        return init;
    } 
    
    /**
     * 한글인지 체크
     * @param char c
     * @return
     */
    public static boolean isHangul(char c) { 
        return HANGUL_BEGIN_UNICODE <= c && c <= HANGUL_LAST_UNICODE; 
    } 
    

    
     
    public static boolean matchString(String value, String search){ 
        int     t       = 0; 
        boolean bFlag   = false;
        int slen = search.length(); 
         
        for(int i = 0;i <slen;i++){ 
            t = 0; 
            for( int j=0; j<value.length(); j++ ) {
                if(isInitialSound(search.charAt(t))==true && isHangul(value.charAt(j))){
                    if(getInitialSound(value.charAt(j))==search.charAt(t)) {
                        bFlag = true;
                        t++;
                    }
                    else {
                        if(bFlag) {
                            t=0;
                            bFlag=false;
                        }
                    }
                } else { 
                    if(value.charAt(j)==search.charAt(t)) { 
                        bFlag = true;
                        t++; 
                    }
                    else {
                        if(bFlag) {
                            t=0;
                            bFlag=false;
                        }
                    }
                }
                if( t>=slen) {
                    return true;
                }
            }
        /*  
            while( (t<slen) && ((i+t)<value.length())){ 
                if(isInitialSound(search.charAt(t))==true && isHangul(value.charAt(i+t))){
                    if(getInitialSound(value.charAt(i+t))==search.charAt(t)) 
                        t++; 
                    else 
                        continue; 
                } else { 
                    if(value.charAt(i+t)==search.charAt(t)) 
                        t++; 
                    else 
                        continue; 
                } 
            } 
            if(t >= slen) 
                return true;
            */
        } 
        return false;
    }

}



