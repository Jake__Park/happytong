package kr.wisestone.happytong.data.common;

public class CommonData
{
    public static final String DETAIL_PAGE_URL      = "market://details?id=kr.wisestone.happytong";
    //[SDJ 130329]내부결제 아이템 리스트
    public static final String IAP_TEST             = "android.test.purchased";
    public static final String IAP_GOLD_100         = "item_gold_100";
    public static final String IAP_GOLD_210         = "item_gold_210";
    public static final String IAP_GOLD_330         = "item_gold_330";
    public static final String IAP_GOLD_600         = "item_gold_600";

    
//    public static String[] m_arrIAPName = 
//        {
//        IAP_GOLD_100,
//        IAP_GOLD_210,
//        IAP_GOLD_330,
//        IAP_GOLD_600
//        };
    
    public static final String AUTO_SYNC_ON                     = "auto_sync_on";
    public static final String AUTO_SYNC_OFF                    = "auto_sync_off";
    
    public static final int    INVITE_SMS                       = 0;
    public static final int    INVITE_KAKAO                     = 1;
    public static final int    FRIEND_SEND_COUPON               = 2;
    public static final int    FRIEND_SEND_STAMP                = 3;
    
    public static final int    ENTRY_PONINT_FRIEND              = 0;
    public static final int    ENTRY_PONINT_CBOX                = 1;
    public static final int    ENTRY_PONINT_CSEND               = 2;
    public static final int    ENTRY_PONINT_CSHOP               = 3;
    public static final int    ENTRY_PONINT_SETTING             = 4;
    
    public static final int    ENTRY_PONINT_COMPAMY_SEND_LIST   = 5;  //[SDJ 130923] 업체 -> 발송내역 탭
    public static final int    ENTRY_PONINT_COMPAMY_SEND        = 6;  //[SDJ 130923] 업체 -> 쿠폰발송 탭
    public static final int    ENTRY_PONINT_COMPAMY_CHARGE      = 7;  //[SDJ 130923] 업체 -> 충전하기 탭
    public static final int    ENTRY_PONINT_COMPAMY_SETTING     = 8;  //[SDJ 130923] 업체 -> 설정 탭    
    
    //SDJ[130812] 개선사항 적용
    public static final int    GROUP_ADD_OK                     = 105;
    
    public static final String DEVICE_TYPE                      = "2";
    
    public static int          EDIT_TEXT_DIALOG_TEXT_MAX_LENGTH = 4;
    
    public static final String FACEBOOK_AD_N                    = "0";  // 페이스북 광고 여부 - 광고안했음
    public static final String FACEBOOK_AD_Y                    = "1";  // 페이스북 광고 여부 - 광고했음
                                                                                                                
    public static final String TOKEN_PREFIX                     = "A_"; // 토큰 접두사
                                                                                                                
    public static final String LOCALE_KOREA                     = "0";
    public static final String LOCALE_JAPAN                     = "1";
    public static final String LOCALE_ENGLISH                   = "2";
    public static final String LOCALE_CHINA                     = "3";
    
    // 친구메인 - 그룹리스트 - 하단 구분 문자열. 20130806.jish
    public static final String GROUP_LIST_FOOTER_STRING         = "...";
    
    public static final int LOCATION_LIST_TYPE_JOIN 			= 100; // 회원가입 지역 선택
    public static final int LOCATION_LIST_TYPE_NORMAL 			= 200; // 일반 모드 지역 설정 선택
    public static final int LOCATION_LIST_TYPE_COMPANY 			= 300; // 업체 모드 지역 설정 선택
    public static final int LOCATION_LIST_TYPE_COMPANY_REG 		= 400; // 사업자 등록
    public static final int LOCATION_LIST_TYPE_COMPANY_SEND 	= 500; // 사업자 등록
}
