package kr.wisestone.happytong.data.common;

public class BasicNameValuePack {
    private String  mName;
    private Object  mValue;
    
    private BasicNameValuePack(){}
    
    public BasicNameValuePack(String name, Object value) {
        this.mName = name;
        this.mValue = value;
    }
    
    public String getName() {
        return mName;
    }
    
    public Object getValue() {
        return mValue;
    }
    
    public Object getValueByName(String name) {
        if( name!=null && name.compareTo(mName)==0 ) {
            return mValue;
        }
        return null;
    }
    
    public String toString() {
        return "name : "+ mName;
    }
}
