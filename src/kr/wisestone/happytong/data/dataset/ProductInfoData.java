package kr.wisestone.happytong.data.dataset;

import java.io.Serializable;

public class ProductInfoData implements Serializable
{
    private static final long serialVersionUID = 5856716914779529044L;
    private String m_couponSEQ;
    private String m_imageURL;
    private String m_cropImageURL;
    private String m_couponType;
    private String m_price;
    private String m_isNew;
    private String m_productName;
    
    private String m_productID;
    private String m_goldCount;
    
    private String m_owlCount;
    private String m_owlID;
    private String m_isFav;
    private String m_isOwned;
    private String m_couponExplanation;
    private String productType;
    
    public String getCouponSEQ() { return m_couponSEQ; }
    public String getImageURL() { return m_imageURL; }
    public String getCropImageURL() { return m_cropImageURL; }
    public String getCouponType() { return m_couponType; }
    public String getPrice() { return m_price; }
    public String getIsNew() { return m_isNew; }
    public String getProductName() { return m_productName; }
    public String getProductID() { return m_productID; }
    public String getGoldCount() { return m_goldCount; }
    public String getOwlCount() { return m_owlCount; }
    public String getOwlID() { return m_owlID; }
    public String getIsFav() { return m_isFav; }
    public String getIsOwned() { return m_isOwned; }
    public String getCouponExplanation() { return m_couponExplanation; }
    public String getProductType() { return productType; }
    
    public void setCouponSEQ(String mCouponSEQ) { this.m_couponSEQ = mCouponSEQ; }
    public void setImageURL(String mImageURL) { this.m_imageURL = mImageURL; }
    public void setCropImageURL(String mCropImageURL) { this.m_cropImageURL = mCropImageURL; }
    public void setCouponType(String mCouponType) { this.m_couponType = mCouponType; }
    public void setPrice(String mPrice) { this.m_price = mPrice; }
    public void setIsNew(String mIsNew) { this.m_isNew = mIsNew; }
    public void setProductName(String mProductName) { this.m_productName = mProductName; }
    public void setProductID(String mProductID) { this.m_productID = mProductID; }
    public void setGoldCount(String mImageURL) { this.m_goldCount = mImageURL; }
    public void setOwlCount(String mOwlCount) { this.m_owlCount = mOwlCount; }
    public void setOwlID(String mOwlID) { this.m_owlID = mOwlID; }
    public void setIsFav(String mIsFav) { this.m_isFav = mIsFav; }
    public void setIsOwned(String mIsOwned) { this.m_isOwned = mIsOwned; }
    public void setCouponExplanation(String mCouponExplanation) { this.m_couponExplanation = mCouponExplanation; }
    public void setProductType(String mProductType) { this.productType = mProductType; }
}
