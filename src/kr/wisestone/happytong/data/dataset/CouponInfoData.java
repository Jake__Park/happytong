package kr.wisestone.happytong.data.dataset;

import java.io.Serializable;

public class CouponInfoData implements Serializable
{
	private static final long serialVersionUID = -8793688492297224436L;
	public String m_senderPhotoURL;
    public String m_couponCropIMGURL;
    public String m_couponType;
    public String m_validPeriod;
    public String m_senderID;
    public String m_createdCouponDate;
    public String m_senderNickname;
    public String m_couponImageURL;
    public String m_couponText;
    public String m_flag;
    public String m_requestState;
    public String m_receiverID;
    public String m_couponSEQ;
    public String m_boxSEQ;
    public String m_receiverPhotoURL;
    public String m_receiverNickname;
    public String m_endCoupon;
    
    public String getSenderPhotoURL() { return m_senderPhotoURL; }
    public String getCouponCropIMGURL() { return m_couponCropIMGURL; }
    public String getCouponType() { return m_couponType; }
    public String getValidPeriod() { return m_validPeriod; }
    public String getSenderID() { return m_senderID; }
    public String getCreatedCouponDate() { return m_createdCouponDate; }
    public String getSenderNickname() { return m_senderNickname; }
    public String getCouponIMGURL() { return m_couponImageURL; }
    public String getCouponText() { return m_couponText; }
    public String getFlag() { return m_flag; }
    public String getRequestState() { return m_requestState; }
    public String getReceiverID() { return m_receiverID; }
    public String getCouponSEQ() { return m_couponSEQ; }
    public String getBoxSEQ() { return m_boxSEQ; }
    public String getReceiverPhotoURL() { return m_receiverPhotoURL; }
    public String getReceiverNickname() { return m_receiverNickname; }
    public String getEndCoupon() { return m_endCoupon; }
    
    public void setSenderPhotoURL(String senderPhotoURL) { this.m_senderPhotoURL = senderPhotoURL; }
    public void setCouponCropIMGURL(String couponCropIMGURL) { this.m_couponCropIMGURL = couponCropIMGURL; }
    public void setCouponType(String couponType) { this.m_couponType = couponType; }
    public void setValidPeriod(String validPeriod) { this.m_validPeriod = validPeriod; }
    public void setSenderID(String senderID) { this.m_senderID = senderID; }
    public void setCreatedCouponDate(String createdCouponDate) { this.m_createdCouponDate = createdCouponDate; }
    public void setSenderNickname(String senderNickname) { this.m_senderNickname = senderNickname; }
    public void setCouponIMGURL(String couponIMGURL) { this.m_couponImageURL = couponIMGURL; }
    public void setCouponText(String couponText) { this.m_couponText = couponText; }
    public void setFlag(String flag) { this.m_flag = flag; }
    public void setRequestState(String requestState) { this.m_requestState = requestState; }
    public void setReceiverID(String receiverID) { this.m_receiverID = receiverID; }
    public void setCouponSEQ(String couponSEQ) { this.m_couponSEQ = couponSEQ; }
    public void setBoxSEQ(String boxSEQ ) { this.m_boxSEQ = boxSEQ; }
    public void setReceiverPhotoURL(String receiverPhotoURL) { this.m_receiverPhotoURL = receiverPhotoURL; }
    public void setReceiverNickname(String receiverNickname) { this.m_receiverNickname = receiverNickname; }
    public void setEndCoupon(String endCoupon) { this.m_endCoupon = endCoupon; }
}
