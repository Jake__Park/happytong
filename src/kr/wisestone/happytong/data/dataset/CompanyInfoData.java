package kr.wisestone.happytong.data.dataset;

import java.io.Serializable;

public class CompanyInfoData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8256798991126519382L;
	
	private String mCompanyName;
	private int mCompanyTel;
	private String mCompanyAddress;
	private String mCompanyNum;
	private String mCompanyLocation;
	
	public String getmCompanyName() {
		return mCompanyName;
	}
	public void setmCompanyName(String mCompanyName) {
		this.mCompanyName = mCompanyName;
	}
	public int getmCompanyTel() {
		return mCompanyTel;
	}
	public void setmCompanyTel(int mCompanyTel) {
		this.mCompanyTel = mCompanyTel;
	}
	public String getmCompanyAddress() {
		return mCompanyAddress;
	}
	public void setmCompanyAddress(String mCompanyAddress) {
		this.mCompanyAddress = mCompanyAddress;
	}
	public String getmCompanyNum() {
		return mCompanyNum;
	}
	public void setmCompanyNum(String mCompanyNum) {
		this.mCompanyNum = mCompanyNum;
	}
	public String getmCompanyLocation() {
		return mCompanyLocation;
	}
	public void setmCompanyLocation(String mCompanyLocation) {
		this.mCompanyLocation = mCompanyLocation;
	}
	
}
