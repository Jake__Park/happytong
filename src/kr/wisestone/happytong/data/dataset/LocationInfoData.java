package kr.wisestone.happytong.data.dataset;

import java.io.Serializable;

public class LocationInfoData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3622813389255189503L;
	String mLocationSmallName;
	String mLocationSmallCode;
	String mLocationBigName;
	String mLocationUserCount;

	public String getLocationUserCount() {
		return mLocationUserCount;
	}

	public void setLocationUserCount(String mLocationUserCount) {
		this.mLocationUserCount = mLocationUserCount;
	}

	public String getLocationSmallName() {
		return mLocationSmallName;
	}

	public void setLocationSmallName(String mLocationSmallName) {
		this.mLocationSmallName = mLocationSmallName;
	}

	public String getLocationSmallCode() {
		return mLocationSmallCode;
	}

	public void setLocationSmallCode(String mLocationSmallCode) {
		this.mLocationSmallCode = mLocationSmallCode;
	}

	public String getLocationBigName() {
		return mLocationBigName;
	}

	public void setLocationBigName(String mLocationBigName) {
		this.mLocationBigName = mLocationBigName;
	}

}
