package kr.wisestone.happytong.data.dataset;

import java.io.Serializable;

public class CompanySendedCouponData implements Serializable {

	private static final long	serialVersionUID	= -8647335097575408256L;

	public String				mRecevier;
	public String				mReceiverCount;
	public String				mLocationSmallName;
	public String				mSendDate;
	public String				mCouponImageURL;
	public boolean				isChecked;
	public String				couponCropImageURL;
	public String				itemGroup;
	public String				couponText;
	public String				couponType;
	public String				validDateEnd;

	@Override
	public String toString() {
		return mRecevier + " / " + mReceiverCount + " / " + mLocationSmallName + " / " + mSendDate + " / " + mCouponImageURL + " / " + couponCropImageURL + " / " + itemGroup + " / "
				+ couponText + " / " + couponType;
	}

}
