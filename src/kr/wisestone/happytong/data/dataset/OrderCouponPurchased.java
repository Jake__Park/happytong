package kr.wisestone.happytong.data.dataset;

public class OrderCouponPurchased {
	private static final String	DELIMITER	= "--6HH6--";

	public String				couponSEQ;
	public String				couponImageURL;
	public String				couponType;
	public String				productName;

	public static String convertDataToString(OrderCouponPurchased data) {
		StringBuilder sb = new StringBuilder();
		sb.append(data.couponSEQ).append(DELIMITER);
		sb.append(data.couponImageURL).append(DELIMITER);
		sb.append(data.couponType).append(DELIMITER);
		sb.append(data.productName);
		return sb.toString();
	}

	public static OrderCouponPurchased convertStringToData(String string) {
		OrderCouponPurchased ocp = new OrderCouponPurchased();
		String[] res = string.split(DELIMITER);
		if (res.length < 4) {
			return ocp;
		}
		ocp.couponSEQ = res[0];
		ocp.couponImageURL = res[1];
		ocp.couponType = res[2];
		ocp.productName = res[3];
		return ocp;
	}

	@Override
	public String toString() {
		return couponSEQ + " / " + couponImageURL + " / " + couponType + " / " + productName;
	}
}
