package kr.wisestone.happytong.data.dataset;


public class FriendInfoData
{
    private boolean m_bIsFirstHeader;
    private char m_initialSound; 
    
    private String m_id;
    private String m_phoneNumber;
    private String m_nickName;
    private String m_userImageUrl;
    private String m_blockState;
    
    public String getId() {
        return m_id;
    }
    public String getPhoneNumber() {
        return m_phoneNumber;
    }
    public String getNickName() {
        return m_nickName;
    }
    public String getUserImageUrl() {
        return m_userImageUrl;
    }
    public String getBlockState() {
        return m_blockState;
    }
    public char getInitialSound() {
        return m_initialSound;
    }
    public boolean getIsFirstHeader() {
        return m_bIsFirstHeader;
    }
    
    public void setId(String id) {
        this.m_id = id;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.m_phoneNumber = phoneNumber;
    }
    public void setNickName(String nickName) {
        this.m_nickName = nickName;
    }
    public void setUserImageUrl(String userImageUrl) {
        this.m_userImageUrl = userImageUrl;
    }
    public void setBlockState(String blockState) {
        this.m_blockState = blockState;
    }
    public void setInitialSound(char initialSound) {
        this.m_initialSound = initialSound;
    }
    public void setIsFirstHeader(boolean isFirstHeader) {
        this.m_bIsFirstHeader = isFirstHeader;
    }

}
