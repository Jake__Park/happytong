package kr.wisestone.happytong.data.dataset;

public class PurchaseInfoData
{
    private String m_freeStampCount;
    private String m_paidStampCount;
    private String m_totalStampCount;
    
    private String m_imgeURL; 
    private String m_goldCount; 
    private String m_price; 
    private String m_productName; 
    private String m_purchasedDate; 
    private String m_purchasedCount; 
    private String m_lackingCount; 
    
    private String m_oneTimeCoupon; 
    private String m_noLimitTimeCoupon; 
    
    public String getFreeStampCount() {
        return m_freeStampCount;
    }
    public String getPaidStampCount() {
        return m_paidStampCount;
    }
    public String getTotalStampCount() {
        return m_totalStampCount;
    }
    public String getImageURL() {
        return m_imgeURL;
    }
    public String getGoldCount() {
        return m_goldCount;
    }
    public String getPrice() {
        return m_price;
    }
    public String getProductName() {
        return m_productName;
    }
    public String getPruchasedDate() {
        return m_purchasedDate;
    }
    public String getPurchasedCount() {
        return m_purchasedCount;
    }
    public String getLackingCount() {
        return m_lackingCount;
    }
    public String getOneTimeCoupon() {
        return m_oneTimeCoupon;
    }
    public String getNoLimitTimeCoupon() {
        return m_noLimitTimeCoupon;
    }    

    public void setFreeStampCount(String count) {
        this.m_freeStampCount = count;
    }
    public void setPaidStampCount(String count) {
        this.m_paidStampCount = count;
    }
    public void setTotalStampCount(String count) {
        this.m_totalStampCount = count;
    }
    public void setImageURL(String imgeURL) {
        this.m_imgeURL = imgeURL;
    }
    public void setGoldCount(String goldCount) {
        this.m_goldCount = goldCount;
    }
    public void setPrice(String price) {
        this.m_price = price;
    }
    public void setProductName(String productName) {
        this.m_productName = productName;
    }
    public void setPruchasedDate(String purchasedDate) {
        this.m_purchasedDate = purchasedDate;
    }
    public void setPurchasedCount(String purchasedCount) {
        this.m_purchasedCount = purchasedCount;
    }
    public void setLackingCount(String lackingCount) {
        this.m_lackingCount = lackingCount;
    }
    public void setOneTimeCoupon(String data) {
        this.m_oneTimeCoupon = data;
    }
    public void setNoLimitTimeCoupon(String data) {
        this.m_noLimitTimeCoupon = data;
    }    
}
