package kr.wisestone.happytong.data.dataset;

import java.io.Serializable;

import kr.wisestone.happytong.data.common.CommonData;

public class AuthInfoData implements Serializable {
	private static final long serialVersionUID = -1642563665516919603L;
	private String id;
	private String password;
	private String deviceId;
	private String phoneNumber;
	private String deviceType = CommonData.DEVICE_TYPE;
	private String nickName;
	private boolean freeStampReceive = false;
    private String freeStampCount;
    private String paidStampCount;
    private String remainGoldEgg;
    private String publicKey;
    private String locationSmallCode;
    private String companyState;
    private String oneTimeCoupon;
    private String noLimitTimeCoupon;
    private String businessNumber;

	public String getBusinessNumber() {
		return businessNumber;
	}

	public void setBusinessNumber(String businessNumber) {
		this.businessNumber = businessNumber;
	}

	public String getOneTimeCoupon() {
		return oneTimeCoupon;
	}

	public void setOneTimeCoupon(String oneTimeCoupon) {
		this.oneTimeCoupon = oneTimeCoupon;
	}

	public String getNoLimitTimeCoupon() {
		return noLimitTimeCoupon;
	}

	public void setNoLimitTimeCoupon(String noLimitTimeCoupon) {
		this.noLimitTimeCoupon = noLimitTimeCoupon;
	}

	public String getCompanyState() {
		return companyState;
	}

	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}

	public String getLocationSmallCode() {
		return locationSmallCode;
	}

	public void setLocationSmallCode(String locationSmallCode) {
		this.locationSmallCode = locationSmallCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
    public boolean isFreeStampReceive() {
		return freeStampReceive;
	}

	public void setFreeStampReceive(boolean freeStampReceive) {
		this.freeStampReceive = freeStampReceive;
	}

	public String getFreeStampCount() {
        return freeStampCount;
    }

    public void setFreeStampCount(String freeStampCount) {
        this.freeStampCount = freeStampCount;
    }

    public String getPaidStampCount() {
        return paidStampCount;
    }

    public void setPaidStampCount(String paidStampCount) {
        this.paidStampCount = paidStampCount;
    }

    public String getRemainGoldEgg() {
        return remainGoldEgg;
    }

    public void setRemainGoldEgg(String remainGoldEgg) {
        this.remainGoldEgg = remainGoldEgg;
    }
    public String getPublicKey() {
        return publicKey;
    }

    public void SetPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }
    

	@Override
	public String toString() {
		return id + " / pw:" + password + " / dId:" + deviceId + " / pN:" + phoneNumber + " / dT:" + deviceType + " / nick:" + nickName;
	}

}
