package kr.wisestone.happytong.data.dataset;

import java.io.Serializable;

public class UserInfoData implements Serializable {
    private static final long serialVersionUID = 7814720498820852518L;
    
    private String            m_nickName;
    private String            m_userImageUrl;
    private String            m_phoneNumber;
    private String            m_date;
    private String            m_fbAd;
    private String            m_locationBigName;
    private String            m_locationBigCode;
    private String            m_locationSmallName;
    private String            m_locationSmallCode;
    
    private String            m_companyName;
    private String            m_address;
    private String            m_companyNumber;
    private String            m_companyBusinessNumber;
    private String            m_companyCropImageURL;
    
    public String getLocationBigName() {
        return m_locationBigName;
    }
    
    public void setLocationBigName(String m_locationBigName) {
        this.m_locationBigName = m_locationBigName;
    }
    
    public String getLocationBigCode() {
        return m_locationBigCode;
    }
    
    public void setLocationBigCode(String m_locationBigCode) {
        this.m_locationBigCode = m_locationBigCode;
    }
    
    public String getLocationSmallName() {
        return m_locationSmallName;
    }
    
    public void setLocationSmallName(String m_locationSmallName) {
        this.m_locationSmallName = m_locationSmallName;
    }
    
    public String getLocationSmallCode() {
        return m_locationSmallCode;
    }
    
    public void setLocationSmallCode(String m_locationSmallCode) {
        this.m_locationSmallCode = m_locationSmallCode;
    }
    
    public String getNickName() {
        return m_nickName;
    }
    
    public void setNickName(String m_nickName) {
        this.m_nickName = m_nickName;
    }
    
    public String getUserImageUrl() {
        return m_userImageUrl;
    }
    
    public void setUserImageUrl(String m_userImageUrl) {
        this.m_userImageUrl = m_userImageUrl;
    }
    
    public String getPhoneNumber() {
        return m_phoneNumber;
    }
    
    public void setPhoneNumber(String m_phoneNumber) {
        this.m_phoneNumber = m_phoneNumber;
    }
    
    public String getDate() {
        return m_date;
    }
    
    public void setDate(String m_date) {
        this.m_date = m_date;
    }
    
    public String getFbAd() {
        return m_fbAd;
    }
    
    public void setFbAd(String m_fbAd) {
        this.m_fbAd = m_fbAd;
    }
    
    public String getCompanyName() {
        return m_companyName;
    }
    
    public void setCompanyName(String data) {
        this.m_companyName = data;
    }
    
    public String getAddress() {
        return m_address;
    }
    
    public void setAddress(String data) {
        this.m_address = data;
    }
    
    public String getCompanyNumber() {
        return m_companyNumber;
    }
    
    public void setCompanyNumber(String data) {
        this.m_companyNumber = data;
    }
    
    public String getCompanyBusinessNumber() {
        return m_companyBusinessNumber;
    }
    
    public void setCompanyBusinessNumber(String data) {
        this.m_companyBusinessNumber = data;
    }  
    
    public String getCompanyCropImageURL() {
        return m_companyCropImageURL;
    }
    
    public void setCompanyCropImageURL(String data) {
        this.m_companyCropImageURL = data;
    }      
    
}
