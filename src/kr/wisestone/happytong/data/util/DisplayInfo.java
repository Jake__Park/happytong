package kr.wisestone.happytong.data.util;

import kr.wisestone.happytong.util.Logg;
import android.content.Context;
import android.view.Display;
import android.view.WindowManager;

public class DisplayInfo {

	private static DisplayInfo instance;

	private int displayWidth;
	private int displayHeight;

	public static DisplayInfo getInstance(Context context) {
		if (instance == null) {
			instance = new DisplayInfo(context.getApplicationContext());
		}
		return instance;
	}

	private DisplayInfo(Context context) {
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		displayWidth = display.getWidth();
		displayHeight = display.getHeight();

		Logg.d("DisplayInfo - width:" + displayWidth + " / height:" + displayHeight);
	}

	public int getDisplayWidth() {
		return displayWidth;
	}

	public int getDisplayHeight() {
		return displayHeight;
	}

}
