package kr.wisestone.happytong.data.util;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

 public class RecycleUtils {
     private RecycleUtils(){};
     
     /***
      * root view 부터 재귀 호출 하며 모든 view 를 제거하여 메모리 leak 을 방지
      * 사용 예) RecycleUtils.recursiveRecycle(getWindow().getDecorView());
      * @param root View
      */
     public static void recursiveRecycle(View root) {
         if (root == null)
             return;
         root.setBackgroundDrawable(null);
         if (root instanceof ViewGroup) {
             ViewGroup group = (ViewGroup)root;
             int count = group.getChildCount();
             for (int i = 0; i < count; i++) {
                 recursiveRecycle(group.getChildAt(i));
             }

             if (!(root instanceof AdapterView)) {
                 group.removeAllViews();
             }
         }
         if (root instanceof ImageView) {
             ((ImageView)root).setImageDrawable(null);
         }
         root = null;
         return;
     }
 }
