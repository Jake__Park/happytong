package kr.wisestone.happytong.data.util;

import kr.wisestone.happytong.R;

public class ErrorCode
{
    public static final int ERR_HTTP_SERVER     = -1;

    public static final int ERR_HTTP_SERVER_200 = 200;
    public static final int ERR_HTTP_SERVER_201 = 201;
    public static final int ERR_HTTP_SERVER_202 = 202;
    public static final int ERR_HTTP_SERVER_203 = 203;
    public static final int ERR_HTTP_SERVER_204 = 204;
    public static final int ERR_HTTP_SERVER_205 = 205;
    public static final int ERR_HTTP_SERVER_209 = 209;
    public static final int ERR_HTTP_SERVER_210 = 210;

    public static final int ERR_HTTP_SERVER_300 = 300;
    public static final int ERR_HTTP_SERVER_301 = 301;
    public static final int ERR_HTTP_SERVER_302 = 302;
    public static final int ERR_HTTP_SERVER_303 = 303;
    public static final int ERR_HTTP_SERVER_304 = 304;
    public static final int ERR_HTTP_SERVER_305 = 305;

    public static final int ERR_HTTP_UNKNOWN    = 999;
    
    public static int getErrorMsg(int code){
        int retId = 0;
        
        switch(code){
            case ERR_HTTP_SERVER:
                retId = R.string.error_code_unknown;
                break;
            case ERR_HTTP_SERVER_200:
                retId = R.string.error_code_200;
                break;
            case ERR_HTTP_SERVER_201:
                retId = R.string.error_code_201;
                break;
            case ERR_HTTP_SERVER_202:
                retId = R.string.error_code_202;
                break;
            case ERR_HTTP_SERVER_203:
                retId = R.string.error_code_203;
                break;
            case ERR_HTTP_SERVER_204:
                retId = R.string.error_code_204;
                break;
            case ERR_HTTP_SERVER_205:
                retId = R.string.error_code_205;
                break;
            case ERR_HTTP_SERVER_209:
                retId = R.string.error_code_209;
                break;
            case ERR_HTTP_SERVER_210:
                retId = R.string.error_code_210;
                break;
            case ERR_HTTP_SERVER_300:
                retId = R.string.error_code_300;
                break;
            case ERR_HTTP_SERVER_301:
                retId = R.string.error_code_301;
                break;
            case ERR_HTTP_SERVER_302:
                retId = R.string.error_code_302;
                break;
            case ERR_HTTP_SERVER_303:
                retId = R.string.error_code_303;
                break;
            case ERR_HTTP_SERVER_304:
                retId = R.string.error_code_304;
                break;
            case ERR_HTTP_SERVER_305:
            	retId = R.string.error_code_305;
            	break;
            case ERR_HTTP_UNKNOWN:
            default:
                retId = R.string.error_code_unknown;
                break;
        }
        return retId;
    }
}
