package kr.wisestone.happytong.data.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.ui.DummyMainActivity;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.IntentDefine;
import kr.wisestone.happytong.util.Logg;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.MediaStore.Images;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

public class Utils {

	/**
	 * 이미지 관련
	 * 
	 * @param is
	 * @param os
	 */
	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	/**
	 * photo load
	 * 
	 * @param context
	 * @param photo
	 * @return
	 */
	public static Bitmap loadBitmap(Context context, String photo) {
		InputStream is = null;
		Bitmap bitmap = null;
		Uri uri = null;

		if (photo != null) {
			if (photo.indexOf("content") == 0) {
				uri = Uri.parse(photo);
				String authority = uri.getAuthority();

				if (authority.compareTo("media") == 0) {
					try {
						bitmap = Images.Media.getBitmap(context.getContentResolver(), uri);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					is = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(), uri);

					if (is != null) {
						bitmap = BitmapFactory.decodeStream(is);
					}
				}
			}
		}

		return bitmap;
	}

	/**
	 * copydb()에 의해 생성된 임시 파일 삭제
	 */
	public static void deleteDB() {
		File dir = new File("/mnt/sdcard");
		if (dir.isDirectory()) {
			File[] dtFiles = dir.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String filename) {
					if (filename.startsWith("Happytong") && filename.endsWith(".db")) {
						return true;
					} else {
						return false;
					}
				}
			});
			if (dtFiles != null && dtFiles.length > 0) {
				for (File f : dtFiles) {
					f.delete();
				}
			}
		}
	}

	@SuppressLint("SdCardPath")
	public static void copyDB() {
		File dbf = new File("/data/data/kr.wisestone.happytong/databases/Happytong.db");

		String dst = "/mnt/sdcard/Happytong.db";
		File newfile = new File(dst);
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		BufferedInputStream bis = null;
		try {

			bis = new BufferedInputStream(new FileInputStream(dbf));

			// 만약에 파일이 있다면 지우고 다시 생성
			if (newfile.exists()) {
				newfile.delete();
				newfile.createNewFile();
			}

			fos = new FileOutputStream(newfile);
			bos = new BufferedOutputStream(fos);

			int read = -1;
			byte[] buffer = new byte[1024];
			while ((read = bis.read(buffer, 0, 1024)) != -1) {
				bos.write(buffer, 0, read);
			}
			bos.flush();

		} catch (IOException e) {
			Logg.e(e);
		} finally {
			try {
				if (fos != null)
					fos.close();
				if (bos != null)
					bos.close();
				if (bis != null)
					bis.close();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	/**
	 * 전화번호 반환
	 * 
	 * @param context
	 * @return
	 */
	public static String getPhoneNumber(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		String phoneNum = tm.getLine1Number();

		Logg.d(phoneNum);
		if (TextUtils.isEmpty(phoneNum)) {
			return "";
		}

		phoneNum = phoneNum.trim().replaceAll("-", "");
		int idx = phoneNum.indexOf("82");
		if (idx != -1 && idx < 3) {
			phoneNum = phoneNum.substring(idx + 2);
		}
		if (phoneNum.startsWith("1")) {
			phoneNum = "0" + phoneNum;
		}
		return AESEncryption.encrypt(phoneNum);
	}

	/**
	 * 단말아이디 대신, GCM 아이디 반환 (암호화 적용) (서버에서 DeviceId는 GCM 연동을 위함)
	 * 
	 * @param context
	 * @return
	 */
	public static String getEncryptDeviceIdForGCM(Context context) {
		// 암호화된 데이터 전달
		return AESEncryption.encrypt(GCMRegistrar.getRegistrationId(context));
	}

	/**
	 * 유심상태 확인
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isSimStateReady(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		int state = tm.getSimState();
		Logg.d(state);
		if (state == TelephonyManager.SIM_STATE_ABSENT) {
			return false;
		}
		// XXX 더 확인하려면 operator 로 체크하는 것도 고려해볼만 함??
		return true;
	}

	/**
	 * 전화번호 반환
	 * 
	 * @param
	 * @return 폰북 전화번호 리스트
	 */
	public static String getContactList(Context mContext) {
        ArrayList<FriendInfo> arrInfo = new ArrayList<FriendInfo>();
        
        StringBuilder sb = new StringBuilder();

        String beforeMdn = "";
        String beforeName = "";
        String mSortOrder = CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

        Cursor cursor = mContext.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, mSortOrder);
        
        while (cursor.moveToNext()) {
            int phone_idx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int name_idx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

            String mdn = cursor.getString(phone_idx);
            String name = cursor.getString(name_idx);
            
            if (mdn == null) {
                continue;
            }

            // "-"제거 부분 추가
            mdn = mdn.replace("-", "");

            if(!beforeMdn.equals(mdn) || !beforeName.equals(name)){
                if (mdn.regionMatches(0, "01", 0, 2) && mdn.length() >= 10) {
                    FriendInfo info = new FriendInfo();
                    
                    beforeMdn = mdn;
                    beforeName = name;
            
                    info.SetPhoneNumber(mdn);
                    info.SetName(name);

                    arrInfo.add(info);
                }
            }
        }

        //[SDJ 131008] 이름/전화번호 동일한 것들은 하나만 추가되도록 변경
        ArrayList<FriendInfo> arrTemp = new ArrayList<FriendInfo>();

        for(int i =0; i<arrInfo.size(); i++){
            boolean ret = true;
            
            FriendInfo info = arrInfo.get(i);
            
            if(i == 0){
                Logg.e("[[" + info.GetPhoneNumber() +" [[" + info.GetName());
                arrTemp.add(info);
                
                String strTemp = AESEncryption.encrypt(info.GetPhoneNumber()) + ":" + AESEncryption.encrypt(info.GetName());
                sb.append(strTemp + "#");
            } else {

                for(int j =0; j<arrTemp.size(); j++){
                    FriendInfo temp = arrTemp.get(j);
                    
                    if(info.GetPhoneNumber().equals(temp.GetPhoneNumber()) && info.GetName().equals(temp.GetName())){
                        ret = false;
                        break;
                    }
                }

                if(ret){
                    arrTemp.add(info);
                    Logg.e("[[" + info.GetPhoneNumber() +" [[" + info.GetName());
                    
                    String strTemp = AESEncryption.encrypt(info.GetPhoneNumber()) + ":" + AESEncryption.encrypt(info.GetName());
                    if (i == arrInfo.size()-1)
                        sb.append(strTemp);
                    else
                        sb.append(strTemp + "#");
                }
            }
        }
        Logg.e(sb);
        cursor.close();

        return sb.toString();
	}

	/**
	 * 서버에서 받은 데이트 문자열을 지정된 패턴, 지역으로 변환하여 문자열 반환
	 * 
	 * @param serverDateFormatString
	 *            서버에서 받은 데이트 문자열(yyyyMMddHHmmss) ex)20130101000000
	 * @param pattern
	 *            적용할 패턴 ex) a h:mm, yyyy/MM/dd EEEE, yyyy.MM.dd 까지
	 * @param locale
	 * @return
	 */
	public static String getDateFormatStringFromServerDateFormat(String serverDateFormatString, String pattern, Locale locale) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", locale);
		String result;
		try {
			Date date = sdf.parse(serverDateFormatString);
			sdf.applyPattern(pattern);
			result = sdf.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
			result = "";
		}
		return result;
	}

	/**
	 * 데이트 변수를 서버에서 사용하는 데이트 문자열로 변환하여 반환 date --> "20130101"
	 * 
	 * @param date
	 *            Date 객체
	 * @return
	 */
	public static String getServerDateFormatString(Date date) {
		return new SimpleDateFormat("yyyyMMdd", Locale.KOREA).format(date);
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isStringMatchEmail(String str) {
		// String emailRegEx = "([0-9a-zA-Z_-]+)@([0-9a-zA-Z_-]+)(\\.[0-9a-zA-Z_-]+){1,2}";
		String emailRegEx = "([\\w\\.-]+)@([\\w_-]+)(\\.[\\w_-]+){1,2}";
		return str.matches(emailRegEx);
	}

	public static void ToastNetworkError(Context context) {
		Toast.makeText(context, R.string.toast_network_error_message, Toast.LENGTH_SHORT).show();
	}

	public static void hideKeypad(Context context, View v) {
		if (v != null) {
			InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			// imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
		}
	}

	public static void showKeypad(Context context, EditText v) {
		if (v != null) {
			InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
			// imm.showSoftInputFromInputMethod(v.getWindowToken(), InputMethodManager.SHOW_IMPLICIT);
		}
	}

	/**
	 * 사용자 데이터 삭제
	 */
	public static void deleteUserData(Context context) {
		HappytongDBHelper hdbHelper = new HappytongDBHelper(context);
		hdbHelper.deleteAllData();
		hdbHelper.close();
		SharedPrefManager.getInstance(context).deletePreferenceValues();

	}

	/**
	 * 사용자 데이터 삭제 후 Activity 종료. 혹은 로그인페이지 실행
	 * 
	 * @param activity
	 * @param startLogin
	 *            true:로그인실행 / false:종료
	 */
	public static void deleteUserDataAndFinishCuFunU(Activity activity, boolean startLogin) {
		deleteUserData(activity);

		Intent mainIntent = new Intent(activity, DummyMainActivity.class);
		mainIntent.putExtra(IntentDefine.EXTRA_MAIN_FINISH, true);
		if (startLogin) {
			mainIntent.putExtra(IntentDefine.EXTRA_MAIN_FINISH_START_LOGIN, true);
		}
		mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		activity.startActivity(mainIntent);
		activity.finish();
	}

	public static void loggingIntentData(Intent intent) {
		Bundle bundle = intent.getExtras();
		Iterator<String> keyIter = bundle.keySet().iterator();
		for (; keyIter.hasNext();) {
			String key = keyIter.next();
			Logg.d(intent + " // " + key + " : " + bundle.get(key));
		}
	}

	public static String getAppVersionName(Context context) {
		try {
			return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	public static void showToast(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}
	
	//[SDJ 131002] Pattern 적용 Method 추가
	public static BitmapDrawable getPatternDrawable(Context context, int resID) {
        BitmapDrawable mPatternBitmapDrawable = (BitmapDrawable) context.getResources().getDrawable(resID);
        mPatternBitmapDrawable.setTileModeX(Shader.TileMode.REPEAT);
        mPatternBitmapDrawable.setTileModeY(Shader.TileMode.REPEAT);
        return mPatternBitmapDrawable;
    }
	
	// 사업자 등록 번호 유효성 검사
	public static boolean checkCompanyNum(String mBusinessNum, int businessNumLength){
		String falseNum = "0000000000"; // 0000000000 일때 false
		if(TextUtils.isEmpty(mBusinessNum) || mBusinessNum.length() != businessNumLength
				|| mBusinessNum.equals(falseNum)){
			Logg.d("checkCompanyNum FALSE!!!!!");
			return false;
		} else {
			int[] checkNum = {1,3,7,1,3,7,1,3,5};
			int mResult = 0;				
			int nineNumLeft = (int) (Math.floor((Integer.parseInt(mBusinessNum.substring(8,9)) * 5) / 10)); // .뒤 숫자 버림
			int nineNumRight = ((Integer.parseInt(mBusinessNum.substring(8,9)) * 5) % 10);
				
			for(int i = 0; i < 7; i++){
				mResult += (Integer.parseInt((mBusinessNum.substring(i, i+1))) * checkNum[i]) % 10;
			}
				
			mResult += ((Integer.parseInt(mBusinessNum.substring(7,8)) * 3) % 10) + nineNumLeft + nineNumRight
					+ (Integer.parseInt(mBusinessNum.substring(9,10)));
				
			Logg.d("mResult =========== " + mResult);
				
			if(mResult % 10 == 0){
				Logg.d("checkCompanyNum SUCCESS!!!!!");
				return true;
			}				
		}
		Logg.d("checkCompanyNum FALSE!!!!!");
		return false;			
	}	
}
