package kr.wisestone.happytong.data.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import kr.wisestone.happytong.util.Logg;

public class DateUtil {
    /**Log의 tag */
    private static final String TAG_CAL = "dateUtil";
    /**하루 */
    public static final int         SCOPE_DAILY                  = 1;
    /**1주 */
    public static final int         SCOPE_WEEK                   = 2;
    /**한 달 */
    public static final int         SCOPE_MONTHLY_ONE            = 3;
    /**두 달 */
    public static final int         SCOPE_MONTHLY_TWO            = 4;
    /**세 달 */
    public static final int         SCOPE_MONTHLY_THREE          = 5;
    /**여섯 달 */
    public static final int         SCOPE_MONTHLY_SIX            = 6;
    
    /**Time zone */
    private static TimeZone timeZone;

    static {
        try {
            timeZone = TimeZone.getTimeZone("GMT+09:00");
        } catch (Exception e) {
        }
    }

    /**
     * date를 출력
     * @param date
     * @param format
     * @return
     */
    public static String dateToString(Date date, String format) {
        SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
        simpleFormat.setTimeZone(timeZone);
        return simpleFormat.format(date);
    }
    
    /**
     * 입력 받은 데이터로 만든 Date 객체를 return
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param minute
     * @param second
     * @return
     */
    public static Date getDate(int year, int month, int day, int hour,
            int minute, int second) {

        GregorianCalendar cal = new GregorianCalendar(timeZone, Locale.KOREAN);
        cal.set(year, month - 1, day, hour, minute, second);
        return cal.getTime();
    }

    /**
     * 현재 날짜를 출력
     * @param s
     * @return
     */
    public static String getDate(String s) {
        GregorianCalendar cal = new GregorianCalendar(timeZone, Locale.KOREAN);
        
        return cal.get(Calendar.YEAR)+s+
                (cal.get(Calendar.MONTH)+1)+s+
                cal.get(Calendar.DAY_OF_MONTH);
    }
    /**
     * 해당 월의 첫 번째 날짜를 구한다.
     * @param year
     * @param month
     * @param format
     * @return
     */
    public static String getCurMonthFirstDate(String year, String month,
            String format) {

        Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);

        int curYear = Integer.parseInt(year);
        int curMonth = Integer.parseInt(month);

        cal.set(curYear, curMonth - 1, 1);
        int curMinDay = cal.getActualMinimum(Calendar.DATE);

        Date curDate = DateUtil.getDate(curYear, curMonth, curMinDay, 0, 0, 0);

        return DateUtil.dateToString(curDate, format);
    }
    
    /**
     * 해당 월의 마지막 날짜를 구한다.
     * @param year
     * @param month
     * @param format
     * @return
     */
    public static String getCurMonthLastDate(String year, String month, String format)
    {
        Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);

        int curYear = Integer.parseInt(year);
        int curMonth = Integer.parseInt(month);

        cal.set(curYear, curMonth - 1, 1);
        int curMinDay = cal.getActualMaximum(Calendar.DATE);

        Date curDate = DateUtil.getDate(curYear, curMonth, curMinDay, 0, 0, 0);

        return DateUtil.dateToString(curDate, format);
    }
    
    /**
     * 해당 월의 마지막 날짜를 구한다.
     * @param year
     * @param month
     * @param format
     * @return
     */
    public static int getCurMonthLastDate(int year, int month)
    {
        Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);
        
        cal.set(year, month - 1, 1);
        int curMinDay = cal.getActualMaximum(Calendar.DATE);
        
        
        return curMinDay;
    }

    /**
     * 현재 요일을 구한다.
     * @return
     */
    public static String getDayEng(Calendar cal)
    {
        int curDay = cal.get(Calendar.DAY_OF_WEEK);
        String[] days = {"", "SU", "MO", "TU", "WE", "TH", "FR", "SA"};
        return days[curDay];
    }
    /**
     * 현재 요일을 구한다.
     * @return
     */
    public static String getDay()
    {
        Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);
        int curDay = cal.get(Calendar.DAY_OF_WEEK);
        String[] days = {"", "일", "월", "화", "수", "목", "금", "토"};
        return days[curDay];
    }
    
    /**
     * 특정 날짜의 요일을 구한다.
     * @param year
     * @param month ( 1일 부터 시작하는 값.)
     * @param day
     * @return
     */
    public static String getDayOfDate( int year, int month, int day)
    {
        Calendar cal= Calendar.getInstance ();
        
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month-1);
        cal.set(Calendar.DATE, day);
        
        int curDay = cal.get(Calendar.DAY_OF_WEEK);
        String[] days = {"", "일", "월", "화", "수", "목", "금", "토"};
        return days[curDay];
    }

    /**
     * 현재 요일을 숫자로 구한다.
     * @return
     */
    public static int getIntDay()
    {
        Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);
        return cal.get(Calendar.DAY_OF_WEEK);
    }
    
    /**
     * 년, 월, 일을 입력 받아 Calendar 객체를 return
     * @param year
     * @param month
     * @param day
     * @return
     */
    public static Calendar getCalendar(int year, int month, int day)
    {
        GregorianCalendar cal = new GregorianCalendar(timeZone, Locale.KOREAN);
        cal.set(year, month - 1, day, 0, 0, 0);
        return cal;
    }
    
    /**
     * millisecond를 Calendar 객체로 만들어 return
     * @param millisecond
     * @return
     */
    public static Calendar convertFromMilli(long millisecond)
    {
        Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);
        cal.setTimeInMillis(millisecond);
        return cal;
    }
    
    /**
     * 지정된 scope에 따라 millisends를 return
     * @param scope SCOPE_DAILY: 하루, SCOPE_MONTHLY: 한 달
     * @param cal 년, 월까지 지정된 Calendar 객체
     * @return scope으로 지정된 기간만큼의 millisends 
     */
    public static long[] makeScope(long millisecond, int scope)
    {
        Calendar cal = DateUtil.convertFromMilli(millisecond);
        printCalendar("start: ", cal);
        
        long[] reVal = new long[2];
        switch(scope)
        {
        case SCOPE_DAILY:
            setDayCalendar(cal, Calendar.AM);
            reVal[0] = cal.getTimeInMillis();
            setDayCalendar(cal, Calendar.PM);
            reVal[1] = cal.getTimeInMillis();
            break;
        case SCOPE_WEEK:
            setDayCalendar(cal, Calendar.PM);
            reVal[1] = cal.getTimeInMillis();
            cal.add(Calendar.DATE, -6);
            setDayCalendar(cal, Calendar.AM);
            reVal[0] = cal.getTimeInMillis();
            break;
        case SCOPE_MONTHLY_ONE:
        case SCOPE_MONTHLY_TWO:
        case SCOPE_MONTHLY_THREE:
        case SCOPE_MONTHLY_SIX:
            setDayCalendar(cal, Calendar.PM);
            reVal[1] = cal.getTimeInMillis();
            cal = getPreMonthCalendar(scope, cal);
            setDayCalendar(cal, Calendar.AM);
            printCalendar("end: ", cal);
            reVal[0] = cal.getTimeInMillis();
            break;
        }
        return reVal;
    }
    
    /**
     * 지정된 scope에 따라 millisends를 return
     * @param scope SCOPE_DAILY: 하루, SCOPE_MONTHLY: 한 달
     * @param cal 년, 월까지 지정된 Calendar 객체
     * @return scope으로 지정된 기간만큼의 millisends 
     */
    public static long[] getMilliseconds(int year, int month, int day, int scope)
    {
        Calendar cal = DateUtil.getCalendar(year, month, day);
//        printCalendar(cal, "getMilliseconds " + scope);
        
        long[] reVal = new long[2];
        switch(scope)
        {
        case SCOPE_DAILY:
            setDayCalendar(cal, Calendar.AM);
            reVal[0] = cal.getTimeInMillis();
            setDayCalendar(cal, Calendar.PM);
            reVal[1] = cal.getTimeInMillis();
            break;
        case SCOPE_MONTHLY_ONE:
            cal.set(Calendar.DAY_OF_MONTH, 1);
            setDayCalendar(cal, Calendar.AM);
            reVal[0] = cal.getTimeInMillis();
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DATE));
            setDayCalendar(cal, Calendar.PM);
            reVal[1] = cal.getTimeInMillis();
        }
        return reVal;
    }
    
    /**
     * Calendar 객체의 달을 변경 
     * @param addMonth
     * @param am_pm
     * @param cal
     */
    public static Calendar getPreMonthCalendar(int dateScope, Calendar today)
    {
        int numberOfThisMonth = -30;
        switch(dateScope)
        {
        case SCOPE_MONTHLY_ONE:
            numberOfThisMonth = (numberOfThisMonth * 1) + 1;
            break;
        case SCOPE_MONTHLY_TWO:
            numberOfThisMonth = (numberOfThisMonth * 2) + 1;
            break;
        case SCOPE_MONTHLY_THREE:
            numberOfThisMonth = (numberOfThisMonth * 3) + 1;
            break;
        case SCOPE_MONTHLY_SIX:
            numberOfThisMonth = (numberOfThisMonth * 6) + 1;
            break;
        }
        today.add(Calendar.DAY_OF_MONTH, numberOfThisMonth);
        setDayCalendar(today, Calendar.AM);
        return today;
    }
    
    /**
     * 입력받은 calendar객체를 오전 0시 0분 0초 또는 오후 23시 59분 59초로 세팅 해 준다.
     * @param cal 
     * @param am_pm
     */
    public static void setDayCalendar(Calendar cal, int am_pm)
    {
        if(am_pm == Calendar.AM)
        {
            cal.set(Calendar.AM_PM, Calendar.AM);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
        }
        else if(am_pm == Calendar.PM)
        {
            cal.set(Calendar.AM_PM, Calendar.PM);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            cal.set(Calendar.MILLISECOND, 59);
        }
    }
    
    /**
     * 입력받은 2개의 milliseconds가 같은 날인지 알아본다.
     * @param firstTime 비교 대상 시간
     * @param secondTime 비교 대상 시간
     * @return true: 2개의 millisecond가 같은 날에 속한다. false: 2개의 milliseconds가 다른 날에 속한다.
     */
    public static boolean isSameDay(long firstTime, long secondTime)
    {
        boolean sameDay = false;
        Calendar fCal = DateUtil.convertFromMilli(firstTime);
        long dayScope[] = getMilliseconds(fCal.get(Calendar.YEAR), 
                fCal.get(Calendar.MONTH) + 1, 
                fCal.get(Calendar.DAY_OF_MONTH), 
                SCOPE_DAILY);
        if(secondTime >= dayScope[0] && secondTime <= dayScope[1])
            sameDay = true;
        return sameDay;
    }
    
    /**
     * 같은 날짜인지 알려주는 flag
     * @param year
     * @param month
     * @param day
     * @param secondTime
     * @return
     */
    public static boolean isSameDay(int year, int month, int day, long secondTime)
    {
        Calendar cal = DateUtil.getCalendar(year, month, day);
        boolean sameDay = false;
        long dayScope[] = getMilliseconds(cal.get(Calendar.YEAR), 
                cal.get(Calendar.MONTH) + 1, 
                cal.get(Calendar.DAY_OF_MONTH), 
                SCOPE_DAILY);
        if(secondTime >= dayScope[0] && secondTime <= dayScope[1])
            sameDay = true;
        return sameDay;
    }
    
    /**
     * Calendar 객체에 저장되어 있는 날짜를 출력
     * @param cal
     */
    public static void printCalendar(String tagAdder, Calendar cal)
    {
        Logg.d(tagAdder + cal.get(Calendar.YEAR) + "." 
                + (cal.get(Calendar.MONTH) + 1) + "." 
                + cal.get(Calendar.DAY_OF_MONTH) + "."
                + cal.get(Calendar.HOUR_OF_DAY) + "."
                + cal.get(Calendar.MINUTE) + "."
                + cal.get(Calendar.SECOND) + "."
                + cal.get(Calendar.MILLISECOND));
    }
    
    /**
     * 영문 달 이름을 입력받아 숫자로 변환 시킨다.
     * @param m
     * @return
     */
    public static String getMonthForInt(int m) {
        String month = "invalid";
        String[] monthName = { "January", "February", "March", "April", "May", "June", "July",
                "August", "September", "October", "November", "December" };
        if (m >= 0 && m <= 11 ) {
            month = monthName[m];
        }
        return month;
    }
    
    /**
     * Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format 
     * @return String representing date in specified format
     */
    public static String getDate(long milliSeconds, String dateFormat)
    {
        DateFormat formatter = new SimpleDateFormat(dateFormat);

         Calendar calendar = Calendar.getInstance();
         calendar.setTimeInMillis(milliSeconds);
         return formatter.format(calendar.getTime());
    }

    
}