package kr.wisestone.happytong.data.util;

import java.util.ArrayList;

import kr.wisestone.happytong.R;
import kr.wisestone.happytong.data.common.CommonData;
import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.db.HappytongDBHelper;
import kr.wisestone.happytong.data.db.SharedPrefManager;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.protocol.FriendProtocol;
import kr.wisestone.happytong.data.wrapper.CallBackParamWrapper;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.Logg;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

public class FriendSynchronizer {

	public static final int RET_NORMAL = 0x100;
	public static final int RET_NETWORK_FAIL = 0x200;
	public static final int RET_NO_DATA = 0x300;

	public interface SyncListener {
		void onStartLoading();

		void onEndLoading();

		void onEndSynchronize(int retValue);
	}

	Context mContext;
	SyncListener mListener;

	private HappytongDBHelper mDbHelper;

	public FriendSynchronizer(Context mContext, SyncListener mListener) {
		super();
		this.mContext = mContext;
		this.mListener = mListener;

		startSynchronize();
	}

	private void startSynchronize() {
		Logg.d("startSynchronize");

		if (mListener != null) {
			mListener.onStartLoading();
		}

		initDB();
	}

	private void initDB() {
		mDbHelper = new HappytongDBHelper(mContext);
		mDbHelper.initDB();
		importContact();
	}

	private void importContact() {
		String phoneNumber = Utils.getContactList(mContext);
		new FriendProtocol(friendSyncNetworkOk, friendSyncNetworkFail).friendWithNameSync(CallBackParamWrapper.CODE_FRIEND_FRIEND_SYNC, SharedPrefManager.getLoginEmail(mContext),
				phoneNumber);
	}

	private void endSynchronize(final int retValue) {
		Logg.d("endSynchronize");

		if (retValue == RET_NORMAL) {
			// 동기화에 대한 설정이되지 않았을 경우, 동기화 처리 한번 하고 이후 처리하지 않도록함
			// ex. 회원가입 - 동기화 - 동기화오프 - 재진입 - 동기화X 이처리를 위한 동작
			SharedPrefManager spm = SharedPrefManager.getInstance(mContext);
			if (TextUtils.isEmpty(spm.getPrefAutoSync())) {
				spm.setPrefAutoSync(CommonData.AUTO_SYNC_OFF);
			}
		}

		if (mDbHelper != null) {
			mDbHelper.close();
			mDbHelper = null;
		}

		if (mListener != null) {
			mListener.onEndSynchronize(retValue);
		}
	}

	private IWiseCallback friendSyncNetworkOk = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			mListener.onEndLoading();
			if (param.param1 != null) {

				@SuppressWarnings("unchecked")
				ArrayList<FriendInfo> arrFriendData = (ArrayList<FriendInfo>) param.param1;

				ContactSync mContactSync = new ContactSync(arrFriendData);
				mContactSync.execute();

			} else {
				endSynchronize(RET_NO_DATA);
			}
			return false;
		}
	};

	// XXX USE Customized NetworkFail
	private IWiseCallback friendSyncNetworkFail = new IWiseCallback() {
		@Override
		public boolean WiseCallback(CallbackParam param) {
			mListener.onEndLoading();
			endSynchronize(RET_NETWORK_FAIL);
			return false;
		}
	};

	public class ContactSync extends AsyncTask<Void, Integer, String> {

		private ProgressDialog mDialog;
		ArrayList<FriendInfo> mData;

		public ContactSync(ArrayList<FriendInfo> data) {
			mData = data;
		}

		@Override
		protected String doInBackground(Void... params) {
			InsertFriendInfo();
			return null;
		}

		@Override
		protected void onCancelled() {
			onPostExecute(null);
		}

		@Override
		protected void onPostExecute(String result) {
			if (mDialog.isShowing()) {
				mDialog.dismiss();
			}

			endSynchronize(RET_NORMAL);
		}

		@Override
		protected void onPreExecute() {
			mDialog = new ProgressDialog(mContext);
			mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mDialog.setTitle(R.string.info);
			mDialog.setMessage(mContext.getString(R.string.txt_doing_contact_sync));
			mDialog.setCancelable(false);
			mDialog.setCanceledOnTouchOutside(false);
			mDialog.setMax(mData.size());
			mDialog.setProgress(0);
			mDialog.show();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			mDialog.setProgress(values[0]);
		}

		public void InsertFriendInfo() {
			ArrayList<FriendInfo> list = (ArrayList<FriendInfo>) mData;

			for (int i = 0; i < list.size(); i++) {
			    FriendInfo dbData = list.get(i);
			    
                if(dbData.GetName()!=null){ 
                    dbData.SetName( AESEncryption.decrypt(dbData.GetName()));
                }
			    
				mDbHelper.InsertQueryForFriendInfo(dbData.getNameValueList());
				publishProgress(i);
			}
		}
	}
}
