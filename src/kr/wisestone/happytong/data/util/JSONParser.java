package kr.wisestone.happytong.data.util;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {
	JSONObject jsonobject = null;
	
	public JSONParser()
	{}
	
	public void setJsonObeject( String jsonString )
	{
		try {
			this.jsonobject = new JSONObject( jsonString );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Object getObjectData( String key )
	{
		Object returnValue = null;
		if( jsonobject == null )
		{
			returnValue = null;
		}
		
		try {
			returnValue = jsonobject.get( key );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public String getStringData( String key )
	{
		String returnValue = null;
		if( jsonobject == null )
		{
			returnValue = null;
		}
		
		try {
			returnValue = jsonobject.getString( key );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public int getIntegerData( String key )
	{
		int returnValue = -1;
		if( jsonobject == null )
		{
			returnValue = -1;
		}
		
		try {
			returnValue = jsonobject.getInt( key );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public boolean getBooleanData( String key )
	{
		boolean returnValue = false;
		if( jsonobject == null )
		{
			returnValue = false;
		}
		
		try {
			returnValue = jsonobject.getBoolean( key );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public long getLongData( String key )
	{
		long returnValue = 0;
		if( jsonobject == null )
		{
			returnValue = 0;
		}
		
		try {
			returnValue = jsonobject.getLong( key );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnValue;
	}
	
	public double getDoubleData( String key )
	{
		double returnValue = 0;
		if( jsonobject == null )
		{
			returnValue = 0;
		}
		
		try {
			returnValue = jsonobject.getDouble( key );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnValue;
	}
}

