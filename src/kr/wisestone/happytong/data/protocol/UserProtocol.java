package kr.wisestone.happytong.data.protocol;

import java.util.ArrayList;
import java.util.List;

import kr.wisestone.happytong.data.dataset.UserInfoData;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.EventCode;
import kr.wisestone.happytong.data.network.HttpConnection;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.util.JSONParser;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.data.wrapper.URLWrapper;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.graphics.Bitmap;


public class UserProtocol extends AProtocol
{
    public UserProtocol(IWiseCallback iOK, IWiseCallback iFail)
    {
        super(iOK, iFail);
    }
    
    class AsyncGetProfileAdd extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getProfileAdd();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncGetProfileGet extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getProfileGet();
                    String strJson = con.openPost(fullAddress, paramData);
                   
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        if((jsonObj.isNull( JsonKeyWrapper.none ) == false)){
                            param.param1 = null;
                        }
                        else {
                            JSONParser jParser = null;
                            
                            UserInfoData mProfileData = new UserInfoData();
                            jParser = new JSONParser();
                            
                            jParser.setJsonObeject( strJson );

                            mProfileData.setPhoneNumber(jParser.getStringData(JsonKeyWrapper.phoneNumber));
                            mProfileData.setNickName(jParser.getStringData(JsonKeyWrapper.nickName));
                            mProfileData.setUserImageUrl(jParser.getStringData(JsonKeyWrapper.userImageURL));
                            mProfileData.setDate(jParser.getStringData(JsonKeyWrapper.updateDate));
                            mProfileData.setFbAd(jParser.getStringData(JsonKeyWrapper.facebookAd));
                            mProfileData.setLocationBigName(jParser.getStringData(JsonKeyWrapper.locationBigName));
                            mProfileData.setLocationBigCode(jParser.getStringData(JsonKeyWrapper.locationBigCode));
                            mProfileData.setLocationSmallName(jParser.getStringData(JsonKeyWrapper.locationSmallName));
                            mProfileData.setLocationSmallCode(jParser.getStringData(JsonKeyWrapper.locationSmallCode));
                                                            
                            Logg.d( "1 = " + jParser.getStringData(JsonKeyWrapper.phoneNumber) );
                            Logg.d( "2 = " + jParser.getStringData(JsonKeyWrapper.nickName) );
                            Logg.d( "3 = " + jParser.getStringData(JsonKeyWrapper.userImageURL) );
                            Logg.d( "4 = " + jParser.getStringData(JsonKeyWrapper.updateDate) );
                            Logg.d( "5 = " + jParser.getStringData(JsonKeyWrapper.facebookAd) );
                            Logg.d( "6 = " + jParser.getStringData(JsonKeyWrapper.locationBigName) );
                            Logg.d( "7 = " + jParser.getStringData(JsonKeyWrapper.locationBigCode) );
                            Logg.d( "8 = " + jParser.getStringData(JsonKeyWrapper.locationSmallName) );
                            Logg.d( "9 = " + jParser.getStringData(JsonKeyWrapper.locationSmallCode) );

                            param.param1 = mProfileData;
                        }
                    }                    
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
 
    
    class GetProfileEdit extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getProfileEdit();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class GetProfilePhotoEdit extends AAsyncProtocol {
    	
    	@Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                //Object objTag, String id,String imgFileName, Bitmap bm
                //Object paramObjectTag 	= (Object)data[0];
                String paramId 			= (String)data[0];
                String paramImgFileName = (String)data[1];
                Bitmap paramBitmap 		= (Bitmap)data[2];
                
                Logg.d("GetProfilePhotoEdit "+paramId+" / " +paramImgFileName+" / "+paramBitmap);
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getProfilePhotoEdit();
                    //String strJson = con.openPost(fullAddress, paramData);
                    //openPost(String url, String value, Bitmap bitmap)
                    String strJson = con.openPost(fullAddress, paramId, paramImgFileName,paramBitmap);
                    
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        if((jsonObj.isNull( JsonKeyWrapper.none ) == false)){
                            param.param1 = null;
                        }
                        else {
                            JSONParser jParser = null;
                            
                            UserInfoData mProfileData = new UserInfoData();
                            jParser = new JSONParser();
                            
                            jParser.setJsonObeject( strJson );

                            mProfileData.setUserImageUrl(jParser.getStringData(JsonKeyWrapper.userImageURL));
                                                            
                            Logg.d( "1 = " + jParser.getStringData(JsonKeyWrapper.userImageURL) );

                            param.param1 = mProfileData;
                        }
                    }                    
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
 
    class AsyncGetProfilePhotoDel extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getProfilePhotoDel();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    
    class GetProfileResign extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getProfileResign();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class GetProfileUnBlock extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getFriendUnBlock();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class GetFacebookAd extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getProfileFacebookAd();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncGetCompaniesProfile extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getCompaniesProfile();
                    String strJson = con.openPost(fullAddress, paramData);
                   
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.fail )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        JSONParser jParser = null;
                        
                        UserInfoData mProfileData = new UserInfoData();
                        jParser = new JSONParser();
                        
                        jParser.setJsonObeject( strJson );

                        mProfileData.setUserImageUrl(jParser.getStringData(JsonKeyWrapper.companyImageURL));
                        mProfileData.setCompanyName(jParser.getStringData(JsonKeyWrapper.companyName));
                        mProfileData.setCompanyNumber(jParser.getStringData(JsonKeyWrapper.callNumber));
                        mProfileData.setAddress(jParser.getStringData(JsonKeyWrapper.companyAddress));
                        mProfileData.setCompanyBusinessNumber(jParser.getStringData(JsonKeyWrapper.businessNumber));
                        mProfileData.setLocationBigName(jParser.getStringData(JsonKeyWrapper.locationBigName));
                        mProfileData.setLocationBigCode(jParser.getStringData(JsonKeyWrapper.locationBigCode));
                        mProfileData.setLocationSmallName(jParser.getStringData(JsonKeyWrapper.locationSmallName));
                        mProfileData.setLocationSmallCode(jParser.getStringData(JsonKeyWrapper.locationSmallCode));
                        mProfileData.setCompanyCropImageURL(jParser.getStringData(JsonKeyWrapper.companyCropImageURL));
                         
                        Logg.d( "1 = " + mProfileData.getUserImageUrl() );
                        Logg.d( "2 = " + mProfileData.getCompanyName() );
                        Logg.d( "3 = " + mProfileData.getCompanyNumber() );
                        Logg.d( "4 = " + mProfileData.getAddress() );
                        Logg.d( "5 = " + mProfileData.getCompanyBusinessNumber() );
                        Logg.d( "6 = " + mProfileData.getLocationBigName() );
                        Logg.d( "7 = " + mProfileData.getLocationBigCode() );
                        Logg.d( "8 = " + mProfileData.getLocationSmallName() );
                        Logg.d( "9 = " + mProfileData.getLocationSmallCode() );
                        Logg.d( "10 = " + mProfileData.getCompanyCropImageURL() );

                        param.param1 = mProfileData;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncCompaniesInfoUpdate extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.companiesInfoUpdate();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.fail )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }    
    
    class AsyncCompaniesImagePathUpdate extends AAsyncProtocol {
        
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                //Object objTag, String id,String imgFileName, Bitmap bm
                //Object paramObjectTag     = (Object)data[0];
                String paramId          = (String)data[0];
                String parambusinessNumber = (String)data[1];
                Bitmap paramBitmap      = (Bitmap)data[2];
                
                Logg.d("GetProfilePhotoEdit "+paramId+" / " +parambusinessNumber+" / "+paramBitmap);
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.companiesImagePathUpdate();
                    String strJson = con.openPost(fullAddress, paramId, parambusinessNumber, parambusinessNumber, paramBitmap);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();                    
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }   
    
    class AsyncCompaniesImagePathDelete extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.companiesImagePathDelete();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }    
    
    public void getProfileAdd(Object objTag, String id, String password, String deviceId, 
            String phoneNumber, String deviceType, String nickname, String locationSmallCode )
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("password", password));
        dataValue.add(new BasicNameValuePair("deviceID", deviceId));
        dataValue.add(new BasicNameValuePair("phoneNumber", phoneNumber));
        dataValue.add(new BasicNameValuePair("deviceType", deviceType));
        dataValue.add(new BasicNameValuePair("nickName", nickname));
        dataValue.add(new BasicNameValuePair("locationSmallCode", locationSmallCode));
        
        startAsyncTask(objTag, new AsyncGetProfileAdd(), dataValue);
    }
    
    public void getProfileGet(Object objTag, String id)
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
    
        startAsyncTask(objTag, new AsyncGetProfileGet(), dataValue);
    }
    
    public void getProfileEdit(Object objTag, String id, String password, String deviceId, 
            String phoneNumber, String deviceType, String nickname )
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("password", password));
        dataValue.add(new BasicNameValuePair("deviceID", deviceId));
        dataValue.add(new BasicNameValuePair("phoneNumber", phoneNumber));
        dataValue.add(new BasicNameValuePair("deviceType", deviceType));
        dataValue.add(new BasicNameValuePair("nickName", nickname));
    
        startAsyncTask(objTag, new GetProfileEdit(), dataValue);
    }

    public void getProfilePhotoEdit(Object objTag, String id,String imgFileName, Bitmap bm)
    {
//        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
//        dataValue.add(new BasicNameValuePair("id", id));
//        dataValue.add(new BasicNameValuePair("photo", imgFileName));
    
        startAsyncTask(objTag, new GetProfilePhotoEdit(), id,imgFileName, bm );
    }
    
    public void getProfilePhotoDel(Object objTag, String id)
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
    
        startAsyncTask(objTag, new AsyncGetProfilePhotoDel(), dataValue);
    }
    
    public void getProfileResign(Object objTag, String id,String password)
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("password", password));
        
        startAsyncTask(objTag, new GetProfileResign(), dataValue);
    }
    
    public void getProfileUnBlock(Object objTag, String id,String blockId)
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("blockID", blockId));
        
        startAsyncTask(objTag, new GetProfileUnBlock(), dataValue);
    }
    
    public void getProfileFacebookAd(Object objTag, String id)
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
    
        startAsyncTask(objTag, new GetFacebookAd(), dataValue);
    }
    
    public void getCompaniesProfile(Object objTag, String id)
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
    
        startAsyncTask(objTag, new AsyncGetCompaniesProfile(), dataValue);
    }
    
    public void companiesInfoUpdate(Object objTag, String name, String businessNumber, String callNumber, String address, String id)
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.name, name));
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.businessNumber, businessNumber));
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.callNumber, callNumber));
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.address, address));
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
    
        startAsyncTask(objTag, new AsyncCompaniesInfoUpdate(), dataValue);
    }
    
    public void companiesImagePathUpdate(Object objTag, String id, String businessNumber, Bitmap bm)
    {
        startAsyncTask(objTag, new AsyncCompaniesImagePathUpdate(), id, businessNumber, bm );
    }  

    public void companiesImagePathDelete(Object objTag, String id, String businessNumber)
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.businessNumber, businessNumber));
    
        startAsyncTask(objTag, new AsyncCompaniesImagePathDelete(), dataValue);
    }
}
