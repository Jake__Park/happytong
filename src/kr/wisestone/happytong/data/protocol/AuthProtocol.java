package kr.wisestone.happytong.data.protocol;

import java.util.ArrayList;
import java.util.List;

import kr.wisestone.happytong.data.dataset.AuthInfoData;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.EventCode;
import kr.wisestone.happytong.data.network.HttpConnection;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.data.wrapper.URLWrapper;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class AuthProtocol extends AProtocol
{
    public AuthProtocol( IWiseCallback iOK, IWiseCallback iFail )
    {
        super( iOK, iFail );
        // TODO Auto-generated constructor stub
    }
    
    class AsyncAuthLogin extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getAuthLogin();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        AuthInfoData mData = new AuthInfoData();
                        
                        mData.setFreeStampCount( jsonObj.getString( JsonKeyWrapper.freeOwlCount ) );
                        mData.setPaidStampCount( jsonObj.getString( JsonKeyWrapper.paidOwlCount) );
                        mData.setRemainGoldEgg( jsonObj.getString( JsonKeyWrapper.remainGoldEgg) );
                        
                        if(jsonObj.has(JsonKeyWrapper.freeOwl) && jsonObj.getString(JsonKeyWrapper.freeOwlCount).equals(JsonKeyWrapper.success)) {
                        	Logg.d("auth_login jsonObj.has(JsonKeyWrapper.KEY_AUTH_FREE_STAMP_RECEIVE) && jsonObj.getString(JsonKeyWrapper.KEY_AUTH_FREE_STAMP_RECEIVE).equals(JsonKeyWrapper.success) - true");
                        	mData.setFreeStampReceive(true);
                        } else {
                        	Logg.d("auth_login jsonObj.has(JsonKeyWrapper.KEY_AUTH_FREE_STAMP_RECEIVE) && jsonObj.getString(JsonKeyWrapper.KEY_AUTH_FREE_STAMP_RECEIVE).equals(JsonKeyWrapper.success) - false");
                        }
                        
                        mData.setNickName( jsonObj.getString( JsonKeyWrapper.nickName ) );
                        mData.setCompanyState( jsonObj.getString( JsonKeyWrapper.companyState ) );
                        if(mData.getCompanyState().equals("1")){
                        	mData.setOneTimeCoupon( jsonObj.getString( JsonKeyWrapper.oneTimeCoupon ) );
                        	mData.setNoLimitTimeCoupon( jsonObj.getString( JsonKeyWrapper.noLimitTimeCoupon ) );
                        	mData.setBusinessNumber( jsonObj.getString( JsonKeyWrapper.businessNumber ) );
                        }
                        
                        param.param1 = mData;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncAuthEmail extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getAuthEmail();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    Logg.e( strJson );
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncAuthResetPassword extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getAuthResetPassword();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    Logg.e( strJson );
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncAuthProvision extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getAuthPovision();

                    String strJson = con.openPost(fullAddress);
                    
                    Logg.e( strJson );
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = jsonObj.getString( JsonKeyWrapper.success);
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    } 
    
    class AsyncAuthPersonal extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getAuthPersonal();

                    String strJson = con.openPost(fullAddress);
                    
                    Logg.e( strJson );
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = jsonObj.getString( JsonKeyWrapper.success);
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    } 

    class AsyncAuthCheckId extends AAsyncProtocol {
    	@Override
    	protected CallbackParam doInBackground(Object... data) {
    		if(!isCancelled())
    		{
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
    			try
    			{
    				String fullAddress = URLWrapper.getAuthCheckId();
    				
                    String strJson = con.openPost(fullAddress, paramData);
    				
    				Logg.e( strJson );
    				
    				JSONObject jsonObj = new JSONObject(strJson);
    				
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = jsonObj.getString( JsonKeyWrapper.success);
                    }
    				
    				if(con.getError() != EventCode.S_SUCCESS)
    					param.param3 = con.getError();
    			}
    			catch(Exception e)
    			{
    				Logg.e(e);
    				e.printStackTrace();
    				param.param3 = EventCode.E_INTERNAL_ERROR;
    			}
    		}
    		return param;
    	}
    } 
    
    class AsyncAuthGetPublicKey extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getAuthPublicKey();
                    
                    String strJson = con.openPost(fullAddress);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        String mData = jsonObj.getString( JsonKeyWrapper.publicKey );
                        
                        param.param1 = mData;
                    }

                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncCheckVersionForAndroid extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getCheckVersionForAndroid();
                    
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    Logg.e( strJson );
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = jsonObj.getString( JsonKeyWrapper.isUpgrade);
                    }                    
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncAuthIdDeviceCheck extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getAuthIdDeviceCheck();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    param.param1 = "success";
                    
                    if(jsonObj.has(JsonKeyWrapper.fail)) {
                    	param.param1 = JsonKeyWrapper.fail;
                    }
                    if(jsonObj.has(JsonKeyWrapper.notDevice)) {
                    	param.param1 = JsonKeyWrapper.notDevice;
                    }
                    if(jsonObj.has(JsonKeyWrapper.notUser)) {
                    	param.param1 = JsonKeyWrapper.notUser;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    public void authLogin(Object objTag, String id, String password, String phoneNumber, String deviceId, String deviceType)
    {
        Logg.d( "authLogin" );
        
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("password", password));
        dataValue.add(new BasicNameValuePair("phoneNumber", phoneNumber));
        dataValue.add(new BasicNameValuePair("deviceID", deviceId));
        dataValue.add(new BasicNameValuePair("deviceType", deviceType));

        startAsyncTask(objTag, new AsyncAuthLogin(), dataValue);
    }
    
    public void authEmail(Object objTag, String id, String authNumber)
    {
        Logg.d( "authEmail" );
        
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("authNumber", authNumber));

        startAsyncTask(objTag, new AsyncAuthEmail(), dataValue);
    }
    
    public void authCheckId(Object objTag, String id)
    {
    	Logg.d( "authCheckId" );
    	
    	List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
    	dataValue.add(new BasicNameValuePair("id", id));
    	
    	startAsyncTask(objTag, new AsyncAuthCheckId(), dataValue);
    }
    
    public void authResetPassword(Object objTag, String id, String password)
    {
        Logg.d( "authResetPassword" );
        
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("password", password));

        startAsyncTask(objTag, new AsyncAuthResetPassword(), dataValue);
    }
    
    public void authProvision(Object objTag)
    {
        Logg.d( "authProvision" );
        startAsyncTask(objTag, new AsyncAuthProvision());
    }
    
    public void authPersonal(Object objTag)
    {
        Logg.d( "authPersonal" );

        startAsyncTask(objTag, new AsyncAuthPersonal());
    }
    
    public void authGetPublicKey(Object objTag)
    {
        Logg.d( "authGetPublicKey" );

        startAsyncTask(objTag, new AsyncAuthGetPublicKey());
    }
    
    public void checkVersionForAndroid(Object objTag, String version)
    {
        Logg.d( "checkVersionForAndroid" );

        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("version", version));

        startAsyncTask(objTag, new AsyncCheckVersionForAndroid(), dataValue);
    }
    
    public void authIdDeviceCheck(Object objTag, String id, String deviceID) {
    	Logg.d( "authIdDeviceCheck" );

    	List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("deviceID", deviceID));

        startAsyncTask(objTag, new AsyncAuthIdDeviceCheck(), dataValue);
    	
    }

}
