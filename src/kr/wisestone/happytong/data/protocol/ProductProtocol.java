package kr.wisestone.happytong.data.protocol;

import java.util.ArrayList;
import java.util.List;

import kr.wisestone.happytong.data.dataset.ProductInfoData;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.EventCode;
import kr.wisestone.happytong.data.network.HttpConnection;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.util.JSONParser;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.data.wrapper.URLWrapper;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProductProtocol extends AProtocol
{
    public ProductProtocol( IWiseCallback iOK, IWiseCallback iFail )
    {
        super( iOK, iFail );
        // TODO Auto-generated constructor stub
    }
    
    class AsyncItemsGetPop extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getItemsGetPop();

                    String strJson = con.openPost(fullAddress, paramData);

                    try {
                        ArrayList<ProductInfoData> arrProductData = new ArrayList<ProductInfoData>();
                        
                        JSONObject jsonObj = new JSONObject(strJson);
                        
                        if(jsonObj.has( JsonKeyWrapper.error )){
                            param.param3 = EventCode.E_FAIL;
                            param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                        } else {
                            if(jsonObj.isNull( JsonKeyWrapper.none ) == false){
                                //[SDJ 131408] 데이터 없음
                                param.param1 = arrProductData;
                                return param;
                            }
                            
                            JSONArray couponList = jsonObj.getJSONArray(JsonKeyWrapper.couponList);
                            
                            JSONParser jParser = null;

                            for( int i = 0 ; i < couponList.length() ; i ++ )
                            {
                                ProductInfoData mData = new ProductInfoData();
                                jParser = new JSONParser();
                                jParser.setJsonObeject( couponList.getString(i) );
                                if( jParser.getStringData(JsonKeyWrapper.productName).length() > 0 )
                                {
                                    mData.setCouponSEQ(jParser.getStringData(JsonKeyWrapper.couponSEQ));
                                    mData.setImageURL(jParser.getStringData(JsonKeyWrapper.couponImageURL));
                                    mData.setCouponType(jParser.getStringData(JsonKeyWrapper.couponType));
                                    mData.setPrice(jParser.getStringData(JsonKeyWrapper.price));
                                    mData.setIsNew(jParser.getStringData(JsonKeyWrapper.isNew));
                                    mData.setProductName(jParser.getStringData(JsonKeyWrapper.productName));
                                    mData.setCouponExplanation(jParser.getStringData(JsonKeyWrapper.couponExplanation));
                                    mData.setIsOwned(jParser.getStringData(JsonKeyWrapper.isOwned));
                                    
                                    arrProductData.add(mData);
                                    
                                    Logg.d( "1 = " + jParser.getStringData(JsonKeyWrapper.couponSEQ) );
                                    Logg.d( "2 = " + jParser.getStringData(JsonKeyWrapper.couponImageURL) );
                                    Logg.d( "3 = " + jParser.getStringData(JsonKeyWrapper.couponType) );
                                    Logg.d( "4 = " + jParser.getStringData(JsonKeyWrapper.price) );
                                    Logg.d( "5 = " + jParser.getStringData(JsonKeyWrapper.isNew) );
                                    Logg.d( "6 = " + jParser.getStringData(JsonKeyWrapper.productName) );
                                }
                            }
                            param.param1 = arrProductData;
                        }
                        
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        Logg.e(e);
                        e.printStackTrace();
                        param.param3 = EventCode.E_INTERNAL_ERROR;
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncItemsGetNormal extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];

                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getItemsGetNormal();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    try {
                        ArrayList<ProductInfoData> arrGoldListData = new ArrayList<ProductInfoData>();
                        ArrayList<ProductInfoData> arrOwlListtData = new ArrayList<ProductInfoData>();
                        ArrayList<ProductInfoData> arrCouponListData = new ArrayList<ProductInfoData>();
                        
                        JSONObject jsonObj = new JSONObject(strJson);
                        JSONParser jParser = null;

                        if(jsonObj.has( JsonKeyWrapper.error )){
                            param.param3 = EventCode.E_FAIL;
                            param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                        } else {
                            if(jsonObj.isNull( JsonKeyWrapper.none ) == false){
                                //[SDJ 130408] 데이터 없음
                                param.param1 = arrGoldListData;
                                param.param2 = arrOwlListtData;
                                param.param3 = arrCouponListData;
                                return param;
                            }
                            
                            if(jsonObj.getInt( JsonKeyWrapper.goldListCount ) > 0) {
                                JSONArray goldList = jsonObj.getJSONArray(JsonKeyWrapper.goldList);
                                
                                for( int i = 0 ; i < goldList.length() ; i ++ )
                                {
                                    ProductInfoData mData = new ProductInfoData();
                                    jParser = new JSONParser();
                                    jParser.setJsonObeject( goldList.getString(i) );
                                    if( jParser.getStringData(JsonKeyWrapper.productID).length() > 0 )
                                    {
                                        mData.setProductID(jParser.getStringData(JsonKeyWrapper.productID));
                                        mData.setImageURL(jParser.getStringData(JsonKeyWrapper.imageURL));
                                        mData.setGoldCount(jParser.getStringData(JsonKeyWrapper.goldCount));
                                        mData.setPrice(jParser.getStringData(JsonKeyWrapper.price));
                                        mData.setProductName(jParser.getStringData(JsonKeyWrapper.productName));
                                        
                                        arrGoldListData.add(mData);
                                    }
                                }
                                
                            }
							
							//[SDJ 130703] 각 리스트 카운트 있을 경우만 데이터 처리
                            if(jsonObj.getInt( JsonKeyWrapper.owlListCount ) > 0) {
                                JSONArray owlList = jsonObj.getJSONArray(JsonKeyWrapper.owlList);
                                for( int i = 0 ; i < owlList.length() ; i ++ )
                                {
                                    ProductInfoData mData = new ProductInfoData();
                                    jParser = new JSONParser();
                                    jParser.setJsonObeject( owlList.getString(i) );
                                    if( jParser.getStringData(JsonKeyWrapper.owlCount).length() > 0 )
                                    {
                                        mData.setOwlCount(jParser.getStringData(JsonKeyWrapper.owlCount));
                                        mData.setOwlID(jParser.getStringData(JsonKeyWrapper.owlID));
                                        mData.setGoldCount(jParser.getStringData(JsonKeyWrapper.goldCount));
                                        mData.setImageURL(jParser.getStringData(JsonKeyWrapper.imageURL));
                                        mData.setProductName(jParser.getStringData(JsonKeyWrapper.productName));
                                        
                                        arrOwlListtData.add(mData);
                                    }
                                }
                            }
							
                            if(jsonObj.getInt( JsonKeyWrapper.couponListCount ) > 0) {
                                JSONArray couponList = jsonObj.getJSONArray(JsonKeyWrapper.couponList);
                                
                                for( int i = 0 ; i < couponList.length() ; i ++ )
                                {
                                    ProductInfoData mData = new ProductInfoData();
                                    jParser = new JSONParser();
                                    jParser.setJsonObeject( couponList.getString(i) );
                                    if( jParser.getStringData(JsonKeyWrapper.couponSEQ).length() > 0 )
                                    {
                                        mData.setCouponSEQ(jParser.getStringData(JsonKeyWrapper.couponSEQ));
                                        mData.setImageURL(jParser.getStringData(JsonKeyWrapper.couponImageURL));
                                        mData.setCropImageURL(jParser.getStringData(JsonKeyWrapper.couponCropImageURL));
                                        mData.setCouponType(jParser.getStringData(JsonKeyWrapper.couponType));
                                        mData.setPrice(jParser.getStringData(JsonKeyWrapper.price));
                                        mData.setIsNew(jParser.getStringData(JsonKeyWrapper.isNew));
                                        mData.setIsOwned(jParser.getStringData(JsonKeyWrapper.isOwned));
                                        mData.setIsFav(jParser.getStringData(JsonKeyWrapper.isFav));
                                        mData.setProductName(jParser.getStringData(JsonKeyWrapper.productName));
                                        mData.setCouponExplanation(jParser.getStringData(JsonKeyWrapper.couponExplanation));
                                        
                                        arrCouponListData.add(mData);
                                    }
                                }
                            }
                            
                            param.param1 = arrGoldListData;
                            param.param2 = arrOwlListtData;
                            param.param3 = arrCouponListData;
                            
                        }
                        
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        Logg.e(e);
                        e.printStackTrace();
                        param.param3 = EventCode.E_INTERNAL_ERROR;
                    }                    
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncGetCompaniesGoods extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];

                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getCompaniesGoods();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    try {
                        ArrayList<ProductInfoData> arrNormalData = new ArrayList<ProductInfoData>();
                        ArrayList<ProductInfoData> arrUnlimitedData = new ArrayList<ProductInfoData>();
                        
                        JSONObject jsonObj = new JSONObject(strJson);
                        JSONParser jParser = null;

                        if(jsonObj.has( JsonKeyWrapper.error )){
                            param.param3 = EventCode.E_FAIL;
                            param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                        } else {
                            if(jsonObj.isNull( JsonKeyWrapper.none ) == false){
                                //[SDJ 130408] 데이터 없음
                                param.param1 = arrNormalData;
                                param.param2 = arrUnlimitedData;
                                return param;
                            }

                            JSONArray dataList = jsonObj.getJSONArray(JsonKeyWrapper.LIST);
                            
                            for( int i = 0 ; i < dataList.length() ; i ++ )
                            {
                                ProductInfoData mData = new ProductInfoData();
                                jParser = new JSONParser();
                                jParser.setJsonObeject( dataList.getString(i) );
                                if( jParser.getStringData(JsonKeyWrapper.productID).length() > 0 )
                                {
                                    mData.setProductID(jParser.getStringData(JsonKeyWrapper.productID));
                                    mData.setImageURL(jParser.getStringData(JsonKeyWrapper.imageURL));
                                    mData.setProductName(jParser.getStringData(JsonKeyWrapper.productName));
                                    mData.setPrice(jParser.getStringData(JsonKeyWrapper.price));
                                    mData.setProductType(jParser.getStringData(JsonKeyWrapper.productType));
                                    mData.setIsNew(jParser.getStringData(JsonKeyWrapper.isNew));
                                    
                                }
                                
                                if(mData.getProductType().equals("1")){
                                    arrNormalData.add(mData);
                                } else {
                                    arrUnlimitedData.add(mData);
                                }
                            }
                            
                        }
                        
                        param.param1 = arrNormalData;
                        param.param2 = arrUnlimitedData;
                     
                        
                        
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        Logg.e(e);
                        e.printStackTrace();
                        param.param3 = EventCode.E_INTERNAL_ERROR;
                    }                    
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }    

    public void itemsGetPop(Object objTag, String id)
    {
        Logg.d( "getItemsGetPop" );

        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        
        startAsyncTask(objTag, new AsyncItemsGetPop(), dataValue);
    }
    
    public void itemsGetNormal(Object objTag, String couponLastSeq, String deviceType, String id) {
        Logg.d( "itemsGetNormal" );
        
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("couponLastSEQ", couponLastSeq));
        dataValue.add(new BasicNameValuePair("deviceType", deviceType));
        dataValue.add(new BasicNameValuePair("id", id));

        startAsyncTask(objTag, new AsyncItemsGetNormal(), dataValue);
    }    
    
    public void getCompaniesGoods(Object objTag, String deviceType, String id) {
        Logg.d( "getCompaniesGoods" );
        
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("deviceType", deviceType));
        dataValue.add(new BasicNameValuePair("id", id));

        startAsyncTask(objTag, new AsyncGetCompaniesGoods(), dataValue);
    }        
}
