package kr.wisestone.happytong.data.protocol;

import java.util.ArrayList;
import java.util.List;

import kr.wisestone.happytong.data.dataset.OrderCouponPurchased;
import kr.wisestone.happytong.data.dataset.PurchaseInfoData;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.EventCode;
import kr.wisestone.happytong.data.network.HttpConnection;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.util.JSONParser;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.data.wrapper.URLWrapper;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PurchaseProtocol extends AProtocol
{
    
    public PurchaseProtocol( IWiseCallback iOK, IWiseCallback iFail )
    {
        super( iOK, iFail );
        // TODO Auto-generated constructor stub
    }
    
    class AsyncOrderStampRemain extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];

                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getOrderStampRemain();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        PurchaseInfoData mData = new PurchaseInfoData();
                        
                        mData.setFreeStampCount( jsonObj.getString( JsonKeyWrapper.freeOwlCount ) );
                        mData.setPaidStampCount( jsonObj.getString( JsonKeyWrapper.paidOwlCount ) );
                        mData.setTotalStampCount( jsonObj.getString( JsonKeyWrapper.totalOwlCount ) );
                        mData.setGoldCount( jsonObj.getString( JsonKeyWrapper.remainGoldEgg ) );
                        
                        param.param1 = mData;
                        
                    }
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                        
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }

    
    class AsyncOrderCouponPurchased extends AAsyncProtocol 
    {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getOrderCouponPurchased();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);
						
	                    if(jsonObj.has( JsonKeyWrapper.error )){
	                        param.param3 = EventCode.E_FAIL;
	                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
	                    } else {
	                        if (jsonObj.isNull(JsonKeyWrapper.none) == false) {
	                            // [SDJ 131408] 데이터 없음
	                            param.param3 = EventCode.E_NONE;
	                        } else {
	                            JSONArray jsonArr = jsonObj.getJSONArray(JsonKeyWrapper.LIST);

	                            JSONParser jParser = null;
	                            Logg.d("arrCouponList size = " + jsonArr.length());

	                            List<OrderCouponPurchased> arrCouponData = new ArrayList<OrderCouponPurchased>();

	                            for (int i = 0; i < jsonArr.length(); i++) {
	                                OrderCouponPurchased mData = new OrderCouponPurchased();
	                                jParser = new JSONParser();
	                                jParser.setJsonObeject(jsonArr.getString(i));

	                                if (jParser.getStringData(JsonKeyWrapper.couponType).length() > 0) {
	                                    mData.couponSEQ = jParser.getStringData("couponSEQ");
	                                    mData.couponImageURL = jParser.getStringData("couponImageURL");
	                                    mData.couponType = jParser.getStringData("couponType");
	                                    mData.productName = jParser.getStringData("productName");
	                                    arrCouponData.add(mData);
	                                }
	                            }
	                            param.param1 = arrCouponData;
	                        }
	                    }
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}
    
    class AsyncOrderGoldeggPurchaseHistory extends AAsyncProtocol 
    {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if (!isCancelled()) {
                @SuppressWarnings("unchecked")
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

                HttpConnection con = new HttpConnection(this);
                try {
                    String fullAddress = URLWrapper.getOrderGoldeggPurchaseHistory();
                    String strJson = con.openPost(fullAddress, paramData);

                    try {
                        JSONObject jsonObj = new JSONObject(strJson);

                        if(jsonObj.has( JsonKeyWrapper.error )){
                            param.param3 = EventCode.E_FAIL;
                            param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                        } else {
                            if (jsonObj.isNull(JsonKeyWrapper.none) == false) {
                                // [SDJ 131408] 데이터 없음
                                param.param1 = null;
                            } else {
                                JSONArray jsonArr = jsonObj.getJSONArray(JsonKeyWrapper.LIST);

                                JSONParser jParser = null;
                                Logg.d("arrCouponList size = " + jsonArr.length());

                                ArrayList<PurchaseInfoData> arrPurchaseData = new ArrayList<PurchaseInfoData>();

                                for (int i = 0; i < jsonArr.length(); i++) {
                                    PurchaseInfoData mData = new PurchaseInfoData();
                                    jParser = new JSONParser();
                                    jParser.setJsonObeject(jsonArr.getString(i));

                                    if (jParser.getStringData(JsonKeyWrapper.goldCount).length() > 0) {
                                        mData.setImageURL( jParser.getStringData( JsonKeyWrapper.imageURL ) );
                                        mData.setGoldCount( jParser.getStringData( JsonKeyWrapper.goldCount ) );
                                        mData.setPrice( jParser.getStringData( JsonKeyWrapper.price ) );
                                        mData.setProductName( jParser.getStringData( JsonKeyWrapper.productName ) );
                                        mData.setPruchasedDate( jParser.getStringData( JsonKeyWrapper.purchasedDate ) );

                                        arrPurchaseData.add(mData);
                                    }
                                }
                                param.param1 = arrPurchaseData;
                            }
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        Logg.e(e);
                        e.printStackTrace();
                        param.param3 = EventCode.E_INTERNAL_ERROR;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncByingRequestCoupon extends AAsyncProtocol 
    {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if (!isCancelled()) {
                @SuppressWarnings("unchecked")
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

                HttpConnection con = new HttpConnection(this);
                try {
                    String fullAddress = URLWrapper.getBuyingRequestCoupon();
                    String strJson = con.openPost(fullAddress, paramData);

                    try {
                        JSONObject jsonObj = new JSONObject(strJson);
                        
                        if(jsonObj.has( JsonKeyWrapper.error )){
                            param.param3 = EventCode.E_FAIL;
                            param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                        } else {
                            PurchaseInfoData mData = new PurchaseInfoData();

                            mData.setPurchasedCount(jsonObj.getString( JsonKeyWrapper.purchasedCount));
                            mData.setLackingCount(jsonObj.getString( JsonKeyWrapper.lackingCount));
                            
                            param.param1 = mData;
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        Logg.e(e);
                        e.printStackTrace();
                        param.param3 = EventCode.E_INTERNAL_ERROR;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncBuyingRequestOwl extends AAsyncProtocol 
    {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if (!isCancelled()) {
                @SuppressWarnings("unchecked")
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

                HttpConnection con = new HttpConnection(this);
                try {
                    String fullAddress = URLWrapper.getBuyingRequestOwl();
                    String strJson = con.openPost(fullAddress, paramData);

                    try {
                        JSONObject jsonObj = new JSONObject(strJson);
                        
                        if(jsonObj.has( JsonKeyWrapper.error )){
                            param.param3 = EventCode.E_FAIL;
                            param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                        } else {
                            PurchaseInfoData mData = new PurchaseInfoData();

                            mData.setPurchasedCount(jsonObj.getString( JsonKeyWrapper.purchasedCount));
                            mData.setLackingCount(jsonObj.getString( JsonKeyWrapper.lackingCount));
                            
                            param.param1 = mData;
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        Logg.e(e);
                        e.printStackTrace();
                        param.param3 = EventCode.E_INTERNAL_ERROR;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncCheckSignature extends AAsyncProtocol 
    {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if (!isCancelled()) {
                @SuppressWarnings("unchecked")
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

                HttpConnection con = new HttpConnection(this);
                try {
                    String fullAddress = URLWrapper.getCheckSignature();
                    String strJson = con.openPost(fullAddress, paramData);

                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param3 = EventCode.S_SUCCESS;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncSetCompaniesMarketBuyValidity extends AAsyncProtocol 
    {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if (!isCancelled()) {
                @SuppressWarnings("unchecked")
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

                HttpConnection con = new HttpConnection(this);
                try {
                    String fullAddress = URLWrapper.setCompaniesMarketBuyValidity();
                    String strJson = con.openPost(fullAddress, paramData);

                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param3 = EventCode.S_SUCCESS;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }        
    

    class AsyncCompaniesItemCount extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];

                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.companiesItemCount();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        PurchaseInfoData mData = new PurchaseInfoData();
                        
                        mData.setOneTimeCoupon( jsonObj.getString( JsonKeyWrapper.oneTimeCoupon ) );
                        mData.setNoLimitTimeCoupon( jsonObj.getString( JsonKeyWrapper.noLimitTimeCoupon ) );
                        
                        param.param1 = mData;
                    }
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                        
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    public void orderStampRemain(Object objTag, String id)
    {
        Logg.d( "orderStampRemain" );
        
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
        
        startAsyncTask(objTag, new AsyncOrderStampRemain(), dataValue );
    }
    
    public void orderCouponPurchased(Object objTag, String id)
    {
		Logg.d("orderCouponPurchased");

		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(objTag, new AsyncOrderCouponPurchased(), dataValue);
    }
    
    public void orderGoldeggPurchaseHistory(Object objTag, String id, String lastSEQ)
    {
        Logg.d("orderGoldeggPurchaseHistory");

        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.lastSEQ, lastSEQ));

        startAsyncTask(objTag, new AsyncOrderGoldeggPurchaseHistory(), dataValue);
    }
    
    public void buyingRequestCoupon(Object objTag, String id, String couponSEQ)
    {
        Logg.d("buyingRequestCoupon");

        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.couponSEQ, couponSEQ));

        startAsyncTask(objTag, new AsyncByingRequestCoupon(), dataValue);
    }
    
    public void buyingRequestOwl(Object objTag, String id, String owlID)
    {
        Logg.d("buyingRequestOwl owlID = " + owlID);

        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.owlID, owlID));

        startAsyncTask(objTag, new AsyncBuyingRequestOwl(), dataValue);
    }
    
    public void checkSignature(Object objTag, String id, String purchaseData, String signature)
    {
        Logg.d("checkSignature purchaseData = " + purchaseData);
        Logg.d("checkSignature signature = " + signature);

        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("signedData", purchaseData));
        dataValue.add(new BasicNameValuePair("signature", signature));

        startAsyncTask(objTag, new AsyncCheckSignature(), dataValue);
    }
    
    public void setCompaniesMarketBuyValidity(Object objTag, String id, String purchaseData, String signature)
    {
        Logg.d("checkSignature purchaseData = " + purchaseData);
        Logg.d("checkSignature signature = " + signature);

        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("deviceType", "2"));
        dataValue.add(new BasicNameValuePair("signedData", purchaseData));
        dataValue.add(new BasicNameValuePair("signature", signature));

        startAsyncTask(objTag, new AsyncSetCompaniesMarketBuyValidity(), dataValue);
    } 
    
    public void companiesItemCount(Object objTag, String id, String businessNumber)
    {
        Logg.d( "companiesItemCount" );
        
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
        dataValue.add(new BasicNameValuePair(JsonKeyWrapper.businessNumber, businessNumber));
        
        startAsyncTask(objTag, new AsyncCompaniesItemCount(), dataValue );
    }
}
