package kr.wisestone.happytong.data.protocol;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kr.wisestone.happytong.data.dataset.CompanySendedCouponData;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.EventCode;
import kr.wisestone.happytong.data.network.HttpConnection;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.util.JSONParser;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.data.wrapper.URLWrapper;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CompanyProtocol extends AProtocol {

	public CompanyProtocol(IWiseCallback iOK, IWiseCallback iFail) {
		super(iOK, iFail);
	}

	class AsyncGetCompaniesSendedCouponList extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCompaniesSendedCouponList();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							if (jsonObj.isNull(JsonKeyWrapper.none) == false) {
								// [SDJ 131408] 데이터 없음
								param.param1 = null;
							} else {
								JSONArray arrCouponList = jsonObj.getJSONArray(JsonKeyWrapper.LIST);

								JSONParser jParser = null;
								Logg.d("arrCouponList size = " + arrCouponList.length());

								List<CompanySendedCouponData> arrCouponData = new ArrayList<CompanySendedCouponData>();

								for (int i = 0; i < arrCouponList.length(); i++) {
									CompanySendedCouponData mData = new CompanySendedCouponData();
									jParser = new JSONParser();
									jParser.setJsonObeject(arrCouponList.getString(i));

									if (jParser.getStringData(JsonKeyWrapper.recevier).length() > 0) {
										mData.mRecevier = (jParser.getStringData(JsonKeyWrapper.recevier));
										mData.mReceiverCount = (jParser.getStringData(JsonKeyWrapper.receiverCount));
										mData.mLocationSmallName = (jParser.getStringData(JsonKeyWrapper.locationSmallName));
										mData.mSendDate = (jParser.getStringData(JsonKeyWrapper.sendDate));
										mData.mCouponImageURL = (jParser.getStringData(JsonKeyWrapper.couponImageURL));

										mData.couponCropImageURL = jParser.getStringData(JsonKeyWrapper.couponCropImageURL);
										mData.itemGroup = jParser.getStringData(JsonKeyWrapper.itemGroup);
										mData.couponText = jParser.getStringData(JsonKeyWrapper.couponText);
										mData.couponType = jParser.getStringData(JsonKeyWrapper.couponType);
										mData.validDateEnd = jParser.getStringData(JsonKeyWrapper.validDateEnd);

										arrCouponData.add(mData);
									}
								}
								param.param1 = arrCouponData;
							}
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncDelCompaniesSendedCoupon extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getDelCompaniesSendedCoupon();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							if (jsonObj.has("success")) {

							}
							if (jsonObj.has("LIST")) {
								List<String> list = new ArrayList<String>();
								JSONArray arr = jsonObj.getJSONArray("LIST");
								for (int i = 0; i < arr.length(); i++) {
									list.add(arr.getString(i));
								}
								param.param1 = list;
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCouponDelete extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCouponDelete();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							if (jsonObj.has("success")) {

							}
							if (jsonObj.has("LIST")) {
								List<String> list = new ArrayList<String>();
								JSONArray arr = jsonObj.getJSONArray("LIST");
								for (int i = 0; i < arr.length(); i++) {
									list.add(arr.getString(i));
								}
								param.param1 = list;
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCompaniesCouponSend extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCompaniesCouponSend();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							param.param3 = EventCode.S_SUCCESS;
							// 업체쿠폰발송은 부분실패 없나? 20130927. jish
							// if (jsonObj.has(JsonKeyWrapper.LIST)) {
							// JSONArray jArr = jsonObj.getJSONArray(JsonKeyWrapper.LIST);
							// List<NameValuePair> list = new ArrayList<NameValuePair>();
							// for (int i = 0; i < jArr.length(); i++) {
							// JSONObject jObj = jArr.getJSONObject(i);
							// list.add(new BasicNameValuePair(jObj.getString(JsonKeyWrapper.id), jObj.getString(JsonKeyWrapper.error)));
							// }
							// param.param1 = list;
							// }
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncGetCompanyAdd extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.setCompaniesInfoRegist();
					String strJson = con.openPost(fullAddress, paramData);

					JSONObject jsonObj = new JSONObject(strJson);

					if (jsonObj.has(JsonKeyWrapper.error)) {
						param.param3 = EventCode.E_FAIL;
						param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
					} else {
						param.param1 = JsonKeyWrapper.success;
					}

					if (con.getError() != EventCode.S_SUCCESS)
						param.param3 = con.getError();
				} catch (Exception e) {
					Logg.e(e);
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	public void getCompaniesSendedCouponList(Object objTag, String id) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(objTag, new AsyncGetCompaniesSendedCouponList(), dataValue);
	}

	/**
	 * 
	 * @param obj
	 * @param itemGroup
	 * @param id
	 *            "789#123#234#567#987#555" (쿠폰함번호#쿠폰함번호#쿠폰함번호)
	 */
	public void delCompaniesSendedCoupon(Object obj, String id, String itemGroup) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.itemGroup, itemGroup));

		startAsyncTask(obj, new AsyncDelCompaniesSendedCoupon(), dataValue);
	}

	/**
	 * 
	 * @param obj
	 * @param boxSEQ
	 *            "789#123#234#567#987#555" (쿠폰함번호#쿠폰함번호#쿠폰함번호)
	 */
	public void couponDelete(Object obj, String boxSEQ, String id) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.boxSEQ, boxSEQ));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(obj, new AsyncCouponDelete(), dataValue);
	}

	/**
	 * 업체쿠폰 발송 - 업체가 사용자들에게 쿠폰을 발송한다.
	 * 
	 * @param obj
	 * @param id
	 * @param locSmallCode
	 * @param content
	 * @param validDateEnd
	 * @param businessNumber
	 * @param productType
	 * @param couponSEQ
	 * @param sendCount
	 */
	public void companiesCouponSend(Object obj, String id, String locSmallCode, String content, String validDateEnd, String businessNumber, String productType, String couponSEQ,
			String sendCount) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.locationSmallCode, locSmallCode));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.content, content));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.validDateEnd, validDateEnd));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.businessNumber, businessNumber));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.productType, productType));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.couponSEQ, couponSEQ));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.sendCount, sendCount));

		startAsyncTask(obj, new AsyncCompaniesCouponSend(), dataValue);
	}

	public void getCompanyAdd(Object objTag, String id, String name, String businessNumber, String callNumber, String address, String locationSmallCode) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair("id", id));
		dataValue.add(new BasicNameValuePair("name", name));
		dataValue.add(new BasicNameValuePair("businessNumber", businessNumber));
		dataValue.add(new BasicNameValuePair("callNumber", callNumber));
		dataValue.add(new BasicNameValuePair("address", address));
		dataValue.add(new BasicNameValuePair("locationSmallCode", locationSmallCode));

		startAsyncTask(objTag, new AsyncGetCompanyAdd(), dataValue);
	}

}
