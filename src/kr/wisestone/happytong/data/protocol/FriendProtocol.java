package kr.wisestone.happytong.data.protocol;

import java.util.ArrayList;
import java.util.List;

import kr.wisestone.happytong.data.db.HappytongDB.FriendInfo;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.EventCode;
import kr.wisestone.happytong.data.network.HttpConnection;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.util.JSONParser;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.data.wrapper.URLWrapper;
import kr.wisestone.happytong.util.AESEncryption;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FriendProtocol extends AProtocol
{
    public FriendProtocol( IWiseCallback iOK, IWiseCallback iFail )
    {
        super( iOK, iFail );
    }
    
    class AsyncFriendWithNameSync extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];

                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getFriendWithNameSync();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    try {
                        ArrayList<FriendInfo> arrFriendData = new ArrayList<FriendInfo>();
                        
                        JSONObject jsonObj = new JSONObject(strJson);
                        
                        if(jsonObj.has( JsonKeyWrapper.error )){
                            param.param3 = EventCode.E_FAIL;
                            param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                        } else {
                            if(jsonObj.isNull( JsonKeyWrapper.none ) == false){
                                //[SDJ 131408] 데이터 없음
                                param.param1 = null;
                            } else {
                            	
                                JSONArray friendList = new JSONArray();
                                JSONArray inviteList = new JSONArray();
                                
                                if(jsonObj.has(JsonKeyWrapper.LIST)) {
                                	friendList = jsonObj.getJSONArray(JsonKeyWrapper.LIST); 
                                }
                                
                                if(jsonObj.has(JsonKeyWrapper.inviteList)) {
                                	inviteList = jsonObj.getJSONArray(JsonKeyWrapper.inviteList);
                                }
                                
                                
                                JSONParser jParser = null;
                                Logg.d( "friendList size = " + friendList.length() );

                                for( int i = 0 ; i < friendList.length() ; i ++ )
                                {
                                    FriendInfo mFriendData = new FriendInfo();
                                    jParser = new JSONParser();
                                    Logg.d( "friendList getString = " + friendList.getString(i) );
                                    jParser.setJsonObeject( friendList.getString(i) );
                                    if( jParser.getStringData(JsonKeyWrapper.id).length() > 0 )
                                    {
                                        mFriendData.SetEmail(jParser.getStringData(JsonKeyWrapper.id));
                                        mFriendData.SetPhoneNumber(jParser.getStringData(JsonKeyWrapper.phoneNumber));
                                        mFriendData.SetName(jParser.getStringData(JsonKeyWrapper.friendName));
                                        mFriendData.SetNickName(jParser.getStringData(JsonKeyWrapper.nickName));
                                        mFriendData.SetUserImgUrl(jParser.getStringData(JsonKeyWrapper.userImageURL));
                                        mFriendData.Setblock(jParser.getStringData(JsonKeyWrapper.blockState));
                                        
                                        if(jParser.getStringData(JsonKeyWrapper.friendName).equals( "_none_" )){
                                            String encryptStr = AESEncryption.encrypt( jParser.getStringData(JsonKeyWrapper.nickName) );
                                            mFriendData.SetName(encryptStr);
                                            mFriendData.SetIsName( "0" );
                                        } else {
                                            mFriendData.SetName(jParser.getStringData(JsonKeyWrapper.friendName));
                                            mFriendData.SetIsName( "1" );
                                        }
                                        mFriendData.SetIsInvitation( "0" );

                                        arrFriendData.add(mFriendData);
                                    }
                                }
                                for( int i = 0 ; i < inviteList.length() ; i ++ )
                                {
                                    FriendInfo mFriendData = new FriendInfo();
                                    jParser = new JSONParser();
                                    Logg.d( "friendList getString = " + inviteList.getString(i) );
                                    jParser.setJsonObeject( inviteList.getString(i) );
                                    // TODO 2013.06.19.jish 전화번호부에 아무것도 없어서 0-length일경우 서버에서 빈데이터 내려옴 에러 발생
                                    // errlog_stack - 06-19 11:56:08.313: E/AndroidRuntime(4646): Caused by: java.lang.NumberFormatException: Invalid int: "_n"
                                    // 서버에서 처리한다고 하였으나, 확인하지 못함. 수정시 여기 수정 필요할지도.
                                    if( jParser.getStringData(JsonKeyWrapper.friendName).length() > 0 )
                                    {
                                        mFriendData.SetName( jParser.getStringData(JsonKeyWrapper.friendName) );
                                        mFriendData.SetPhoneNumber(jParser.getStringData(JsonKeyWrapper.phoneNumber));
                                        mFriendData.SetIsName( "1" );
                                        mFriendData.SetIsInvitation( "1" );

                                        arrFriendData.add(mFriendData);
                                    }
                                }                                
                                param.param1 = arrFriendData;
                            }
                        }
                        
                    } catch (JSONException e) {
                        Logg.e(e);
                        e.printStackTrace();
                        param.param3 = EventCode.E_INTERNAL_ERROR;
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    class AsyncFriendBlock extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];

                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.getFriendBlock();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }
    
    public void friendWithNameSync(Object objTag, String id, String phoneNumber)
    {
        Logg.d( "friendWithNameSync" );
        
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("phoneNumber", phoneNumber));
        
        startAsyncTask(objTag, new AsyncFriendWithNameSync(), dataValue);
    }
    
    public void friendBlock(Object objTag, String id, String blockId)
    {
        Logg.d( "friendBlock blockId = " + blockId );
        
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("blockID", blockId));
        
        startAsyncTask(objTag, new AsyncFriendBlock(), dataValue);
    }
}
