package kr.wisestone.happytong.data.protocol;

import java.lang.reflect.Method;

import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.EventCode;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.util.ErrorCode;
import kr.wisestone.happytong.util.Logg;
import android.os.AsyncTask;


public abstract class AAsyncProtocol extends AsyncTask<Object, Void, CallbackParam>
{
    IWiseCallback m_iOK;
    IWiseCallback m_iFail;
    CallbackParam   param = new CallbackParam();

    public static AAsyncProtocol getInstans(AAsyncProtocol protocol, Object objTag, IWiseCallback iOK, IWiseCallback iFail)
    {
        protocol.init( objTag, iOK, iFail );
        return protocol;
    }

    public static Object getInstans(Class<?> cls, String strMethod, Object... params)
    {
        Object newCls = null;

        try
        {
            Class<?>[] partypes = null;

            if ( params.length > 0 )
            {
                partypes = new Class[params.length];
                for ( int iIndex = 0; iIndex < params.length; iIndex++ )
                    partypes[iIndex] = params[iIndex].getClass();
            }

            Method method = cls.getMethod( strMethod, partypes );
            if ( method != null )
            {
                newCls = cls.newInstance();
                method.invoke( cls.newInstance(), params );
            }
        }
        catch ( Throwable e )
        {
            Logg.e("e = " + e);
            e.printStackTrace();
        }
        return newCls;
    }

    protected void init(Object objTag, IWiseCallback iOK, IWiseCallback iFail)
    {
        m_iOK         = iOK;
        m_iFail       = iFail;
        param         = new CallbackParam();
        param.eventId = objTag;
        param.param3  = EventCode.S_SUCCESS;
        param.errorCode = ErrorCode.ERR_HTTP_UNKNOWN;
    }

    protected void onPostExecute(CallbackParam param)
    {
        if(!isCancelled())
        {
            if(param.param3 instanceof EventCode && (EventCode)param.param3 != EventCode.S_SUCCESS)
            {
                if(m_iFail != null) m_iFail.WiseCallback(param);
            }
            else
                if(m_iOK != null) m_iOK.WiseCallback(param);
        }
    }
}
