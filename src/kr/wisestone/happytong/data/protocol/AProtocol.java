package kr.wisestone.happytong.data.protocol;

import java.util.HashMap;
import java.util.Iterator;

import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.util.Logg;
import android.os.AsyncTask;


public abstract class AProtocol
{
    protected IWiseCallback m_iOK;
    protected IWiseCallback m_iFail;
    HashMap<Object, Object> m_hmAsync;

    /**
     * @param m_iCallback
     */
    public AProtocol(IWiseCallback iOK, IWiseCallback iFail)
    {
        super();
        m_iOK       = iOK;
        m_iFail     = iFail;
        m_hmAsync   = new HashMap<Object, Object>();
    }

    public void cancel(Object objTag)
    {
        if(m_hmAsync.containsKey( objTag ))
        {
            AsyncTask<?, ?, ?> task = (AsyncTask<?, ?, ?>)m_hmAsync.get( objTag );

            if(task != null) task.cancel(true);
            task = null;
            m_hmAsync.remove( objTag );
        }
        else
        {
            Logg.w( "Unknown protocol : " + objTag );
        }
    }

    public void cancelAll()
    {
        Iterator<Object> itKey = m_hmAsync.keySet().iterator();

        while(itKey.hasNext())
        {
            cancel(itKey.next());
            itKey = m_hmAsync.keySet().iterator();
        }
    }

    protected void addAsync(Object objTag, AsyncTask<?, ?, ?> task)
    {
        m_hmAsync.put( objTag, task );
    }

    protected void startAsyncTask(Object objTag, AAsyncProtocol asyncTask, Object... args)
    {
        cancel(objTag);
        AAsyncProtocol task = AAsyncProtocol.getInstans( asyncTask, objTag, m_iOK, m_iFail );
        task.execute(args);
        addAsync(objTag, task);
    }
}
