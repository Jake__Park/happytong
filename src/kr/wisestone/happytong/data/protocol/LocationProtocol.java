package kr.wisestone.happytong.data.protocol;

import java.util.ArrayList;
import java.util.List;

import kr.wisestone.happytong.data.dataset.LocationInfoData;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.EventCode;
import kr.wisestone.happytong.data.network.HttpConnection;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.util.JSONParser;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.data.wrapper.URLWrapper;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationProtocol extends AProtocol {

	public LocationProtocol(IWiseCallback iOK, IWiseCallback iFail) {
		super(iOK, iFail);
	}

	class AsyncLocationGet extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {

				HttpConnection con = new HttpConnection(this);

				try {
					String fullAddress = URLWrapper.getLocation();
					String strJson = con.openPost(fullAddress);

					ArrayList<LocationInfoData> arrLocationData = new ArrayList<LocationInfoData>();

					JSONObject jsonObj = new JSONObject(strJson);

					if (jsonObj.has(JsonKeyWrapper.error)) {
						param.param3 = EventCode.E_FAIL;
						param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
					} else {
						if (jsonObj.isNull(JsonKeyWrapper.error) == false) {
							// [SDJ 131408] 데이터 없음
							param.param1 = arrLocationData;
							return param;
						}

						JSONArray locationList = jsonObj.getJSONArray(JsonKeyWrapper.biglocationInfoList);

						JSONParser jParser = null;

						ArrayList<String> bigCode = new ArrayList<String>();

						for (int i = 0; i < locationList.length(); i++) {
							jParser = new JSONParser();
							jParser.setJsonObeject(locationList.getString(i));
							bigCode.add(jParser.getStringData(JsonKeyWrapper.locationBigCode));

						}

						for (int i = 0; i < bigCode.size(); i++) {
							locationList = jsonObj.getJSONArray(bigCode.get(i));
							for (int j = 0; j < locationList.length(); j++) {

								LocationInfoData mData = new LocationInfoData();

								jParser = new JSONParser();
								jParser.setJsonObeject(locationList.getString(j));
								mData.setLocationBigName(jParser.getStringData(JsonKeyWrapper.locationBigName));
								mData.setLocationSmallName(jParser.getStringData(JsonKeyWrapper.locationSmallName));
								mData.setLocationSmallCode(jParser.getStringData(JsonKeyWrapper.locationSmallCode));

								arrLocationData.add(mData);

								Logg.d("1 = " + jParser.getStringData(JsonKeyWrapper.locationBigName));
								Logg.d("2 = " + jParser.getStringData(JsonKeyWrapper.locationSmallName));
								Logg.d("3 = " + jParser.getStringData(JsonKeyWrapper.locationSmallCode));

							}
						}
						param.param1 = arrLocationData;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Logg.e(e);
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}
	
	class AsyncLocationUserCountListGet extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {

				HttpConnection con = new HttpConnection(this);
                @SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];

				try {
					String fullAddress = URLWrapper.getLocationUserCountList();
					String strJson = con.openPost(fullAddress, paramData);

					ArrayList<LocationInfoData> arrLocationData = new ArrayList<LocationInfoData>();

					JSONObject jsonObj = new JSONObject(strJson);

					if (jsonObj.has(JsonKeyWrapper.error)) {
						param.param3 = EventCode.E_FAIL;
						param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
					} else {
						if (jsonObj.isNull(JsonKeyWrapper.error) == false) {
							// [SDJ 131408] 데이터 없음
							param.param1 = arrLocationData;
							return param;
						}

						JSONArray locationList = jsonObj
								.getJSONArray(JsonKeyWrapper.biglocationInfoList);

						JSONParser jParser = null;

						ArrayList<String> bigCode = new ArrayList<String>();

						for (int i = 0; i < locationList.length(); i++) {
							jParser = new JSONParser();
							jParser.setJsonObeject(locationList.getString(i));
							bigCode.add(jParser
									.getStringData(JsonKeyWrapper.locationBigCode));

						}

						for (int i = 0; i < bigCode.size(); i++) {

							locationList = jsonObj.getJSONArray(bigCode.get(i));

							for (int j = 0; j < locationList.length(); j++) {

								LocationInfoData mData = new LocationInfoData();

								jParser = new JSONParser();
								jParser.setJsonObeject(locationList
										.getString(j));
								mData.setLocationBigName(jParser.getStringData(JsonKeyWrapper.locationBigName));
								mData.setLocationSmallName(jParser.getStringData(JsonKeyWrapper.locationSmallName));
								mData.setLocationSmallCode(jParser.getStringData(JsonKeyWrapper.locationSmallCode));
								mData.setLocationUserCount(jParser.getStringData(JsonKeyWrapper.locationUserCount));

								arrLocationData.add(mData);

								Logg.d("1 = " + jParser.getStringData(JsonKeyWrapper.locationBigName));
								Logg.d("2 = " + jParser.getStringData(JsonKeyWrapper.locationSmallName));
								Logg.d("3 = " + jParser.getStringData(JsonKeyWrapper.locationSmallCode));
								Logg.d("4 = " + jParser.getStringData(JsonKeyWrapper.locationUserCount));

							}
						}
						param.param1 = arrLocationData;
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Logg.e(e);
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}
	
	class GetLocationDataEdit extends AAsyncProtocol {
        @Override
        protected CallbackParam doInBackground(Object... data) {
            if(!isCancelled())
            {
                @SuppressWarnings( "unchecked" )
                List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>)data[0];
                
                HttpConnection con = new HttpConnection(this);
                try
                {
                    String fullAddress = URLWrapper.setLocation();
                    String strJson = con.openPost(fullAddress, paramData);
                    
                    JSONObject jsonObj = new JSONObject(strJson);
                    
                    if(jsonObj.has( JsonKeyWrapper.error )){
                        param.param3 = EventCode.E_FAIL;
                        param.errorCode = jsonObj.getInt( JsonKeyWrapper.error );
                    } else {
                        param.param1 = JsonKeyWrapper.success;
                    }
                    
                    if(con.getError() != EventCode.S_SUCCESS)
                        param.param3 = con.getError();
                }
                catch(Exception e)
                {
                    Logg.e(e);
                    e.printStackTrace();
                    param.param3 = EventCode.E_INTERNAL_ERROR;
                }
            }
            return param;
        }
    }

	public void locationData(Object objTag) {
		Logg.d("locationData");

		startAsyncTask(objTag, new AsyncLocationGet());
	}
	
	public void locationUserCountListData(Object objTag, String id) {		
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair("id", id));		
		startAsyncTask(objTag, new AsyncLocationUserCountListGet(), dataValue);		
	}
		
	public void getLocationDataEdit(Object objTag, String id, String setFlag, String locationSmallCode )
    {
        List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
        dataValue.add(new BasicNameValuePair("id", id));
        dataValue.add(new BasicNameValuePair("setFlag", setFlag));
        dataValue.add(new BasicNameValuePair("locationSmallCode", locationSmallCode));
    
        startAsyncTask(objTag, new GetLocationDataEdit(), dataValue);
    }

}
