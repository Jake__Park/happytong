package kr.wisestone.happytong.data.protocol;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kr.wisestone.happytong.data.dataset.CouponInfoData;
import kr.wisestone.happytong.data.network.CallbackParam;
import kr.wisestone.happytong.data.network.EventCode;
import kr.wisestone.happytong.data.network.HttpConnection;
import kr.wisestone.happytong.data.network.IWiseCallback;
import kr.wisestone.happytong.data.util.JSONParser;
import kr.wisestone.happytong.data.util.Utils;
import kr.wisestone.happytong.data.wrapper.JsonKeyWrapper;
import kr.wisestone.happytong.data.wrapper.URLWrapper;
import kr.wisestone.happytong.ui.cbox.CouponInfoData2;
import kr.wisestone.happytong.util.Logg;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CouponProtocol extends AProtocol {

	public CouponProtocol(IWiseCallback iOK, IWiseCallback iFail) {
		super(iOK, iFail);
		// TODO Auto-generated constructor stub
	}

	class AsyncCouponReceived extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCouponReceived();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							if (jsonObj.isNull(JsonKeyWrapper.none) == false) {
								// [SDJ 131408] 데이터 없음
								param.param1 = null;
							} else {
								JSONArray arrCouponList = jsonObj.getJSONArray(JsonKeyWrapper.LIST);

								JSONParser jParser = null;
								Logg.d("arrCouponList size = " + arrCouponList.length());

								List<CouponInfoData> arrCouponData = new ArrayList<CouponInfoData>();

								for (int i = 0; i < arrCouponList.length(); i++) {
									CouponInfoData mData = new CouponInfoData();
									jParser = new JSONParser();
									jParser.setJsonObeject(arrCouponList.getString(i));

									if (jParser.getStringData(JsonKeyWrapper.couponType).length() > 0) {
										mData.setSenderPhotoURL(jParser.getStringData(JsonKeyWrapper.senderPhotoURL));
										mData.setCouponCropIMGURL(jParser.getStringData(JsonKeyWrapper.couponCropImageURL));
										mData.setCouponType(jParser.getStringData(JsonKeyWrapper.couponType));
										mData.setValidPeriod(jParser.getStringData(JsonKeyWrapper.validPeriod));
										mData.setSenderID(jParser.getStringData(JsonKeyWrapper.senderID));
										mData.setCreatedCouponDate(jParser.getStringData(JsonKeyWrapper.createdCouponDate));
										mData.setSenderNickname(jParser.getStringData(JsonKeyWrapper.senderNickname));
										mData.setCouponIMGURL(jParser.getStringData(JsonKeyWrapper.couponImageURL));
										mData.setCouponText(jParser.getStringData(JsonKeyWrapper.couponText));
										mData.setFlag(jParser.getStringData(JsonKeyWrapper.flag));
										mData.setRequestState(jParser.getStringData(JsonKeyWrapper.requestState));
										mData.setReceiverID(jParser.getStringData(JsonKeyWrapper.receiverID));
										mData.setCouponSEQ(jParser.getStringData(JsonKeyWrapper.couponSEQ));
										mData.setBoxSEQ(jParser.getStringData(JsonKeyWrapper.boxSEQ));
										mData.setReceiverPhotoURL(jParser.getStringData(JsonKeyWrapper.receiverPhotoURL));
										mData.setReceiverNickname(jParser.getStringData(JsonKeyWrapper.receiverNickname));
										mData.setEndCoupon(jParser.getStringData(JsonKeyWrapper.endCoupon));

										arrCouponData.add(mData);
									}
								}
								param.param1 = arrCouponData;
							}
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCouponSent extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCouponSent();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							if (jsonObj.isNull(JsonKeyWrapper.none) == false) {
								// [SDJ 131408] 데이터 없음
								param.param1 = null;
							} else {
								JSONArray arrCouponList = jsonObj.getJSONArray(JsonKeyWrapper.LIST);

								JSONParser jParser = null;
								Logg.d("arrCouponList size = " + arrCouponList.length());

								List<CouponInfoData> arrCouponData = new ArrayList<CouponInfoData>();

								for (int i = 0; i < arrCouponList.length(); i++) {
									CouponInfoData mData = new CouponInfoData();
									jParser = new JSONParser();
									jParser.setJsonObeject(arrCouponList.getString(i));

									if (jParser.getStringData(JsonKeyWrapper.couponType).length() > 0) {
										mData.setSenderPhotoURL(jParser.getStringData(JsonKeyWrapper.senderPhotoURL));
										mData.setCouponCropIMGURL(jParser.getStringData(JsonKeyWrapper.couponCropImageURL));
										mData.setCouponType(jParser.getStringData(JsonKeyWrapper.couponType));
										mData.setValidPeriod(jParser.getStringData(JsonKeyWrapper.validPeriod));
										mData.setSenderID(jParser.getStringData(JsonKeyWrapper.senderID));
										mData.setCreatedCouponDate(jParser.getStringData(JsonKeyWrapper.createdCouponDate));
										mData.setSenderNickname(jParser.getStringData(JsonKeyWrapper.senderNickname));
										mData.setCouponIMGURL(jParser.getStringData(JsonKeyWrapper.couponImageURL));
										mData.setCouponText(jParser.getStringData(JsonKeyWrapper.couponText));
										mData.setFlag(jParser.getStringData(JsonKeyWrapper.flag));
										mData.setRequestState(jParser.getStringData(JsonKeyWrapper.requestState));
										mData.setReceiverID(jParser.getStringData(JsonKeyWrapper.receiverID));
										mData.setCouponSEQ(jParser.getStringData(JsonKeyWrapper.couponSEQ));
										mData.setBoxSEQ(jParser.getStringData(JsonKeyWrapper.boxSEQ));
										mData.setReceiverPhotoURL(jParser.getStringData(JsonKeyWrapper.receiverPhotoURL));
										mData.setReceiverNickname(jParser.getStringData(JsonKeyWrapper.receiverNickname));
										mData.setEndCoupon(jParser.getStringData(JsonKeyWrapper.endCoupon));

										arrCouponData.add(mData);
									}
								}
								param.param1 = arrCouponData;
							}
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCouponUsedExpired extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCouponUsedExpired();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							if (jsonObj.isNull(JsonKeyWrapper.none) == false) {
								// [SDJ 131408] 데이터 없음
								param.param1 = null;
							} else {
								JSONArray arrCouponList = jsonObj.getJSONArray(JsonKeyWrapper.LIST);

								JSONParser jParser = null;
								Logg.d("arrCouponList size = " + arrCouponList.length());

								List<CouponInfoData> arrCouponData = new ArrayList<CouponInfoData>();

								for (int i = 0; i < arrCouponList.length(); i++) {
									CouponInfoData mData = new CouponInfoData();
									jParser = new JSONParser();
									jParser.setJsonObeject(arrCouponList.getString(i));

									if (jParser.getStringData(JsonKeyWrapper.couponType).length() > 0) {
										mData.setSenderPhotoURL(jParser.getStringData(JsonKeyWrapper.senderPhotoURL));
										mData.setCouponCropIMGURL(jParser.getStringData(JsonKeyWrapper.couponCropImageURL));
										mData.setCouponType(jParser.getStringData(JsonKeyWrapper.couponType));
										mData.setValidPeriod(jParser.getStringData(JsonKeyWrapper.validPeriod));
										mData.setSenderID(jParser.getStringData(JsonKeyWrapper.senderID));
										mData.setCreatedCouponDate(jParser.getStringData(JsonKeyWrapper.createdCouponDate));
										mData.setSenderNickname(jParser.getStringData(JsonKeyWrapper.senderNickname));
										mData.setCouponIMGURL(jParser.getStringData(JsonKeyWrapper.couponImageURL));
										mData.setCouponText(jParser.getStringData(JsonKeyWrapper.couponText));
										mData.setFlag(jParser.getStringData(JsonKeyWrapper.flag));
										mData.setRequestState(jParser.getStringData(JsonKeyWrapper.requestState));
										mData.setReceiverID(jParser.getStringData(JsonKeyWrapper.receiverID));
										mData.setCouponSEQ(jParser.getStringData(JsonKeyWrapper.couponSEQ));
										mData.setBoxSEQ(jParser.getStringData(JsonKeyWrapper.boxSEQ));
										mData.setReceiverPhotoURL(jParser.getStringData(JsonKeyWrapper.receiverPhotoURL));
										mData.setReceiverNickname(jParser.getStringData(JsonKeyWrapper.receiverNickname));
										mData.setEndCoupon(jParser.getStringData(JsonKeyWrapper.endCoupon));

										arrCouponData.add(mData);
									}
								}
								param.param1 = arrCouponData;
							}
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCouponCompanies extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCompaniesCouponBox();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							if (jsonObj.isNull(JsonKeyWrapper.none) == false) {
								// [SDJ 131408] 데이터 없음
								param.param1 = null;
							} else {
								JSONArray arrCouponList = jsonObj.getJSONArray(JsonKeyWrapper.LIST);

								JSONParser jParser = null;
								Logg.d("arrCouponList size = " + arrCouponList.length());

								List<CouponInfoData> arrCouponData = new ArrayList<CouponInfoData>();

								for (int i = 0; i < arrCouponList.length(); i++) {
									CouponInfoData2 mData = new CouponInfoData2();
									jParser = new JSONParser();
									jParser.setJsonObeject(arrCouponList.getString(i));

									if (jParser.getStringData(JsonKeyWrapper.boxSEQ).length() > 0) {
										mData.m_boxSEQ = jParser.getStringData(JsonKeyWrapper.boxSEQ);
										mData.m_couponSEQ = jParser.getStringData(JsonKeyWrapper.couponSEQ);
										mData.mCompanyID = jParser.getStringData(JsonKeyWrapper.companyID);
										mData.m_receiverID = jParser.getStringData(JsonKeyWrapper.receiverID);
										mData.m_couponText = jParser.getStringData(JsonKeyWrapper.couponText);
										mData.m_couponImageURL = jParser.getStringData(JsonKeyWrapper.couponImageURL);
										mData.m_validPeriod = jParser.getStringData(JsonKeyWrapper.validDateEnd);
										mData.m_createdCouponDate = jParser.getStringData(JsonKeyWrapper.createdCouponDate);
										mData.m_receiverNickname = jParser.getStringData(JsonKeyWrapper.receiverNickname);
										mData.mCompanyPhotoURL = jParser.getStringData(JsonKeyWrapper.companyPhotoURL);
										mData.m_receiverPhotoURL = jParser.getStringData(JsonKeyWrapper.receiverPhotoURL);
										mData.mCompanyName = jParser.getStringData(JsonKeyWrapper.companyName);
										mData.mCompanyAddress = jParser.getStringData(JsonKeyWrapper.companyAddress);
										mData.mCompanyPhoneNumber = jParser.getStringData(JsonKeyWrapper.companyPhoneNumber);
										mData.m_flag = jParser.getStringData(JsonKeyWrapper.flag);
										mData.m_endCoupon = jParser.getStringData(JsonKeyWrapper.endCoupon);
										mData.m_couponType = jParser.getStringData(JsonKeyWrapper.couponType);
										mData.m_couponCropIMGURL = jParser.getStringData(JsonKeyWrapper.couponCropImageURL);
										mData.mCompanyCouponCreatedDate = Utils.getDateFormatStringFromServerDateFormat(mData.m_createdCouponDate, "yyyy.mm.dd", Locale.ENGLISH);

										mData.m_senderPhotoURL = mData.mCompanyPhotoURL;
										mData.m_validPeriod = mData.m_validPeriod;
										mData.m_senderID = mData.mCompanyName;
										mData.m_senderNickname = mData.mCompanyName;
										mData.m_requestState = "";

										arrCouponData.add(mData);
									}
								}
								param.param1 = arrCouponData;
							}
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCouponDelete extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCouponDelete();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							if (jsonObj.has("success")) {

							}
							if (jsonObj.has("LIST")) {
								List<String> list = new ArrayList<String>();
								JSONArray arr = jsonObj.getJSONArray("LIST");
								for (int i = 0; i < arr.length(); i++) {
									list.add(arr.getString(i));
								}
								param.param1 = list;
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCompanyCouponDelete extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCompanyCouponDelete();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							if (jsonObj.has("success")) {

							}
							if (jsonObj.has("LIST")) {
								List<String> list = new ArrayList<String>();
								JSONArray arr = jsonObj.getJSONArray("LIST");
								for (int i = 0; i < arr.length(); i++) {
									list.add(arr.getString(i));
								}
								param.param1 = list;
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCouponUsingRequest extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCouponUsingRequest();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							if (jsonObj.has(JsonKeyWrapper.success)) {
								param.param3 = EventCode.S_SUCCESS;
							} else if (jsonObj.has(JsonKeyWrapper.none)) {
								param.param3 = EventCode.E_NONE;
							}
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCompaniesCouponUse extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCompaniesCouponUse();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							param.param3 = EventCode.S_SUCCESS;
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCouponPermit extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCouponPermit();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							param.param3 = EventCode.S_SUCCESS;
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	class AsyncCouponSending extends AAsyncProtocol {
		@Override
		protected CallbackParam doInBackground(Object... data) {
			if (!isCancelled()) {
				@SuppressWarnings("unchecked")
				List<BasicNameValuePair> paramData = (ArrayList<BasicNameValuePair>) data[0];

				HttpConnection con = new HttpConnection(this);
				try {
					String fullAddress = URLWrapper.getCouponSending();
					String strJson = con.openPost(fullAddress, paramData);

					try {
						JSONObject jsonObj = new JSONObject(strJson);

						if (jsonObj.has(JsonKeyWrapper.error)) {
							param.param3 = EventCode.E_FAIL;
							param.errorCode = jsonObj.getInt(JsonKeyWrapper.error);
						} else {
							param.param3 = EventCode.S_SUCCESS;

							if (jsonObj.has(JsonKeyWrapper.LIST)) {
								JSONArray jArr = jsonObj.getJSONArray(JsonKeyWrapper.LIST);
								List<NameValuePair> list = new ArrayList<NameValuePair>();
								for (int i = 0; i < jArr.length(); i++) {
									JSONObject jObj = jArr.getJSONObject(i);
									list.add(new BasicNameValuePair(jObj.getString(JsonKeyWrapper.id), jObj.getString(JsonKeyWrapper.error)));
								}
								param.param1 = list;
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Logg.e(e);
						e.printStackTrace();
						param.param3 = EventCode.E_INTERNAL_ERROR;
					}
				} catch (Exception e) {
					e.printStackTrace();
					param.param3 = EventCode.E_INTERNAL_ERROR;
				}
			}
			return param;
		}
	}

	public void couponReceived(Object objTag, String id) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(objTag, new AsyncCouponReceived(), dataValue);
	}

	public void couponSent(Object objTag, String id) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(objTag, new AsyncCouponSent(), dataValue);
	}

	public void couponUsedExpired(Object objTag, String id) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(objTag, new AsyncCouponUsedExpired(), dataValue);
	}

	public void couponCompaniesCouponBox(Object objTag, String id) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(objTag, new AsyncCouponCompanies(), dataValue);
	}

	/**
	 * 
	 * @param obj
	 * @param boxSEQ
	 *            "789#123#234#567#987#555" (쿠폰함번호#쿠폰함번호#쿠폰함번호)
	 */
	public void couponDelete(Object obj, String boxSEQ, String id) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.boxSEQ, boxSEQ));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(obj, new AsyncCouponDelete(), dataValue);
	}

	/**
	 * 
	 * @param obj
	 * @param companiesCouponSEQ
	 *            "789#123#234#567#987#555" (쿠폰함번호#쿠폰함번호#쿠폰함번호)
	 */
	public void companyCouponDelete(Object obj, String companiesCouponSEQ, String id) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.companyBoxSEQ, companiesCouponSEQ));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(obj, new AsyncCompanyCouponDelete(), dataValue);
	}

	/**
	 * 쿠폰 사용 요청
	 * 
	 * @param obj
	 * @param boxSEQ
	 * @param id
	 */
	public void couponUsingRequest(Object obj, String boxSEQ, String id) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.boxSEQ, boxSEQ));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(obj, new AsyncCouponUsingRequest(), dataValue);
	}

	/**
	 * 쿠폰 사용 허가
	 * 
	 * @param obj
	 * @param boxSEQ
	 * @param id
	 * @param permit
	 *            0:사용 거절, 1:사용 허가
	 */
	public void couponPermit(Object obj, String boxSEQ, String id, String permit) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.boxSEQ, boxSEQ));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.permit, permit));

		startAsyncTask(obj, new AsyncCouponPermit(), dataValue);
	}

	/**
	 * 업체 쿠폰 사용 하기
	 * 
	 * @param obj
	 * @param companiesCouponSEQ
	 * @param id
	 */
	public void companiesCouponUse(Object obj, String companiesCouponSEQ, String id) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.companiesCouponSeq, companiesCouponSEQ));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, id));

		startAsyncTask(obj, new AsyncCompaniesCouponUse(), dataValue);
	}

	/**
	 * 쿠폰 보내기
	 * 
	 * @param obj
	 * @param couponSEQ
	 * @param senderID
	 * @param receiverID
	 *            아이디#아이디#아이디#아이디, lsj@nate.com#dwjung@wisestone.kr#jjang0005@naver.com
	 * @param couponText
	 * @param validPeriod
	 *            (쿠폰 유효기간 yyyyMMdd)
	 */
	public void couponSending(Object obj, String couponSEQ, String senderID, String receiverID, String couponText, String validPeriod) {
		List<BasicNameValuePair> dataValue = new ArrayList<BasicNameValuePair>();
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.couponSEQ, couponSEQ));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.senderID, senderID));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.receiverID, receiverID));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.couponText, couponText));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.validPeriod, validPeriod));
		dataValue.add(new BasicNameValuePair(JsonKeyWrapper.id, senderID));

		startAsyncTask(obj, new AsyncCouponSending(), dataValue);
	}

}
