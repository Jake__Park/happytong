package kr.wisestone.happytong;

public final class HappyTongConfig {

	public static final BuildType buildType = BuildType.DEBUG;

	public static final ServerType serverType = ServerType.REAL;

	public static final String LOG_TAG = "HappyTong";

	public enum BuildType {
		DEBUG, RELEASE
	}

	public enum ServerType {
		DEV, REAL
	}

}
