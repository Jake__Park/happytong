package kr.wisestone.happytong.util;

public class IntentDefine {

	private static int			REQCODE_SEQUENCE					= 222;
	// Define RequestCode
	public static final int		REQCODE_COUPON_DETAIL				= REQCODE_SEQUENCE++;
	public static final int		REQCODE_GET_FRIENDS					= REQCODE_SEQUENCE++;
	public static final int		REQCODE_BUY_OWL						= REQCODE_SEQUENCE++;
	public static final int		REQCODE_MOVE_SHOP					= REQCODE_SEQUENCE++;
	public static final int		REQCODE_MOVE_COMPANY_SHOP			= REQCODE_SEQUENCE++;
	public static final int		REQCODE_CHANGE_PASSWORD				= REQCODE_SEQUENCE++;
	public static final int		REQCODE_REGISTER					= REQCODE_SEQUENCE++;
	public static final int		REQCODE_MAIN						= REQCODE_SEQUENCE++;
	public static final int		REQCODE_SEARCH_TYPE					= REQCODE_SEQUENCE++;
	public static final int		REQCODE_GET_NUMBER					= REQCODE_SEQUENCE++;
	public static final int		REQCODE_GET_LOCATION				= REQCODE_SEQUENCE++;

	// Define Action
	public static final String	ACTION_HAPPYTONG					= "action_happytong";

	// Define Category
	public static final String	CATEGORY_TEST						= "category_main_test";

	// Define Intent Extra Name
	public static final String	EXTRA_TEST							= "extra_test";
	public static final String	EXTRA_AUTH_INFO_DATA				= "extra_auth_info_data";
	public static final String	EXTRA_CBOX_LIST						= "extra_coupon_box_list";
	public static final String	EXTRA_CBOX_TYPE						= "extra_coupon_box_type";
	public static final String	EXTRA_SBOX_LIST						= "extra_stamp_box_list";
	public static final String	EXTRA_SBOX_TYPE						= "extra_stamp_box_type";
	public static final String	EXTRA_CBOX_LIST_SELECT_POSITION		= "extra_coupon_box_list_select_position";
	public static final String	EXTRA_FRIEND_EDIT_TYPE				= "extra_friend_edit_type";
	public static final String	EXTRA_FRIEND_GROUP_ID				= "extra_friend_group_id";
	public static final String	EXTRA_CSHOP_LIST					= "extra_cshop_list";
	public static final String	EXTRA_CSHOP_SELECT_POSITION			= "extra_cshop_position";
	public static final String	EXTRA_FRIEND_SELECT_LIST			= "extra_friend_list";
	public static final String	EXTRA_GCM_BADGE_COUNT				= "badgeCount";
	public static final String	EXTRA_GCM_COUPON_BOX_SEQ			= "couponBoxSeq";
	public static final String	EXTRA_GCM_PUSH_TYPE					= "pushType";
	public static final String	EXTRA_MAIN_ENTRY					= "extra_main_entry";
	public static final String	EXTRA_MAIN_FINISH					= "extra_main_finish";
	public static final String	EXTRA_MAIN_FINISH_START_LOGIN		= "extra_main_finish_and_start_login";
	public static final String	EXTRA_CSEND_STARTED					= "extra_csend_started";
	public static final String	EXTRA_AREA_SEARCH_TYPE				= "extra_area_search";
	public static final String	EXTRA_COMPANY_INFO					= "extra_company_info";
	public static final String	EXTRA_LOCATION_SELECT_DATA			= "extra_location_select_data";
	public static final String	EXTRA_BUSINESS_CUPON_SEND_NUM_DATA	= "extra_business_cupon_send_num_data";
	public static final String	EXTRA_LOCATION_LIST_TYPE			= "extra_location_list_type";
	public static final String	EXTRA_LOCATION_USER_DATA			= "extra_location_user_data";
	public static final String	EXTRA_LOCATION_COMPANY_DATA			= "extra_location_company_data";
}
