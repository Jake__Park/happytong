package kr.wisestone.happytong.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckValidation {
    
    public static int CHECK_SUCCESS            = 0;
    public static int CHECK_ERR_EMPTY          = 1;
    public static int CHECK_ERR_LENGTH_SHORT   = 2;
    public static int CHECK_ERR_LENGTH_OVER    = 3;
    public static int CHECK_ERR_INVALID_CHAR   = 4;
    public static int CHECK_ERR_INVALID_FORMAT = 5;   
    
    public static int checkTstoreEmailValidation(String userEmail){
        int result = CHECK_SUCCESS;
        
        if(userEmail.contains(" ")){
            return CHECK_ERR_INVALID_CHAR;
        }
                
        int length = userEmail.length();
        if(length == 0){
            return CHECK_ERR_EMPTY;            
        }else if(length < 6 ){
            return CHECK_ERR_LENGTH_SHORT;            
        }else if(length > 64 ){
            return CHECK_ERR_LENGTH_OVER;
        }
        
        Pattern np = Pattern.compile("[0-9]*"); // 숫자만 있는 경우를 확인하기 위한 패턴 생성
        Matcher nm = np.matcher(userEmail);

        Pattern cp = Pattern.compile("[a-zA-Z0-9-_.]*"); // 영문 소문자, 대문자 만 있는 경우를 확인하기 위한 패턴 생성
        Matcher cm = cp.matcher(userEmail);

        Pattern pattern = Pattern.compile("[a-zA-Z0-9-_.@]*"); // 영문 소문자, 대문자, 숫자가 함께 있는지 확인하기 위한 패턴 생성
        Matcher m = pattern.matcher(userEmail);
        
        boolean isOnlyNum = nm.matches();
        boolean isOnlyChar = cm.matches();
        boolean isStrChek = m.matches();
                
        if (isOnlyNum || isOnlyChar) {
            return CHECK_ERR_INVALID_FORMAT;
        }else if(isStrChek == false){
            return CHECK_ERR_INVALID_CHAR;
        }

        if ((isStrChek && !isOnlyChar)) {
            int n = userEmail.lastIndexOf("@");
            if (length == n){
                result = CHECK_ERR_INVALID_FORMAT;                
            }
                
            if (n > 0) {
                String subStr = userEmail.substring(0,n);
                int j = subStr.lastIndexOf("@");
                if(j > 0){
                    result = CHECK_ERR_INVALID_FORMAT;                    
                }
            } else {
                result = CHECK_ERR_INVALID_FORMAT;                
            }
            
            int l = userEmail.lastIndexOf(".");
            if (length == l){
                result = CHECK_ERR_INVALID_FORMAT;                
            }
            
            if (l > 0){
                String subStr = userEmail.substring(0,l);
                int j = subStr.lastIndexOf(".");
                
                if(j > 0){
                    String subSubStr = subStr.substring(0,j);
                    int k = subSubStr.lastIndexOf(".");
                    if(k > 0){
                        result = CHECK_ERR_INVALID_FORMAT;                        
                    }
                }
            } else{
                result = CHECK_ERR_INVALID_FORMAT;                
            }
            
            if (n > l || n+1 == l){
                result = CHECK_ERR_INVALID_FORMAT;                
            }
        }
        
        return result;
    } 
}
