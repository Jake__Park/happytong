package kr.wisestone.happytong.util;

import java.util.HashMap;
import java.util.Map;

import kr.wisestone.happytong.HappyTongConfig;

import android.util.Log;

public class DelayTimeChecker {

	public static final String TAG = DelayTimeChecker.class.getName() + " ";

	private enum TrackType {
		Start, End, None
	}

	private Map<String, Long> mTrackerMap;

	private boolean activeLog = (HappyTongConfig.buildType == HappyTongConfig.BuildType.DEBUG);

	private DelayTimeChecker() {
		super();
		mTrackerMap = new HashMap<String, Long>(2);
	}

	private static class SingletonHolder {
		static final DelayTimeChecker single = new DelayTimeChecker();
	}

	/**
	 * DelayTimeTracker
	 * 
	 * @return
	 */
	public static DelayTimeChecker getInstance() {
		return SingletonHolder.single;
	}

	/**
	 * 로그 활성화 여부
	 * 
	 * @param logActive
	 */
	public void setActiveLog(boolean logActive) {
		this.activeLog = logActive;
	}

	/**
	 * 키를 이용한 시작 시각 지정
	 * 
	 * @param key
	 */
	public void setTime(String key) {
		long time = System.currentTimeMillis();
		mTrackerMap.put(key, time);
		printTrackLog(TrackType.Start, key, 0);
	}

	/**
	 * 키를 이용한 완료 시각 반환. 키값 자동 제거
	 * 
	 * @param key
	 * @return
	 */
	public long getTime(String key) {
		return getTime(key, true);
	}

	/**
	 * 키를 이용한 완료 시각 반환. 키값 제거 여부 선택
	 * 
	 * @param key
	 *            키
	 * @param remove
	 *            키값제거여부
	 * @return
	 */
	public long getTime(String key, boolean remove) {
		long delayTime = -1;
		if (mTrackerMap.containsKey(key)) {
			if (remove) {
				delayTime = System.currentTimeMillis() - mTrackerMap.remove(key);
			} else {
				delayTime = System.currentTimeMillis() - mTrackerMap.get(key);
			}
			printTrackLog(TrackType.End, key, delayTime);
		} else {
			printTrackLog(TrackType.None, key, 0);
		}
		return delayTime;
	}

	/**
	 * 추적에 사용된 맵 비우기
	 */
	public void clearTrackerMap() {
		mTrackerMap.clear();
	}

	private void printTrackLog(TrackType type, String key, long time) {
		if (!this.activeLog) {
			return;
		}

		switch (type) {
		case Start:
			Log.d(TAG, key + " :: --> START");
			break;
		case End:
			Log.d(TAG, key + " :: --------> END: " + time);
			break;
		case None:
			Log.d(TAG, key + " :: is Not Contained !");
			break;
		}
	}
}
