package kr.wisestone.happytong.util;

import kr.wisestone.happytong.HappyTongConfig;
import android.util.Log;

public class Logg {

	// resres 파일 저장 지원 추가
	// resres 특정 파일 존재 여부로 로그 활성화??
	private final static boolean isActive = HappyTongConfig.buildType == HappyTongConfig.BuildType.DEBUG ? true : false;

	private final static String TAG = HappyTongConfig.LOG_TAG;

	private final static int MAX_LENGTH = 1024 * 3;

	private static boolean isSimple = false;

	/**
	 * 로그 라인 마지막에 클래스명 표시 or 패키지 클래스 메서드 전체 표시 여부
	 * 
	 * @param isSimple
	 */
	public static void setSimple(boolean isSimple) {
		Logg.isSimple = isSimple;
	}

	public static void v(Object msg) {
		makeMessage(Log.VERBOSE, msg, null);
	}

	public static void i(Object msg) {
		makeMessage(Log.INFO, msg, null);
	}

	public static void d(Object msg) {
		makeMessage(Log.DEBUG, msg, null);
	}

	public static void w(Object msg) {
		makeMessage(Log.WARN, msg, null);
	}

	public static void e(Object msg) {
		makeMessage(Log.ERROR, msg.toString(), null);
	}

	public static void e(String msg, Throwable tr) {
		makeMessage(Log.ERROR, msg, tr);
	}

	private static void makeMessage(int priority, Object obj, Throwable tr) {
		if (isActive) {
			Exception e = new Exception();
			if (isSimple) {
				StackTraceElement stElmn = e.getStackTrace()[2];
				obj = obj + " (at " + stElmn.getFileName() + ":" + stElmn.getLineNumber() + ")";
			} else {
				obj = obj + " (at " + e.getStackTrace()[2] + ")";
			}

			if (tr != null) {
				obj = obj + "\n" + Log.getStackTraceString(tr);
			}

			printMessage(priority, obj);
		}
	}

	private static void printMessage(int priority, Object obj) {
		String message = obj.toString();
		if (message.length() < MAX_LENGTH) {
			Log.println(priority, TAG, message);
		} else {
			Log.println(priority, TAG, message.substring(0, MAX_LENGTH));
			printMessage(priority, message.substring(MAX_LENGTH));
		}
	}

}
